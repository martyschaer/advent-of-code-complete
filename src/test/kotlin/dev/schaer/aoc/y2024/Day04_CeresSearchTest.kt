package dev.schaer.aoc.y2024

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class Day04_CeresSearchTest {
    @Test
    fun examplePartOne() {
        val input = """
        MMMSXXMASM
        MSAMXMSMSA
        AMXSXMAAMM
        MSAMASMSMX
        XMASAMXAMM
        XXAMMXXAMA
        SMSMSASXSS
        SAXAMASAAA
        MAMMMXMMMM
        MXMXAXMASX
        """.trimIndent()

        val solution = Day04_CeresSearch().partOne(input)

        assertEquals(18, solution)
    }

    @Test
    fun examplePartTwo() {
        val input = """
        MMMSXXMASM
        MSAMXMSMSA
        AMXSXMAAMM
        MSAMASMSMX
        XMASAMXAMM
        XXAMMXXAMA
        SMSMSASXSS
        SAXAMASAAA
        MAMMMXMMMM
        MXMXAXMASX
        """.trimIndent()

        val solution = Day04_CeresSearch().partTwo(input)

        assertEquals(9, solution)
    }
}