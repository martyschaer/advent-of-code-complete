package dev.schaer.aoc.y2024

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class Day23_LANPartyTest {
    @Test
    fun examplePartOne() {
        val input = """
            kh-tc
            qp-kh
            de-cg
            ka-co
            yn-aq
            qp-ub
            cg-tb
            vc-aq
            tb-ka
            wh-tc
            yn-cg
            kh-ub
            ta-co
            de-co
            tc-td
            tb-wq
            wh-td
            ta-ka
            td-qp
            aq-cg
            wq-ub
            ub-vc
            de-ta
            wq-aq
            wq-vc
            wh-yn
            ka-de
            kh-ta
            co-tc
            wh-qp
            tb-vc
            td-yn
        """.trimIndent()

        val solution = Day23_LANParty().partOne(input)

        assertEquals(7, solution)
    }

    @Test
    fun examplePartTwo() {
        val input = """
            ka-co
            ta-co
            de-co
            ta-ka
            de-ta
            ka-de
        """.trimIndent()

        val solution = Day23_LANParty().partTwo(input)

        assertEquals("co,de,ka,ta", solution)
    }
}