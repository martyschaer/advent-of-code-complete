package dev.schaer.aoc.y2024

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class Day03_MullItOverTest {
    @Test
    fun examplePartOne() {
        val input = """
        xmul(2,4)%&mul[3,7]!@^do_not_mul(5,5)+mul(32,64]then(mul(11,8)mul(8,5))
        """.trimIndent()

        val solution = Day03_MullItOver().partOne(input)

        assertEquals(161, solution)
    }

    @Test
    fun examplePartTwo() {
        val input = """
        xmul(2,4)&mul[3,7]!^don't()_mul(5,5)+mul(32,64](mul(11,8)undo()?mul(8,5))
        """.trimIndent()

        val solution = Day03_MullItOver().partTwo(input)

        assertEquals(48, solution)
    }
}