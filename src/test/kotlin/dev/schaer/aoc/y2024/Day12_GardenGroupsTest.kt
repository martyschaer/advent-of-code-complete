package dev.schaer.aoc.y2024

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class Day12_GardenGroupsTest {
    @Test
    fun examplePartOneSmall() {
        val input = """
            AAAA
            BBCD
            BBCC
            EEEC
        """.trimIndent()

        val solution = Day12_GardenGroups().partOne(input)

        assertEquals(140, solution)
    }

    @Test
    fun examplePartOneMedium() {
        val input = """
            OOOOO
            OXOXO
            OOOOO
            OXOXO
            OOOOO
        """.trimIndent()

        val solution = Day12_GardenGroups().partOne(input)

        assertEquals(772, solution)
    }

    @Test
    fun examplePartOne() {
        val input = """
            RRRRIICCFF
            RRRRIICCCF
            VVRRRCCFFF
            VVRCCCJFFF
            VVVVCJJCFE
            VVIVCCJJEE
            VVIIICJJEE
            MIIIIIJJEE
            MIIISIJEEE
            MMMISSJEEE
        """.trimIndent()

        val solution = Day12_GardenGroups().partOne(input)

        assertEquals(1930, solution)
    }

    @Test
    fun examplePartTwoTiny() {
        val input = """
            XOX
            OOO
            XOX
        """.trimIndent()

        val solution = Day12_GardenGroups().partTwo(input)

        assertEquals(76, solution)
    }

    @Test
    fun examplePartTwoTinyDonut() {
        val input = """
            XXX
            XOX
            XXX
        """.trimIndent()

        val solution = Day12_GardenGroups().partTwo(input)

        assertEquals(68, solution)
    }

    @Test
    fun examplePartTwoTinyComplexDonut() {
        val input = """
            XUU
            XOU
            XII
        """.trimIndent()

        val solution = Day12_GardenGroups().partTwo(input)

        assertEquals(3 * 4 + 1 * 4 + 2 * 4 + 3 * 6, solution)
    }

    @Test
    fun examplePartTwoTinyPatchwork() {
        val input = """
            XOX
            OXO
            XOX
        """.trimIndent()

        val solution = Day12_GardenGroups().partTwo(input)

        assertEquals(9 * 4, solution)
    }

    @Test
    fun examplePartTwoTinyLongDonut() {
        val input = """
            XXX
            XOX
            XOX
            XXX
        """.trimIndent()

        val solution = Day12_GardenGroups().partTwo(input)

        assertEquals(88, solution)
    }

    @Test
    fun examplePartTwoTinyRedditEdgeCase() {
        val input = """
            OOXXX
            OOXOX
            OOOOX
            OOXOX
            OOXXX
        """.trimIndent()

        val solution = Day12_GardenGroups().partTwo(input)

        assertEquals(12 * 14 + 12 * 11, solution)
    }

    @Test
    fun examplePartTwoTinyRedditEdgeCase2() {
        val input = """
            OOOOO
            OXOXO
            OXXXO
        """.trimIndent()

        val solution = Day12_GardenGroups().partTwo(input)

        assertEquals(160, solution)
    }

    @Test
    fun examplePartTwoMediumRedditEdgeCase() {
        val input = """
            AAAAAAAA
            AACBBDDA
            AACBBAAA
            ABBAAAAA
            ABBADDDA
            AAAADADA
            AAAAAAAA
        """.trimIndent()

        val solution = Day12_GardenGroups().partTwo(input)

        assertEquals(946, solution)
    }

    @Test
    fun examplePartTwoSmall() {
        val input = """
            AAAA
            BBCD
            BBCC
            EEEC
        """.trimIndent()

        val solution = Day12_GardenGroups().partTwo(input)

        assertEquals(80, solution)
    }

    @Test
    fun examplePartTwoE() {
        val input = """
            EEEEE
            EXXXX
            EEEEE
            EXXXX
            EEEEE
        """.trimIndent()

        val solution = Day12_GardenGroups().partTwo(input)

        assertEquals(236, solution)
    }

    @Test
    fun examplePartTwoBAB() {
        val input = """
            AAAAAA
            AAABBA
            AAABBA
            ABBAAA
            ABBAAA
            AAAAAA
        """.trimIndent()

        val solution = Day12_GardenGroups().partTwo(input)

        assertEquals(368, solution)
    }

    @Test
    fun examplePartTwo() {
        val input = """
            RRRRIICCFF
            RRRRIICCCF
            VVRRRCCFFF
            VVRCCCJFFF
            VVVVCJJCFE
            VVIVCCJJEE
            VVIIICJJEE
            MIIIIIJJEE
            MIIISIJEEE
            MMMISSJEEE
        """.trimIndent()

        val solution = Day12_GardenGroups().partTwo(input)

        assertEquals(1206, solution)
    }
}