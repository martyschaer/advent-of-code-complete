package dev.schaer.aoc.y2024

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class Day09_DiskFragmenterTest {
    @Test
    fun examplePartOneSimplified() {
        val input = """
           12345
        """.trimIndent()

        val solution = Day09_DiskFragmenter().partOne(input)

        assertEquals(60, solution)
    }

    @Test
    fun examplePartOne() {
        val input = """
           2333133121414131402
        """.trimIndent()

        val solution = Day09_DiskFragmenter().partOne(input)

        assertEquals(1928, solution)
    }

    @Test
    fun examplePartTwo() {
        val input = """
           2333133121414131402
        """.trimIndent()

        val solution = Day09_DiskFragmenter().partTwo(input)

        assertEquals(2858, solution)
    }
}