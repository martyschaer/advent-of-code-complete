package dev.schaer.aoc.y2024

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class Day18_RAMRunTest {
    @Test
    fun examplePartOne() {
        val input = """
            5,4
            4,2
            4,5
            3,0
            2,1
            6,3
            2,4
            1,5
            0,6
            3,3
            2,6
            5,1
            1,2
            5,5
            2,5
            6,5
            1,4
            0,4
            6,4
            1,1
            6,1
            1,0
            0,5
            1,6
            2,0
        """.trimIndent()

        val solution = Day18_RAMRun(7, 7, 12).partOne(input)

        assertEquals(22, solution)
    }

    @Test
    fun examplePartTwo() {
        val input = """
            5,4
            4,2
            4,5
            3,0
            2,1
            6,3
            2,4
            1,5
            0,6
            3,3
            2,6
            5,1
            1,2
            5,5
            2,5
            6,5
            1,4
            0,4
            6,4
            1,1
            6,1
            1,0
            0,5
            1,6
            2,0
        """.trimIndent()

        val solution = Day18_RAMRun(7, 7).partTwo(input)

        assertEquals("6,1", solution)
    }
}