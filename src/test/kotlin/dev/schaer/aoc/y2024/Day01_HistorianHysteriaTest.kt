package dev.schaer.aoc.y2024

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class Day01_HistorianHysteriaTest {
    @Test
    fun examplePartOne() {
        val input = """
        3   4
        4   3
        2   5
        1   3
        3   9
        3   3
        """.trimIndent()

        val solution = Day01_HistorianHysteria().partOne(input)

        assertEquals(11, solution)
    }

    @Test
    fun examplePartTwo() {
        val input = """
        3   4
        4   3
        2   5
        1   3
        3   9
        3   3
        """.trimIndent()

        val solution = Day01_HistorianHysteria().partTwo(input)

        assertEquals(31, solution)
    }
}