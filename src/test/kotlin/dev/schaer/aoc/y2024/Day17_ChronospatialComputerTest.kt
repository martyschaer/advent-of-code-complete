package dev.schaer.aoc.y2024

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class Day17_ChronospatialComputerTest {
    @Test
    fun examplePartOne() {
        val input = """
            Register A: 729
            Register B: 0
            Register C: 0

            Program: 0,1,5,4,3,0
        """.trimIndent()

        val solution = Day17_ChronospatialComputer().partOne(input)

        assertEquals("4,6,3,5,6,3,5,2,1,0", solution)
    }

    @Test
    fun testComputer1() {
        val computer = Day17_ChronospatialComputer.Computer(0, 0, 9)
        computer.execute(listOf(2,6))
        assertEquals(1, computer.rB)
    }

    @Test
    fun testComputer2() {
        val computer = Day17_ChronospatialComputer.Computer(10, 0, 0)
        val output = computer.execute(listOf(5,0,5,1,5,4))
        assertEquals(listOf(0,1,2), output)
    }

    @Test
    fun testComputer3() {
        val computer = Day17_ChronospatialComputer.Computer(2024, 0, 0)
        val output = computer.execute(listOf(0,1,5,4,3,0))
        assertEquals(listOf(4,2,5,6,7,7,7,7,3,1,0), output)
    }

    @Test
    fun testComputer4() {
        val computer = Day17_ChronospatialComputer.Computer(0, 29, 0)
        val output = computer.execute(listOf(1,7))
        assertEquals(26, computer.rB)
    }

    @Test
    fun testComputer5() {
        val computer = Day17_ChronospatialComputer.Computer(0, 2024, 43690)
        val output = computer.execute(listOf(4,0))
        assertEquals(44354, computer.rB)
    }
}