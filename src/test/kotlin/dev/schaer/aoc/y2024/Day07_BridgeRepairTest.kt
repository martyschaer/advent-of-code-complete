package dev.schaer.aoc.y2024

import org.junit.jupiter.api.Test
import kotlin.random.Random
import kotlin.test.assertEquals
import kotlin.time.Duration
import kotlin.time.ExperimentalTime
import kotlin.time.measureTimedValue

class Day07_BridgeRepairTest {
    @Test
    fun examplePartOne() {
        val input = """
            190: 10 19
            3267: 81 40 27
            83: 17 5
            156: 15 6
            7290: 6 8 6 15
            161011: 16 10 13
            192: 17 8 14
            21037: 9 7 18 13
            292: 11 6 16 20
        """.trimIndent()

        val solution = Day07_BridgeRepair().partOne(input)

        assertEquals(3749, solution)
    }

    @Test
    fun examplePartTwo() {
        val input = """
            190: 10 19
            3267: 81 40 27
            83: 17 5
            156: 15 6
            7290: 6 8 6 15
            161011: 16 10 13
            192: 17 8 14
            21037: 9 7 18 13
            292: 11 6 16 20
        """.trimIndent()

        val solution = Day07_BridgeRepair().partTwo(input)

        assertEquals(11387, solution)
    }
}