package dev.schaer.aoc.y2024

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class Day20_RaceConditionTest {
    @Test
    fun examplePartOne() {
        val input = """
            ###############
            #...#...#.....#
            #.#.#.#.#.###.#
            #S#...#.#.#...#
            #######.#.#.###
            #######.#.#...#
            #######.#.###.#
            ###..E#...#...#
            ###.#######.###
            #...###...#...#
            #.#####.#.###.#
            #.#...#.#.#...#
            #.#.#.#.#.#.###
            #...#...#...###
            ###############
        """.trimIndent()

        val solution = Day20_RaceCondition(10).partOne(input)

        assertEquals(10, solution)
    }

    @Test
    fun examplePartTwo() {
        val input = """
            ###############
            #...#...#.....#
            #.#.#.#.#.###.#
            #S#...#.#.#...#
            #######.#.#.###
            #######.#.#...#
            #######.#.###.#
            ###..E#...#...#
            ###.#######.###
            #...###...#...#
            #.#####.#.###.#
            #.#...#.#.#...#
            #.#.#.#.#.#.###
            #...#...#...###
            ###############
        """.trimIndent()

        val solution = Day20_RaceCondition(50).partTwo(input)

        assertEquals(285, solution)
    }
}