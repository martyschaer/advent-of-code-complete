package dev.schaer.aoc.y2024

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class Day21_KeypadConundrumTest {
    @Test
    fun examplePartOne() {
        val input = """
            029A
            980A
            179A
            456A
            379A
        """.trimIndent()

        val solution = Day21_KeypadConundrum().partOne(input)

        assertEquals(126384, solution)
    }

    @Test
    fun `calculate complexity of 029A`() {
        val result = Day21_KeypadConundrum().partOne("029A")
        assertEquals(68 * 29, result)
    }

    @Test
    fun `calculate complexity of 980A`() {
        val result = Day21_KeypadConundrum().partOne("980A")
        assertEquals(60 * 980, result)
    }

    @Test
    fun `calculate complexity of 179A`() {
        val result = Day21_KeypadConundrum().partOne("179A")
        assertEquals(68 * 179, result)
    }

    @Test
    fun `calculate complexity of 456A`() {
        val result = Day21_KeypadConundrum().partOne("456A")
        assertEquals(64 * 456, result)
    }

    @Test
    fun `calculate complexity of 379A`() {
        val result = Day21_KeypadConundrum().partOne("379A")
        assertEquals(64 * 379, result)
    }
}