package dev.schaer.aoc.y2024

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class Day10_HoofItTest {
    @Test
    fun examplePartOne() {
        val input = """
            89010123
            78121874
            87430965
            96549874
            45678903
            32019012
            01329801
            10456732
        """.trimIndent()

        val solution = Day10_HoofIt().partOne(input)

        assertEquals(36, solution)
    }

    @Test
    fun examplePartTwoSmall() {
        val input = """
            012345
            123456
            234567
            345678
            416789
            567891
        """.trimIndent()

        val solution = Day10_HoofIt().partTwo(input)

        assertEquals(227, solution)
    }

    @Test
    fun examplePartTwo() {
        val input = """
            89010123
            78121874
            87430965
            96549874
            45678903
            32019012
            01329801
            10456732
        """.trimIndent()

        val solution = Day10_HoofIt().partTwo(input)

        assertEquals(81, solution)
    }
}