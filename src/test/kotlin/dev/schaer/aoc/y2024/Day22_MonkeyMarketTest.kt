package dev.schaer.aoc.y2024

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class Day22_MonkeyMarketTest {
    @Test
    fun examplePartOne() {
        val input = """
            1
            10
            100
            2024
        """.trimIndent()

        val solution = Day22_MonkeyMarket().partOne(input)

        assertEquals(37327623, solution)
    }

    @Test
    fun examplePartOne_123() {
        val solution = Day22_MonkeyMarket()
        var secret = 123

        val prng = solution.prng(secret).iterator()

        secret = prng.next()
        assertEquals(15887950, secret)

        secret = prng.next()
        assertEquals(16495136, secret)

        secret = prng.next()
        assertEquals(527345, secret)

        secret = prng.next()
        assertEquals(704524, secret)

        secret = prng.next()
        assertEquals(1553684, secret)

        secret = prng.next()
        assertEquals(12683156, secret)

        secret = prng.next()
        assertEquals(11100544, secret)

        secret = prng.next()
        assertEquals(12249484, secret)

        secret = prng.next()
        assertEquals(7753432, secret)

        secret = prng.next()
        assertEquals(5908254, secret)
    }

    @Test
    fun examplePartTwo() {
        val input = """
            1
            2
            3
            2024
        """.trimIndent()

        val solution = Day22_MonkeyMarket().partTwo(input)

        assertEquals(23, solution)
    }
}