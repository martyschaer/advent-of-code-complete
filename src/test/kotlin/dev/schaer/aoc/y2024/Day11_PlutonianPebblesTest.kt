package dev.schaer.aoc.y2024

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class Day11_PlutonianPebblesTest {
    @Test
    fun examplePartOne() {
        val input = """
            125 17
        """.trimIndent()

        val solution = Day11_PlutonianPebbles().partOne(input)

        assertEquals(55312, solution)
    }

    @Test
    fun examplePartTwo() {
        val input = """
            125 17
        """.trimIndent()

        val solution = Day11_PlutonianPebbles().partTwo(input)

        assertEquals(65601038650482, solution)
    }
}