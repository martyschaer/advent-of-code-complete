package dev.schaer.aoc.y2024

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class Day02_RedNosedReportsTest {
    @Test
    fun examplePartOne() {
        val input = """
        7 6 4 2 1
        1 2 7 8 9
        9 7 6 2 1
        1 3 2 4 5
        8 6 4 4 1
        1 3 6 7 9
        """.trimIndent()

        val solution = Day02_RedNosedReports().partOne(input)

        assertEquals(2, solution)
    }

    @Test
    fun examplePartTwo() {
        val input = """
        7 6 4 2 1
        1 2 7 8 9
        9 7 6 2 1
        1 3 2 4 5
        8 6 4 4 1
        1 3 6 7 9
        """.trimIndent()

        val solution = Day02_RedNosedReports().partTwo(input)

        assertEquals(4, solution)
    }
}