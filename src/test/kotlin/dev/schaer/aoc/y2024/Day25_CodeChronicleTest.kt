package dev.schaer.aoc.y2024

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class Day25_CodeChronicleTest {
    @Test
    fun examplePartOne() {
        val input = """
            #####
            .####
            .####
            .####
            .#.#.
            .#...
            .....

            #####
            ##.##
            .#.##
            ...##
            ...#.
            ...#.
            .....

            .....
            #....
            #....
            #...#
            #.#.#
            #.###
            #####

            .....
            .....
            #.#..
            ###..
            ###.#
            ###.#
            #####

            .....
            .....
            .....
            #....
            #.#..
            #.#.#
            #####
        """.trimIndent()

        val solution = Day25_CodeChronicle().partOne(input)

        assertEquals(3, solution)
    }

    @Test
    fun examplePartTwo() {
        val input = """
        """.trimIndent()

        val solution = Day25_CodeChronicle().partTwo(input)

        assertEquals(0, solution)
    }
}