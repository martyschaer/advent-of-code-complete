package dev.schaer.aoc.y2024

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class Day19_LinenLayoutTest {
    @Test
    fun examplePartOne() {
        val input = """
            r, wr, b, g, bwu, rb, gb, br

            brwrr
            bggr
            gbbr
            rrbgbr
            ubwu
            bwurrg
            brgr
            bbrgwb
        """.trimIndent()

        val solution = Day19_LinenLayout().partOne(input)

        assertEquals(6, solution)
    }

    @Test
    fun examplePartTwo() {
        val input = """
            r, wr, b, g, bwu, rb, gb, br

            brwrr
            bggr
            gbbr
            rrbgbr
            ubwu
            bwurrg
            brgr
            bbrgwb
        """.trimIndent()

        val solution = Day19_LinenLayout().partTwo(input)

        assertEquals(16, solution)
    }
}