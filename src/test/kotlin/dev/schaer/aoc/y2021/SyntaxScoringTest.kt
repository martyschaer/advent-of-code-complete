package dev.schaer.aoc.y2021

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

internal class SyntaxScoringTest {
    private val solver = SyntaxScoring()

    private val input = """
            [({(<(())[]>[[{[]{<()<>>
            [(()[<>])]({[<{<<[]>>(
            {([(<{}[<>[]}>{[]{[(<()>
            (((({<>}<{<{<>}{[]{[]{}
            [[<[([]))<([[{}[[()]]]
            [{[{({}]{}}([{[{{{}}([]
            {<[[]]>}<{[{[{[]{()[[[]
            [<(<(<(<{}))><([]([]()
            <{([([[(<>()){}]>(<<{{
            <{([{{}}[<[[[<>{}]]]>[]]
        """.trimIndent()

    @Test
    fun testPartOne() {
        assertEquals(26397, solver.partOne(input))
    }

    @Test
    fun testPartTwo() {
        assertEquals(288957, solver.partTwo(input))
    }

    @Test
    fun testAutoCompleteScoreCalculation() {
        assertEquals(288957, solver.calcAutoCompleteScore("}}]])})]".toCharArray().reversed()))
        assertEquals(5566, solver.calcAutoCompleteScore(")}>]})".toCharArray().reversed()))
        assertEquals(1480781, solver.calcAutoCompleteScore("}}>}>))))".toCharArray().reversed()))
        assertEquals(995444, solver.calcAutoCompleteScore("]]}}]}]}>".toCharArray().reversed()))
        assertEquals(294, solver.calcAutoCompleteScore("])}>".toCharArray().reversed()))
    }
}