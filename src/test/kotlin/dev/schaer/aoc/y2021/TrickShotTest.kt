package dev.schaer.aoc.y2021

import org.junit.jupiter.api.Test
import java.awt.Point
import kotlin.test.assertEquals

internal class TrickShotTest {
    private val solver = TrickShot()

    @Test
    fun testPartOneA() {
        assertEquals(45, solver.partOne("target area: x=20..30, y=-10..-5"))
    }

    @Test
    fun testPartOneB() {
        assertEquals(66, solver.partOne("target area: x=352..377, y=-49..-30"))
    }

    @Test
    fun testPartOneC() {
        assertEquals(36, solver.partOne("target area: x=819..820, y=-11..-5"))
    }

    @Test
    fun testPartOneD() {
        assertEquals(4095, solver.partOne("target area: x=244..303, y=-91..-54"))
    }

    @Test
    fun testPartTwo() {
        assertEquals(112, solver.partTwo("target area: x=20..30, y=-10..-5"))
    }

    @Test
    fun testPartTwoDetail()  {
        val expectedStr = """
            23,-10  25,-9   27,-5   29,-6   22,-6   21,-7   9,0     27,-7   24,-5
            25,-7   26,-6   25,-5   6,8     11,-2   20,-5   29,-10  6,3     28,-7
            8,0     30,-6   29,-8   20,-10  6,7     6,4     6,1     14,-4   21,-6
            26,-10  7,-1    7,7     8,-1    21,-9   6,2     20,-7   30,-10  14,-3
            20,-8   13,-2   7,3     28,-8   29,-9   15,-3   22,-5   26,-8   25,-8
            25,-6   15,-4   9,-2    15,-2   12,-2   28,-9   12,-3   24,-6   23,-7
            25,-10  7,8     11,-3   26,-7   7,1     23,-9   6,0     22,-10  27,-6
            8,1     22,-8   13,-4   7,6     28,-6   11,-4   12,-4   26,-9   7,4
            24,-10  23,-8   30,-8   7,0     9,-1    10,-1   26,-5   22,-9   6,5
            7,5     23,-6   28,-10  10,-2   11,-1   20,-9   14,-2   29,-7   13,-3
            23,-5   24,-8   27,-9   30,-7   28,-5   21,-10  7,9     6,6     21,-5
            27,-10  7,2     30,-9   21,-8   22,-7   24,-9   20,-6   6,9     29,-5
            8,-2    27,-8   30,-5   24,-7
        """.trimIndent()
        val expected = expectedStr
            .replace("\n", " ")
            .split(Regex("\\s+"))
            .map { line ->
                val (x, y) = line.split(",").map { it.toInt() }
                Point(x, y)
            }
            .toSet()
            .sortedBy { it.x }
            .sortedBy { it.y }
        val actual = solver.calculateValidLaunchVectors("target area: x=20..30, y=-10..-5")
            .sortedBy { it.x }
            .sortedBy { it.y }

        assertEquals(expected, actual)
    }
}