package dev.schaer.aoc.y2021

import dev.schaer.aoc.datastructures.StringLengthProvider
import dev.schaer.aoc.y2021.bits.LiteralPacket
import dev.schaer.aoc.y2021.bits.OperatorPacket
import dev.schaer.aoc.y2021.bits.PacketData
import dev.schaer.aoc.y2021.bits.PacketType
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream
import kotlin.test.assertEquals

internal class PacketDecoderTest {
    private val solver = PacketDecoder()

    private companion object {
        @JvmStatic
        fun partTwoExamples() = Stream.of(
            Arguments.of("C200B40A82", 3L),
            Arguments.of("04005AC33890", 54L),
            Arguments.of("880086C3E88112", 7L),
            Arguments.of("CE00C43D881120", 9L),
            Arguments.of("D8005AC2A8F0", 1L),
            Arguments.of("F600BC2D8F", 0L),
            Arguments.of("9C005AC2F8F0", 0L),
            Arguments.of("9C0141080250320F1802104A08", 1L),
        )
    }

    @Test
    fun testPartOne1() {
        assertEquals(16, solver.partOne("8A004A801A8002F478"))
    }
    @Test
    fun testPartOne2() {
        assertEquals(12, solver.partOne("620080001611562C8802118E34"))
    }
    @Test
    fun testPartOne3() {
        assertEquals(23, solver.partOne("C0015000016115A2E0802F182340"))
    }
    @Test
    fun testPartOne4() {
        assertEquals(31, solver.partOne("A0016C880162017C3686B18A3D4780"))
    }

    @ParameterizedTest(name = "Parsing and evaluating '{0}' should result in {1}")
    @MethodSource("partTwoExamples")
    fun testPartTwo(input: String, expected: Long) {
        assertEquals(expected, solver.partTwo(input))
    }

    @Test
    fun testparse() {
        val input = "EE00D40C823060"
        val expected = OperatorPacket(PacketData("111", PacketType.MAX), listOf(
            LiteralPacket(PacketData("010", PacketType.LITERAL), 1L),
            LiteralPacket(PacketData("100", PacketType.LITERAL), 2L),
            LiteralPacket(PacketData("001", PacketType.LITERAL), 3L),
        ))
        assertEquals(listOf(expected), solver.parse(StringLengthProvider(solver.hex2bits(input)), 1))
    }

    @Test
    fun testHex2Bits() {
        for(i in (0..15)) {
            val bin = i.toString(2).padStart(4, '0')
            val hex = i.toString(16).uppercase()
            assertEquals(bin, solver.hex2bits(hex), "[$i] $hex -> $bin")
        }
        assertEquals("110100101111111000101000", solver.hex2bits("D2FE28"))
    }
}