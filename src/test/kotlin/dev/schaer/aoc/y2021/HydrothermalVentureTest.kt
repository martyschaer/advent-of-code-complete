package dev.schaer.aoc.y2021

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

internal class HydrothermalVentureTest {
    private val solver = HydrothermalVenture()

    private val input = """
0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2
        """.trimIndent()

    @Test
    fun testPartOne() {
        assertEquals(5, solver.partOne(input))
    }

    @Test
    fun testPartTwo() {
        assertEquals(12, solver.partTwo(input))
    }
}