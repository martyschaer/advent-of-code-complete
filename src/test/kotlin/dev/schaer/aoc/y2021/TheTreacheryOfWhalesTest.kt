package dev.schaer.aoc.y2021

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

internal class TheTreacheryOfWhalesTest {
    private val solver = TheTreacheryOfWhales()

    private val input = """
            16,1,2,0,4,2,7,1,2,14
        """.trimIndent()

    @Test
    fun testPartOne() {
        assertEquals(37, solver.partOne(input))
    }

    @Test
    fun testPartTwo() {
        assertEquals(168, solver.partTwo(input))
    }

    @Test
    fun testFuelCostAdjustment() {
        assertEquals(66, solver.adjustFuelCost(11) )
        assertEquals(10, solver.adjustFuelCost(4) )
        assertEquals(6, solver.adjustFuelCost(3) )
        assertEquals(1, solver.adjustFuelCost(1) )
    }
}
