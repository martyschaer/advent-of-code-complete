package dev.schaer.aoc.y2021

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

internal class DiveTest {
    private val solver = Dive()

    private val input = """
            forward 5
            down 5
            forward 8
            up 3
            down 8
            forward 2
        """.trimIndent()

    @Test
    fun testPartOne() {
        assertEquals(150, solver.partOne(input))
    }

    @Test
    fun testPartTwo() {
        assertEquals(900, solver.partTwo(input))
    }
}