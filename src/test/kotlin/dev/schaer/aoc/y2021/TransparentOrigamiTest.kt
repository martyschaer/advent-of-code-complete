package dev.schaer.aoc.y2021

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

internal class TransparentOrigamiTest {
    private val solver = TransparentOrigami()

    private val input = """
            6,10
            0,14
            9,10
            0,3
            10,4
            4,11
            6,0
            6,12
            4,1
            0,13
            10,12
            3,4
            3,0
            8,4
            1,10
            2,14
            8,10
            9,0

            fold along y=7
            fold along x=5
        """.trimIndent()

    @Test
    fun testPartOne() {
        assertEquals(17, solver.partOne(input))
    }

    @Test
    fun testPartTwo() {
        assertEquals("""
█████.
█...█.
█...█.
█...█.
█████.
......
......
......
""", solver.partTwo(input))
    }
}