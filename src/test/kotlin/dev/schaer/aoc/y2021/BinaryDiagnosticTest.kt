package dev.schaer.aoc.y2021

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

internal class BinaryDiagnosticTest {
    private val solver = BinaryDiagnostic()

    private val input = """
            00100
            11110
            10110
            10111
            10101
            01111
            00111
            11100
            10000
            11001
            00010
            01010
        """.trimIndent()

    @Test
    fun testPartOne() {
        assertEquals(198, solver.partOne(input))
    }

    @Test
    fun testPartTwo() {
        assertEquals(230, solver.partTwo(input))
    }
}