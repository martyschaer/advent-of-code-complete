package dev.schaer.aoc.y2021

import dev.schaer.aoc.datastructures.Array2D
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

internal class ChitonTest {
    private val solver = Chiton()

    private val input = """
            1163751742
            1381373672
            2136511328
            3694931569
            7463417111
            1319128137
            1359912421
            3125421639
            1293138521
            2311944581
        """.trimIndent()

    @Test
    fun testPartOne() {
        assertEquals(40, solver.partOne(input))
    }

    @Test
    fun testPartTwo() {
        assertEquals(315, solver.partTwo(input))
    }

    @Test
    fun testBuildRealMap() {
        assertEquals("""
            7 1 8 2 9 3 1 4 2 5 
            2 3 3 4 4 5 5 6 6 7 
            8 2 9 3 1 4 2 5 3 6 
            3 4 4 5 5 6 6 7 7 8 
            9 3 1 4 2 5 3 6 4 7 
            4 5 5 6 6 7 7 8 8 9 
            1 4 2 5 3 6 4 7 5 8 
            5 6 6 7 7 8 8 9 9 1 
            2 5 3 6 4 7 5 8 6 9 
            6 7 7 8 8 9 9 1 1 2 
            
        """.trimIndent(), solver.buildRealMap(Array2D(arrayOf(7, 1, 2, 3), 2, 2, -1)).toString())
    }
}