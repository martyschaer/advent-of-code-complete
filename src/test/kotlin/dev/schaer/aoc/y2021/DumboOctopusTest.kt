package dev.schaer.aoc.y2021

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

internal class DumboOctopusTest {
    private val solver = DumboOctopus()

    private val input = """
            5483143223
            2745854711
            5264556173
            6141336146
            6357385478
            4167524645
            2176841721
            6882881134
            4846848554
            5283751526
        """.trimIndent()

    @Test
    fun testPartOne() {
        assertEquals(1656, solver.partOne(input))
    }

    @Test
    fun testPartTwo() {
        assertEquals(195, solver.partTwo(input))
    }
}