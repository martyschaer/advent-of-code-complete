package dev.schaer.aoc.y2021

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

internal class ExtendedPolymerizationTest {
    private val solver = ExtendedPolymerization()

    private val input = """
           NNCB

           CH -> B
           HH -> N
           CB -> H
           NH -> C
           HB -> C
           HC -> B
           HN -> C
           NN -> C
           BH -> H
           NC -> B
           NB -> B
           BN -> B
           BB -> N
           BC -> B
           CC -> N
           CN -> C
        """.trimIndent()

    @Test
    fun testPartOne() {
        assertEquals(1588L, solver.partOne(input))
    }

    @Test
    fun testPartTwo() {
        assertEquals(2188189693529L, solver.partTwo(input))
    }
}