package dev.schaer.aoc.y2021

import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

internal class PassagePathingTest {
    private val solver = PassagePathing()

    private val example = """
           start-AA
           start-bb
           AA-cc
           AA-bb
           bb-dd
           AA-end
           bb-end
        """.trimIndent()

    private val mediumExample = """
        dc-end
        HN-start
        start-kj
        dc-start
        dc-HN
        LN-dc
        HN-end
        kj-sa
        kj-HN
        kj-dc
    """.trimIndent()

    private val largeExample = """
        fs-end
        he-DX
        fs-he
        start-DX
        pj-DX
        end-zg
        zg-sl
        zg-pj
        pj-he
        RW-he
        fs-DX
        pj-RW
        zg-RW
        start-pj
        he-WI
        zg-he
        pj-fs
        start-RW
    """.trimIndent()

    @AfterEach
    fun reset() {
        solver.reset()
    }

    @Test
    fun testPartOneSmall() {
        assertEquals(10, solver.partOne(example))
    }

    @Disabled
    @Test
    fun testPartOneMedium() {
        assertEquals(19, solver.partOne(mediumExample))
    }

    @Disabled
    @Test
    fun testPartOneLarge() {
        assertEquals(226, solver.partOne(largeExample))
    }

    @Test
    fun testPartTwoSmall() {
        assertEquals(36, solver.partTwo(example))
    }

    @Disabled
    @Test
    fun testPartTwoMedium() {
        assertEquals(103, solver.partTwo(mediumExample))
    }

    @Disabled
    @Test
    fun testPartTwoLarge() {
        assertEquals(3509, solver.partTwo(largeExample))
    }
}