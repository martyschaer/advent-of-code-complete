package dev.schaer.aoc.y2021

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

internal class SonarSweepTest {
    private val solver = SonarSweep()

    @Test
    fun testPartOne() {
        val input = """
            199
            200
            208
            210
            200
            207
            240
            269
            260
            263
        """.trimIndent()

        assertEquals(7, solver.partOne(input))
    }

    @Test
    fun testPartTwo() {
        val input = """
            199
            200
            208
            210
            200
            207
            240
            269
            260
            263
        """.trimIndent()

        assertEquals(5, solver.partTwo(input))
    }
}