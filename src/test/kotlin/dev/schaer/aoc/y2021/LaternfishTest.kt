package dev.schaer.aoc.y2021

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

internal class LaternfishTest {
    private val solver = Lanternfish()

    private val input = """
            3,4,3,1,2
        """.trimIndent()

    @Test
    fun testPartOne() {
        assertEquals(5934L, solver.partOne(input))
    }

    @Test
    fun testPartTwo() {
        assertEquals(26984457539L, solver.partTwo(input))
    }
}