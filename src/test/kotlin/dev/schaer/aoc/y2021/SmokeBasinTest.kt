package dev.schaer.aoc.y2021

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

internal class SmokeBasinTest {
    private val solver = SmokeBasin()

    private val input = """
        2199943210
        3987894921
        9856789892
        8767896789
        9899965678
        """.trimIndent()

    @Test
    fun testPartOne() {
        assertEquals(15, solver.partOne(input))
    }

    @Test
    fun testPartTwo() {
        assertEquals(1134, solver.partTwo(input))
    }
}