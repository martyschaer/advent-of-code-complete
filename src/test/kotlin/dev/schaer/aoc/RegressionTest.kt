package dev.schaer.aoc

import dev.schaer.aoc.common.*
import dev.schaer.aoc.common.annotations.Answer
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.TestFactory
import org.reflections.Reflections
import kotlin.test.assertEquals
import kotlin.test.fail

class RegressionTest {

    @TestFactory
    fun `Check all Answers`(): Collection<DynamicTest> {
        val reflections = Reflections(RegressionTest::class.java.packageName)
        val allClasses = reflections.getSubTypesOf(Solution::class.java)
        return allClasses.map { generateDynamicTest(it) }.flatten().sortedBy {
            it.displayName.replace("/", "").toInt()
        }
    }

    private fun generateDynamicTest(clazz: Class<out Solution>): Collection<DynamicTest> {
        if (clazz.name.contains("Template")) {
            return emptyList()
        }

        val day = (clazz.getDeclaredAnnotationsByType(Day::class.java).firstOrNull()?.day
            ?: error("${clazz.name} needs @Day annotation"))
        val year = (clazz.getDeclaredAnnotationsByType(Year::class.java).firstOrNull()?.year
            ?: error("${clazz.name} needs @Year annotation"))

        val dayName = "${year}/${day}"
        val input = InputProvider.instance().getInput(year, day)
        val instance = clazz.constructors.first().newInstance() as Solution

        val partOneExpected = getPartOneExpected(clazz)
        val partOne = DynamicTest.dynamicTest("$dayName/1") {
            instance.reset()
            val actual = instance.partOne(input)
            partOneExpected?.let { assertEquals(it, actual.toString(), "$dayName/1 regressed!") }
                ?: println("No @Answer defined for $dayName/1")
        }

        val partTwoExpected = getPartTwoExpected(clazz)
        val partTwo = DynamicTest.dynamicTest("$dayName/2") {
            instance.reset()
            val actual = instance.partTwo(input)
            partTwoExpected?.let { assertEquals(it, actual.toString(), "$dayName/2 regressed!") }
                ?: println("No @Answer defined for $dayName/2")
        }

        return listOf(partOne, partTwo)
    }

    private fun getPartOneExpected(clazz: Class<out Solution>): String? {
        return getExpectedAnswer(clazz, "partOne")
    }

    private fun getPartTwoExpected(clazz: Class<out Solution>): String? {
        return getExpectedAnswer(clazz, "partTwo")
    }

    private fun getExpectedAnswer(clazz: Class<out Solution>, solverFunName: String): String? {
        val solverFun = clazz.getDeclaredMethod(solverFunName, String::class.java)
        val solution = solverFun.getDeclaredAnnotationsByType(Answer::class.java).firstOrNull()?.solution
        if (solution != null && solution.isBlank()) {
            fail("@Answer defined but empty for ${clazz.name}::$solverFunName")
        }
        return solution
    }

}