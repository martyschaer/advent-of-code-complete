package dev.schaer.aoc.algorithms

import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import kotlin.test.assertEquals
import kotlin.test.assertNull

internal class MathKtTest {

    private companion object {
        @JvmStatic
        val triangeNumbers = mapOf(
            0 to 0,
            1 to 1,
            2 to 3,
            3 to 6,
            4 to 10,
            5 to 15,
            6 to 21,
            7 to 28,
            8 to 36,
            9 to 45,
            10 to 55,
            11 to 66,
        )

        @JvmStatic
        fun triangularNumbers() = triangeNumbers.entries.stream()
            .map { Arguments.of(it.key, it.value) }
    }

    @ParameterizedTest
    @MethodSource("triangularNumbers")
    fun testTriangle(n: Int, t: Int) {
        assertEquals(t, n.triangle())
    }

    @ParameterizedTest
    @MethodSource("triangularNumbers")
    fun testUntriangle(n: Int, t: Int) {
        assertEquals(n, t.untriangle())
    }

    @Test
    fun testUntriangleAll() {
        for (i in (0..60)) {
            if (i !in triangeNumbers.values) {
                assertNull(i.untriangle())
            }
        }
    }
}