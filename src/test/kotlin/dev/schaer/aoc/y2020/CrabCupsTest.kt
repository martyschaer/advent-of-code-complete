package dev.schaer.aoc.y2020

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.TestFactory

internal class CrabCupsTest {
    private val solver = CrabCups()

    @TestFactory
    fun generateTestCasesPartOne(): Collection<DynamicTest> {
        val cases = listOf(
            "389125467" to "67384529",
        )

        return cases.mapIndexed { idx, case ->
            DynamicTest.dynamicTest("Testcase #${idx + 1}") {
                assertEquals(case.second, solver.partOne(case.first))
            }
        }
    }

    @TestFactory
    fun generateTestCasesPartTwo(): Collection<DynamicTest> {
        val cases = listOf(
            "389125467" to 149245887792L,
        )

        return cases.mapIndexed { idx, case ->
            DynamicTest.dynamicTest("Testcase #${idx + 1}") {
                assertEquals(case.second, solver.partTwo(case.first))
            }
        }
    }
}