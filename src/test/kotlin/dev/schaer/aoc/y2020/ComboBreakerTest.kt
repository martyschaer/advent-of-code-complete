package dev.schaer.aoc.y2020

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class ComboBreakerTest {
    private val solver = ComboBreaker()

    @Test
    fun partOne() {
        val input = "5764801\n17807724"
        assertEquals(14897079, solver.partOne(input))
    }

    @Test
    fun partOneExponent() {
        assertEquals(11, solver.calculateExponent(17807724))
    }
}