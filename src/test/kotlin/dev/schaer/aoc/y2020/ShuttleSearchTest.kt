package dev.schaer.aoc.y2020

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.TestFactory

internal class ShuttleSearchTest {
    private val solver = ShuttleSearch()

    @TestFactory
    fun generateTestCases(): Collection<DynamicTest> {
        val cases = listOf(
            "7,13,x,x,59,x,31,19" to 1068781L,
            "17,x,13,19" to 3417L,
            "67,7,59,61" to 754018L,
            "67,x,7,59,61" to 779210L,
            "67,7,x,59,61" to 1261476L,
            "1789,37,47,1889" to 1202161486L,
        )

        return cases.mapIndexed { idx, case ->
            DynamicTest.dynamicTest("Testcase #${idx + 1}") {
                assertEquals(case.second, solver.solvePart2(case.first))
            }
        }
    }
}