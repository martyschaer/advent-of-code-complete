package dev.schaer.aoc.y2020

import dev.schaer.aoc.algorithms.setBit
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class DockingDataTest {
    @Test
    fun testLongSetBit() {
        assertEquals(16, (0L).setBit(4, true))
        assertEquals(0, (1L).setBit(0, false))
        assertEquals(1, (3L).setBit(1, false))
    }
}