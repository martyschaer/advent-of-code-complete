package dev.schaer.aoc.y2020

import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Timeout
import java.util.concurrent.TimeUnit
import kotlin.test.assertEquals
import kotlin.test.assertTrue

internal class CrabCombatTest {
    private val solver = CrabCombat()

    @AfterEach
    fun reset() {
        solver.reset()
    }

    @Test
    fun testPartOne() {
        val input = """
            Player 1:
            9
            2
            6
            3
            1

            Player 2:
            5
            8
            4
            7
            10
        """.trimIndent()

        assertEquals(306, solver.partOne(input))
    }

    @Test
    fun testPartTwo() {
        val input = """
            Player 1:
            9
            2
            6
            3
            1

            Player 2:
            5
            8
            4
            7
            10
        """.trimIndent()

        assertEquals(291, solver.partTwo(input))
    }

    @Test
    @Timeout(2000, unit = TimeUnit.MILLISECONDS)
    fun testPartTwoDoesNotLoop() {
        val input = """
            Player 1:
            43
            19

            Player 2:
            2
            29
            14
        """.trimIndent()

        solver.partTwo(input)

        // if solver.partTwo didn't loop infinitely, the test should succeed.
        assertTrue(true)
    }
}