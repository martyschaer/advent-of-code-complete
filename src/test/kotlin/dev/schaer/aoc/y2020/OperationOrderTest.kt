package dev.schaer.aoc.y2020

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.TestFactory

internal class OperationOrderTest {
    private val solver = OperationOrder()

    @TestFactory
    fun generateTestCasesPartOne(): Collection<DynamicTest> {
        val cases = listOf(
            "1 + 2 * 3 + 4 * 5 + 6" to 71L,
            "1 + (2 * 3) + (4 * (5 + 6))" to 51L,
            "2 * 3 + (4 * 5)" to 26L,
            "5 + (8 * 3 + 9 + 3 * 4 * 3)" to 437L,
            "5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))" to 12240L,
            "((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2" to 13632L,
        )

        return cases.mapIndexed { idx, case ->
            DynamicTest.dynamicTest("Testcase #${idx + 1}") {
                Assertions.assertEquals(case.second, solver.partOne(case.first))
            }
        }
    }

    @TestFactory
    fun generateTestCasesPartTwo(): Collection<DynamicTest> {
        val cases = listOf(
            "1 + 2 * 3 + 4 * 5 + 6" to 231L,
            "1 + (2 * 3) + (4 * (5 + 6))" to 51L,
            "2 * 3 + (4 * 5)" to 46L,
            "5 + (8 * 3 + 9 + 3 * 4 * 3)" to 1445L,
            "5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))" to 669060L,
            "((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2" to 23340L,
        )

        return cases.mapIndexed { idx, case ->
            DynamicTest.dynamicTest("Testcase #${idx + 1}") {
                Assertions.assertEquals(case.second, solver.partTwo(case.first))
            }
        }
    }
}