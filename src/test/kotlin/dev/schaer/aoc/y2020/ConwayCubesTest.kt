package dev.schaer.aoc.y2020

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

internal class ConwayCubesTest {
    private val solver = ConwayCubes()

    @Test
    fun testPartOne() {
        val input = """
            .#.
            ..#
            ###
        """.trimIndent()

        assertEquals(112, solver.partOne(input))
    }

    @Test
    fun testPartTwo() {
        val input = """
            .#.
            ..#
            ###
        """.trimIndent()

        assertEquals(848, solver.partTwo(input))
    }
}