package dev.schaer.aoc.y2020

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.TestFactory

internal class RambunctiousRecitationTest {
    private val solver = RambunctiousRecitation()

    @TestFactory
    fun generateTestCasesForPartOne(): Collection<DynamicTest> {
        val cases = listOf(
            "0,3,6" to 436,
            "1,3,2" to 1,
            "2,1,3" to 10,
            "1,2,3" to 27,
            "2,3,1" to 78,
            "3,2,1" to 438,
            "3,1,2" to 1836,
        )

        return cases.mapIndexed { idx, case ->
            DynamicTest.dynamicTest("Testcase #${idx + 1} (${case.first})") {
                assertEquals(case.second, solver.partOne(case.first))
            }
        }
    }
}