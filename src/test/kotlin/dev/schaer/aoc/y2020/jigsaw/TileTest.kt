package dev.schaer.aoc.y2020.jigsaw

import dev.schaer.aoc.y2020.Fragment
import dev.schaer.aoc.y2020.Position.*
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

internal class TileTest {

    @Test
    fun getEdge() {
        val input = """
            ..........
            .________.
            .________.
            .________.
            .________.
            .________.
            .________.
            #________#
            #________#
            .......#.#
        """.trimIndent().replace("\n", "")
        val tile = Fragment(42, input.toCharArray())

        assertEquals(0, tile.getEdge(TOP))
        assertEquals(14, tile.getEdge(RIGHT))
        assertEquals(10, tile.getEdge(BOTTOM))
        assertEquals(12, tile.getEdge(LEFT))
    }
}