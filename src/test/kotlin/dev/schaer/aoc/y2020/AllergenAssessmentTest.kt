package dev.schaer.aoc.y2020

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

internal class AllergenAssessmentTest {
    private val solver = AllergenAssessment()

    @Test
    fun testPartOne() {
        val input = """
            mxmxvkd kfcds sqjhc nhms (contains dairy, fish)
            trh fvjkl sbzzf mxmxvkd (contains dairy)
            sqjhc fvjkl (contains soy)
            sqjhc mxmxvkd sbzzf (contains fish)
        """.trimIndent()

        assertEquals(5, solver.partOne(input))
    }

    @Test
    fun testPartTwo() {
        val input = """
            mxmxvkd kfcds sqjhc nhms (contains dairy, fish)
            trh fvjkl sbzzf mxmxvkd (contains dairy)
            sqjhc fvjkl (contains soy)
            sqjhc mxmxvkd sbzzf (contains fish)
        """.trimIndent()

        assertEquals("mxmxvkd,sqjhc,fvjkl", solver.partTwo(input))
    }
}