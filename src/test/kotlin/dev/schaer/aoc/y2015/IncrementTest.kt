package dev.schaer.aoc.y2015

import kotlin.test.Test

class IncrementTest {
    @Test
    fun incrementSimple() {
        val chars = "abc".toCharArray()
        Solution2015_11().increment(chars)
        assert(chars contentEquals "abd".toCharArray())
    }

    @Test
    fun incrementSingleOverruns() {
        val chars = "abz".toCharArray()
        Solution2015_11().increment(chars)
        assert(chars contentEquals "aca".toCharArray())
    }

    @Test
    fun incrementMultipleOverruns() {
        val chars = "azz".toCharArray()
        Solution2015_11().increment(chars)
        assert(chars contentEquals "baa".toCharArray())
    }

    @Test
    fun incrementPreOverrun() {
        val chars = "azz".toCharArray()
        Solution2015_11().increment(chars)
        assert(chars contentEquals "baa".toCharArray())
    }
}