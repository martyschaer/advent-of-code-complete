package dev.schaer.aoc

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.InputProvider
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.LongRunning
import dev.schaer.aoc.datastructures.toMicros
import dev.schaer.aoc.y2024.Day07_BridgeRepair
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory
import org.reflections.Reflections
import java.time.Duration
import kotlin.system.measureNanoTime
import kotlin.test.Ignore

private const val DEFAULT_ITERATIONS = 128

class BenchmarkTest {

    companion object {
        val timeByYear = mutableMapOf<Int, Double>()

        @AfterAll
        @JvmStatic
        fun printStats() {
            timeByYear.forEach { (year, micros) ->
                println("$year Total (∅)" + ("%.04fms".format(micros / 1000).padStart(16, ' ')))
            }
        }
    }

    private val yearsToExclude = setOf("2015", "2016", "2020", "2021", "2022", "2023")

    @Test
    @Ignore
    fun `Single Day`() {
        val (p1, p2) = generateDynamicTest(Day07_BridgeRepair::class.java, 64).toList()
        p1.executable.execute()
        p2.executable.execute()
    }

    @TestFactory
    fun `All Solutions`(): Collection<DynamicTest> {
        val reflections = Reflections(BenchmarkTest::class.java.packageName)
        val allClasses = reflections.getSubTypesOf(Solution::class.java)
        return allClasses.map { generateDynamicTest(it) }.flatten().sortedBy { it.displayName }
    }

    private fun generateDynamicTest(clazz: Class<out Solution>, iterations: Int = getIterations(clazz)): Collection<DynamicTest> {
        if (clazz.name.contains("Template") || yearsToExclude.any { clazz.packageName.contains(it) }) {
            return emptyList()
        }

        val day = (clazz.getDeclaredAnnotationsByType(Day::class.java).firstOrNull()?.day
            ?: throw IllegalStateException("${clazz.name} needs @Day annotation"))
        val year = (clazz.getDeclaredAnnotationsByType(Year::class.java).firstOrNull()?.year
            ?: throw IllegalStateException("${clazz.name} needs @Year annotation"))

        val input = InputProvider.instance().getInput(year, day)
        val instance = clazz.constructors.first{ it.parameterCount == 0 }.newInstance() as Solution

        val partA = createTest(year, day, "A", instance, iterations) { it.partOne(input) }
        val partB = createTest(year, day, "B", instance, iterations) { it.partTwo(input) }

        return listOf(partA, partB)
    }

    fun getIterations(clazz: Class<out Solution>): Int {
        return clazz.getDeclaredAnnotationsByType(LongRunning::class.java) //
            .firstOrNull() //
            ?.repetition //
            ?: DEFAULT_ITERATIONS
    }

    private fun createTest(
        year: Int, day: Int, part: String, instance: Solution, iterations: Int, runnable: (Solution) -> Any
    ): DynamicTest {
        val name = "%04d/%02d/%s".format(year, day, part)
        return DynamicTest.dynamicTest(name) {
            val times = (0 until iterations).associateWith {
                instance.reset()
                measureTime { runnable.invoke(instance) }.toMicros()
            }
            instance.reset()

            val sorted = times.values.sorted()
            val quartileCount = iterations / 4

            val middle50 = sorted //
                .drop(quartileCount) //
                .dropLast(quartileCount)

            val avgTime = middle50.average().toLong()
            val minTime = middle50.first()
            val maxTime = middle50.last()

            timeByYear[year] = (timeByYear[year] ?: 0.0) + avgTime

            val solution = runnable.invoke(instance).toString()

            val avgStr = "∅=${fmtTime(avgTime)}"
            val minStr = fmtTime(minTime)
            val maxStr = fmtTime(maxTime)

            println("$name = [${formatSolution(solution)}]($avgStr of [$minStr <= x <= $maxStr])")

        }
    }

    private fun formatSolution(input: String): String {
        val padded = input.padStart(15, ' ')
        if (padded.length == 15) {
            return padded
        }
        return "${padded.substring(0, 14)}…"
    }

    private fun fmtTime(micros: Long): String {
        val timeDigitLength = 6

        if (micros >= 100_000) {
            val millis = micros / 1_000

            if (millis >= 1000) {
                val seconds = millis / 1_000.0
                val str = "%.3f".format(seconds).padStart(timeDigitLength + 1, ' ')
                return "\u001B[31m${str}s\u001B[0m"
            }

            return "\u001B[33m${millis.toString().padStart(timeDigitLength, ' ')}ms\u001B[0m"
        }
        return "${micros.toString().padStart(timeDigitLength, ' ')}μs"
    }

    private fun measureTime(runnable: () -> Any): Duration {
        val nanos = measureNanoTime { runnable.invoke() }
        return Duration.ofNanos(nanos)
    }

}