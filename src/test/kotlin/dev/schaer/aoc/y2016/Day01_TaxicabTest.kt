package dev.schaer.aoc.y2016

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

class Day01_TaxicabTest {

    @Test
    fun partOne() {
        val instance = Day01_Taxicab()
        assertEquals(5, instance.partOne("R2, L3"))
        assertEquals(2, instance.partOne("R2, R2, R2"))
        assertEquals(12, instance.partOne("R5, L5, R5, R3"))
    }

    @Test
    fun partTwo() {
        val instance = Day01_Taxicab()
        assertEquals(4, instance.partTwo("R8, R4, R4, R8"))
    }
}