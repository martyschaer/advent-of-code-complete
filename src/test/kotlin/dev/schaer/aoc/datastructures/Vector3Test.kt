package dev.schaer.aoc.datastructures

import org.junit.jupiter.api.Test
import kotlin.math.sqrt

internal class Vector3Test {
    @Test
    fun vectorDistance() {
        val a = Vector3(1, 2, 3)
        val b = Vector3(3, 2, 1)
        kotlin.test.assertEquals(2.8284271247461903, a.distance(b))
    }

    @Test
    fun vectorDistanceUnit() {
        val a = Vector3(0, 0, 0)
        val b = Vector3(0, 0, 1)
        kotlin.test.assertEquals(1.0, a.distance(b))
    }

    @Test
    fun vectorDistanceZero() {
        val a = Vector3(0, 0, 1)
        val b = Vector3(0, 0, 1)
        kotlin.test.assertEquals(0.0, a.distance(b))
    }

    @Test
    fun vectorDistance2D() {
        val a = Vector3(1, 0, 0)
        val b = Vector3(0, 1, 0)
        kotlin.test.assertEquals(sqrt(2.0), a.distance(b))
    }

    @Test
    fun vectorDistance3D() {
        val a = Vector3(1, 0, 0)
        val b = Vector3(0, 1, 1)
        kotlin.test.assertEquals(sqrt(3.0), a.distance(b))
    }

    @Test
    fun vectorNeighbours() {
        val n = Vector3(0, 0, 0).neighbours()
        kotlin.test.assertEquals(26, n.size)
    }
}