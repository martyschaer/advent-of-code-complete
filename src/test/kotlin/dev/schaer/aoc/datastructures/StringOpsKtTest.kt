package dev.schaer.aoc.datastructures

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

internal class StringOpsKtTest {
    @Test
    fun testSplitString() {
        val input = "abcdef"
        val output = input.split(3)
        assertEquals("abc", output.first)
        assertEquals("def", output.second)
    }
}