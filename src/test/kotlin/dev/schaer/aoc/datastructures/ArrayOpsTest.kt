package dev.schaer.aoc.datastructures

import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory
import kotlin.test.assertEquals

internal class ArrayOpsTest {
    @TestFactory
    fun generateTestCases(): Collection<DynamicTest> {
        val array = "1234".toCharArray()
        return listOf(
            generateTest('1', array, 0, 0, Rotation.ZERO),
            generateTest('2', array, 1, 0, Rotation.ZERO),
            generateTest('3', array, 0, 1, Rotation.ZERO),
            generateTest('4', array, 1, 1, Rotation.ZERO),
            generateTest('3', array, 0, 0, Rotation.NINETY),
            generateTest('1', array, 1, 0, Rotation.NINETY),
            generateTest('4', array, 0, 1, Rotation.NINETY),
            generateTest('2', array, 1, 1, Rotation.NINETY),
            generateTest('4', array, 0, 0, Rotation.ONEEIGHTY),
            generateTest('3', array, 1, 0, Rotation.ONEEIGHTY),
            generateTest('2', array, 0, 1, Rotation.ONEEIGHTY),
            generateTest('1', array, 1, 1, Rotation.ONEEIGHTY),
            generateTest('2', array, 0, 0, Rotation.TWOSEVENTY),
            generateTest('4', array, 1, 0, Rotation.TWOSEVENTY),
            generateTest('1', array, 0, 1, Rotation.TWOSEVENTY),
            generateTest('3', array, 1, 1, Rotation.TWOSEVENTY),
        )
    }

    private fun generateTest(
        expected: Char,
        array: CharArray,
        x: Int,
        y: Int,
        rotation: Rotation,
    ): DynamicTest {
        return DynamicTest.dynamicTest("arr[$x, $y, $rotation] == '$expected'") {
            val actual = array.get(x, y, 2, 2, false, rotation)
            assertEquals(expected, actual)
        }
    }

    @Test
    fun largeArrayTest() {
        val array = "ABCDEFGHIJKLMNOP".toCharArray()
        assertEquals('A', array.get(0, 0, 4, 4, false, Rotation.ZERO))
        assertEquals('M', array.get(0, 0, 4, 4, false, Rotation.NINETY))
        assertEquals('P', array.get(0, 0, 4, 4, false, Rotation.ONEEIGHTY))
        assertEquals('D', array.get(0, 0, 4, 4, false, Rotation.TWOSEVENTY))
        assertEquals('P', array.get(3, 3, 4, 4, false, Rotation.ZERO))
        assertEquals('D', array.get(3, 3, 4, 4, false, Rotation.NINETY))
        assertEquals('A', array.get(3, 3, 4, 4, false, Rotation.ONEEIGHTY))
        assertEquals('M', array.get(3, 3, 4, 4, false, Rotation.TWOSEVENTY))

        assertEquals('M', array.get(0, 0, 4, 4, true, Rotation.ZERO))
        assertEquals('F', array.get(2, 2, 4, 4, true, Rotation.NINETY))
    }
}