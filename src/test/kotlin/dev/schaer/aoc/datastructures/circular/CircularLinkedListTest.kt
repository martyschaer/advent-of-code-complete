package dev.schaer.aoc.datastructures.circular

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

internal class CircularLinkedListTest {

    @Test
    fun addLast() {
        val circle = CircularLinkedList<Int>()
        circle.addLast(1)
        circle.addLast(2)
        circle.addLast(3)
        assertEquals(3, circle.size())
        assertEquals("[1,2,3]", circle.toString())
    }

    @Test
    fun addAll() {
        val circle = CircularLinkedList<Int>()
        circle.addAll(listOf(1, 2, 3, 4))
        assertEquals(4, circle.size())
        assertEquals("[1,2,3,4]", circle.toString())
    }

    @Test
    fun insertAfter() {
        val circle = CircularLinkedList<Int>()
        circle.addAll(listOf(1, 2, 4, 5))
        circle.insertAfter(3, 2)
        assertEquals(5, circle.size())
        assertEquals("[1,2,3,4,5]", circle.toString())
    }

    @Test
    fun insertAfterTail() {
        val circle = CircularLinkedList<Int>()
        circle.addAll(listOf(1, 2, 3, 4))
        circle.insertAfter(5, 4)
        assertEquals(5, circle.size())
        assertEquals("[1,2,3,4,5]", circle.toString())
    }

    @Test
    fun insertAfterMultiple() {
        val circle = CircularLinkedList(listOf(1, 5))
        circle.insertAfter(1, 2, 3, 4)
        assertEquals(5, circle.size())
        assertEquals("[1,2,3,4,5]", circle.toString())
    }

    @Test
    fun insertAfterMultipleTail() {
        val circle = CircularLinkedList(listOf(1, 2))
        circle.insertAfter(2, 3, 4, 5)
        assertEquals(5, circle.size())
        assertEquals("[1,2,3,4,5]", circle.toString())
    }

    @Test
    fun removeAfter() {
        val circle = CircularLinkedList<Int>()
        circle.addAll(listOf(1, 2, 3, 4))
        assertEquals(4, circle.removeAfter(3))
        assertEquals("[1,2,3]", circle.toString())
    }

    @Test
    fun getAfter() {
        val circle = CircularLinkedList<Int>()
        circle.addAll(listOf(1, 2, 3, 4))
        assertEquals(4, circle.getAfter(3))
        assertEquals("[1,2,3,4]", circle.toString())
    }

    @Test
    fun iterateFrom() {
        val circle = CircularLinkedList(listOf(1, 2, 3, 4, 5))
        assertEquals(listOf(2, 3, 4, 5), circle.iterateFromExclusive(1).toList())
    }
}