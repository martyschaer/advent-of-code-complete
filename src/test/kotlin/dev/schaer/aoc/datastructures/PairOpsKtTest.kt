package dev.schaer.aoc.datastructures

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

internal class PairOpsKtTest {
    @Test
    fun testIntPairMapping() {
        val ints = 2 to -4
        val result = ints.map { it * -1 }
        assertEquals(-2 to 4, result)
    }

    @Test
    fun testStringPairMapping() {
        val strings = "abc" to "xyz"
        val result = strings.map { it.reversed() }
        assertEquals("cba" to "zyx", result)
    }
}