package dev.schaer.aoc.datastructures

import org.junit.jupiter.api.Test
import java.awt.Point
import kotlin.test.assertEquals

internal class Array2DTest {
    @Test
    fun testAccess() {
        val values = arrayOf(1, 2, 3, 4, 5, 6, 7, 8, 9)
        val array = Array2D(values, 3, 3, -1)

        assertEquals(-1, array[0, -1])
        assertEquals(-1, array[-1, 0])
        assertEquals(-1, array[-1, -1])
        assertEquals(-1, array[0, 3])
        assertEquals(-1, array[3, 0])
        assertEquals(-1, array[3, 3])
        assertEquals(1, array[0, 0])
        assertEquals(2, array[1, 0])
        assertEquals(3, array[2, 0])
        assertEquals(4, array[0, 1])
        assertEquals(5, array[1, 1])
        assertEquals(6, array[2, 1])
        assertEquals(7, array[0, 2])
        assertEquals(8, array[1, 2])
        assertEquals(9, array[2, 2])
    }

    @Test
    fun testMap() {
        val values = arrayOf(1, 2, 3, 4, 5, 6, 7, 8, 9)
        val array = Array2D(values, 3, 3, -1)
        assertEquals(8, array.map(Point(2, 2)))
        assertEquals(3, array.map(Point(0, 1)))
    }

    @Test
    fun testUnmap() {
        val values = arrayOf(1, 2, 3, 4, 5, 6, 7, 8, 9)
        val array = Array2D(values, 3, 3, -1)
        assertEquals(Point(2, 2), array.unmap(8))
        assertEquals(Point(0, 1), array.unmap(3))
    }
}