package dev.schaer.aoc.y2023

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class Day11CosmicExpansionTest {
    private val solver = Day11_CosmicExpansion()

    val input = """
        ...#......
        .......#..
        #.........
        ..........
        ......#...
        .#........
        .........#
        ..........
        .......#..
        #...#.....
    """.trimIndent()

    @Test
    fun testPartOne() {
        assertEquals(374, solver.partOne(input))
    }

    @Test
    fun testPartTwo10() {
        assertEquals(1030, solver.solve(input, 10))
    }

    @Test
    fun testPartTwo100() {
        assertEquals(8410, solver.solve(input, 100))
    }

    @Test
    fun testPartTwo() {
        assertEquals(82000210, solver.partTwo(input))
    }
}