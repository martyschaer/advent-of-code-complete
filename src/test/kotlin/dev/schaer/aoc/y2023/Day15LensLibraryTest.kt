package dev.schaer.aoc.y2023

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class Day15LensLibraryTest {
    private val solver = Day15_LensLibrary()

    private val input = """
        rn=1,cm-,qp=3,cm=2,qp-,pc=4,ot=9,ab=5,pc-,pc=6,ot=7
    """.trimIndent()

    @Test
    fun testSmallExample() {
        assertEquals(52, solver.partOne("HASH"))
    }

    @Test
    fun testPartOne() {
        assertEquals(1320, solver.partOne(input))
    }

    @Test
    fun testPartTwo() {
        assertEquals(145, solver.partTwo(input))
    }
}