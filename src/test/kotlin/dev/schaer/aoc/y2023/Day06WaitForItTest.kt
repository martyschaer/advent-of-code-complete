package dev.schaer.aoc.y2023

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class Day06WaitForItTest {
    private val solver = Day06_WaitForIt()

    private val input = """
        Time:      7  15   30
        Distance:  9  40  200
    """.trimIndent()

    @Test
    fun testPartOne() {
        assertEquals(288, solver.partOne(input))
    }

    @Test
    fun testPartTwo() {
        assertEquals(71503, solver.partTwo(input))
    }
}