package dev.schaer.aoc.y2023

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class Day16LavaFloorTest {
    private val solver = Day16_LavaFloor()

    private val input = """
        .|...\....
        |.-.\.....
        .....|-...
        ........|.
        ..........
        .........\
        ..../.\\..
        .-.-/..|..
        .|....-|.\
        ..//.|....
    """.trimIndent()

    @Test
    fun testPartOne() {
        assertEquals(46, solver.partOne(input))
    }

    @Test
    fun testPartTwo() {
        assertEquals(51, solver.partTwo(input))
    }
}