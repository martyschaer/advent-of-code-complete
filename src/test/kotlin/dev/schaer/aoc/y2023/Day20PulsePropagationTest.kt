package dev.schaer.aoc.y2023

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class Day20PulsePropagationTest {
    private val solver = Day20_PulsePropagation()

    private val input = """
    """.trimIndent()

    @Test
    fun testPartOneSimple() {
        val input = """
            broadcaster -> a, b, c
            %a -> b
            %b -> c
            %c -> inv
            &inv -> a
        """.trimIndent()
        assertEquals(32000000, solver.partOne(input))
    }

    @Test
    fun testPartOneComplex() {
        val input = """
            broadcaster -> a
            %a -> inv, con
            &inv -> b
            %b -> con
            &con -> output
        """.trimIndent()
        assertEquals(11687500, solver.partOne(input))
    }
}