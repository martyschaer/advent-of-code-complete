package dev.schaer.aoc.y2023

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class Day12HotSpringsTest {
    private val solver = Day12_HotSprings()

    private val input = """
        ???.### 1,1,3
        .??..??...?##. 1,1,3
        ?#?#?#?#?#?#?#? 1,3,1,6
        ????.#...#... 4,1,1
        ????.######..#####. 1,6,5
        ?###???????? 3,2,1
    """.trimIndent()

    @Test
    fun testPartOne() {
        assertEquals(21, solver.partOne(input))
    }

    @Test
    fun testPartOneLine1() {
        assertEquals(1, solver.partOne(input.lines()[0]))
    }

    @Test
    fun testPartOneLine2() {
        assertEquals(4, solver.partOne(input.lines()[1]))
    }

    @Test
    fun testPartOneLine3() {
        assertEquals(1, solver.partOne(input.lines()[2]))
    }

    @Test
    fun testPartOneLine4() {
        assertEquals(1, solver.partOne(input.lines()[3]))
    }

    @Test
    fun testPartOneLine5() {
        assertEquals(4, solver.partOne(input.lines()[4]))
    }

    @Test
    fun testPartOneLine6() {
        assertEquals(10, solver.partOne(input.lines()[5]))
    }

    @Test
    fun testPartOneDebug1() {
        assertEquals(1, solver.partOne("???# 1"))
    }

    @Test
    fun testPartOneDebug2() {
        assertEquals(1, solver.partOne(".##.?#??.#.?# 2,1,1,1"))
    }

    @Test
    fun testPartOneDebug3() {
        assertEquals(0, solver.partOne("###.### 3"))
    }

    @Test
    fun testPartOneDebug4() {
        assertEquals(3, solver.partOne("???#??.??????.??#.. 4,3"))
    }

    @Test
    fun testPartOneDebug5() {
        val input = """
            ?#?###???#??#?.??? 11,1,1
            .????#?#???#??????? 12,1,1
            ?#???##??#?????#.??? 2,11,1
            #??##???????????. 1,11,1
            .??.?.?#?##?#???#?? 1,11
            ??#.?#???####??#??.? 1,11,1
        """.trimIndent()

        assertEquals(31, solver.partOne(input))
    }

    @Test
    fun testPartOneDebug6() {
        assertEquals(6, solver.partOne(".??.?.?#?##?#???#?? 1,11"))
    }

    @Test
    fun testPartOneRealInput1() {
        assertEquals(1, solver.partOne("?#???.??##?#?. 3,1,3,1"))
    }

    @Test
    fun testPartOneRealInput2() {
        assertEquals(2, solver.partOne("..??#.#??. 1,1,1"))
    }

    @Test
    fun testPartOneRealInput3() {
        assertEquals(2, solver.partOne("?????????.?##. 8,3"))
    }

    @Test
    fun testPartOneRealInput4() {
        assertEquals(3, solver.partOne("??##?.???? 5,1,1"))
    }

    @Test
    fun testPartOneRealInput5() {
        assertEquals(3, solver.partOne("?.??#?????#???.?? 8,2"))
    }

    @Test
    fun testPartTwo() {
        val lines = input.lines()
        assertEquals(1, solver.partTwo(lines[0]))
        assertEquals(16384, solver.partTwo(lines[1]))
        assertEquals(1, solver.partTwo(lines[2]))
        assertEquals(16, solver.partTwo(lines[3]))
        assertEquals(2500, solver.partTwo(lines[4]))
        assertEquals(525152, solver.partTwo(input))
    }
}