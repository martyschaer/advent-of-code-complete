package dev.schaer.aoc.y2023

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class Day17ClumsyCrucibleTest {
    private val solver = Day17_ClumsyCrucible()

    private val input = """
        2413432311323
        3215453535623
        3255245654254
        3446585845452
        4546657867536
        1438598798454
        4457876987766
        3637877979653
        4654967986887
        4564679986453
        1224686865563
        2546548887735
        4322674655533
    """.trimIndent()

    @Test
    fun testPartOneDebug1() {
        val input = """
            11111
            99991
        """.trimIndent()
        assertEquals(13, solver.partOne(input))
    }

    @Test
    fun testPartOneDebug2() {
        val input = """
            241343
            321545
            999991
        """.trimIndent()
        assertEquals(21, solver.partOne(input))
    }

    @Test
    fun testPartOne() {
        assertEquals(102, solver.partOne(input))
    }

    @Test
    fun testPartTwo() {
        assertEquals(94, solver.partTwo(input))
    }

    @Test
    fun testPartTwoAdditonalExample() {
        val input = """
            111111111111
            999999999991
            999999999991
            999999999991
            999999999991
        """.trimIndent()
        assertEquals(71, solver.partTwo(input))
    }
}