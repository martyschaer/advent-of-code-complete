package dev.schaer.aoc.y2023

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class Day08HauntedWastelandTest {
    private val solver = Day08_HauntedWasteland()

    @Test
    fun testPartOne() {
        val input = """
        RL

        AAA = (BBB, CCC)
        BBB = (DDD, EEE)
        CCC = (ZZZ, GGG)
        DDD = (DDD, DDD)
        EEE = (EEE, EEE)
        GGG = (GGG, GGG)
        ZZZ = (ZZZ, ZZZ)
    """.trimIndent()
        assertEquals(2, solver.partOne(input))
    }

    @Test
    fun testPartOneRepeatingSteps() {
        val input = """
            LLR

            AAA = (BBB, BBB)
            BBB = (AAA, ZZZ)
            ZZZ = (ZZZ, ZZZ)
        """.trimIndent()
        assertEquals(6, solver.partOne(input))
    }

    @Test
    fun testPartTwo() {
        val input = """
            LR

            11A = (11B, XXX)
            11B = (XXX, 11Z)
            11Z = (11B, XXX)
            22A = (22B, XXX)
            22B = (22C, 22C)
            22C = (22Z, 22Z)
            22Z = (22B, 22B)
            XXX = (XXX, XXX)
        """.trimIndent()

        assertEquals(6, solver.partTwo(input))
    }
}