package dev.schaer.aoc.y2023

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class Day18LavaductLagoonTest {
    private val solver = Day18_LavaductLagoon()

    private val input = """
        R 6 (#70c710)
        D 5 (#0dc571)
        L 2 (#5713f0)
        D 2 (#d2c081)
        R 2 (#59c680)
        D 2 (#411b91)
        L 5 (#8ceee2)
        U 2 (#caa173)
        L 1 (#1b58a2)
        U 2 (#caa171)
        R 2 (#7807d2)
        U 3 (#a77fa3)
        L 2 (#015232)
        U 2 (#7a21e3)
    """.trimIndent()

    @Test
    fun testPartOne() {
        assertEquals(62, solver.partOne(input))
    }

    @Test
    fun testPartOneDebug1() {
        val input = """
            R 2 (#000000)
            D 4 (#000000)
            L 2 (#000000)
            U 4 (#000000)
        """.trimIndent()
        assertEquals(15, solver.partOne(input))
    }

    @Test
    fun testPartOneDebug2() {
        val input = """
            R 3 (#000000)
            D 4 (#000000)
            L 3 (#000000)
            U 4 (#000000)
        """.trimIndent()
        assertEquals(20, solver.partOne(input))
    }

    @Test
    fun testPartTwo() {
        assertEquals(952408144115, solver.partTwo(input))
    }
}