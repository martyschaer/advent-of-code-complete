package dev.schaer.aoc.y2023

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class Day07CamelCardsTest {
    private val solver = Day07_CamelCards()

    private val input = """
        32T3K 765
        T55J5 684
        KK677 28
        KTJJT 220
        QQQJA 483
    """.trimIndent()

    @Test
    fun testPartOne() {
        assertEquals(6440, solver.partOne(input))
    }

    @Test
    fun testPartTwo() {
        assertEquals(5905, solver.partTwo(input))
    }

    /**
     * Input created by u/LxsterGames with better edge-case coverage
     *
     * https://old.reddit.com/r/adventofcode/comments/18cr4xr
     */
    private val communityInput = """
        2345A 1
        Q2KJJ 13
        Q2Q2Q 19
        T3T3J 17
        T3Q33 11
        2345J 3
        J345A 2
        32T3K 5
        T55J5 29
        KK677 7
        KTJJT 34
        QQQJA 31
        JJJJJ 37
        JAAAA 43
        AAAAJ 59
        AAAAA 61
        2AAAA 23
        2JJJJ 53
        JJJJ2 41
    """.trimIndent()

    @Test
    fun testPartOneCommunity() {
        assertEquals(6592, solver.partOne(communityInput))
    }

    @Test
    fun testPartTwoCommunity() {
        assertEquals(6839, solver.partTwo(communityInput))
    }
}