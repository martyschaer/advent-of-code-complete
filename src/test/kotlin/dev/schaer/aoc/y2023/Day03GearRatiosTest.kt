package dev.schaer.aoc.y2023

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class Day03GearRatiosTest {
    private val solver = Day03_GearRatios()

    private val input = """
        467..114..
        ...*......
        ..35..633.
        ......#...
        617*......
        .....+.58.
        ..592.....
        ......755.
        ...${'$'}.*....
        .664.598..
    """.trimIndent()

    @Test
    fun testPartOne() {
        assertEquals(4361, solver.partOne(input))
    }

    @Test
    fun testPartTwo() {
        assertEquals(467835, solver.partTwo(input))
    }
}