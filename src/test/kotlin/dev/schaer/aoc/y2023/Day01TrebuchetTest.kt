package dev.schaer.aoc.y2023

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

internal class Day01TrebuchetTest {
    private val solver = Day01_Trebuchet()

    @Test
    fun testPartOne() {
        val input = """
            1abc2
            pqr3stu8vwx
            a1b2c3d4e5f
            treb7uchet
        """.trimIndent()
        val solution = solver.partOne(input)
        assertEquals(142, solution)
    }

    @Test
    fun testPartTwo() {
        val input = """
            two1nine
            eightwothree
            abcone2threexyz
            xtwone3four
            4nineeightseven2
            zoneight234
            7pqrstsixteen
        """.trimIndent()
        val solution = solver.partTwo(input)
        assertEquals(281, solution)
    }

    @Test
    fun testOverlappingNumbers() {
        val input = "175rpdmxfeightwos"
        val solution = solver.partTwo(input)
        assertEquals(12, solution)
    }
}