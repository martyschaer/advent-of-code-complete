package dev.schaer.aoc.y2023

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class Day09MirageMaintenanceTest {
    private val solver = Day09_MirageMaintenance()

    private val input = """
        0 3 6 9 12 15
        1 3 6 10 15 21
        10 13 16 21 30 45
    """.trimIndent()

    @Test
    fun testPartOne() {
        val lines = input.lines()
        assertEquals(18, solver.partOne(lines[0]))
        assertEquals(28, solver.partOne(lines[1]))
        assertEquals(68, solver.partOne(lines[2]))
        assertEquals(114, solver.partOne(input))
    }

    @Test
    fun testPartTwo() {
        val lines = input.lines()
        assertEquals(-3, solver.partTwo(lines[0]))
        assertEquals(0, solver.partTwo(lines[1]))
        assertEquals(5, solver.partTwo(lines[2]))
        assertEquals(2, solver.partTwo(input))
    }
}