package dev.schaer.aoc.y2023

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class Day10PipeMazeTest {
    private val solver = Day10_PipeMaze()

    @Test
    fun testPartOneSmall() {
        val input = """
        .....
        .S-7.
        .|.|.
        .L-J.
        .....
    """.trimIndent()
        assertEquals(4, solver.partOne(input))
    }

    @Test
    fun testPartOneBig() {
        val input = """
        ..F7.
        .FJ|.
        SJ.L7
        |F--J
        LJ...
    """.trimIndent()
        assertEquals(8, solver.partOne(input))
    }

    @Test
    fun testPartTwoSmall() {
        val input = """
            ...........
            .S-------7.
            .|F-----7|.
            .||.....||.
            .||.....||.
            .|L-7.F-J|.
            .|..|.|..|.
            .L--J.L--J.
            ...........
        """.trimIndent()
        assertEquals(4, solver.partTwo(input))
    }

    @Test
    fun testPartTwoIslands() {
        val input = """
            ..........
            .S------7.
            .|F----7|.
            .||OOOO||.
            .||OOOO||.
            .|L-7F-J|.
            .|II||II|.
            .L--JL--J.
            ..........
        """.trimIndent()
        assertEquals(4, solver.partTwo(input))
    }

    @Test
    fun testPartTwoBig() {
        val input = """
            .F----7F7F7F7F-7....
            .|F--7||||||||FJ....
            .||.FJ||||||||L7....
            FJL7L7LJLJ||LJ.L-7..
            L--J.L7...LJS7F-7L7.
            ....F-J..F7FJ|L7L7L7
            ....L7.F7||L7|.L7L7|
            .....|FJLJ|FJ|F7|.LJ
            ....FJL-7.||.||||...
            ....L---J.LJ.LJLJ...
        """.trimIndent()
        assertEquals(8, solver.partTwo(input))
    }

    @Test
    fun testPartTwoBigWithJunk() {
        val input = """
            FF7FSF7F7F7F7F7F---7
            L|LJ||||||||||||F--J
            FL-7LJLJ||||||LJL-77
            F--JF--7||LJLJ7F7FJ-
            L---JF-JLJ.||-FJLJJ7
            |F|F-JF---7F7-L7L|7|
            |FFJF7L7F-JF7|JL---7
            7-L-JL7||F7|L7F-7F7|
            L.L7LFJ|||||FJL7||LJ
            L7JLJL-JLJLJL--JLJ.L
        """.trimIndent()
        assertEquals(10, solver.partTwo(input))
    }
}