package dev.schaer.aoc.y2023

import dev.schaer.aoc.datastructures.parseIntoArray2dOfChars
import dev.schaer.aoc.model.Direction
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class Day14ParabolicReflectorDishTest {
    private val solver = Day14_ParabolicReflectorDish()

    private val input = """
        O....#....
        O.OO#....#
        .....##...
        OO.#O....O
        .O.....O#.
        O.#..O.#.#
        ..O..#O..O
        .......O..
        #....###..
        #OO..#....
    """.trimIndent()

    @Test
    fun testPartOne() {
        assertEquals(136, solver.partOne(input))
    }

    @Test
    fun testTiltNorth() {
        val platform = parseIntoArray2dOfChars(input, '.')
        val expected = """
            OOOO.#.O..
            OO..#....#
            OO..O##..O
            O..#.OO...
            ........#.
            ..#....#.#
            ..O..#.O.O
            ..O.......
            #....###..
            #....#....
        """.trimIndent()

        solver.tilt(platform, Direction.Cardinal.NORTH)
        val actual = platform.toString("").trimEnd('\n')
        assertEquals(expected, actual)
    }

    @Test
    fun testPartTwo() {
        assertEquals(64, solver.partTwo(input))
    }
}