package dev.schaer.aoc.y2023

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class Day13PointOfIncidenceTest {
    private val solver = Day13_PointOfIncidence()

    private val input = """
        #.##..##.
        ..#.##.#.
        ##......#
        ##......#
        ..#.##.#.
        ..##..##.
        #.#.##.#.

        #...##..#
        #....#..#
        ..##..###
        #####.##.
        #####.##.
        ..##..###
        #....#..#
    """.trimIndent()

    @Test
    fun testDebug() {
        val str = """
            #.#.#
            #.#.#
            ..#..
        """.trimIndent()
        assertEquals(100, solver.partOne(str))
    }

    @Test
    fun testPartOne() {
        assertEquals(405, solver.partOne(input))
    }

    @Test
    fun testPartTwo() {
        assertEquals(400, solver.partTwo(input))
    }
}