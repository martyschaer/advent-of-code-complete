package dev.schaer.aoc.y2022

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

internal class CampCleanupTest {
    private val solver = CampCleanup()

    private val input = """
        2-4,6-8
        2-3,4-5
        5-7,7-9
        2-8,3-7
        6-6,4-6
        2-6,4-8
        """.trimIndent()

    @Test
    fun testPartOne() {
        assertEquals(2, solver.partOne(input))
    }

    @Test
    fun testPartTwo() {
        assertEquals(4, solver.partTwo(input))
    }
}