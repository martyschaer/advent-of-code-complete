package dev.schaer.aoc.y2022

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

internal class CalorieCountingTest {
    private val solver = CalorieCounting()

    private val input = """
            1000
            2000
            3000

            4000

            5000
            6000

            7000
            8000
            9000

            10000

        """.trimIndent()

    @Test
    fun testPartOne() {
        assertEquals(24000, solver.partOne(input))
    }

    @Test
    fun testPartTwo() {
        assertEquals(45000, solver.partTwo(input))
    }
}