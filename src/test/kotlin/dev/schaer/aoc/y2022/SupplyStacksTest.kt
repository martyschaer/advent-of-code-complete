package dev.schaer.aoc.y2022

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

internal class SupplyStacksTest {
    private val solver = SupplyStacks()

    private val input = """
    [D]    
[N] [C]    
[Z] [M] [P]
 1   2   3 

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2
        """.trimIndent()

    @Test
    fun testPartOne() {
        assertEquals("CMZ", solver.partOne(input))
    }

    @Test
    fun testPartTwo() {
        assertEquals("MCD", solver.partTwo(input))
    }
}