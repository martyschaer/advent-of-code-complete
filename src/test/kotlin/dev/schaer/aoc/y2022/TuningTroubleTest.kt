package dev.schaer.aoc.y2022

import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import kotlin.test.assertEquals

internal class TuningTroubleTest {
    private val solver = TuningTrouble()

    companion object {
        @JvmStatic
        fun partOneValues(): List<Array<Any>> {
            return listOf(
                arrayOf("mjqjpqmgbljsphdztnvjfqwrcgsmlb", 7),
                arrayOf("bvwbjplbgvbhsrlpgdmjqwftvncz", 5),
                arrayOf("nppdvjthqldpwncqszvftbrmjlhg", 6),
                arrayOf("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg", 10),
                arrayOf("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw", 11),
            )
        }

        @JvmStatic
        fun partTwoValues(): List<Array<Any>> {
            return listOf(
                arrayOf("mjqjpqmgbljsphdztnvjfqwrcgsmlb", 19),
                arrayOf("bvwbjplbgvbhsrlpgdmjqwftvncz", 23),
                arrayOf("nppdvjthqldpwncqszvftbrmjlhg", 23),
                arrayOf("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg", 29),
                arrayOf("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw", 26),
            )
        }
    }

    @ParameterizedTest(name = "TuningTrouble::partOne({0}) => {1}")
    @MethodSource("partOneValues")
    fun `partOne() solves the problem correctly for a variety of inputs`(input: String, expected: Int) {
        assertEquals(expected, solver.partOne(input))
    }

    @ParameterizedTest(name = "TuningTrouble::partTwo({0}) => {1}")
    @MethodSource("partTwoValues")
    fun `partTwo() solves the problem correctly for a variety of inputs`(input: String, expected: Int) {
        assertEquals(expected, solver.partTwo(input))
    }
}