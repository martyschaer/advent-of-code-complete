package dev.schaer.aoc.y2022

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

internal class RockPaperScissorsTest {
    private val solver = RockPaperScissors()

    private val input = """
A Y
B X
C Z
        """.trimIndent()

    @Test
    fun testPartOne() {
        assertEquals(15, solver.partOne(input))
    }

    @Test
    fun testPartTwo() {
        assertEquals(12, solver.partTwo(input))
    }
}