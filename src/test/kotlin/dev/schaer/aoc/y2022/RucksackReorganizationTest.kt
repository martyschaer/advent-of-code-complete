package dev.schaer.aoc.y2022

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

internal class RucksackReorganizationTest {
    private val solver = RucksackReorganization()

    private val input = """
        vJrwpWtwJgWrhcsFMMfFFhFp
        jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
        PmmdzqPrVvPwwTWBwg
        wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
        ttgJtRGJQctTZtZT
        CrZsJsPPZsGzwwsLwLmpwMDw
        """.trimIndent()

    @Test
    fun testPartOne() {
        assertEquals(157, solver.partOne(input))
    }

    @Test
    fun testPartTwo() {
        assertEquals(70, solver.partTwo(input))
    }
}