package dev.schaer.metaprogramming.aoc

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertDoesNotThrow
import org.junit.jupiter.api.assertThrows
import java.nio.file.Paths
import java.time.LocalDate
import kotlin.test.assertEquals

class TemplateInstantiatorTest {
    @Test
    fun firstOfDecember_shouldNotThrowError() {
        assertDoesNotThrow { TemplateInstantiator(LocalDate.of(2024, 12, 1)) }
    }

    @Test
    fun twentyfifthOfDecember_shouldNotThrowError() {
        assertDoesNotThrow { TemplateInstantiator(LocalDate.of(2024, 12, 25)) }
    }

    @Test
    fun thirtiethOfNovember_shouldThrowError() {
        assertThrows<IllegalArgumentException> { TemplateInstantiator(LocalDate.of(2024, 11, 30)) }
    }

    @Test
    fun twentysixthOfDecember_shouldThrowError() {
        assertThrows<IllegalArgumentException> { TemplateInstantiator(LocalDate.of(2024, 12, 26)) }
    }

    @Test
    fun getTitle_shouldReturnNameOfDay() {
        val date = LocalDate.of(2024, 12, 4)
        val instantiator = TemplateInstantiator(date) { _ ->
            """<article class="day-desc"><h2>--- Day 4: Ceres Search ---</h2><p>"Looks like the Chief's not here. Next!" One of The Historians pulls out
a device and pushes the only button on it. After a brief flash, you recognize the interior of the <a href="/2019/day/10">Ceres monitoring station</a>!</p>"""
        }

        assertEquals("Day04_CeresSearch", instantiator.getTitle())
    }

    @Test
    fun determinePathForDayClass() {
        val date = LocalDate.of(2024, 12, 4)
        val instantiator = TemplateInstantiator(date) { _ ->
            """<article class="day-desc"><h2>--- Day 4: Ceres Search ---</h2><p>"Looks like the Chief's not here. Next!" One of The Historians pulls out
a device and pushes the only button on it. After a brief flash, you recognize the interior of the <a href="/2019/day/10">Ceres monitoring station</a>!</p>"""
        }

        val expected = Paths.get("src/main/kotlin/dev/schaer/aoc/y2024/Day04_CeresSearch.kt")

        assertEquals(expected, instantiator.determinePathForDayClass())
    }

    @Test
    fun determinePathForDayTestClass() {
        val date = LocalDate.of(2024, 12, 4)
        val instantiator = TemplateInstantiator(date) { _ ->
            """<article class="day-desc"><h2>--- Day 4: Ceres Search ---</h2><p>"Looks like the Chief's not here. Next!" One of The Historians pulls out
a device and pushes the only button on it. After a brief flash, you recognize the interior of the <a href="/2019/day/10">Ceres monitoring station</a>!</p>"""
        }

        val expected = Paths.get("src/test/kotlin/dev/schaer/aoc/y2024/Day04_CeresSearchTest.kt")

        assertEquals(expected, instantiator.determinePathForDayTestClass())
    }

    @Test
    fun filLDayClassTemplate_shouldReturnFilledTemplate() {
        val date = LocalDate.of(2024, 12, 4)
        val instantiator = TemplateInstantiator(date) { _ ->
            """<article class="day-desc"><h2>--- Day 4: Ceres Search ---</h2><p>"Looks like the Chief's not here. Next!" One of The Historians pulls out
a device and pushes the only button on it. After a brief flash, you recognize the interior of the <a href="/2019/day/10">Ceres monitoring station</a>!</p>"""
        }

        val expected = """
package dev.schaer.aoc.y2024

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer

fun main() {
    Day04_CeresSearch().singleRun()
}

@Year(2024)
@Day(4)
class Day04_CeresSearch : Solution {

    @Answer("")
    override fun partOne(input: String): Int {
        TODO("Implement part one")
    }

    @Answer("")
    override fun partTwo(input: String): Int {
        TODO("Implement part two")
    }
}
        """.trimIndent()

        assertEquals(expected, instantiator.fillDayClassTemplate())
    }

    @Test
    fun filLDayClassTestTemplate_shouldReturnFilledTemplate() {
        val date = LocalDate.of(2024, 12, 4)
        val instantiator = TemplateInstantiator(date) { _ ->
            """<article class="day-desc"><h2>--- Day 4: Ceres Search ---</h2><p>"Looks like the Chief's not here. Next!" One of The Historians pulls out
a device and pushes the only button on it. After a brief flash, you recognize the interior of the <a href="/2019/day/10">Ceres monitoring station</a>!</p>"""
        }

        val quotes = "\"\"\""

        val expected = """
package dev.schaer.aoc.y2024

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class Day04_CeresSearchTest {
    @Test
    fun examplePartOne() {
        val input = $quotes
        $quotes.trimIndent()

        val solution = Day04_CeresSearch().partOne(input)

        assertEquals(TODO(), solution)
    }

    @Test
    fun examplePartTwo() {
        val input = $quotes
        $quotes.trimIndent()

        val solution = Day04_CeresSearch().partTwo(input)

        assertEquals(TODO(), solution)
    }
}
        """.trimIndent()

        assertEquals(expected, instantiator.fillDayClassTemplateTest())
    }
}