package dev.schaer.metaprogramming.aoc

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import okhttp3.OkHttpClient
import okhttp3.Request
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.time.LocalDate
import java.time.Month
import java.util.regex.Pattern

class TemplateInstantiator(private val date: LocalDate, private val request: (url: String) -> String = ::makeRequest) {
    companion object {
        private fun makeRequest(url: String): String {
            val request = Request.Builder() //
                .get() //
                .url(url) //
                .build()

            return runBlocking {
                withContext(Dispatchers.IO) {
                    OkHttpClient().newCall(request).execute().use { response ->
                        if (!response.isSuccessful) {
                            error("[${response.code}] Unable to get $url: ${response.message}")
                        }
                        response.body?.string() ?: error("Received successful but empty response for $url")
                    }
                }
            }
        }
    }

    init {
        if (date.month != Month.DECEMBER || date.dayOfMonth > 25) {
            throw IllegalArgumentException("$date is not during advent")
        }
    }

    fun getTitle(): String {
        val content = runBlocking { request(createUrl()) }
        val titlePattern = Pattern.compile("<h2>--- Day \\d+: ([A-Za-z ]+) ---<\\/h2>")
        val matcher = titlePattern.matcher(content)
        if (!matcher.find()) {
            error("Unable to find title in $content")
        }
        val name = matcher.group(1)

        return "Day${getTwoDigitDay()}_${name.replace(Regex("\\s"), "")}"
    }

    private fun createUrl(): String {
        return "https://adventofcode.com/${date.year}/day/${date.dayOfMonth}"
    }

    fun fillDayClassTemplate(): String {
        val templatePath = Paths.get("src/main/resources/template/Template.kt")
        var template = Files.readString(templatePath)
        template = template.replace("Template", getTitle())
        template = template.replace("@Year(0)", "@Year(${date.year})")
        template = template.replace("@Day(0)", "@Day(${date.dayOfMonth})")
        template = template.replace("package dev.schaer.metaprogramming.aoc", "package dev.schaer.aoc.y${date.year}")
        return template
    }

    fun fillDayClassTemplateTest(): String {
        val templatePath = Paths.get("src/main/resources/template/TemplateTest.kt")
        var template = Files.readString(templatePath)
        template = template.replace("TemplateTest", getTitle() + "Test")
        template = template.replace("Template", getTitle())
        template = template.replace("package dev.schaer.metaprogramming.aoc", "package dev.schaer.aoc.y${date.year}")
        return template
    }

    fun determinePathForDayClass(): Path {
        return Paths.get("${getSourcePackage()}/y${date.year}/${getTitle()}.kt")
    }

    fun determinePathForDayTestClass(): Path {
        return Paths.get("${getTestPackage()}/y${date.year}/${getTitle()}Test.kt")
    }

    private fun getSourcePackage(): String {
        return "src/main/kotlin/dev/schaer/aoc"
    }

    private fun getTestPackage(): String {
        return "src/test/kotlin/dev/schaer/aoc"
    }

    private fun getTwoDigitDay(): String {
        return "%02d".format(date.dayOfMonth)
    }

}