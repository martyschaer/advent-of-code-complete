package dev.schaer.metaprogramming.aoc

import java.nio.file.Files
import java.nio.file.Files.exists
import java.nio.file.StandardOpenOption.CREATE_NEW
import java.nio.file.StandardOpenOption.WRITE
import java.time.LocalDate

private val date = LocalDate.now()
private val instantiator = TemplateInstantiator(date)

fun main() {
    println("Preparing for $date.")
    createClass()
    createTestClass()
    println("Done.")
}

private fun createClass() {
    val path = instantiator.determinePathForDayClass()
    val name = path.fileName.toString()
    if (exists(path)) {
        println("Class $name already exists, not doing anything.")
        return
    }
    val content = instantiator.fillDayClassTemplate()
    Files.writeString(path, content, CREATE_NEW, WRITE)
    println("Created $name.")
}

private fun createTestClass() {
    val path = instantiator.determinePathForDayTestClass()
    val name = path.fileName.toString()
    if (exists(path)) {
        println("Class $name already exists, not doing anything.")
        return
    }
    val content = instantiator.fillDayClassTemplateTest()
    Files.writeString(path, content, CREATE_NEW, WRITE)
    println("Created $name.")
}