package dev.schaer.aoc.y2024

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import dev.schaer.aoc.datastructures.Array2D
import dev.schaer.aoc.datastructures.Vector2
import dev.schaer.aoc.model.Direction
import dev.schaer.aoc.y2024.Day18_RAMRun.Color.*
import java.util.*

fun main() {
    Day18_RAMRun().singleRun()
}

private const val WIDTH = 71
private const val HEIGHT = 71
private const val LIMIT = 1024

@Year(2024)
@Day(18)
class Day18_RAMRun(val width: Int = WIDTH, val height: Int = HEIGHT, val limit: Int = LIMIT) : Solution {

    /**
     * Make a map of the $limit dropped blocked tiles
     * and use simple BFS to walk it.
     */
    @Answer("404")
    override fun partOne(input: String): Int {
        val walkable = produceWalkabilityMap(input)
        val start = Vector2(0, 0)
        val end = Vector2(width - 1, height - 1)

        data class Node(val pos: Vector2, val steps: Int)

        val queue = PriorityQueue<Node>(compareBy { it.steps })
        queue.offer(Node(start, 0))
        val seen = mutableMapOf<Vector2, Int>()

        while (queue.isNotEmpty()) {
            val (pos, score) = queue.poll()
            if (pos == end) return score
            val nextScore = score + 1
            Direction.Relative.values().forEach { dir ->
                val nextPos = pos + dir.vec
                if (walkable[nextPos] && (seen[nextPos] ?: Int.MAX_VALUE) > nextScore) {
                    seen[nextPos] = nextScore
                    queue.offer(Node(nextPos, score + 1))
                }
            }
        }

        return Int.MAX_VALUE
    }

    private fun produceWalkabilityMap(input: String): Array2D<Boolean> {
        val walkable = Array2D(Array(width * height) { true }, width, height, false) {
            if (it) " " else "#"
        }

        input.lineSequence().take(limit).forEach { line ->
            val (x, y) = line.split(',').map { it.toInt() }
            walkable[x, y] = false
        }

        return walkable
    }

    /**
     * We traverse from top left to bottom right.
     * When dropping bytes, we keep track which "side" they are on.
     * Either touching the top/right side or bottom/left side.
     * Touching propagates to neighbours, orthogonally and diagonally.
     * When a byte would touch neighbours of both colors, we know the path is blocked.
     */
    @Answer("27,60")
    override fun partTwo(input: String): String {
        val map = Array2D<Color?>(Array(width * height) { null }, width, height, null) {
            when (it) {
                RED -> "\u001B[31m#\u001B[0m"
                BLU -> "\u001B[34m#\u001B[0m"
                UNKNOWN -> "?"
                null -> " "
            }
        }
        val sequence = input.lineSequence().map { line ->
            val (x, y) = line.split(',').map { it.toInt() }
            Vector2(x, y)
        }

        sequence.forEach { pos ->
            map[pos] = UNKNOWN

            val neighbours = Direction.Compass.values().map { dir ->
                pos + dir.vec
            }.filter { map.isInBounds(it) }.associateWith {
                map[it]
            }.filter { it.value != null }

            val neighbouringColors = neighbours.values.toSet()

            val colorByEdge = colorByEdge(pos)
            val colorByNeighbours = when {
                RED in neighbouringColors -> RED
                BLU in neighbouringColors -> BLU
                else -> UNKNOWN
            }

            if (colorByEdge != colorByNeighbours) {
                if (RED in neighbouringColors && BLU in neighbouringColors || colorByNeighbours != UNKNOWN && colorByEdge != UNKNOWN) {
                    // connecting the two colors, the path is now blocked
                    return "${pos.x},${pos.y}"
                }
            }

            val newColor = if (colorByNeighbours != UNKNOWN) colorByNeighbours else colorByEdge
            if (newColor != UNKNOWN) {
                updateNeighbourColors(map, pos, newColor)
            }
        }

        error("Unable to find a coordinate that blocks the path")
    }

    private fun updateNeighbourColors(map: Array2D<Color?>, pos: Vector2, color: Color) {
        if (map[pos] != UNKNOWN) return
        map[pos] = color
        Direction.Compass.values().forEach { dir ->
            val next = pos + dir.vec
            updateNeighbourColors(map, next, color)
        }
    }

    private fun colorByEdge(pos: Vector2 ): Color {
        if (pos.x == width - 1 || pos.y == 0) {
            // top and right edge
            return RED
        }
        if (pos.x == 0 || pos.y == height - 1) {
            // bottom and left edge
            return BLU
        }
        return UNKNOWN
    }

    private enum class Color {
        RED, BLU, UNKNOWN
    }
}