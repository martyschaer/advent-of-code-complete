package dev.schaer.aoc.y2024

import dev.schaer.aoc.algorithms.pow
import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import java.util.Stack

fun main() {
    Day17_ChronospatialComputer().singleRun()
}

@Year(2024)
@Day(17)
class Day17_ChronospatialComputer : Solution {

    @Answer("7,6,5,3,6,5,7,0,4")
    override fun partOne(input: String): String {
        val computer = parseComputer(input)
        val program = parseProgram(input)
        val output = computer.execute(program)
        return output.joinToString(separator = ",") { it.toString() }
    }

    private fun parseComputer(input: String): Computer {
        val (a, b, c) = input.split("\n\n").first().lines().map { line ->
            line.split(":").last().trim().toInt()
        }
        return Computer(a, b, c)
    }

    private fun parseProgram(input: String): List<Int> {
        return input.split("\n\n").last().split(": ").last().split(',').map { it.toInt() }
    }

    class Computer(var rA: Int, var rB: Int, var rC: Int) {
        private val output = mutableListOf<Int>()

        fun execute(program: List<Int>): List<Int> {
            output.clear()

            var programCounter = 0
            while (programCounter in program.indices) {
                val opcode = program[programCounter]
                val operand = program[programCounter + 1]
                programCounter += 2
                when (opcode) {
                    0 -> adv(operand)
                    1 -> bxl(operand)
                    2 -> bst(operand)
                    3 -> jnz(operand) { programCounter = it }
                    4 -> bxc(operand)
                    5 -> out(operand)
                    6 -> bdv(operand)
                    7 -> cdv(operand)
                }
            }

            return output
        }

        private fun adv(operand: Int) {
            val denominator = 2.pow(combo(operand))
            rA = rA / denominator
        }

        private fun bxl(operand: Int) {
            rB = rB xor operand
        }

        private fun bst(operand: Int) {
            rB = combo(operand) % 8
        }

        private fun jnz(operand: Int, sideEffect: (Int) -> Unit) {
            if (rA == 0) return
            sideEffect(operand)
        }

        private fun bxc(ignoredOperand: Int) {
            rB = rB xor rC
        }

        private fun out(operand: Int) {
            val outvalue = combo(operand) % 8
            output.add(outvalue)
        }

        private fun bdv(operand: Int) {
            val denominator = 2.pow(combo(operand))
            rB = rA / denominator
        }

        private fun cdv(operand: Int) {
            val denominator = 2.pow(combo(operand))
            rC = rA / denominator
        }

        private fun combo(operand: Int): Int {
            return when (operand) {
                in 0..3 -> operand
                4 -> rA
                5 -> rB
                6 -> rC
                else -> error("Halt & Catch Fire: $operand")
            }
        }
    }

    /**
     * Solution to Part two is specific to my input.
     * I decompiled the instructions, and reverse engineered them.
     * This iteratively goes through, 3 bits of the A Register (rA)
     * at a time, and finds compatible solutions.
     * If at any point an inconsistency is discovered, the solution-path
     * is discarded.
     *
     * Some analysis:
     * - rA can only ever decrease
     * - rA is always divided by 8 -> "consumed" 3 bits at a time by adv
     * - iterates until rA % 8 == 0 -> no bits left
     * - to achieve 16 outputs, means that the input must have 48 bits
     * - on the order of 10^13 combinations -> intractable to brute force
     */
    @Answer("190615597431823")
    override fun partTwo(input: String): Long {
        val target = getTargetBits(input)

        data class Solution(val bits: Long, val mask: Long, val knownBits: Int) {
            override fun toString(): String {
                return """
                    Solution{
                        knownBits   = $knownBits
                        bits        = ${bitsToString(bits)}
                        mask        = ${bitsToString(mask)}
                    }
                """.trimIndent()
            }
        }

        val toCheck = Stack<Solution>()
        toCheck.push(Solution(0, 0, 0))

        val validSolutions = mutableListOf<Long>()

        while (toCheck.isNotEmpty()) {
            val solution = toCheck.pop()

            if (solution.knownBits == 48) {
                if ((validSolutions.minOrNull() ?: Long.MAX_VALUE) > solution.bits) {
                    validSolutions.add(solution.bits)
                }
                continue
            }
            // the next 3 output bits
            val output = (target shr (45 - solution.knownBits)) and 0b111L
            // take a guess at all possible input bits at the current position
            for (rightMost in 0b000..0b111) {
                val rightMostBits = rightMost.toLong() shl solution.knownBits
                val rightMostMask = 0b111L shl solution.knownBits

                val initialOverlap = solution.mask and rightMostMask
                if ((rightMostBits and initialOverlap) != (solution.bits and initialOverlap)) {
                    // incompatible guess for the rightMost bits
                    continue
                }

                // set new rightmost bits, given rightMost
                var newBits = solution.bits or rightMostBits
                var newMask = solution.mask or rightMostMask

                // calculate what B would be, given rightMost
                val b = (rightMost xor 0b101).toLong()

                // calculate what C would be, given the desired output and B
                val c = b xor output

                // calculate the offset into A taken to calculate C
                val offset = rightMost xor 0b010

                // calculate the bits based on C and the offset
                val offsetBits = c shl (offset + solution.knownBits)
                val offsetMask = 0b111L shl (offset + solution.knownBits)

                val offsetOverlap = newMask and offsetMask
                // if there is some overlap, make sure they are compatible
                if ((offsetBits and offsetOverlap) != (newBits and offsetOverlap)) {
                    // if they aren't compatible, ignore this potential rightMost
                    continue
                }

                newBits = newBits or offsetBits
                newMask = newMask or offsetMask

                toCheck.push(Solution(newBits, newMask, solution.knownBits + 3))
            }
        }

        return validSolutions.min()
    }

    private fun bitsToString(bits: Long): String {
        return bits.toString(2).padStart(48, '0').windowed(3, 3, false).joinToString(separator = "|")
    }

    private fun getTargetBits(input: String): Long {
        val ints = input.split("\n\n").last().split(": ").last().split(',').map { it.toLong() }
        var bits = 0L
        ints.forEach { bits = (bits shl 3) or it }
        return bits
    }
}