package dev.schaer.aoc.y2024

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import dev.schaer.aoc.datastructures.Array2D
import dev.schaer.aoc.datastructures.Vector2
import dev.schaer.aoc.datastructures.combineWith
import dev.schaer.aoc.datastructures.parseIntoArray2dOfChars
import dev.schaer.aoc.model.Direction

fun main() {
    Day12_GardenGroups().singleRun()
}

@Year(2024)
@Day(12)
class Day12_GardenGroups : Solution {

    @Answer("1486324")
    override fun partOne(input: String): Int {
        return calculateCost(input) { location, regions ->
            Direction.Cardinal.values().count { dir -> regions[location + dir.vec] != regions[location] }
        }
    }

    @Answer("898684")
    override fun partTwo(input: String): Int {
        return calculateCost(input) { location, regions -> countCorners(location, regions) }
    }

    private fun calculateCost(input: String, cost: (location: Vector2, regions: Array2D<Int>) -> Int): Int {
        val map = parseIntoArray2dOfChars(input, '.')
        val plots = mapToPlots(map, cost)

        val plotsByRegion = plots.groupingBy { it.regionId }

        val areaByRegion = plotsByRegion.eachCount()
        val costFactorByRegion = plotsByRegion.fold(0) { acc, it -> acc + it.costFactor }

        return areaByRegion.combineWith(costFactorByRegion) { area, costFactor -> area * costFactor }.values.sum()
    }

    private fun mapToPlots(map: Array2D<Char>, cost: (location: Vector2, regions: Array2D<Int>) -> Int): List<Plot> {
        var regionCounter = 0
        val regions = Array2D(Array(map.size) { -1 }, map.width, map.height, default = -1)
        for (y in 0 until map.height) {
            for (x in 0 until map.width) {
                if (regions[x, y] == -1) {
                    // we are on an "un-regioned" plot
                    exploreRegion(Vector2(x, y), map, regions, regionCounter++, map[x, y])
                }
            }
        }

        return calculatePlots(regions, cost)
    }

    private fun exploreRegion(
        location: Vector2, map: Array2D<Char>, regions: Array2D<Int>, regionId: Int, plotType: Char
    ) {
        if (regions[location] == regionId) {
            // already explored
            return
        }
        if (map[location] != plotType) {
            // not part of the region we're currently exploring
            return
        }

        // mark region
        regions[location] = regionId

        // explore all neighbours
        for (dir in Direction.Cardinal.values()) {
            exploreRegion(location + dir.vec, map, regions, regionId, plotType)
        }
    }

    private fun calculatePlots(
        regions: Array2D<Int>, cost: (location: Vector2, regions: Array2D<Int>) -> Int
    ): List<Plot> {
        return (0 until regions.size).map { idx ->
            val location = regions.unmapToVec(idx)
            val regionId = regions[location]
            Plot(regionId, cost(location, regions))
        }
    }

    private fun countCorners(location: Vector2, regions: Array2D<Int>): Int {
        var corners = 0
        val regionId = regions[location]
        // look at the four corners of the location
        for (cornerDir in Direction.Intercardinal.values()) {
            // look in the x, y, and diagonal directions
            val x = regions[location + cornerDir.vec.xComponent()]
            val y = regions[location + cornerDir.vec.yComponent()]
            val diagonal = regions[location + cornerDir.vec]
            if (y == regionId && x == regionId && diagonal != regionId) {
                // case: DOWNRIGHT
                // inward corners like  AA
                //                      AZ
                corners++
            } else if (y != regionId && x != regionId) {
                // case: DOWNRIGHT
                // outward corners like AX
                //                      Y*
                corners++
            }
        }
        return corners
    }

    data class Plot(val regionId: Int, val costFactor: Int)
}