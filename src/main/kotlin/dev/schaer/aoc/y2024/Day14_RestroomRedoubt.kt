package dev.schaer.aoc.y2024

import dev.schaer.aoc.algorithms.chineseRemainderTheorem
import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.InputProvider
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import dev.schaer.aoc.datastructures.Array2D
import dev.schaer.aoc.datastructures.Vector2
import java.util.regex.Pattern

fun main() {
    Day14_RestroomRedoubt().singleRun()

    val input = InputProvider.instance().getInput(2024, 14)
    Day14_RestroomRedoubt().drawEasterEgg(input)
}

private const val WIDTH = 101
private const val HEIGHT = 103
private const val ELAPSED_TIME = 100
private val PATTERN = Pattern.compile("^p=([-0-9]+),([-0-9]+) v=([-0-9]+),([-0-9]+)\$")

@Year(2024)
@Day(14)
class Day14_RestroomRedoubt(val width: Int = WIDTH, val height: Int = HEIGHT) : Solution {
    private val left = 0 until width / 2
    private val right = (width / 2 + 1) until width
    private val top = 0 until height / 2
    private val bottom = (height / 2 + 1) until height

    @Answer("217328832")
    override fun partOne(input: String): Int {
        val robots = parse(input)
        robots.forEach { it.step(ELAPSED_TIME) }
        return calculateSafetyFactor(robots)
    }

    private fun calculateSafetyFactor(robots: List<Robot>): Int {
        var topLeft = 0
        var topRight = 0
        var bottomLeft = 0
        var bottomRight = 0
        for (robot in robots) {
            when {
                robot.y in top && robot.x in left -> topLeft++
                robot.y in top && robot.x in right -> topRight++
                robot.y in bottom && robot.x in left -> bottomLeft++
                robot.y in bottom && robot.x in right -> bottomRight++
            }
        }
        return topLeft * topRight * bottomLeft * bottomRight
    }

    /*
    Originally solved this by
    1) looking at all rendered outputs
    2) noticing a repeating pattern of horizontal/vertical clustering
    3) only displayed the images with some sort of clustering
    4) found the Easter egg
    5) later came up with
     */
    @Answer("7412")
    override fun partTwo(input: String): Int {
        var robots = parse(input)
        var seconds = 0
        var firstVerticalClustering: Int? = null
        var firstHorizontalClustering: Int? = null

        // find pattern in vertical/horizontal clustering recurrence
        while (firstVerticalClustering == null || firstHorizontalClustering == null) {
            robots.forEach { it.step() }
            seconds++
            if (firstHorizontalClustering == null && hasHorizontalClustering(robots)) {
                firstHorizontalClustering = seconds
            }
            if (firstVerticalClustering == null && hasVerticalClustering(robots)) {
                firstVerticalClustering = seconds
            }
        }

        // find when both patterns overlap using CRT
        return chineseRemainderTheorem(firstHorizontalClustering, firstVerticalClustering, width, height)
    }

    fun drawEasterEgg(input: String) {
        // determine when the easter egg occurs
        val secondsAtWhichEasterEggOccurs = partTwo(input)
        val robots = parse(input)

        // simulate robots at that time
        robots.forEach { it.step(secondsAtWhichEasterEggOccurs) }

        // draw to stdout
        val display = Array2D<Char>(Array(width * height) { ' ' }, width, height, ' ')
        robots.forEach { display[Vector2(it.x, it.y)] = '⍟' }
        println(display.toString(separator = ""))
    }

    private fun hasVerticalClustering(robots: List<Robot>): Boolean {
        val totalRobots = robots.size.toDouble()
        val robotsInTopHalf = robots.count { it.y < height / 2 }
        val fraction = robotsInTopHalf / totalRobots
        return fraction >= 0.70
    }

    private fun hasHorizontalClustering(robots: List<Robot>): Boolean {
        val totalRobots = robots.size.toDouble()
        val robotsInLeftHalf = robots.count { it.x < width / 2 }
        val fraction = robotsInLeftHalf / totalRobots
        return fraction >= 0.70
    }

    private fun parse(input: String): List<Robot> {
        return input.lines().map { parseSingle(it) }
    }

    private fun parseSingle(input: String): Robot {
        val matcher = PATTERN.matcher(input)
        matcher.find()
        return Robot(
            x = matcher.group(1).toInt(),
            y = matcher.group(2).toInt(),
            velocity = Vector2(matcher.group(3).toInt(), matcher.group(4).toInt()),
            widthLimit = width,
            heightLimit = height
        )
    }

    private class Robot(var x: Int, var y: Int, val velocity: Vector2, val widthLimit: Int, val heightLimit: Int) {
        fun step(numSteps: Int = 1) {
            x = (x + velocity.x * numSteps) % widthLimit
            y = (y + velocity.y * numSteps) % heightLimit
            if (x < 0) {
                x += widthLimit
            }
            if (y < 0) {
                y += heightLimit
            }
        }
    }
}