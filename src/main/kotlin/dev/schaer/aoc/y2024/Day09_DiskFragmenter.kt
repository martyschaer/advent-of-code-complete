package dev.schaer.aoc.y2024

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer

fun main() {
    Day09_DiskFragmenter().singleRun()
}

private const val EMPTY = -1

@Year(2024)
@Day(9)
class Day09_DiskFragmenter : Solution {

    @Answer("6382875730645")
    override fun partOne(input: String): Long {
        val diskMap = input.map { it.digitToInt() }
        val expandedDiskMap = expandPrimitiveDiskRange(diskMap)
        val compacted = compactBlocks(expandedDiskMap)
        val checksum = calculateChecksum(compacted)
        return checksum
    }

    private fun expandPrimitiveDiskRange(diskmap: List<Int>): List<Int> {
        val iterator = diskmap.iterator()
        val result = mutableListOf<Int>()
        var fileId = 0
        while (iterator.hasNext()) {
            val fileSize = iterator.next()
            repeat(fileSize) {
                result.add(fileId)
            }
            fileId++
            if (iterator.hasNext()) {
                val gapSize = iterator.next()
                repeat(gapSize) {
                    result.add(EMPTY)
                }
            }
        }
        return result
    }

    private fun compactBlocks(original: List<Int>): List<Int> {
        val compactedSize = original.count { it != EMPTY }
        val fromTheEnd = original.reversed().filter { it != EMPTY }.iterator()
        val compacted = original.subList(0, compactedSize).toMutableList()
        var insertPointer = 0
        while (insertPointer < compacted.size) {
            while (insertPointer < compacted.size && compacted[insertPointer] != EMPTY) {
                insertPointer++
            }
            if (insertPointer >= compacted.size) {
                break
            }
            compacted[insertPointer] = fromTheEnd.next()
            insertPointer++
        }
        return compacted.toList()
    }

    @Answer("6420913943576")
    override fun partTwo(input: String): Long {
        val diskMap = input.map { it.digitToInt() }
        val compacted = compactFiles(diskMap)
        val checksum = calculateChecksum(compacted)
        return checksum
    }

    private fun compactFiles(diskMap: List<Int>): List<Int> {
        val ranges = diskMapToRanges(diskMap)

        // go through the files, from the back
        var filePtr = ranges.size - 1
        while (filePtr >= 0) {
            // find the next file
            if (ranges[filePtr] is Space) {
                filePtr--
                continue
            }

            // find the first free space with enough room, from the beginning
            var file = ranges[filePtr]
            var spacePtr = 0
            while (spacePtr < filePtr) {
                if (ranges[spacePtr] is Space && ranges[spacePtr].size >= file.size) {
                    val space = ranges[spacePtr]
                    ranges[filePtr] = Space(file.size) // replace the file with space
                    ranges[spacePtr] = file // replace the found space with the file
                    if (space.size > file.size) {
                        // if the file doesn't fill the space fully, keep track of the remaining space
                        ranges.add(spacePtr + 1, Space(space.size - file.size))
                    }
                    break
                }
                spacePtr++
            }

            // go on to the next file
            filePtr--
        }

        val normalized = normalizeRanges(ranges)

        return expandModeledDiskRange(normalized)
    }

    private fun expandModeledDiskRange(ranges: List<Range>): List<Int> {
        val result = mutableListOf<Int>()
        for (range in ranges) {
            if (range is File) {
                repeat(range.size) {
                    result.add(range.id)
                }
            } else {
                repeat(range.size) {
                    result.add(EMPTY)
                }
            }
        }
        return result
    }

    private fun normalizeRanges(ranges: List<Range>): MutableList<Range> {
        val normalized = mutableListOf<Range>()
        for (range in ranges) {
            if (range is File) {
                if (normalized.lastOrNull() is File) {
                    // ensure there is always a space between two files
                    normalized.add(Space(0))
                }
                normalized.add(range)
            } else {
                // compact consecutive spaces into a single space
                if (normalized.lastOrNull() is Space) {
                    normalized[normalized.lastIndex] = Space(normalized.last().size + range.size)
                } else {
                    normalized.add(range)
                }
            }
        }
        return normalized
    }

    private fun diskMapToRanges(original: List<Int>): MutableList<Range> {
        var fileIdCounter = 0
        val ranges = original.mapIndexed { index, size ->
            if (index % 2 == 0) {
                File(size, fileIdCounter++)
            } else {
                Space(size)
            }
        }.toMutableList()
        return ranges
    }

    private interface Range {
        val size: Int
    }

    private data class Space(override val size: Int) : Range
    private data class File(override val size: Int, val id: Int) : Range

    private fun calculateChecksum(diskmap: List<Int>): Long {
        return diskmap.map { if (it == EMPTY) 0 else it }.mapIndexed { index, value -> index * value.toLong() }.sum()
    }
}