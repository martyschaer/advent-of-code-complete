package dev.schaer.aoc.y2024

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import dev.schaer.aoc.datastructures.Array2D
import dev.schaer.aoc.datastructures.Vector2
import dev.schaer.aoc.datastructures.parseIntoArray2dOfChars
import dev.schaer.aoc.model.Direction
import dev.schaer.aoc.model.Direction.Relative.*

fun main() {
    Day15_WarehouseWoes().singleRun()
}

private const val WALL = '#'
private const val ROBOT = '@'
private const val BOX = 'O'
private const val AIR = '.'
private const val BOX_LEFT = '['
private const val BOX_RIGHT = ']'

@Year(2024)
@Day(15)
class Day15_WarehouseWoes : Solution {

    @Answer("1448589")
    override fun partOne(input: String): Int {
        val map = parseMap(input)
        val instructions = parseInstructions(input)

        var robotPos = map.unmapToVec(map.values.indexOfFirst { it == ROBOT })

        for (instruction in instructions) {
            robotPos = performInstruction(map, instruction, robotPos)
        }

        return calculateSumOfBoxGPSCoordinates(map)
    }

    private fun calculateSumOfBoxGPSCoordinates(map: Array2D<Char>): Int {
        return map.values.mapIndexed { index, value ->
            if (value == BOX) {
                val coords = map.unmapToVec(index)
                100 * coords.y + coords.x
            } else {
                0
            }
        }.sumOf { it }
    }

    private fun performInstruction(map: Array2D<Char>, instruction: Direction, lastPos: Vector2): Vector2 {
        val nextPos = lastPos + instruction.vec
        if (map[nextPos] == AIR) {
            return moveRobot(map, lastPos, nextPos)
        }
        if (map[nextPos] == WALL) {
            return lastPos
        }
        val boxesToMove = determineBoxesToMove(map, nextPos, instruction)
        if (boxesToMove == 0) {
            return lastPos
        }
        // since all boxes are interchangeable, simply add a box on the end
        // the robot movement will "eat" the box being pushed
        map[nextPos + instruction.vec * boxesToMove] = BOX
        return moveRobot(map, lastPos, nextPos)
    }

    private fun determineBoxesToMove(
        map: Array2D<Char>, pos: Vector2, direction: Direction, toMove: Int = 1
    ): Int {
        val nextPos = pos + direction.vec

        if (map[nextPos] == AIR) {
            return toMove
        }
        if (map[nextPos] == WALL) {
            return 0
        }

        return determineBoxesToMove(map, nextPos, direction, toMove + 1)
    }

    private fun parseMap(input: String): Array2D<Char> {
        return parseIntoArray2dOfChars(input.split("\n\n").first(), AIR)
    }

    private fun parseInstructions(input: String): Sequence<Direction> {
        return input.split("\n\n").last().asSequence().filter { it != '\n' }.map {
            when (it) {
                '^' -> UP
                'v' -> DOWN
                '<' -> LEFT
                '>' -> RIGHT
                else -> error("Unrecognizable instruction: $it")
            }
        }
    }

    @Answer("1472235")
    override fun partTwo(input: String): Int {
        val map = parseMap(makeWide(input))
        val instructions = parseInstructions(input)

        var robotPos = map.unmapToVec(map.values.indexOfFirst { it == ROBOT })

        for (instruction in instructions) {
            robotPos = performWideInstruction(map, instruction, robotPos)
        }

        return calculateSumOfWideBoxGPSCoordinates(map)
    }

    private fun performWideInstruction(map: Array2D<Char>, instruction: Direction, lastPos: Vector2): Vector2 {
        val nextPos = lastPos + instruction.vec
        if (map[nextPos] == AIR) {
            return moveRobot(map, lastPos, nextPos)
        }
        if (map[nextPos] == WALL) {
            return lastPos
        }

        if (instruction.horizontal) {
            if (moveBoxHorizontal(map, nextPos, instruction)) {
                return moveRobot(map, lastPos, nextPos)
            }
        } else {
            if (moveBoxVertical(map, nextPos, instruction)) {
                return moveRobot(map, lastPos, nextPos)
            }
        }

        return lastPos
    }

    private fun moveRobot(map: Array2D<Char>, pos: Vector2, nextPos: Vector2): Vector2 {
        map[pos] = AIR
        map[nextPos] = ROBOT
        return nextPos
    }

    /**
     * Recursively goes through all boxes ahead of the current position in the specified direction.
     * Move the box-half if we encounter free air at the other end, otherwise back-propagate
     * a failure to move and move none of them.
     */
    private fun moveBoxHorizontal(map: Array2D<Char>, pos: Vector2, direction: Direction): Boolean {
        val nextPos = pos + direction.vec
        if (map[nextPos] == WALL) {
            return false
        }
        if (map[nextPos] == BOX_LEFT || map[nextPos] == BOX_RIGHT) {
            if (!moveBoxHorizontal(map, nextPos, direction)) {
                return false
            }
            map[nextPos] = map[pos]
            map[pos] = AIR
            return true
        }
        map[nextPos] = map[pos]
        map[pos] = AIR
        return true
    }

    private fun moveBoxVertical(map: Array2D<Char>, pos: Vector2, direction: Direction): Boolean {
        // find the affected box-half-positions
        val boxPositions = mutableSetOf<Vector2>()
        val movable = gatherVerticallyMovableBoxes(map, pos, direction, boxPositions)
        // if any one of them isn't movable, stop
        if (!movable) {
            return false
        }

        // grab the symbol currently there
        val boxes = boxPositions.associateWith { map[it] }
        // clear the space
        boxPositions.forEach { map[it] = AIR }
        // re-put the symbol in the new position
        boxes.forEach { position, symbol -> map[position + direction.vec] = symbol }

        return true
    }

    /**
     * Find all box-halves that would be affected by a vertical move,
     * if at any point any box-half is unmovable, discard the whole thing
     * and return false.
     */
    private fun gatherVerticallyMovableBoxes(
        map: Array2D<Char>, pos: Vector2, direction: Direction, boxes: MutableSet<Vector2>
    ): Boolean {
        if (pos in boxes) {
            return true
        }
        if (map[pos] == AIR) {
            return true
        }
        if (map[pos] == WALL) {
            return false
        }

        boxes.add(pos)

        val partnerHalfPos = if (map[pos] == BOX_LEFT) {
            pos + RIGHT.vec
        } else if (map[pos] == BOX_RIGHT) {
            pos + LEFT.vec
        } else {
            error("!?")
        }

        boxes.add(partnerHalfPos)

        return gatherVerticallyMovableBoxes(map, partnerHalfPos + direction.vec, direction, boxes) //
                && gatherVerticallyMovableBoxes(map, pos + direction.vec, direction, boxes)
    }

    private fun calculateSumOfWideBoxGPSCoordinates(map: Array2D<Char>): Int {
        return map.values.mapIndexed { index, value ->
            if (value == BOX_LEFT) {
                val coords = map.unmapToVec(index)
                100 * coords.y + coords.x
            } else {
                0
            }
        }.sumOf { it }
    }

    private fun makeWide(input: String): String {
        return input.replace("#", "##").replace("O", "[]").replace(".", "..").replace("@", "@.")
    }
}