package dev.schaer.aoc.y2024

import dev.schaer.aoc.algorithms.digits
import dev.schaer.aoc.algorithms.memoize
import dev.schaer.aoc.algorithms.pow
import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer

fun main() {
    Day11_PlutonianPebbles().singleRun()
}

@Year(2024)
@Day(11)
class Day11_PlutonianPebbles : Solution {

    @Answer("199986")
    override fun partOne(input: String): Long {
        val stones = parse(input)
        val memoized = memoize(::blink)
        return stones.sumOf { memoized(it, 25) }
    }

    @Answer("236804088748754")
    override fun partTwo(input: String): Long {
        val stones = parse(input)
        val memoized = memoize(::blink)
        return stones.sumOf { memoized(it, 75) }
    }

    private fun parse(input: String): List<Long> {
        return input.split(' ').map { it.toLong() }
    }

    private fun blink(stone: Long, remainingBlinks: Int, memoized: (Long, Int) -> Long): Long {
        if (remainingBlinks <= 0) {
            return 1
        }
        if (stone == 0L) {
            return memoized(1L, remainingBlinks - 1)
        }
        val digits = stone.digits()
        if (digits % 2 == 0) {
            val x = 10.pow(digits / 2)
            val a = stone / x
            val b = stone % x
            return memoized(a, remainingBlinks - 1) + memoized(b, remainingBlinks - 1)
        }
        return memoized(stone * 2024, remainingBlinks - 1)
    }
}