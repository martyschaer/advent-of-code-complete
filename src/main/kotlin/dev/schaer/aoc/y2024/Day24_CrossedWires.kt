package dev.schaer.aoc.y2024

import dev.schaer.aoc.algorithms.memoize
import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import kotlin.collections.contains
import kotlin.math.min

fun main() {
    Day24_CrossedWires().singleRun()
}

@Year(2024)
@Day(24)
class Day24_CrossedWires : Solution {

    @Answer("55730288838374")
    override fun partOne(input: String): Long {
        val gates = parseGates(input)
        val initialValues = parseInitial(input)
        return calculate(gates, initialValues)
    }

    private fun calculate(gates: Map<String, Gate>, initialValues: Map<String, Boolean>): Long {
        val outputs = gates.keys.filter { it.startsWith('z') }

        fun eval(wire: String, memoized: (String) -> Boolean): Boolean {
            if (wire in initialValues) return initialValues[wire]!!
            val gate = gates[wire]!!
            return gate.operation.fn(memoized(gate.a), memoized(gate.b))
        }

        val memoized = memoize(::eval)

        var result = 0L
        outputs.sorted().reversed().forEach { outWire ->
            result = result shl 1
            if (memoized(outWire)) {
                result = result or 1
            }
        }
        return result
    }

    private fun parseInitial(input: String): Map<String, Boolean> {
        return input.split("\n\n").first().lines().associate { line ->
            val (name, valueStr) = line.split(": ")
            val value = valueStr == "1"
            name to value
        }
    }

    private fun parseGates(input: String): Map<String, Gate> {
        return input.split("\n\n").last().lines().associate { parseGate(it) }
    }

    private fun parseGate(line: String): Pair<String, Gate> {
        val (a, opStr, b, _, dst) = line.split(' ')
        val operation = Operation.valueOf(opStr)
        return dst to Gate(a, b, operation)
    }

    private data class Gate(val a: String, val b: String, val operation: Operation)

    private enum class Operation(val fn: (Boolean, Boolean) -> Boolean) {
        AND({ a, b -> a and b }), OR({ a, b -> a or b }), XOR({ a, b -> a xor b });
    }

    /**
     * Not sure if this solution works for all inputs
     * or just mine. The heuristics seem sensible enough,
     * but they might break for other inputs.
     */
    @Answer("fvw,grf,mdb,nwq,wpq,z18,z22,z36")
    override fun partTwo(input: String): String {
        val gates = parseGates(input)

        /**
         * Calculate the wires/gates reachable from the given node up to a depth of 3
         */
        fun getPaths(wire: String, path: String = "", depth: Int = 0): List<String> {
            if (depth >= 3) {
                if (wire in gates) {
                    val gate = gates[wire]!!
                    return listOf("$path.$wire.${gate.operation.name}")
                }

                return listOf("$path.$wire")
            }
            if (wire !in gates) {
                return listOf("$path.$wire")
            }
            val gate = gates[wire]!!
            return getPaths(gate.a, "$path.$wire.${gate.operation.name}", depth + 1) + //
                    getPaths(gate.b, "$path.$wire.${gate.operation.name}", depth + 1)
        }

        val result = mutableListOf<String>()

        for (z in 3 until 45) {
            val zN = "z%02d".format(z)
            val xN = "x%02d".format(z)
            val yN = "y%02d".format(z)
            val xNSub1 = "x%02d".format(z - 1)
            val yNSub1 = "y%02d".format(z - 1)

            val expectedPaths = listOf(
                Regex("""\.$zN.XOR\.(\w{3})\.XOR\.$xN"""),
                Regex("""\.$zN.XOR\.(\w{3})\.XOR\.$yN"""),
                Regex("""\.$zN.XOR\.(\w{3})\.OR\.(\w{3})\.AND\.$xNSub1"""),
                Regex("""\.$zN.XOR\.(\w{3})\.OR\.(\w{3})\.AND\.$yNSub1"""),
                Regex("""\.$zN.XOR\.(\w{3})\.OR\.(\w{3})\.AND\.(\w{3})\.OR"""),
                Regex("""\.$zN.XOR\.(\w{3})\.OR\.(\w{3})\.AND\.(\w{3})\.XOR""")
            )

            val wrong = mutableListOf<String>()
            var goodPaths = 0
            var ignoredPaths = 0

            // attempt to match the given paths against the expected paths
            for (path in getPaths(zN)) {
                if (expectedPaths.none { expectedPath -> path.matches(expectedPath) }) {
                    // if it didn't match any expected paths
                    if (path.split('.').none { it in result }) {
                        // and doesn't involve a known bad-wire
                        wrong.add(path)
                    } else {
                        // if it involves a known-bad wire, ignore it
                        ignoredPaths++
                    }
                } else {
                    goodPaths++
                }
            }

            if (wrong.isEmpty()) {
                continue
            }

            if (goodPaths + wrong.size + ignoredPaths != 6 || (goodPaths == 0)) {
                // if we can't match all 6 paths, or if there are no good paths
                // we assume that the final output is connected wrong
                result.add(zN)
                continue
            }

            // find out what all wrong paths have in common
            val commonWrongSubstring = wrong.foldRight(wrong.first()) { string, acc -> commonPrefix(string, acc) }
            // take the last wire from the common wrong prefix
            val lastWrongWire = commonWrongSubstring.split('.').filter { it.isNotEmpty() }.dropLast(1).last()
            result.add(lastWrongWire)
        }

        return result.sorted().joinToString(",")
    }

    private fun commonPrefix(a: String, b: String): String {
        val result = StringBuilder()
        for (i in 0 until min(a.length, b.length)) {
            if (a[i] == b[i]) {
                result.append(a[i])
            }
        }
        return result.toString()
    }
}