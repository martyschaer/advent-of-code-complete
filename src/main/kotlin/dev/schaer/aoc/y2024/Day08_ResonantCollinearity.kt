package dev.schaer.aoc.y2024

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import dev.schaer.aoc.datastructures.Array2D
import dev.schaer.aoc.datastructures.Vector2
import dev.schaer.aoc.datastructures.parseIntoArray2dOfChars

fun main() {
    Day08_ResonantCollinearity().singleRun()
}

private const val EMPTY = '.'

@Year(2024)
@Day(8)
class Day08_ResonantCollinearity : Solution {

    @Answer("261")
    override fun partOne(input: String): Int {
        val map = parseIntoArray2dOfChars(input, EMPTY)
        val antennae = findAntennae(map)
        val allAntiNodes = antennae.values
            .flatMap { findSimplifiedAntiNodes(it) }
            .filter { map.isInBounds(it) }
            .toSet()
        return allAntiNodes.size
    }

    private fun findSimplifiedAntiNodes(antennae: List<Vector2>): Set<Vector2> {
        val antiNodes = mutableSetOf<Vector2>()
        for (a in antennae) {
            for (b in antennae) {
                if (a == b) {
                    continue
                }
                // add the point in-line with both A and B, but twice as far away from A as from B
                antiNodes.add(b + (b - a))
            }
        }
        return antiNodes
    }

    @Answer("898")
    override fun partTwo(input: String): Int {
        val map = parseIntoArray2dOfChars(input, EMPTY)
        val antennae = findAntennae(map)
        val allAntiNodes = antennae.values
            .flatMap { findAntiNodes(map, it) }
            .toSet()
        return allAntiNodes.size
    }

    private fun findAntiNodes(map: Array2D<Char>, antennae: List<Vector2>): Set<Vector2> {
        val antiNodes = mutableSetOf<Vector2>()
        for (a in antennae) {
            for (b in antennae) {
                if (a == b) {
                    continue
                }
                val ab = b - a
                var antiNode = b
                while (map.isInBounds(antiNode)) {
                    antiNodes.add(antiNode)
                    antiNode = antiNode + ab
                }
            }
        }
        return antiNodes
    }

    private fun findAntennae(map: Array2D<Char>): Map<Char, List<Vector2>> {
        val antennae = mutableMapOf<Char, MutableList<Vector2>>()
        for (y in 0 until map.height) {
            for (x in 0 until map.width) {
                val frequency = map[x, y]
                if (frequency == EMPTY) {
                    continue
                }
                antennae.getOrPut(frequency) { mutableListOf() }.add(Vector2(x, y))
            }
        }
        return antennae
    }
}