package dev.schaer.aoc.y2024

import dev.schaer.aoc.algorithms.cache
import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer

fun main() {
    Day23_LANParty().singleRun()
}

@Year(2024)
@Day(23)
class Day23_LANParty : Solution {

    @Answer("1337")
    override fun partOne(input: String): Int {
        val connections = parse(input)
        val nodes = connections.keys.toTypedArray()
        val adjacency = createAdjacencyMatrix(nodes, connections)
        var tCliques = 0
        // find all 3-cliques that contain a node that starts with 't'
        for (a in 0 until nodes.size) {
            for (b in a + 1 until nodes.size) {
                for (c in b + 1 until nodes.size) {
                    if (adjacency[a][b] && adjacency[a][c] && adjacency[b][c]) {
                        if (nodes[a].startsWith('t') || nodes[b].startsWith('t') || nodes[c].startsWith('t')) {
                            tCliques++
                        }
                    }
                }
            }
        }
        return tCliques
    }

    @Answer("aw,fk,gv,hi,hp,ip,jy,kc,lk,og,pj,re,sr")
    override fun partTwo(input: String): String {
        val connections = parse(input)
        val nodes = connections.keys.toTypedArray()
        val adjacency = createAdjacencyMatrix(nodes, connections)

        fun neighbours(vIdx: Int): Set<Int> {
            return adjacency[vIdx].mapIndexedNotNull { idx, it -> if (it) idx else null }.toSet()
        }

        val N = cache(::neighbours)

        fun singletonSet(v: Int): Set<Int> {
            return setOf(v)
        }

        val set = cache(::singletonSet)

        /**
         * Since all nodes in the input have degree 13,
         * the choice of pivot doesn't really matter.
         */
        fun pivot(vertices: Set<Int>): Int {
            return vertices.random()
        }

        val cliques = mutableListOf<Set<Int>>()

        /**
         * https://en.wikipedia.org/wiki/Bron%E2%80%93Kerbosch_algorithm
         */
        fun bronKerbosch(R: Set<Int>, p: Set<Int>, x: Set<Int>) {
            var P = p
            var X = x
            if (P.isEmpty() && X.isEmpty()) {
                cliques.add(R)
            }
            if (P.isEmpty()) return
            val u = pivot(P union X)
            for (v in P subtract N(u)) {
                bronKerbosch(R union set(v), P intersect N(v), X intersect N(v))
                P = P subtract set(v)
                X = X union set(v)
            }
        }

        bronKerbosch(emptySet(), nodes.indices.toSet(), emptySet())

        return cliques.maxBy { it.size }.map { nodes[it] }.sorted().joinToString(",")
    }

    private fun createAdjacencyMatrix(
        nodes: Array<String>, connections: Map<String, List<String>>
    ): Array<Array<Boolean>> {
        val matrix = Array(nodes.size) { x ->
            Array(nodes.size) { y ->
                val a = nodes[x]
                val b = nodes[y]
                connections[a]?.contains(b) == true
            }
        }
        return matrix
    }

    private fun parse(input: String): Map<String, List<String>> {
        val connections = mutableMapOf<String, MutableList<String>>()

        input.lineSequence().forEach { line ->
            val (a, b) = line.split('-')
            connections.getOrPut(a) { mutableListOf() }.add(b)
            connections.getOrPut(b) { mutableListOf() }.add(a)
        }

        return connections
    }
}