package dev.schaer.aoc.y2024

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import kotlin.math.abs
import kotlin.math.sign

fun main() {
    Day02_RedNosedReports().singleRun()
}

private const val MIN_DELTA = 1
private const val MAX_DELTA = 3

@Year(2024)
@Day(2)
class Day02_RedNosedReports : Solution {

    @Answer("246")
    override fun partOne(input: String): Int {
        return parse(input).count { isSafe(it) }
    }

    /**
     * A report is safe, given it follows these two rules:
     * - strictly in- / decreasing
     * - gradual: any adjacent levels differ by:
     *  - at least 1
     *  - at most 3
     */
    private fun isSafe(report: List<Int>): Boolean {
        val reportSign = (report[0] - report[1]).sign
        for (i in 1 until report.size) {
            val diff = report[i - 1] - report[i]
            if (reportSign != diff.sign) {
                return false
            }
            val delta = abs(diff)
            if (delta < MIN_DELTA || delta > MAX_DELTA) {
                return false
            }
        }
        return true
    }

    @Answer("318")
    override fun partTwo(input: String): Int {
        return parse(input).count { isSafeWithProblemDampener(it) }
    }

    /**
     * A report is safe, given it follows these two rules:
     * - strictly in- / decreasing
     * - gradual: any adjacent levels differ by:
     *  - at least 1
     *  - at most 3
     *
     * The Problem Dampener allows us to tolerate a single unsafe level
     */
    private fun isSafeWithProblemDampener(report: List<Int>): Boolean {
        if (isSafe(report)) {
            return true
        }

        (report.indices).forEach { unsafeCandidate ->
            val withoutUnsafeCandidate = report.toMutableList()
            withoutUnsafeCandidate.removeAt(unsafeCandidate)
            if (isSafe(withoutUnsafeCandidate)) {
               return true
            }
        }

        return false
    }

    private fun parse(input: String): List<List<Int>> {
        return input.lines().filter { it.isNotBlank() }.map { line ->
            line.split(" ").map { it.toInt() }
        }
    }
}