package dev.schaer.aoc.y2024

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import dev.schaer.aoc.datastructures.Array2D
import dev.schaer.aoc.datastructures.Vector2
import dev.schaer.aoc.datastructures.parseIntoArray2dOfDigits
import dev.schaer.aoc.model.Direction

fun main() {
    Day10_HoofIt().singleRun()
}

@Year(2024)
@Day(10)
class Day10_HoofIt : Solution {

    @Answer("489")
    override fun partOne(input: String): Int {
        val topo = parseIntoArray2dOfDigits(input)
        val trailheads = findTrailHeads(topo)
        return trailheads.sumOf { countDistinctReachableTrailEnds(topo, it) }
    }

    private fun countDistinctReachableTrailEnds(topo: Array2D<Int>, trailhead: Vector2): Int {
        return findDistinctReachableTrailEnds(topo, trailhead).size
    }

    private fun findDistinctReachableTrailEnds(topo: Array2D<Int>, position: Vector2): Set<Vector2> {
        val foundTrailHeads = mutableSetOf<Vector2>()
        val currentHeight = topo[position]

        if (currentHeight == 9) {
            return setOf(position)
        }

        val nextHeight = currentHeight + 1
        for (direction in Direction.Cardinal.values()) {
            val nextPosition = position + direction.vec
            if (topo[nextPosition] == nextHeight) {
                foundTrailHeads += findDistinctReachableTrailEnds(topo, nextPosition)
            }
        }

        return foundTrailHeads
    }

    @Answer("1086")
    override fun partTwo(input: String): Int {
        val topo = parseIntoArray2dOfDigits(input)
        val trailheads = findTrailHeads(topo)
        return trailheads.sumOf { countDistinctTrails(topo, it) }
    }

    private fun countDistinctTrails(topo: Array2D<Int>, position: Vector2): Int {
        var foundTrailHeads = 0
        val currentHeight = topo[position]

        if (currentHeight == 9) {
            return 1
        }

        val nextHeight = currentHeight + 1
        for (direction in Direction.Cardinal.values()) {
            val nextPosition = position + direction.vec
            if (topo[nextPosition] == nextHeight) {
                foundTrailHeads += countDistinctTrails(topo, nextPosition)
            }
        }

        return foundTrailHeads
    }

    private fun findTrailHeads(topo: Array2D<Int>): List<Vector2> {
        return topo.values.mapIndexed { index, value -> if (value == 0) index else null }.filterNotNull()
            .map { topo.unmapToVec(it) }
    }
}