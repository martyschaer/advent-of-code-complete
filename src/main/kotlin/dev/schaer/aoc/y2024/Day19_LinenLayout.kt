package dev.schaer.aoc.y2024

import dev.schaer.aoc.algorithms.memoize
import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer

fun main() {
    Day19_LinenLayout().singleRun()
}

@Year(2024)
@Day(19)
class Day19_LinenLayout : Solution {

    @Answer("342")
    override fun partOne(input: String): Int {
        val towels = parseAvailableTowels(input)
        val designs = parseDesiredDesigns(input)

        fun isPossible(design: String): Boolean {
            if (design.isEmpty()) {
                return true
            }
            for (towel in towels) {
                if (design.startsWith(towel)) {
                    if (isPossible(design.substring(towel.length))) {
                        return true
                    }
                }
            }
            return false
        }

        return designs.count { isPossible(it) }
    }

    private fun parseAvailableTowels(input: String): List<String> {
        return input.split("\n\n").first().split(',').map { it.trim() }
    }

    private fun parseDesiredDesigns(input: String): List<String> {
        return input.split("\n\n").last().lines()
    }

    @Answer("891192814474630")
    override fun partTwo(input: String): Long {
        val towels = parseAvailableTowels(input).map { it.toCharArray() }
        val designs = parseDesiredDesigns(input).map { it.toCharArray() }

        fun CharArray.matchAt(offset: Int, toMatch: CharArray): Boolean {
            if (size < offset + toMatch.size) return false
            for (idx in toMatch.indices) {
                if (this[offset + idx] != toMatch[idx]) {
                    return false
                }
            }
            return true
        }

        fun countPossibleArrangements(design: CharArray, matchAt: Int, memoized: (CharArray, Int) -> Long): Long {
            if (matchAt >= design.size) {
                return 1
            }
            var possibleDesigns = 0L
            for (towel in towels) {
                if (design.matchAt(matchAt, towel)) {
                    possibleDesigns += memoized(design, matchAt + towel.size)
                }
            }
            return possibleDesigns
        }
        val memoized = memoize(::countPossibleArrangements)
        return designs.sumOf { memoized(it, 0) }
    }
}