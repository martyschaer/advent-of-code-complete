package dev.schaer.aoc.y2024

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import java.util.regex.Pattern
import kotlin.math.abs
import kotlin.math.sign

fun main() {
    Day03_MullItOver().singleRun()
}

@Year(2024)
@Day(3)
class Day03_MullItOver : Solution {

    @Answer("170068701")
    override fun partOne(input: String): Long {
        val pattern = Pattern.compile("mul\\(\\d{1,3},\\d{1,3}\\)")
        val matcher = pattern.matcher(input)
        var sum = 0L
        while (matcher.find()) {
            sum += runInstruction(matcher.group())
        }
        return sum
    }

    @Answer("78683433")
    override fun partTwo(input: String): Long {
        val pattern = Pattern.compile("(mul\\(\\d{1,3},\\d{1,3}\\)|do\\(\\)|don't\\(\\))")
        val matcher = pattern.matcher(input)
        var sum = 0L
        var enabled = true
        while (matcher.find()) {
            val match = matcher.group()
            when (match) {
                "don't()" -> enabled = false
                "do()" -> enabled = true
                else -> sum += if (enabled) runInstruction(match) else 0
            }
        }
        return sum
    }

    private fun runInstruction(instruction: String): Long {
        val (x, y) = instruction.drop(4).dropLast(1).split(',').map { it.toLong() }
        return x * y
    }
}