package dev.schaer.aoc.y2024

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import dev.schaer.aoc.datastructures.Array2D
import dev.schaer.aoc.datastructures.Vector2
import dev.schaer.aoc.datastructures.parseIntoArray2dOfChars
import dev.schaer.aoc.model.Direction
import dev.schaer.aoc.model.Direction.Cardinal.NORTH
import kotlin.experimental.and
import kotlin.experimental.or

fun main() {
    Day06_GuardGallivant().singleRun()
}

private const val SPACE = '.'
private const val OBSTACLE = '#'
private const val GUARD_START = '^'
private const val ZERO = 0.toByte()

@Year(2024)
@Day(6)
class Day06_GuardGallivant : Solution {

    @Answer("5239")
    override fun partOne(input: String): Int {
        val map = parseIntoArray2dOfChars(input, '.')
        val visited = simulateGuardMovement(map)
        return visited?.size ?: error("Unable to determine visited locations")
    }

    @Answer("1753")
    override fun partTwo(input: String): Int {
        val map = parseIntoArray2dOfChars(input, '.')
        val startingPosition = findGuardStartingPosition(map.clone())
        val visited = simulateGuardMovement(map.clone()) ?: error("Unable to determine visited locations")

        var numObstaclesThatCauseCycles = 0

        for (potentialObstaclePlacement in visited) {
            if (potentialObstaclePlacement == startingPosition) {
                continue
            }

            map[potentialObstaclePlacement] = OBSTACLE
            if (simulateGuardMovement(map, startingPosition, true) == null) {
                numObstaclesThatCauseCycles++
            }
            map[potentialObstaclePlacement] = SPACE
        }

        return numObstaclesThatCauseCycles
    }

    private fun simulateGuardMovement(map: Array2D<Char>): List<Vector2>? {
        return simulateGuardMovement(map, findGuardStartingPosition(map))
    }

    private fun simulateGuardMovement(
        map: Array2D<Char>,
        startingPosition: Vector2,
        detectLoopOnly: Boolean = false
    ): List<Vector2>? {
        var guardPosition = startingPosition
        var guardDirection: Direction = NORTH
        val visited = ByteArray(map.size)

        while (map.isInBounds(guardPosition)) {
            val guardPositionIdx = map.map(guardPosition)
            val visitedSet = visited[guardPositionIdx]
            if (visitedSet and guardDirection.binaryOrdinal != ZERO) {
                // loop!
                return null
            }
            visited[guardPositionIdx] = visitedSet or guardDirection.binaryOrdinal

            val nextGuardPosition = guardPosition + guardDirection.vec
            if (map[nextGuardPosition] == OBSTACLE) {
                guardDirection = guardDirection.right
            } else {
                guardPosition = nextGuardPosition
            }
        }

        if (detectLoopOnly) {
            return emptyList()
        }

        return visited.mapIndexed { idx, value -> if (value != ZERO) map.unmapToVec(idx) else null }.filterNotNull()
    }

    private fun findGuardStartingPosition(map: Array2D<Char>): Vector2 {
        return map.unmapToVec(map.values.indexOf(GUARD_START))
    }
}