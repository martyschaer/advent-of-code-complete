package dev.schaer.aoc.y2024

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import dev.schaer.aoc.datastructures.Array2D
import dev.schaer.aoc.datastructures.Vector2
import dev.schaer.aoc.datastructures.parseIntoArray2dOfChars
import dev.schaer.aoc.model.Direction
import java.util.concurrent.atomic.AtomicInteger
import kotlin.math.ceil
import kotlin.math.log2

fun main() {
    Day20_RaceCondition().singleRun()
}

private const val WALL = '#'

@Year(2024)
@Day(20)
class Day20_RaceCondition(val threshold: Int = 100) : Solution {

    @Answer("1393")
    override fun partOne(input: String): Int {
        val (nodes, path) = parse(input)

        val cheatSkipDistance = 2
        val cheatsThatSaveMoreThanThreshold = AtomicInteger(0)

        nodes.parallelStream().forEach { node ->
            val localStep = path[node]
            for (dir in Direction.Cardinal.values()) {
                val potentialNode = node + (dir.vec * cheatSkipDistance)
                val potentialStep = path[potentialNode]
                if (potentialStep < 0) continue
                val timeSaved = (potentialStep - localStep) - cheatSkipDistance
                if (timeSaved >= threshold) {
                    cheatsThatSaveMoreThanThreshold.addAndGet(1)
                }
            }
        }

        return cheatsThatSaveMoreThanThreshold.get()
    }

    @Answer("990096")
    override fun partTwo(input: String): Int {
        Object()
        val parsed = parse(input)
        val (nodes, path) = parsed
        val reachable = Array(19) { calculateReachable(it + 2) }

        val bitsPerVector = ceil(log2(path.size.toFloat())).toInt()
        val cheatsOverThreshold = BooleanArray(1 shl (bitsPerVector * 2))

        nodes.parallelStream().forEach { node ->
            val localStep = path[node]
            for (cheatSkipDistance in 2..20) {
                for (offset in reachable[cheatSkipDistance - 2]) {
                    val potentialNode = node + offset
                    val potentialStep = path[potentialNode]
                    if (potentialStep < 0) continue
                    val timeSaved = (potentialStep - localStep) - cheatSkipDistance
                    if (timeSaved >= threshold) {
                        val key = (path.map(node) shl bitsPerVector) or (path.map(potentialNode))
                        cheatsOverThreshold[key] = true
                    }
                }
            }
        }

        return cheatsOverThreshold.count { it }
    }

    private fun calculateReachable(steps: Int): List<Vector2> {
        val reachable = mutableSetOf<Vector2>()
        for (a in steps downTo 1) {
            val b = steps - a
            for (dir in Direction.Cardinal.values()) {
                reachable.add(dir.vec * a + dir.cw.vec * b)
            }
        }
        return reachable.toList()
    }

    private fun parse(input: String): Pair<List<Vector2>, Array2D<Int>> {
        val map = parseIntoArray2dOfChars(input, WALL)
        val path = Array2D(Array(map.size) { -1 }, map.width, map.height, -1) {
            if (it == -1) " #" else "%02d".format(it)
        }
        val pathNodes = mutableListOf<Vector2>()
        val start = map.pos('S')!!
        val end = map.pos('E')!!
        var counter = 0
        var pos = start
        while (pos != end) {
            path[pos] = counter++
            pathNodes.add(pos)
            val next = Direction.Cardinal.values() //
                .map { dir -> pos + dir.vec } //
                .first { next -> map[next] != WALL && path[next] < 0 }
            pos = next
        }
        path[end] = counter
        pathNodes.add(end)
        return pathNodes to path
    }
}