package dev.schaer.aoc.y2024

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer

fun main() {
    Day25_CodeChronicle().singleRun()
}

private const val SPACE = 5
private const val NUM_PINS = 5
private const val EMPTY = '.'
private const val PIN = '#'

@Year(2024)
@Day(25)
class Day25_CodeChronicle : Solution {

    @Answer("3508")
    override fun partOne(input: String): Int {
        val (keys, locks) = parse(input)

        var fit = 0

        for (key in keys) {
            for (lock in locks) {
                if (lock and key == 0) {
                    fit++
                }
            }
        }

        return fit
    }

    private fun parse(input: String): Pair<List<Int>, List<Int>> {
        val keys = mutableListOf<Int>()
        val locks = mutableListOf<Int>()

        input.split("\n\n") //
            .forEach { block ->
                var combo = 0
                block.lineSequence().drop(1).take(SPACE).forEach { line ->
                    for (i in 0 until NUM_PINS) {
                        combo = (combo shl 1) or if (line[i] == PIN) 1 else 0
                    }
                }
                if (isKey(block)) {
                    keys.add(combo)
                } else {
                    locks.add(combo)
                }
            }
        return keys to locks
    }

    private fun isKey(block: String): Boolean {
        return block.lineSequence().first().all { it == EMPTY }
    }

    @Answer("0")
    override fun partTwo(input: String): Int {
        // no part 2 for day 25
        return 0
    }
}