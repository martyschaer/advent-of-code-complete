package dev.schaer.aoc.y2024

import dev.schaer.aoc.algorithms.digits
import dev.schaer.aoc.algorithms.pow
import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import dev.schaer.aoc.datastructures.head
import dev.schaer.aoc.datastructures.tail
import dev.schaer.aoc.y2024.Day07_BridgeRepair.Operator.*

fun main() {
    Day07_BridgeRepair().singleRun()
}

@Year(2024)
@Day(7)
class Day07_BridgeRepair : Solution {

    @Answer("3119088655389")
    override fun partOne(input: String): Long {
        val equations = input.lines().map { parse(it, part2 = false) }
        return equations.filter { it.isValid() }.sumOf { it.expectedResult }
    }

    @Answer("264184041398847")
    override fun partTwo(input: String): Long {
        val equations = input.lines().map { parse(it, part2 = true) }
        return equations.filter { it.isValid() }.sumOf { it.expectedResult }
    }

    private fun parse(line: String, part2: Boolean): Equation {
        val tokens = line.split(" ")
        val result = tokens.head().dropLast(1).toLong()
        val operands = tokens.tail().map { it.toLong() }
        return Equation(result, operands, part2)
    }

    private class Equation(
        val expectedResult: Long, private val operands: List<Long>, private val part2: Boolean = false
    ) {
        fun isValid(lhs: Long = 0, operator: Operator = ADD, pointer: Int = 0): Boolean {
            if (lhs > expectedResult) {
                return false
            }
            if (pointer >= operands.size) {
                return lhs == expectedResult
            }

            val rhs = operands[pointer]

            val nextResult = when (operator) {
                ADD -> lhs + rhs
                MUL -> lhs * rhs
                CON -> when {
                    rhs < 10 -> lhs * 10 + rhs
                    rhs < 100 -> lhs * 100 + rhs
                    rhs < 1000 -> lhs * 1000 + rhs
                    else -> lhs * 10.pow(rhs.digits()) + rhs
                }
            }

            val nextPointer = pointer + 1

            return isValid(nextResult, ADD, nextPointer) //
                    || isValid(nextResult, MUL, nextPointer) //
                    || (part2 && isValid(nextResult, CON, nextPointer))
        }
    }

    private enum class Operator {
        ADD, MUL, CON
    }
}