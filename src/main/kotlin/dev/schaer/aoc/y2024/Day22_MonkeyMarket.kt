package dev.schaer.aoc.y2024

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer

fun main() {
    Day22_MonkeyMarket().singleRun()
}

@Year(2024)
@Day(22)
class Day22_MonkeyMarket : Solution {

    @Answer("13022553808")
    override fun partOne(input: String): Long {
        val seeds = input.lines().map { it.toInt() }
        return seeds.sumOf { seed ->
            prng(seed).take(2000).last().toLong()
        }
    }

    @Answer("1555")
    override fun partTwo(input: String): Int {
        val sumByDelta = IntArray(0x100000)

        input.lines().parallelStream() //
            .map { it.toInt() } //
            .map { prng(it) } // calculate the prng values
            .map { sequence -> sequence.take(2000) } // take the first 2000
            .map { sequence -> sequence.map { (it % 10) } } // determine the price
            .map { sequence ->
                sequence.zipWithNext { a, b -> (b - a) + 9 } // calculate deltas (and keep them positive)
                    .windowed(size = 4, step = 1) { hash(it) } // take a four-delta sequence and hash it
                    .zip(sequence.drop(4)) // zip deltas to the priced reached then (drop 4 to ensure proper alignment with the windowed sequence)
                    .distinctBy { it.first } // only keep the first occurrence, the monkey will buy then and ignore all following
            }.forEach { sequence ->
                sequence.forEach { (deltaHash, price) -> sumByDelta[deltaHash] += price }
            }

        return sumByDelta.max()
    }

    fun prng(seed: Int): Sequence<Int> {
        return generateSequence(next(seed)) { next(it) }
    }

    private fun next(seed: Int): Int {
        var resultL = seed.toLong()
        var result = seed
        result = (result xor (result shl 6)) and 0xffffff
        resultL = (resultL xor (resultL shl 6)) and 0xffffff
        result = (result xor (result shr 5)) and 0xffffff
        resultL = (resultL xor (resultL shr 5)) and 0xffffff
        result = (result xor (result shl 11)) and 0xffffff
        resultL = (resultL xor (resultL shl 11)) and 0xffffff
        return result
    }

    /**
     * Turn a list of 4 5-bit numbers into an int
     */
    private fun hash(list: List<Int>): Int {
        return (list[0]) or (list[1] shl 5) or (list[2] shl 10) or (list[3] shl 15)
    }
}