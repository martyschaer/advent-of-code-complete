package dev.schaer.aoc.y2024

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer

fun main() {
    Day05_PrintQueue().singleRun()
}

@Year(2024)
@Day(5)
class Day05_PrintQueue : Solution {

    @Answer("4957")
    override fun partOne(input: String): Int {
        val rules = parseRules(input)
        val updates = parseUpdates(input)
        return updates //
            .filter { isCorrectlyOrdered(it, rules) } //
            .sumOf { middleElement(it) }
    }


    @Answer("6938")
    override fun partTwo(input: String): Int {
        val rules = parseRules(input)
        val updates = parseUpdates(input)
        return updates //
            .filter { !isCorrectlyOrdered(it, rules) } //
            .map { update -> update.sortedWith(rules) } //
            .sumOf { middleElement(it) }
    }

    private fun parseUpdates(input: String): List<List<Int>> {
        val (_, updatesTxt) = input.split("\n\n")
        return updatesTxt.lines().map { line -> line.split(',').map { it.toInt() } }
    }

    private fun parseRules(input: String): Rules {
        val (rulesTxt, _) = input.split("\n\n")
        val ordering = mutableMapOf<Int, MutableSet<Int>>()

        rulesTxt.lines().forEach { line ->
            val (a, b) = line.split('|').map { it.toInt() }
            ordering.getOrPut(a) { mutableSetOf() }.add(b)
        }

        return Rules(ordering)
    }

    private class Rules(val ordering: Map<Int, Set<Int>>) : Comparator<Int> {
        override fun compare(a: Int, b: Int): Int {
            return if (ordering[a]?.contains(b) == true) -1 else 0
        }

    }

    private fun isCorrectlyOrdered(update: List<Int>, rules: Comparator<Int>): Boolean {
        return update.sortedWith(rules) == update
    }

    private fun middleElement(list: List<Int>): Int {
        if (list.size % 2 == 0) error("List with even number of elements doesn't have a middle element")
        return list[list.size / 2]
    }
}