package dev.schaer.aoc.y2024

import dev.schaer.aoc.algorithms.memoize
import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import dev.schaer.aoc.datastructures.Array2D
import dev.schaer.aoc.datastructures.Vector2
import dev.schaer.aoc.datastructures.parseIntoArray2dOfChars
import dev.schaer.aoc.model.Direction
import java.util.*

fun main() {
    Day21_KeypadConundrum().singleRun()
}

private const val ACTIVATE = "A"
private const val FORBIDDEN = '.'

@Year(2024)
@Day(21)
class Day21_KeypadConundrum : Solution {

    @Answer("176452")
    override fun partOne(input: String): Long {
        return solve(input, 2)
    }

    @Answer("218309335714068")
    override fun partTwo(input: String): Long {
        return solve(input, 25)
    }

    private fun solve(input: String, limit: Int): Long {
        val numPad = parseIntoArray2dOfChars(
            """
                        789
                        456
                        123
                        .0A
                """.trimIndent(), '.'
        )
        val dirPad = parseIntoArray2dOfChars(
            """
                        .^A
                        <v>
                """.trimIndent(), '.'
        )

        fun solutionLength(code: String, depth: Int, memoized: (String, Int) -> Long): Long {
            val pad = if (depth == 0) numPad else dirPad
            return code.fold(Pair(pad.pos('A')!!, 0L)) { (pos, cost), char ->
                val next = pad.pos(char)!!
                val shortestPaths = findAllShortestPaths(pad, pos, next)
                next to cost + if (depth == limit) {
                    // we're at the end, just pick the shortest
                    shortestPaths.minOf { it.length }.toLong()
                } else {
                    // consider what's optimal for the levels above
                    shortestPaths.minOfOrNull { memoized(it, depth + 1) }
                        // if nothing is found, just take the one for this level
                        ?: shortestPaths.minOf { it.length }.toLong()
                }
            }.second
        }

        val memoized = memoize(::solutionLength)
        return input.lines().sumOf { code -> memoized(code, 0) * code.filter { it.isDigit() }.toLong() }
    }

    private fun findAllShortestPaths(layout: Array2D<Char>, src: Vector2, dst: Vector2): Set<String> {
        data class Node(val path: String, val pos: Vector2)

        val queue = PriorityQueue<Node>(compareBy { it.path.length })
        queue.offer(Node("", src))

        val seen = mutableMapOf<Vector2, Int>()
        var minScore = Int.MAX_VALUE
        val best = mutableSetOf<String>()

        while (queue.isNotEmpty()) {
            val node = queue.poll()
            if (layout[node.pos] == FORBIDDEN) continue

            if (node.pos == dst) {
                if (node.path.length <= minScore) {
                    minScore = node.path.length
                } else {
                    return best
                }
                best.add(node.path + ACTIVATE)
                continue
            }

            if ((seen[node.pos] ?: Int.MAX_VALUE) < node.path.length) {
                continue
            }
            seen[node.pos] = node.path.length
            Direction.Relative.values().forEach { dir ->
                queue.add(Node(node.path + dir.symbol, node.pos + dir.vec))
            }
        }

        return best
    }
}