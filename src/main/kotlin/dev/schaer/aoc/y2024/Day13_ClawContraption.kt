package dev.schaer.aoc.y2024

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import java.util.regex.Pattern

fun main() {
    Day13_ClawContraption().singleRun()
}

private val BUTTON_EXPRESSION = Pattern.compile("Button [AB]: X\\+(\\d+), Y\\+(\\d+)")
private val PRIZE_EXPRESSION = Pattern.compile("Prize: X=(\\d+), Y=(\\d+)")
private const val BUTTON_A_COST = 3L
private const val BUTTON_B_COST = 1L
private const val PART_TWO_INCREASE = 10000000000000L

@Year(2024)
@Day(13)
class Day13_ClawContraption : Solution {

    @Answer("34787")
    override fun partOne(input: String): Long {
        val contraptions = parse(input)
        return contraptions.mapNotNull { calculateCostToWin(it) }.sum()
    }

    @Answer("85644161121698")
    override fun partTwo(input: String): Long {
        val contraptions = parse(input).map { partTwoifyContraption(it) }
        return contraptions.mapNotNull { calculateCostToWin(it) }.sum()
    }

    private fun parse(input: String): List<Contraption> {
        return input.split("\n\n").map { parseContraption(it) }
    }

    private fun parseContraption(input: String): Contraption {
        val (aStr, bStr, prizeStr) = input.lines()
        val a = parsePart(aStr, BUTTON_EXPRESSION)
        val b = parsePart(bStr, BUTTON_EXPRESSION)
        val prize = parsePart(prizeStr, PRIZE_EXPRESSION)
        return Contraption(a, b, prize)
    }

    private fun parsePart(input: String, expression: Pattern): Pair<Long, Long> {
        val matcher = expression.matcher(input)
        matcher.find()
        val xStr = matcher.group(1)
        val yStr = matcher.group(2)
        return Pair(xStr.toLong(), yStr.toLong())
    }

    private fun calculateCostToWin(contraption: Contraption): Long? {
        /*
        Given: Px, Py (Prize Coordinates), Ax, Ay (Button A Movement), Bx, By (Button B Movement)
        Unknown: a, b (how many times must each button be pressed)
        Px = a * Ax + b * Bx
        Py = a * Ay + b * By
         */

        val (Px, Py) = contraption.prize
        val (Ax, Ay) = contraption.a
        val (Bx, By) = contraption.b

        // closed form solution for the system of linear equations
        val a = (Py * Bx - By * Px) / (Ay * Bx - Ax * By)
        val b = (Px * Ay - Py * Ax) / (Ay * Bx - Ax * By)

        // is a real solution?
        if (a * Ax + b * Bx == Px && a * Ay + b * By == Py) {
            return BUTTON_A_COST * a + BUTTON_B_COST * b
        }

        return null
    }

    private fun partTwoifyContraption(input: Contraption): Contraption {
        return Contraption(input.a, input.b, Pair(input.prize.first + PART_TWO_INCREASE, input.prize.second + PART_TWO_INCREASE))
    }

    data class Contraption(val a: Pair<Long, Long>, val b: Pair<Long, Long>, val prize: Pair<Long, Long>)
}