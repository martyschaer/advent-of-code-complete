package dev.schaer.aoc.y2024

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import dev.schaer.aoc.datastructures.Array2D
import dev.schaer.aoc.datastructures.parseIntoArray2dOfChars
import dev.schaer.aoc.model.Direction

fun main() {
    Day04_CeresSearch().singleRun()
}

private const val XMAS = "XMAS"

@Year(2024)
@Day(4)
class Day04_CeresSearch : Solution {

    /**
     * Counts occurrences of the following in all directions on the given input grid:
     *
     *      XMAS
     */
    @Answer("2401")
    override fun partOne(input: String): Int {
        var foundXmases = 0
        val grid = parseIntoArray2dOfChars(input, '.')
        for (y in 0 until grid.height) {
            for (x in 0 until grid.width) {
                for (dir in Direction.Compass.values()) {
                    if (hasXmasAtCoordsInDirection(grid, x, y, dir)) {
                        foundXmases++
                    }
                }
            }
        }
        return foundXmases
    }

    private fun hasXmasAtCoordsInDirection(
        grid: Array2D<Char>, x: Int, y: Int, dir: Direction
    ): Boolean {
        for (i in 0 until 4) {
            if (grid[x + dir.x * i, y + dir.y * i] != XMAS[i]) {
                return false
            }
        }
        return true
    }

    /**
     * Counts occurrences of the following on the given input grid:
     *
     *      M S
     *       A
     *      M S
     * The above is an example, the MAS-es may point in any direction along the diagonals.
     */
    @Answer("1822")
    override fun partTwo(input: String): Int {
        var foundXmases = 0
        val grid = parseIntoArray2dOfChars(input, '.')
        for (y in 1 until grid.height - 1) {
            for (x in 1 until grid.width - 1) {
                if (grid[x, y] == 'A') {
                    var foundOnDiagonal = 0
                    if (grid[x - 1, y - 1] == 'M' && grid[x + 1, y + 1] == 'S') {
                        foundOnDiagonal++
                    }
                    if (grid[x - 1, y - 1] == 'S' && grid[x + 1, y + 1] == 'M') {
                        foundOnDiagonal++
                    }
                    if (grid[x + 1, y - 1] == 'M' && grid[x - 1, y + 1] == 'S') {
                        foundOnDiagonal++
                    }
                    if (grid[x + 1, y - 1] == 'S' && grid[x - 1, y + 1] == 'M') {
                        foundOnDiagonal++
                    }
                    if (foundOnDiagonal == 2) {
                        foundXmases++
                    }
                }
            }
        }
        return foundXmases
    }
}