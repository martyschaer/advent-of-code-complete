package dev.schaer.aoc.y2024

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import dev.schaer.aoc.common.annotations.LongRunning
import dev.schaer.aoc.datastructures.Vector2
import dev.schaer.aoc.datastructures.parseIntoArray2dOfChars
import dev.schaer.aoc.model.Direction
import dev.schaer.aoc.model.Direction.Cardinal
import java.util.*

fun main() {
    Day16_ReindeerMaze().singleRun()
}

private const val COST_MOVE = 1
private const val COST_TURN = 1000
private const val WALL = '#'

@Year(2024)
@Day(16)
class Day16_ReindeerMaze : Solution {

    @Answer("122492")
    override fun partOne(input: String): Int {
        val map = parseIntoArray2dOfChars(input, WALL)
        val start = map.unmapToVec(map.values.indexOfFirst { it == 'S' })
        val end = map.unmapToVec(map.values.indexOfFirst { it == 'E' })

        data class Node(val position: Vector2, val dir: Direction, val score: Int)

        val queue: Queue<Node> = PriorityQueue<Node>(compareBy { it.score })
        queue.add(Node(start, Cardinal.EAST, 0))

        val seen = mutableMapOf<Pair<Vector2, Direction>, Int>()

        while (queue.isNotEmpty()) {
            val node = queue.poll()
            val (position, direction, score) = node
            if (position == end) {
                return score
            }

            val key = Pair(position, direction)
            if ((seen[key] ?: Int.MAX_VALUE) < node.score) {
                continue
            }
            seen[key] = node.score

            for (nextDirection in Cardinal.values()) {
                if (nextDirection == direction.opposite) continue
                val nextPosition = position + nextDirection.vec
                if (map[nextPosition] == WALL) continue
                val cost = COST_MOVE + if (nextDirection == direction) 0 else COST_TURN
                queue.add(Node(nextPosition, nextDirection, score + cost))
            }
        }

        return 0
    }

    @Answer("520")
    override fun partTwo(input: String): Int {
        val map = parseIntoArray2dOfChars(input, WALL)
        val start = map.unmapToVec(map.values.indexOfFirst { it == 'S' })
        val end = map.unmapToVec(map.values.indexOfFirst { it == 'E' })

        data class Node(val path: List<Vector2>, val dir: Direction, val score: Int)

        val queue: Queue<Node> = PriorityQueue<Node>(compareBy { it.score })
        queue.add(Node(listOf(start), Cardinal.EAST, 0))

        val seen = mutableMapOf<Pair<Vector2, Direction>, Int>()

        var minScore = Int.MAX_VALUE
        val best = mutableSetOf<Vector2>()

        while (queue.isNotEmpty()) {
            val node = queue.poll()
            val last = node.path.last()
            if (last == end) {
                if (node.score <= minScore) {
                    minScore = node.score
                } else {
                    return best.size
                }
                best.addAll(node.path)
            }

            val key = last to node.dir
            if ((seen[key] ?: Int.MAX_VALUE) < node.score) {
                continue
            }
            seen[key] = node.score

            if (map[last + node.dir.vec] != WALL) {
                queue.add(Node(node.path + (last + node.dir.vec), node.dir, node.score + COST_MOVE))
            }
            queue.add(Node(node.path, node.dir.cw, node.score + COST_TURN))
            queue.add(Node(node.path, node.dir.ccw, node.score + COST_TURN))
        }

        return 0
    }


}