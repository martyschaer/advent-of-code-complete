package dev.schaer.aoc.y2024

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import kotlin.math.abs

fun main() {
    Day01_HistorianHysteria().singleRun()
}

@Year(2024)
@Day(1)
class Day01_HistorianHysteria : Solution {

    @Answer("2375403")
    override fun partOne(input: String): Long {
        val (first, second) = parse(input)

        val firstOrdered = first.sorted()
        val secondOrdered = second.sorted()

        return firstOrdered.zip(secondOrdered).sumOf { (a, b) -> abs(a - b) }
    }

    @Answer("23082277")
    override fun partTwo(input: String): Long {
        val (first, second) = parse(input)
        val frequencies = second.groupingBy { it }.eachCount()
        return first.sumOf { it * (frequencies[it] ?: 0) }
    }

    private fun parse(input: String): Pair<List<Long>, List<Long>> {
        val first = mutableListOf<Long>()
        val second = mutableListOf<Long>()
        input.lines().filter { it.isNotBlank() }.forEach { line ->
            val (a, b) = line.split("   ")
            first.add(a.toLong())
            second.add(b.toLong())
        }

        return Pair(first, second)
    }
}