package dev.schaer.aoc.y2023

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer

fun main() {
    Day15_LensLibrary().singleRun()
}

@Year(2023)
@Day(15)
class Day15_LensLibrary : Solution {

    @Answer("511257")
    override fun partOne(input: String): Int {
        return input.split(',').sumOf { hash(it) }
    }

    @Answer("239484")
    override fun partTwo(input: String): Int {
        val operations = input.split(',')
        val boxes = Array(256) { Box() }

        runOperations(boxes, operations)

        return calculateFocusingPower(boxes)
    }

    private fun calculateFocusingPower(boxes: Array<Box>): Int {
        return boxes.mapIndexed { boxIdx, box ->
            calculateFocusingPower(box, boxNumber = boxIdx + 1)
        }.sum()
    }

    private fun calculateFocusingPower(box: Box, boxNumber: Int): Int {
        return box.lenses.mapIndexed { lensIdx, lens ->
            val slotNumber = lensIdx + 1
            boxNumber * slotNumber * lens.focalLength
        }.sum()
    }

    private fun runOperations(boxes: Array<Box>, operations: List<String>) {
        for (operation in operations) {
            if (isRemove(operation)) {
                remove(boxes, operation)
            } else {
                replace(boxes, operation)
            }
        }
    }

    private fun isRemove(operation: String): Boolean {
        return operation.last() == '-'
    }

    private fun replace(boxes: Array<Box>, operation: String) {
        val (label, focalLength) = operation.split('=')
        val newLens = Lens(label, focalLength.toInt())
        val hash = hash(label)
        val box = boxes[hash]

        val existingIndex = box.lenses.indexOfFirst { it.label == label }
        if (existingIndex == -1) {
            box.lenses += newLens
        } else {
            box.lenses[existingIndex] = newLens
        }
    }

    private fun remove(boxes: Array<Box>, operation: String) {
        val label = operation.dropLast(1)
        val hash = hash(label)
        boxes[hash].lenses.removeIf { lens -> lens.label == label }
    }

    private data class Lens(val label: String, val focalLength: Int)

    // TODO? implement Box with custom "Hash-Linked-List"
    private class Box(val lenses: MutableList<Lens> = mutableListOf())

    private fun hash(input: String): Int {
        return input.fold(0) { acc, char ->
            ((acc + char.code) * 17) % 256
        }
    }
}