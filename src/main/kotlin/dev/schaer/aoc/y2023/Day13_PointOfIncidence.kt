package dev.schaer.aoc.y2023

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import dev.schaer.aoc.datastructures.Array2D
import dev.schaer.aoc.datastructures.getColsAsString
import dev.schaer.aoc.datastructures.getRowsAsString

fun main() {
    Day13_PointOfIncidence().singleRun()
}

@Year(2023)
@Day(13)
class Day13_PointOfIncidence : Solution {

    @Answer("30487")
    override fun partOne(input: String): Int {
        val tiles = parse(input)
        return tiles.sumOf { tile ->
            val row = calculateRowOfReflection(tile, 0)
            val col = calculateColOfReflection(tile, 0)
            row * 100 + col
        }
    }

    @Answer("31954")
    override fun partTwo(input: String): Int {
        val tiles = parse(input)
        return tiles.sumOf { tile ->
            val row = calculateRowOfReflection(tile, 1)
            val col = calculateColOfReflection(tile, 1)
            row * 100 + col
        }
    }

    private fun calculateRowOfReflection(tile: Array2D<Char>, smudges: Int = 0): Int {
        return calculateLeadingReflections(tile.getRowsAsString(), smudges)
    }

    private fun calculateColOfReflection(tile: Array2D<Char>, smudges: Int = 0): Int {
        return calculateLeadingReflections(tile.getColsAsString(), smudges)
    }

    private fun calculateLeadingReflections(lines: List<String>, allowedSmudges: Int = 0): Int {
        val after = mutableListOf<String>()
        after.addAll(lines)
        val before = mutableListOf<String>()

        while (after.size > 1) {
            before.add(0, after.removeFirst())
            val diffs = before.joinToString("").zip(after.joinToString("")) { a, b -> if (a != b) 1 else 0 }.sum()

            if (diffs == allowedSmudges) {
                return before.size
            }
        }

        return 0
    }

    private fun parse(input: String): List<Array2D<Char>> {
        return input.split("\n\n").map { parseTile(it) }
    }

    private fun parseTile(tile: String): Array2D<Char> {
        val lines = tile.lines()
        val height = lines.size
        val width = lines.first().length
        val values = tile.replace("\n", "").toCharArray().toTypedArray()
        return Array2D(values, width, height, '.')
    }
}