package dev.schaer.aoc.y2023

import dev.schaer.aoc.algorithms.State
import dev.schaer.aoc.algorithms.Transition
import dev.schaer.aoc.algorithms.dijkstra
import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import dev.schaer.aoc.common.annotations.LongRunning
import dev.schaer.aoc.datastructures.Array2D
import dev.schaer.aoc.datastructures.parseIntoArray2dOfDigits
import dev.schaer.aoc.model.Direction

fun main() {
    Day17_ClumsyCrucible().singleRun()
}

@Year(2023)
@Day(17)
@LongRunning(repetition = 16)
class Day17_ClumsyCrucible : Solution {
    @Answer("942")
    override fun partOne(input: String): Int {
        return solve(input, 0, 3)
    }

    @Answer("1082")
    override fun partTwo(input: String): Any {
        return solve(input, 4, 10) { node ->
            node.consecutiveDirectionCount + 1 >= node.conf.minConsecDir
        }
    }

    private fun solve(
        input: String, //
        minConsDir: Int, //
        maxConsDir: Int, //
        additionalEndCriteria: (Node) -> Boolean = { true } //
    ): Int {
        val map = parseIntoArray2dOfDigits(input)

        val conf = Configuration(map, minConsDir, maxConsDir)

        val start = Node(null, 0, 0, conf)

        val path = dijkstra(start) { state ->
            state as Node
            val onEndTile = state.x == map.width - 1 && state.y == map.height - 1
            additionalEndCriteria(state) && onEndTile
        }

        // first node does not contribute to the cost
        val cost = path.drop(1).sumOf {
            it as Node
            map[it.x, it.y]
        }

        return cost
    }

    private data class Configuration(val map: Array2D<Int>, val minConsecDir: Int, val maxConsecDir: Int)

    private data class Node(
        override val parent: State?,
        val x: Int,
        val y: Int,
        val conf: Configuration,
        val lastDirection: Direction? = null,
        val consecutiveDirectionCount: Int = 0
    ) : State {
        override fun transitions(): List<Transition> {
            return Direction.Cardinal.values() //
                .filter { wouldBeInBounds(it) } //
                .filter { canMoveInDirection(it) } //
                .map { createTransition(it) } //
        }

        private fun wouldBeInBounds(nextDir: Direction): Boolean {
            val newX = x + nextDir.vec.x
            val newY = y + nextDir.vec.y

            return conf.map.isInBounds(newX, newY)
        }

        private fun createTransition(nextDir: Direction): Transition {
            val dst = Node(
                this,
                x + nextDir.vec.x, y + nextDir.vec.y, //
                conf, //
                nextDir, //
                calcConsecutiveDirectionCount(nextDir) //
            )

            return Transition(dst, conf.map[dst.x, dst.y])
        }

        private fun calcConsecutiveDirectionCount(nextDir: Direction): Int {
            if (nextDir != lastDirection) {
                return 0
            }
            return consecutiveDirectionCount + 1
        }

        private fun canMoveInDirection(nextDir: Direction): Boolean {
            // at first, we can move in any direction
            if (lastDirection == null) {
                return true
            }

            // can't turn around
            if (nextDir == lastDirection.opposite) {
                return false
            }

            val isSameDirection = lastDirection == nextDir

            if (isSameDirection) {
                return consecutiveDirectionCount + 1 < conf.maxConsecDir
            }

            return consecutiveDirectionCount + 1 >= conf.minConsecDir
        }


        override fun toString(): String {
            return "[$x, $y] ld=$lastDirection, cdc=$consecutiveDirectionCount"
        }

        /**
         * The config val is expensive to compare and doesn't change between nodes within a single execution
         */
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as Node

            if (x != other.x) return false
            if (y != other.y) return false
            if (lastDirection != other.lastDirection) return false
            if (consecutiveDirectionCount != other.consecutiveDirectionCount) return false

            return true
        }

        override fun hashCode(): Int {
            var result = x
            result = 31 * result + y
            result = 31 * result + (lastDirection?.hashCode() ?: 0)
            result = 31 * result + consecutiveDirectionCount
            return result
        }
    }
}