package dev.schaer.aoc.y2023

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import dev.schaer.aoc.common.nothing
import dev.schaer.aoc.datastructures.Array2D
import dev.schaer.aoc.datastructures.Vector2
import dev.schaer.aoc.datastructures.parseIntoArray2dOfChars
import dev.schaer.aoc.model.Direction
import dev.schaer.aoc.model.Direction.Relative
import dev.schaer.aoc.model.Direction.Relative.*
import kotlin.experimental.and
import kotlin.experimental.or

fun main() {
    Day16_LavaFloor().singleRun()
}

private const val EMPTY = '.'
private const val MIRROR_FORWARD = '/'
private const val MIRROR_BACKWARD = '\\'
private const val SPLITTER_VERTICAL = '|'
private const val SPLITTER_HORIZONTAL = '-'

@Year(2023)
@Day(16)
class Day16_LavaFloor : Solution {

    @Answer("7498")
    override fun partOne(input: String): Int {
        val contraption = parse(input)
        // find energy when starting at 0,0, pointing right
        return calculateEnergizedTiles(Vector2(0, 0), RIGHT, contraption)
    }

    @Answer("7846")
    override fun partTwo(input: String): Int {
        val contraption = parse(input)

        // find maximum energy for any edge starting position pointing inward
        var maxEnergized = 0

        for (y in 0 until contraption.height) {
            maxEnergized = maxOf(maxEnergized, calculateEnergizedTiles(Vector2(0, y), RIGHT, contraption))
            maxEnergized =
                maxOf(maxEnergized, calculateEnergizedTiles(Vector2(contraption.width - 1, y), LEFT, contraption))
        }

        for (x in 0 until contraption.width) {
            maxEnergized = maxOf(maxEnergized, calculateEnergizedTiles(Vector2(x, 0), DOWN, contraption))
            maxEnergized =
                maxOf(maxEnergized, calculateEnergizedTiles(Vector2(x, contraption.height - 1), UP, contraption))
        }

        return maxEnergized
    }

    private fun calculateEnergizedTiles(pos: Vector2, dir: Relative, contraption: Array2D<Char>): Int {
        val energized = emptyEnergized(contraption.width, contraption.height)
        simulateBeam(Beam(pos, dir), contraption, energized)
        return energized.values.count { it > 0 }
    }

    private fun emptyEnergized(width: Int, height: Int): Array2D<Byte> {
        return Array2D(Array(width * height) { 0 }, width, height, 0) {
            if (it == 0.toByte()) "#" else it.toString()
        }
    }

    private fun simulateBeam(beam: Beam, contraption: Array2D<Char>, energized: Array2D<Byte>) {
        while (true) {
            if (contraption.isOutOfBounds(beam.position)) {
                // beam left the area
                break
            }

            if (isEnergizedAtPositionAndInDirection(energized, beam)) {
                // been there, done that
                break
            }

            setEnergizedAtPositionAndInDirection(energized, beam)

            when (contraption[beam.position]) {
                EMPTY -> nothing()
                SPLITTER_VERTICAL -> when {
                    beam.direction.vertical -> nothing()
                    beam.direction.horizontal -> {
                        simulateBeam(Beam(beam.position.copy(), UP), contraption, energized)
                        simulateBeam(Beam(beam.position.copy(), DOWN), contraption, energized)
                        break
                    }
                }

                SPLITTER_HORIZONTAL -> when {
                    beam.direction.horizontal -> nothing()
                    beam.direction.vertical -> {
                        simulateBeam(Beam(beam.position.copy(), LEFT), contraption, energized)
                        simulateBeam(Beam(beam.position.copy(), RIGHT), contraption, energized)
                        break
                    }
                }

                MIRROR_FORWARD -> beam.direction = forwardMirror(beam.direction)
                MIRROR_BACKWARD -> beam.direction = backwardMirror(beam.direction)
            }

            beam.move()
        }
    }

    private fun isEnergizedAtPositionAndInDirection(energized: Array2D<Byte>, beam: Beam): Boolean {
        return (beam.direction.binaryOrdinal and energized[beam.position]) > 0
    }

    private fun setEnergizedAtPositionAndInDirection(energized: Array2D<Byte>, beam: Beam) {
        energized[beam.position] = energized[beam.position] or beam.direction.binaryOrdinal
    }

    /**
     * Mirrors the given direction along a mirror aligned south-west to north-east (/)
     */
    private fun forwardMirror(currentDirection: Relative): Relative {
        return when (currentDirection) {
            UP -> RIGHT
            DOWN -> LEFT
            LEFT -> DOWN
            RIGHT -> UP
        }
    }

    /**
     * Mirrors the given direction along a mirror aligned south-east to north-west (\)
     */
    private fun backwardMirror(currentDirection: Relative): Relative {
        return when (currentDirection) {
            UP -> LEFT
            DOWN -> RIGHT
            LEFT -> UP
            RIGHT -> DOWN
        }
    }

    private fun parse(input: String): Array2D<Char> {
        return parseIntoArray2dOfChars(input, EMPTY)
    }

    private class Beam(val position: Vector2, var direction: Relative) {
        fun move() {
            position += direction.vec
        }
    }
}