package dev.schaer.aoc.y2023

import dev.schaer.aoc.algorithms.pow
import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer

fun main() {
    Day04_Scratchcards().singleRun()
}

@Year(2023)
@Day(4)
class Day04_Scratchcards : Solution {
    private data class Card(val id: Int, val given: Set<Int>, val winning: Set<Int>) {
        val matches = given.intersect(winning).size
        val score = 2.pow(matches - 1)
    }

    @Answer("23678")
    override fun partOne(input: String): Int {
        val cards = parse(input)
        return cards.sumOf { it.score }
    }

    @Answer("15455663")
    override fun partTwo(input: String): Int {
        val cards = parse(input)

        return calculateCopiesWon(cards)
    }

    private fun calculateCopiesWon(cards: List<Card>): Int {
        val copiesPerCard = IntArray(cards.size) { 1 }
        for (card in cards) {
            for (copyIdx in card.id + 1..card.id + card.matches) {
                copiesPerCard[copyIdx] += copiesPerCard[card.id]
            }
        }
        return copiesPerCard.sum()
    }

    private fun parse(input: String): List<Card> {
        return input.lines().map { parseCard(it) }
    }

    private fun parseCard(line: String): Card {
        val tokens = line.split(':')

        val cardName = tokens.first()
        // zero-index the cardIds
        val cardId = cardName.split(' ').last().toInt() - 1

        val numberSets = tokens.last().split('|')
        val given = parseNumberSet(numberSets.first())
        val winning = parseNumberSet(numberSets.last())
        return Card(cardId, given, winning)
    }

    private fun parseNumberSet(str: String): Set<Int> {
        return str.split(' ') //
            .filter { it.isNotEmpty() } //
            .map { it.toInt() } //
            .toSet()
    }
}