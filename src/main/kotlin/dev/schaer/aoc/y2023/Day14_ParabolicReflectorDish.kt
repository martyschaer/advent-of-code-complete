package dev.schaer.aoc.y2023

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import dev.schaer.aoc.datastructures.Array2D
import dev.schaer.aoc.datastructures.getRowsAsString
import dev.schaer.aoc.datastructures.parseIntoArray2dOfChars
import dev.schaer.aoc.model.Direction
import dev.schaer.aoc.model.Direction.Cardinal.*

fun main() {
    Day14_ParabolicReflectorDish().singleRun()
}

private const val EMPTY = '.'
private const val ROUND = 'O'

@Year(2023)
@Day(14)
class Day14_ParabolicReflectorDish : Solution {

    private val cycle = listOf(NORTH, WEST, SOUTH, EAST)

    @Answer("109345")
    override fun partOne(input: String): Int {
        val platform = parseIntoArray2dOfChars(input, EMPTY)
        tilt(platform, NORTH)

        return calculateLoadOnNorthSupportBeam(platform)
    }

    @Answer("112452")
    override fun partTwo(input: String): Int {
        val platform = parseIntoArray2dOfChars(input, EMPTY)

        val cyclesToDo = 1_000_000_000

        val (patternLength, cyclesPerformed) = findPatternLength(platform)

        val pattern = loadOnNorthSupportBeamAsSequence(platform).take(patternLength).toList()

        /*
         * calculate the value based on an eventually repeating sequence
         * from the cycles to do, subtract the cycles performed in finding the pattern
         *
         * and find the remainder when dividing the such remaining cycles by the pattern length
         */
        return pattern[(cyclesToDo - cyclesPerformed) % patternLength]
    }

    /**
     * Heuristically determines pattern length, by counting repeating patterns
     * based on their hashes. This probably does not work generally,
     * but works for my input and test cases.
     *
     * @return Pair(pattern length, cyclesPerformed)
     */
    private fun findPatternLength(platform: Array2D<Char>): Pair<Int, Int> {
        val patternOccurrences = hashMapOf<Int, Int>()
        var cyclesPerformed = 0
        while (true) {
            doCycle(platform)
            cyclesPerformed++

            val key = platform.hash()
            patternOccurrences[key] = 1 + (patternOccurrences[key] ?: 0)
            if ((patternOccurrences[key] ?: 0) >= 3) {
                // we've seen the same pattern thrice
                // we must have seen the cycle by now as well
                break
            }
        }

        // count the repeatedly seen patterns
        val avgPatternOccurrence = patternOccurrences.values.average()
        val patternLength = patternOccurrences.values.count { it > avgPatternOccurrence }

        return Pair(patternLength, cyclesPerformed)
    }

    private fun loadOnNorthSupportBeamAsSequence(platform: Array2D<Char>): Sequence<Int> {
        val copy = platform.clone()

        val seed = copy to calculateLoadOnNorthSupportBeam(copy)
        return generateSequence(seed) { (p, _) ->
            doCycle(p)
            p to calculateLoadOnNorthSupportBeam(p)
        }.map { it.second }
    }

    private fun calculateLoadOnNorthSupportBeam(platform: Array2D<Char>): Int {
        return platform.getRowsAsString().mapIndexed { distance, row ->
            val load = platform.height - distance
            row.count { it == ROUND } * load
        }.sum()
    }

    private fun doCycle(platform: Array2D<Char>) {
        cycle.forEach { direction -> tilt(platform, direction) }
    }

    fun tilt(platform: Array2D<Char>, direction: Direction) {
        val dx = direction.x
        val dy = direction.y

        when {
            direction.vertical -> {
                val yRange = if (dy < 0) 0 until platform.height else platform.height - 1 downTo 0
                for (y in yRange) {
                    for (x in 0 until platform.width) {
                        moveRock(platform, x, y, dx, dy)
                    }
                }
            }

            direction.horizontal -> {
                val xRange = if (dx < 0) 0 until platform.width else platform.width - 1 downTo 0
                for (x in xRange) {
                    for (y in 0 until platform.height) {
                        moveRock(platform, x, y, dx, dy)
                    }
                }
            }
        }
    }

    private fun moveRock(platform: Array2D<Char>, x: Int, y: Int, dx: Int, dy: Int) {
        if (platform[x, y] == ROUND) {
            var nx = x
            var ny = y
            while (isValidMove(platform, nx + dx, ny + dy)) {
                nx += dx
                ny += dy
            }

            if (nx != x || ny != y) {
                platform[nx, ny] = platform[x, y]
                platform[x, y] = EMPTY
            }
        }
    }

    private fun isValidMove(platform: Array2D<Char>, x: Int, y: Int): Boolean {
        return x in 0 until platform.width && y in 0 until platform.height // in bounds
                && platform[x, y] == EMPTY // destination is available
    }

}