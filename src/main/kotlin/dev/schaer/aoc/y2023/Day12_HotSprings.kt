package dev.schaer.aoc.y2023

import dev.schaer.aoc.algorithms.memoize
import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import dev.schaer.aoc.datastructures.head
import dev.schaer.aoc.datastructures.repeat
import dev.schaer.aoc.datastructures.tail

fun main() {
    Day12_HotSprings().singleRun()
}

private const val OK = '.'
private const val DAMAGED = '#'
private const val UNKNOWN = '?'

private const val VALID = 1L
private const val INVALID = 0L

@Year(2023)
@Day(12)
class Day12_HotSprings : Solution {

    @Answer("7402")
    override fun partOne(input: String): Long {
        return parse(input).sumOf { (springs, groups) ->
            solve(springs, groups)
        }
    }

    @Answer("3384337640277")
    override fun partTwo(input: String): Long {
        return parse(input).sumOf { (springs, groups) ->
            solve(springs, groups, 5)
        }
    }

    private fun solve(rawSprings: String, rawGroups: List<Int>, repetitions: Int = 1): Long {
        val springs = listOf(rawSprings).repeat(repetitions).joinToString("?")
        val groups = rawGroups.repeat(repetitions)
        val memoized = memoize(::recurse)
        return memoized(springs, groups)
    }

    private fun recurse(springs: String, groups: List<Int>, memoized: (String, List<Int>) -> Long): Long {
        if (groups.isEmpty()) {
            // no more groups left
            if (DAMAGED !in springs) {
                // are there no more damaged springs?
                return VALID
            }
            return INVALID
        }

        if (springs.isEmpty()) {
            // springs is empty, but there are more groups left
            return INVALID
        }

        val head = springs.head()
        val tail = springs.tail()

        if (head == OK) {
            // the next spring is OK, check the remaining
            return memoized(tail, groups)
        }

        if (head == UNKNOWN) {
            // next spring could be both, try both possibilities
            return memoized(OK + tail, groups) + //
                    memoized(DAMAGED + tail, groups)
        }

        // head is a damaged spring
        val group = groups.head()

        val springsWithoutGroup = springs.drop(group)
        val potentiallyInGroup = springs.take(group).replace(UNKNOWN, DAMAGED)

        if (potentiallyInGroup.length < group) {
            // not enough springs left to match group
            return INVALID
        }

        if (potentiallyInGroup.none { it == OK }) {
            // able to fit the entire group into springs
            if (springsWithoutGroup.isEmpty()) {
                return memoized(springsWithoutGroup, groups.tail())
            }
            if (springsWithoutGroup.head() != DAMAGED) {
                // group may not be followed by another group directly
                return memoized(springsWithoutGroup.tail(), groups.tail())
            }
        }

        return INVALID
    }

    private fun parse(input: String): List<Pair<String, List<Int>>> {
        return input.lines().map { parseLine(it) }
    }

    private fun parseLine(line: String): Pair<String, List<Int>> {
        val (states, groups) = line.split(' ')
        return states to groups.split(',').map { it.toInt() }
    }

}