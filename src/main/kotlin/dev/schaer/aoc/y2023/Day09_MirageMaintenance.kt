package dev.schaer.aoc.y2023

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer

fun main() {
    Day09_MirageMaintenance().singleRun()
}

@Year(2023)
@Day(9)
class Day09_MirageMaintenance : Solution {

    @Answer("2005352194")
    override fun partOne(input: String): Long {
        return parse(input).sumOf { predictNextValue(it) }
    }

    @Answer("1077")
    override fun partTwo(input: String): Long {
        return parse(input).sumOf { predictNextValue(it.reversed()) }
    }

    private fun predictNextValue(history: List<Long>): Long {
        val deltas = history.zipWithNext { a, b -> b - a}
        if (deltas.all { it == 0L }) {
            return history.last()
        }
        return history.last() + predictNextValue(deltas)
    }

    private fun parse(input: String): List<List<Long>> {
        return input.lines().map { line -> line.split(' ').map { it.toLong() } }
    }
}