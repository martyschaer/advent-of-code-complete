package dev.schaer.aoc.y2023

import dev.schaer.aoc.algorithms.outerArea
import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import dev.schaer.aoc.datastructures.Vector2
import dev.schaer.aoc.model.Direction.Cardinal.*

fun main() {
    Day18_LavaductLagoon().singleRun()
}

@Year(2023)
@Day(18)
class Day18_LavaductLagoon : Solution {

    @Answer("106459")
    override fun partOne(input: String): Long {
        return solve(input, ::parsePartOne)
    }

    @Answer("63806916814808")
    override fun partTwo(input: String): Long {
        return solve(input, ::parsePartTwo)
    }

    private fun solve(input: String, parser: (String) -> Vector2): Long {
        val plan = parse(input, parser)
        val perimeter = execute(plan)

        val area = outerArea(perimeter)
        val depth = 1

        return area * depth
    }

    private fun execute(plan: List<Vector2>): List<Vector2> {
        val perimeter = mutableListOf<Vector2>()
        perimeter.add(Vector2.ZERO)

        for (step in plan) {
            perimeter += perimeter.last() + step
        }

        return perimeter
    }

    private fun parse(input: String, parser: (String) -> Vector2): List<Vector2> {
        return input.lines().map { parser(it) }
    }

    private fun parsePartOne(line: String): Vector2 {
        val (dirStr, distStr) = line.split(' ')
        val direction = when (dirStr) {
            "L" -> WEST
            "R" -> EAST
            "U" -> NORTH
            "D" -> SOUTH
            else -> error("Unknown direction '$dirStr'")
        }
        val distance = distStr.toInt()
        return direction.vec * distance
    }

    private fun parsePartTwo(line: String): Vector2 {
        val hexFormat = line.dropLast(1).takeLast(6)
        val dirIndicator = hexFormat.last()
        val direction = when (dirIndicator) {
            '0' -> EAST
            '1' -> SOUTH
            '2' -> WEST
            '3' -> NORTH
            else -> error("'#$hexFormat' may not be a valid dig-instruction")
        }
        val distance = hexFormat.dropLast(1).toInt(16)
        return direction.vec * distance
    }
}