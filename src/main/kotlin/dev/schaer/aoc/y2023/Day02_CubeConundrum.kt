package dev.schaer.aoc.y2023

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import kotlin.math.max

fun main() {
    Day02_CubeConundrum().singleRun()
}

@Year(2023)
@Day(2)
class Day02_CubeConundrum : Solution {

    @Answer("2447")
    override fun partOne(input: String): Int {
        val bag = Bag(12, 13, 14)
        val games = parse(input)
        return games.filter { game -> game.isPossible(bag) }.sumOf { game -> game.id }
    }

    @Answer("56322")
    override fun partTwo(input: String): Int {
        val games = parse(input)
        return games.map { it.minimalCubeSet }.sumOf { it.power }
    }

    private data class Game(val id: Int, val draws: Set<Draw>) {
        val minimalCubeSet = draws.fold(Bag(0, 0, 0)) { (r, g, b), draw ->
            Bag(max(r, draw.red), max(g, draw.green), max(b, draw.blue))
        }

        fun isPossible(bag: Bag): Boolean {
            return draws.all { it.isPossible(bag) }
        }
    }

    private data class Draw(val red: Int, val green: Int, val blue: Int) {
        fun isPossible(bag: Bag): Boolean {
            return red <= bag.red && green <= bag.green && blue <= bag.blue
        }
    }

    private data class Bag(val red: Int, val green: Int, val blue: Int) {
        val power = red * green * blue
    }

    private fun parse(input: String): List<Game> {
        return input.lines().map { parseGame(it) }
    }

    /**
     * Parses a given game, e.g.
     *
     * `Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green`
     */
    private fun parseGame(line: String): Game {
        val tokens = line.split(':')

        val gameName = tokens.first()
        val gameId = gameName.split(' ').last().toInt()

        val draws = tokens.last().split(';').map { parseDraw(it) }.toSet()

        return Game(gameId, draws)
    }

    /**
     * Parses a given draw, e.g.
     *
     * `1 red, 2 green, 6 blue`
     */
    private fun parseDraw(drawStr: String): Draw {
        val colorInfo = drawStr.split(',').associate { parseColorInfo(it) }
        // when zero of a color are drawn, that color is not specified
        val r = colorInfo["red"] ?: 0
        val g = colorInfo["green"] ?: 0
        val b = colorInfo["blue"] ?: 0
        return Draw(r, g, b)
    }

    /**
     * Parses a given color info, e.g.
     *
     * `1 red`
     */
    private fun parseColorInfo(colorInfo: String): Pair<String, Int> {
        val tokens = colorInfo.trim().split(' ')
        return tokens.last() to tokens.first().toInt()
    }
}