package dev.schaer.aoc.y2023

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import dev.schaer.aoc.datastructures.product
import kotlin.math.ceil
import kotlin.math.floor
import kotlin.math.sqrt

fun main() {
    Day06_WaitForIt().singleRun()
}

@Year(2023)
@Day(6)
class Day06_WaitForIt : Solution {
    @Answer("5133600")
    override fun partOne(input: String): Long {
        val races = parse(input)

        return races.map { calculateWaysToBeatRace(it) }.product()
    }

    @Answer("40651271")
    override fun partTwo(input: String): Long {
        val races = parse(input.replace(" ", ""))

        return calculateWaysToBeatRace(races.single())
    }

    private fun calculateWaysToBeatRace(race: Race): Long  {

        val t = race.time
        val d = race.recordDistance

        val discriminant = sqrt(t * t - 4.0 * d)

        var lowerBound = ceil((t - discriminant) / 2).toLong()
        var upperBound = floor((t + discriminant) / 2).toLong()

        // in some cases, ceil/floor is not enough to reach a valid integer solution.
        // in that case, we correct +/- 1 further
        lowerBound = if (race.doesHoldTimeBeatRecord(lowerBound)) lowerBound else lowerBound + 1
        upperBound = if (race.doesHoldTimeBeatRecord(upperBound)) upperBound else upperBound - 1

        return upperBound - lowerBound + 1
    }

    private fun parse(input: String): List<Race> {
        val (times, distances) = input.lines().map { it.split(':').last() }.map { parseNumList(it) }

        return times.zip(distances) { time, distance -> Race(time, distance) }
    }

    private fun parseNumList(list: String): List<Long> {
        return list.split(' ') //
            .map { it.trim() } //
            .filter { it.isNotBlank() } //
            .map { it.toLong() }
    }

    private data class Race(val time: Long, val recordDistance: Long) {
        /**
         * Each unit held, increases speed by one unit,
         * thus speed = hold.
         *
         * @return `speed * (time - hold) > recordDistance`
         */
        fun doesHoldTimeBeatRecord(hold: Long): Boolean {
            return hold * (time - hold) > recordDistance
        }
    }
}