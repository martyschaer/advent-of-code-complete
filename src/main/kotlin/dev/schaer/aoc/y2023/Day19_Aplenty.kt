package dev.schaer.aoc.y2023

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer

fun main() {
    Day19_Aplenty().singleRun()
}

private const val ACCEPT = "A"
private const val REJECT = "R"
private const val IN = "in"

private val RULE_PATTERN = Regex("(\\w)([<>])(\\d+):(\\w+)")
private val WORKFLOW_PATTERN = Regex("^(\\w+)\\{(.*),(\\w+)}\$")

@Year(2023)
@Day(19)
class Day19_Aplenty : Solution {

    @Answer("449531")
    override fun partOne(input: String): Int {
        val (workflows, parts) = parse(input)

        val accepted = mutableListOf<Part>()

        for (part in parts) {
            var currentWorkflow = IN
            while (currentWorkflow != ACCEPT && currentWorkflow != REJECT) {
                val workflow = workflows[currentWorkflow] ?: error("Workflow '$currentWorkflow' doesn't exist")
                currentWorkflow = workflow.apply(part)
            }

            if (currentWorkflow == ACCEPT) {
                accepted.add(part)
            }
        }

        return accepted.sumOf { part -> part.attributes.values.sum() }
    }

    private fun parse(input: String): Pair<Map<String, Workflow>, List<Part>> {
        val (workflowsStr, partsStr) = input.split("\n\n")
        val workflows = workflowsStr.lines().map { parseWorkflow(it) }.associateBy { it.name }
        val parts = partsStr.lines().map { parsePart(it) }
        return Pair(workflows, parts)
    }

    private fun parseWorkflow(line: String): Workflow {
        val groups = WORKFLOW_PATTERN.find(line)?.groupValues ?: error("Unable to parse '$line'")
        val (_, name, rules, default) = groups

        return Workflow(name, rules.split(',').map { parseRule(it) }, default)
    }

    private fun parseRule(ruleStr: String): Pair<(Part) -> Boolean, String> {
        val groups = RULE_PATTERN.find(ruleStr)?.groupValues ?: error("Unable to parse '$ruleStr'")
        val (_, attr, symbol, valueStr, next) = groups
        val value = valueStr.toInt()

        val predicate: (Part) -> Boolean = when (symbol) {
            "<" -> { part -> part.attributes[attr]!! < value }
            ">" -> { part -> part.attributes[attr]!! > value }
            else -> error("Unknown symbol '$symbol'")
        }

        return Pair(predicate, next)
    }

    private fun parsePart(line: String): Part {
        val attributeStr = line.drop(1).dropLast(1)
        val attributes = attributeStr.split(',').associate { attr ->
            val (name, value) = attr.split('=')
            name to value.toInt()
        }
        return Part(attributes)
    }

    private data class Part(val attributes: Map<String, Int>)

    private data class Workflow(
        val name: String,
        val rules: List<Pair<(Part) -> Boolean, String>>,
        val default: String
    ) {
        fun apply(part: Part): String {
            for ((rule, nextflow) in rules) {
                if (rule(part)) {
                    return nextflow
                }
            }
            return default
        }
    }

    @Answer("122756210763577")
    override fun partTwo(input: String): Long {
        val workflows = parseWorkflows(input)

        val accepted = calculateAcceptablePartRanges(workflows)

        return accepted.sumOf { it.volume }
    }

    private fun calculateAcceptablePartRanges(
        workflows: Map<String, Pair<List<Splitter>, String>>
    ): MutableList<Hypercuboid> {
        val allParts = Hypercuboid(Dimension.values().associateWith { 1..4000 })

        val hypercuboidsToProcess = mutableListOf(allParts to IN)

        val accepted = mutableListOf<Hypercuboid>()

        while (hypercuboidsToProcess.isNotEmpty()) {
            val (hypercuboid, workflowName) = hypercuboidsToProcess.removeFirst()

            if (workflowName == ACCEPT) {
                accepted.add(hypercuboid)
                continue
            }

            if (workflowName == REJECT) {
                continue
            }

            val (rules, default) = workflows[workflowName] ?: error("Unable to find workflow '$workflowName'")

            var remaining = hypercuboid

            for (rule in rules) {
                if (remaining.isSplitBy(rule)) {
                    val (match, other) = remaining.split(rule)
                    hypercuboidsToProcess.add(match to rule.next)
                    remaining = other
                }
            }

            hypercuboidsToProcess.add(remaining to default)
        }

        return accepted
    }

    private fun parseWorkflows(input: String): Map<String, Pair<List<Splitter>, String>> {
        val workflowsStr = input.split("\n\n").first()
        val workflows = workflowsStr.lines().associate { line ->
            val (_, name, rules, default) = WORKFLOW_PATTERN.find(line)?.groupValues //
                ?: error("Unable to parse '$line'")

            val splitters = rules.split(',').map { ruleStr ->
                val (_, dimensionStr, symbol, valueStr, nextWorkflowName) = RULE_PATTERN.find(ruleStr)?.groupValues //
                    ?: error("Unable to parse '$ruleStr'")
                val value = valueStr.toInt()
                val dimension = Dimension.of(dimensionStr)
                Splitter(dimension, value, nextWorkflowName, symbol)
            }
            name to Pair(splitters, default)
        }
        return workflows
    }

    private data class Splitter(val dimension: Dimension, val value: Int, val next: String, val symbol: String)

    private data class Hypercuboid(val dimensions: Map<Dimension, IntRange>) {
        val volume: Long = dimensions.values.fold(1L) { acc, range -> acc * range.count() }

        fun isSplitBy(splitter: Splitter): Boolean {
            return splitter.value in dimensions[splitter.dimension]!!
        }

        fun split(splitter: Splitter): Pair<Hypercuboid, Hypercuboid> {
            val lower = Hypercuboid(lower(splitter))
            val upper = Hypercuboid(upper(splitter))
            return if (splitter.symbol == "<") lower to upper else upper to lower
        }

        private fun lower(splitter: Splitter): Map<Dimension, IntRange> {
            return dimensions.mapValues { (dimension, current) ->
                if (dimension != splitter.dimension) {
                    current
                } else if (splitter.symbol == "<") {
                    current.first..splitter.value - 1
                } else {
                    current.first..splitter.value
                }
            }
        }

        private fun upper(splitter: Splitter): Map<Dimension, IntRange> {
            return dimensions.mapValues { (dimension, current) ->
                if (dimension != splitter.dimension) {
                    current
                } else if (splitter.symbol == "<") {
                    splitter.value..current.last
                } else {
                    splitter.value + 1..current.last
                }
            }
        }
    }

    private enum class Dimension {
        X, M, A, S;

        companion object {
            fun of (string: String): Dimension {
                return Dimension.valueOf(string.uppercase())
            }
        }
    }
}