package dev.schaer.aoc.y2023

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import dev.schaer.aoc.datastructures.RangeTree

fun main() {
    Day05_SeedFertilizer().singleRun()
}

@Year(2023)
@Day(5)
class Day05_SeedFertilizer : Solution {
    @Answer("261668924")
    override fun partOne(input: String): Long {
        val packets = input.split("\n\n")

        val seeds = parseSeedFragments(packets.first())

        val mappings = packets.drop(1).map { parseMapping(it) }

        return applyMappingsToSeeds(seeds, mappings)
    }

    @Answer("24261545")
    override fun partTwo(input: String): Long {
        val packets = input.split("\n\n")

        val inputSeedRanges = parseSeedRanges(packets.first())

        val mappings = packets.drop(1).map { parseMapping(it) }
        val potentiallyMinimalSeeds = determinePotentialMinimalSeeds(mappings)

        // only actually process the potential seeds that are part of the problem input ranges
        val seeds = potentiallyMinimalSeeds.filter { inputSeedRanges.contains(it) }

        return applyMappingsToSeeds(seeds, mappings)
    }

    private data class Mapping(
        val name: String,
        val forwardRanges: RangeTree<Long>,
        val reverseRanges: RangeTree<Long>
    ) {
        val endPoints: List<Long> = run {
            val points = mutableSetOf<Long>()
            forwardRanges.ranges.map { it.first }.forEach {
                if (it.first != 0L) points.add(it.first - 1)
                points.add(it.first)
                points.add(it.last())
                points.add(it.last() + 1)
            }
            points.sorted()
        }

        /**
         * Checks if the given input is in a mapped forward range.
         * If yes, applies the delta, if no returns the input.
         */
        fun apply(input: Long): Long {
            val delta = forwardRanges.queryValue(input) ?: return input
            return input + delta
        }

        /**
         * Checks if the given input is in a mapped reverse range.
         * If yes, reverse applies the delta, if no returns the input.
         */
        fun reverse(input: Long): Long {
            val delta = reverseRanges.queryValue(input) ?: return input
            return input - delta
        }
    }

    private fun applyMappingsToSeeds(seeds: List<Long>, mappings: List<Mapping>): Long {
        return seeds.minOfOrNull { initialSeed ->
            mappings.fold(initialSeed) { seed, mapping -> mapping.apply(seed) }
        } ?: error("Unable to map seeds")
    }

    /**
     * Determine the potential input seeds that could lead to a minimal output location
     * by back-calculating the breakpoints of each mapping/function.
     */
    private fun determinePotentialMinimalSeeds(forwardMappings: List<Mapping>): Set<Long> {
        val mappings = forwardMappings.reversed()

        var pointsToCheck = mutableSetOf<Long>()
        pointsToCheck.add(0)
        pointsToCheck.addAll(mappings.first().endPoints)
        pointsToCheck.add(Long.MAX_VALUE)

        for (mapping in mappings.drop(1)) {
            pointsToCheck = pointsToCheck.map { mapping.reverse(it) }.toMutableSet()
            pointsToCheck.addAll(mapping.endPoints)
        }

        return pointsToCheck
    }

    private fun parseSeedRanges(seedPacket: String): RangeTree<Unit> {
        val seedRanges = parseSeedFragments(seedPacket)//
            .chunked(2) //
            .map { it.first() until it.first() + it.last() }

        return RangeTree.of(seedRanges)
    }

    private fun parseSeedFragments(seedPacket: String): List<Long> {
        return seedPacket //
            .split(':')[1] //
            .split(' ') //
            .filter { it.isNotEmpty() } //
            .map { it.toLong() }
    }

    private fun parseMapping(input: String): Mapping {
        val lines = input.lines()
        val name = lines.first().dropLast(1)

        val (forward, reverse) = lines.drop(1) //
            .map { parseRange(it) }
            .fold(Pair(mutableListOf<Pair<LongRange, Long>>(), mutableListOf<Pair<LongRange, Long>>())) { acc, triple ->
                acc.first.add(triple.first to triple.third)
                acc.second.add(triple.second to triple.third)
                acc
            }

        return Mapping(name, RangeTree(forward), RangeTree(reverse))
    }

    private fun parseRange(input: String): Triple<LongRange, LongRange, Long> {
        val (dstStr, srcStr, lenStr) = input.split(' ')

        val src = srcStr.toLong()
        val dst = dstStr.toLong()
        val delta = dst - src

        val len = lenStr.toLong()
        val srcRange = src until src + len
        val dstRange = dst until dst + len

        return Triple(srcRange, dstRange, delta)
    }
}