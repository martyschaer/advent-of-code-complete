package dev.schaer.aoc.y2023

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import dev.schaer.aoc.datastructures.Vector2
import dev.schaer.aoc.datastructures.transformPairwise

fun main() {
    Day11_CosmicExpansion().singleRun()
}

private const val GALAXY = '#'

@Year(2023)
@Day(11)
class Day11_CosmicExpansion : Solution {

    @Answer("9418609")
    override fun partOne(input: String): Long {
        return solve(input, 2)
    }

    @Answer("593821230983")
    override fun partTwo(input: String): Long {
        return solve(input, 1_000_000)
    }

    fun solve(input: String, expansionRatio: Int): Long {
        val lines = input.lines()
        val height = lines.size
        val width = lines.first().length
        val galaxies = parse(lines)

        val expanded: List<Vector2> = expandUniverse(galaxies, width, height, expansionRatio)

        return expanded.toList() //
            .transformPairwise { a, b -> (a - b).manhattanLength() } //
            .sumOf { it.toLong() }
    }

    private fun expandUniverse(galaxies: List<Vector2>, width: Int, height: Int, expansionRatio: Int): List<Vector2> {
        val colExpansion = calculateExpansions(height, galaxies.map { it.x }.toSet(), expansionRatio)
        val rowExpansion = calculateExpansions(width, galaxies.map { it.y }.toSet(), expansionRatio)

        return galaxies.map { galaxy ->
            Vector2(
                x = galaxy.x + (colExpansion[galaxy.x] ?: 0), //
                y = galaxy.y + (rowExpansion[galaxy.y] ?: 0)
            )
        }
    }

    private fun calculateExpansions(maxDistance: Int, occupied: Set<Int>, expansionRatio: Int): Map<Int, Int> {
        val expansionByDistance = mutableMapOf<Int, Int>()
        // each single-gap is `expansionRatio` times bigger.
        // expand by `expansionRatio - 1`, because the 1 is already included in the original data
        val expandBy = expansionRatio - 1
        var expansion = 0
        for (d in 0 until maxDistance) {
            if (d in occupied) {
                // for each occupied row/col, remember how much it is affected by the expansion
                expansionByDistance[d] = expansion
            } else {
                // each un-occupied row/col is expanded
                expansion += expandBy
            }
        }

        return expansionByDistance
    }

    private fun parse(lines: List<String>): List<Vector2> {
        val galaxies = mutableListOf<Vector2>()
        lines.forEachIndexed { y, row ->
            row.forEachIndexed { x, char ->
                if (char == GALAXY) {
                    galaxies += Vector2(x, y)
                }
            }
        }
        return galaxies
    }
}