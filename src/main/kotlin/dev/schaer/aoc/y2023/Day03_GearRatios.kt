package dev.schaer.aoc.y2023

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import dev.schaer.aoc.datastructures.Array2D
import dev.schaer.aoc.datastructures.parseIntoArray2dOfChars
import java.awt.Point

fun main() {
    Day03_GearRatios().singleRun()
}

private const val EMPTY = '.'
private const val GEAR = '*'
private val DIGIT = ('0'..'9')

@Year(2023)
@Day(3)
class Day03_GearRatios : Solution {

    @Answer("531561")
    override fun partOne(input: String): Int {
        val schematic = parseIntoArray2dOfChars(input, EMPTY)
        return schematic.values.mapIndexed { idx, char -> idx to char } //
            .filter { (_, char) -> !(char == EMPTY || char in DIGIT) } //
            .map { (idx, _) -> schematic.unmap(idx) } //
            .flatMap { idxOfSymbol -> findAllAdjacentNumbers(schematic, idxOfSymbol) } //
            .sum()
    }

    @Answer("83279367")
    override fun partTwo(input: String): Int {
        val schematic = parseIntoArray2dOfChars(input, EMPTY)
        return schematic.values.mapIndexed { idx, char -> idx to char } //
            .filter { (_, char) -> char == GEAR } //
            .map { (idx, _) -> schematic.unmap(idx) } //
            .sumOf { idxOfGear -> findGearRatioOrZero(schematic, idxOfGear) } //
    }

    private fun findGearRatioOrZero(schematic: Array2D<Char>, point: Point): Int {
        val adjacentNumbers = findAllAdjacentNumbers(schematic, point)
        return if (adjacentNumbers.size == 2) adjacentNumbers.first() * adjacentNumbers.last() else 0

    }

    /**
     * Assumes that there are only ever <= 2 adjacent numbers for each symbol
     */
    private fun findAllAdjacentNumbers(schematic: Array2D<Char>, point: Point): List<Int> {
        val adjacentNumbers = mutableMapOf<Int, Int>()
        for (yOff in -1..1) {
            for (xOff in -1..1) {
                val x = point.x + xOff
                val y = point.y + yOff
                if (schematic[x, y].isDigit()) {
                    adjacentNumbers += findWholeNumber(schematic, x, y)
                }
            }
        }
        return adjacentNumbers.values.toList()
    }

    /**
     * Finds the entire number associated with the digit at the given [x, y].
     *
     * @return a pair of (the starting index, the numbers value)
     */
    private fun findWholeNumber(schematic: Array2D<Char>, x: Int, y: Int): Pair<Int, Int> {
        var startIndex = x
        while (schematic[startIndex - 1, y].isDigit() && (startIndex - 1) >= 0) {
            startIndex--
        }
        var endIndex = x // inclusive
        while (schematic[endIndex + 1, y].isDigit() && (endIndex + 1) < schematic.width) {
            endIndex++
        }
        var number = ""
        for (idx in startIndex..endIndex) {
            number += schematic[idx, y]
        }
        return schematic.map(startIndex, y) to number.toInt()
    }
}