package dev.schaer.aoc.y2023

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import dev.schaer.aoc.datastructures.Array2D
import dev.schaer.aoc.y2023.Day20_PulsePropagation.Pulse.HI
import dev.schaer.aoc.y2023.Day20_PulsePropagation.Pulse.LO
import java.time.Duration
import java.util.*
import kotlin.collections.HashMap

fun main() {
    Day20_PulsePropagation().singleRun()
}

private const val BROADCAST = "broadcaster"
private const val RX = "rx"

@Year(2023)
@Day(20)
class Day20_PulsePropagation : Solution {

    @Answer("763500168")
    override fun partOne(input: String): Long {
        val modules = parseModules(input).associateBy { it.name }
        val adjacency = createAdjacencyMap(input, modules)

        val counter = PulseCounter()

        repeat(1000) {
            simulate(modules, adjacency, counter)
        }

        return counter.getLoCount() * counter.getHiCount()
    }

    override fun partTwo(input: String): Long {
        return 0
        // FIXME this would take approximately 18 years to run
        // assuming a solution of around 250'000'000'000'000
        // and a time/simulation of 2300ns
//        val modules = parseModules(input).associateBy { it.name }
//        val adjacency = createAdjacencyMap(input, modules)
//
//        var time = 0L
//        var simulations = 1L
//        while (!simulate(modules, adjacency)) {
//            val start = System.nanoTime()
//            val result = simulate(modules, adjacency)
//            val taken = System.nanoTime() - start
//            time += taken
//            if (result) {
//                break
//            }
//            if (simulations % 10_000L == 0L) {
//                val duration = Duration.ofNanos(time).dividedBy(simulations)
//                println("[$simulations] Took ${duration.toNanos()} ns per simulation")
//            }
//            simulations++
//        }
//
//        return simulations
    }

    private fun simulate(modules: Map<String, Module>, adjacency: Map<String, List<String>>, counter: PulseCounter? = null): Boolean {
        // source, pulse, target
        val eventQueue = EventQueue()

        // inject pulses from button
        eventQueue.offer("button", LO, BROADCAST)

        while (eventQueue.hasEvents()) {
            val (source, pulse, destination) = eventQueue.take()
            counter?.count(pulse)

            if (destination == RX && pulse == LO) {
                return true
            }

            modules[destination]?.process(pulse, source)?.let { outPulse ->
                adjacency[destination]?.forEach { nextDestination ->
                    eventQueue.offer(destination, outPulse, nextDestination)
                }
            }
        }

        return false
    }

    private fun createAdjacencyMap(input: String, modules: Map<String, Module>): Map<String, List<String>> {
        val adjacencyMap = HashMap<String, MutableList<String>>()

        input.lines().map { line ->
            val (sourceStr, drains) = line.split(" -> ")
            val sourceName = getName(sourceStr)

            drains.split(", ").forEach { drainName ->

                adjacencyMap.computeIfAbsent(sourceName) { _ -> mutableListOf() }.add(drainName)

                val drain = modules[drainName]!!
                if (drain is ConjunctionModule) {
                    drain.memory[sourceName] = LO
                }

            }
        }

        return adjacencyMap
    }

    private fun parseModules(input: String): List<Module> {
        val primaryModules = input.lines() //
            .map { it.split(" ->").first() } //
            .map { moduleDeclaration ->
                val name = getName(moduleDeclaration)
                when (moduleDeclaration.first()) {
                    '%' -> FlipFlopModule(name)
                    '&' -> ConjunctionModule(name)
                    else -> SimpleModule(name)
                }
            }

        val secondaryModules = input.lines() //
            .map { it.split("-> ").last() } //
            .map { name -> SimpleModule(name) }

        val modules = mutableListOf<Module>()

        modules.addAll(primaryModules)

        secondaryModules.forEach { module ->
            if (!modules.map { it.name }.contains(module.name)) {
                modules.add(module)
            }
        }

        return modules
    }

    private fun getName(module: String): String {
        return when (module.first()) {
            '%', '&' -> module.drop(1)
            else -> module
        }
    }

    private data class PulseCounter(private var lo: Long = 0L, private var hi: Long = 0L) {
        fun getLoCount() = lo
        fun getHiCount() = hi

        fun count(pulse: Pulse) = when (pulse) {
            LO -> lo++
            HI -> hi++
        }
    }

    private data class EventQueue(private val queue: Queue<Triple<String, Pulse, String>> = LinkedList()) {
        fun hasEvents() = queue.isNotEmpty()
        fun offer(source: String, pulse: Pulse, destination: String) = queue.add(Triple(source, pulse, destination))
        fun take() = queue.poll() ?: error("No more events in queue")
    }

    private enum class Pulse {
        LO, HI
    }

    private interface Module {
        val name: String
        var toSend: Pulse?
        fun process(pulse: Pulse, source: String): Pulse?
        override fun toString(): String
    }

    private data class SimpleModule(override val name: String, override var toSend: Pulse? = null) : Module {
        override fun process(pulse: Pulse, source: String): Pulse {
            return pulse
        }

        override fun toString(): String {
            return "Simple[name='$name', toSend=$toSend]"
        }
    }

    private data class FlipFlopModule(
        override val name: String, override var toSend: Pulse? = null, private var isOn: Boolean = false
    ) : Module {
        override fun process(pulse: Pulse, source: String): Pulse? {
            if (pulse == HI) {
                return null
            }

            isOn = !isOn
            return when (isOn) {
                false -> LO
                true -> HI
            }
        }

        override fun toString(): String {
            return "FlipFlop[name='$name', toSend=$toSend, state=${if (isOn) "ON" else "OFF"}]"
        }
    }

    private data class ConjunctionModule(override val name: String, override var toSend: Pulse? = null) : Module {
        val memory = mutableMapOf<String, Pulse>()

        override fun process(pulse: Pulse, source: String): Pulse {
            memory[source] = pulse
            return if (memory.values.all { it == HI }) LO else HI
        }

        override fun toString(): String {
            return "Conjunct[name='$name', toSend=$toSend, memory=${
                memory.map { (name, last) -> if (last == HI) name.uppercase() else name.lowercase() }.joinToString(",")
            }]"
        }
    }
}