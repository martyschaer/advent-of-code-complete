package dev.schaer.aoc.y2023

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer

fun main() {
    Day07_CamelCards().singleRun()
}

private const val HAND_SIZE = 5

@Year(2023)
@Day(7)
class Day07_CamelCards : Solution {

    @Answer("248422077")
    override fun partOne(input: String): Int {
        val hands = parse(input).sortedWith(Hand.partOneComparator)
        return (hands.size downTo 1).zip(hands) { rank, hand -> rank * hand.bid }.sum()
    }

    @Answer("249817836")
    override fun partTwo(input: String): Int {
        // replace Jacks (J) with Jokers (X)
        val hands = parse(input.replace('J', 'X')).sortedWith(Hand.partTwoComparator)
        return (hands.size downTo 1).zip(hands) { rank, hand -> rank * hand.bid }.sum()
    }

    private fun parse(input: String): List<Hand> {
        return input.lines().map { parseHand(it) }
    }

    private fun parseHand(line: String): Hand {
        val (cards, bid) = line.split(' ')
        return Hand(cards.map { Card.of(it) }, bid.toInt())
    }

    private enum class Card {
        A, K, Q, J, T, N9, N8, N7, N6, N5, N4, N3, N2, X;

        val hexOrdinal = ordinal.toString(16)

        companion object {
            private val numberRange = ('2'..'9')
            fun of(label: Char): Card {
                return when (label) {
                    in numberRange -> Card.valueOf("N$label")
                    else -> Card.valueOf(label.toString())
                }
            }
        }
    }

    private enum class Type {
        FIVE_OF_KIND, FOUR_OF_KIND, FULL_HOUSE, THREE_OF_KIND, TWO_PAIR, ONE_PAIR, HIGH_CARD;
    }

    private data class Hand(val cards: List<Card>, val bid: Int) {

        private val digits = cards.joinToString("") { it.hexOrdinal }
        private val type by lazy { determineHandType(cards) }
        private val typeWithJoker by lazy { determineHandTypeWithJoker(cards) }

        companion object {
            val partOneComparator = Comparator<Hand> { o1, o2 ->
                val typeComparison = o1.type.compareTo(o2.type)
                if (typeComparison != 0) typeComparison else o1.digits.compareTo(o2.digits)
            }

            val partTwoComparator = Comparator<Hand> { o1, o2 ->
                val typeComparison = o1.typeWithJoker.compareTo(o2.typeWithJoker)
                if (typeComparison != 0) typeComparison else o1.digits.compareTo(o2.digits)
            }
        }

        private fun determineHandType(cards: List<Card>): Type {
            return determineHandType(numberOfGroupsBySize(cards))
        }

        private fun determineHandTypeWithJoker(cards: List<Card>): Type {
            val groupsBySize = numberOfGroupsBySize(cards)
            val jokerCount = cards.count { it == Card.X }

            // if there are jokers and the hand isn't all jokers
            if (jokerCount in 1 until HAND_SIZE) {

                // splitting up a jokerCount-sized grouping (the jokers) to augment the other largest groupings
                groupsBySize[jokerCount]--

                var largestGroup = (HAND_SIZE downTo 1).first { groupsBySize[it] > 0 }

                for (i in (0 until jokerCount)) {
                    // use a joker to "promote" the largest group
                    groupsBySize[largestGroup]--
                    groupsBySize[++largestGroup]++
                }
            }

            return determineHandType(groupsBySize)
        }

        private fun numberOfGroupsBySize(cards: List<Card>): IntArray {
            val asArray = IntArray(HAND_SIZE + 1) { 0 }

            cards.groupingBy { it }.eachCount() //
                .values.groupingBy { it }.eachCount() //
                .entries.forEach { (size, count) -> asArray[size] = count }

            return asArray
        }

        private fun determineHandType(groupsOfSize: IntArray): Type {
            return when {
                groupsOfSize[1] == 5 -> Type.HIGH_CARD
                groupsOfSize[2] == 2 -> Type.TWO_PAIR
                groupsOfSize[3] == 1 && groupsOfSize[2] == 1 -> Type.FULL_HOUSE
                groupsOfSize[3] == 1 -> Type.THREE_OF_KIND
                groupsOfSize[2] == 1 -> Type.ONE_PAIR
                groupsOfSize[4] == 1 -> Type.FOUR_OF_KIND
                groupsOfSize[5] == 1 -> Type.FIVE_OF_KIND
                else -> error("No hand configuration for $groupsOfSize")
            }
        }
    }
}