package dev.schaer.aoc.y2023

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer

fun main() {
    Day01_Trebuchet().singleRun()
}

@Year(2023)
@Day(1)
class Day01_Trebuchet : Solution {
    private val replacements = listOf(
        "one" to "one1one",
        "two" to "two2two",
        "three" to "three3three",
        "four" to "four4four",
        "five" to "five5five",
        "six" to "six6six",
        "seven" to "seven7seven",
        "eight" to "eight8eight",
        "nine" to "nine9nine"
    )

    @Answer("54159")
    override fun partOne(input: String): Int {
        return input.lines().sumOf { extractNumberFromFirstAndLastDigit(it) }
    }

    @Answer("53866")
    override fun partTwo(input: String): Int {/*
         * Replace each word-digit with its numeric counterpart pre- and postfixed
         * with the word-digit. This avoids problems with overlapping digits
         * (e.g. "sevenineight")
         */
        var sanitized = input
        for ((original, replacement) in replacements) {
            sanitized = sanitized.replace(original, replacement)
        }
        return sanitized.lines().sumOf { extractNumberFromFirstAndLastDigit(it) }
    }

    private fun extractNumberFromFirstAndLastDigit(line: String): Int {
        val first = line.first { it in ('0'..'9') }
        val last = line.last { it in ('0'..'9') }
        return (first - '0') * 10 + (last - '0')
    }
}