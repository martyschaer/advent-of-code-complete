package dev.schaer.aoc.y2023

import dev.schaer.aoc.algorithms.innerArea
import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import dev.schaer.aoc.datastructures.Array2D
import dev.schaer.aoc.datastructures.Vector2
import dev.schaer.aoc.datastructures.any
import dev.schaer.aoc.model.Direction
import dev.schaer.aoc.model.Direction.Cardinal.*

fun main() {
    Day10_PipeMaze().singleRun()
}

private const val START = 'S'
private const val EMPTY = '.'

@Year(2023)
@Day(10)
class Day10_PipeMaze : Solution {

    @Answer("6870")
    override fun partOne(input: String): Int {
        val loop = findLoop(parse(input))
        return loop.size / 2
    }

    @Answer("287")
    override fun partTwo(input: String): Int {
        val loop = findLoop(parse(input))

        return innerArea(loop, loop.size.toLong()).toInt()
    }

    private fun findLoop(map: Array2D<Char>): List<Vector2> {
        val startingPoint = map.unmapToVec(map.values.indexOf(START))

        val loop = mutableListOf<Vector2>()
        var currentPos = startingPoint
        var currentDirection = determineFirstDirection(currentPos, map)
        do {
            loop += currentPos
            currentPos = currentPos + (currentDirection.vec)
            val pipe = map[currentPos]
            if (pipe == START) {
                break
            }
            currentDirection = determineNextDirection(pipe, currentDirection)
        } while (currentPos != startingPoint)

        return loop
    }

    private fun determineFirstDirection(start: Vector2, map: Array2D<Char>): Direction {
        return Direction.Cardinal.values().first { direction ->
            val symbol = map[start.add(direction.vec)]
            if (symbol != EMPTY) {
                determinePossibleDirections(symbol).any { it == direction.opposite }
            } else {
                false
            }
        }
    }

    private fun determineNextDirection(pipe: Char, lastDirection: Direction): Direction {
        val directions = determinePossibleDirections(pipe)

        return if (directions.first != lastDirection.opposite) {
            directions.first
        } else {
            directions.second
        }
    }

    private fun determinePossibleDirections(pipe: Char): Pair<Direction, Direction> {
        return when (pipe) {
            '|' -> Pair(NORTH, SOUTH)
            '-' -> Pair(EAST, WEST)
            'L' -> Pair(NORTH, EAST)
            'J' -> Pair(NORTH, WEST)
            '7' -> Pair(SOUTH, WEST)
            'F' -> Pair(SOUTH, EAST)
            else -> error("'$pipe' is not a known pipe configuration")
        }
    }

    private fun parse(input: String): Array2D<Char> {
        val lines = input.lines()
        val height = lines.size
        val width = lines.first().length

        return Array2D(input.replace("\n", "").toCharArray().toTypedArray(), width, height, default = '.')
    }
}