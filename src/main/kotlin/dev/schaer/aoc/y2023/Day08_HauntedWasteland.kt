package dev.schaer.aoc.y2023

import dev.schaer.aoc.algorithms.lcm
import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import dev.schaer.aoc.datastructures.repeat

fun main() {
    Day08_HauntedWasteland().singleRun()
}

@Year(2023)
@Day(8)
class Day08_HauntedWasteland : Solution {

    private val networkRegex = Regex("^(\\w{3}) = \\((\\w{3}), (\\w{3})\\)$")

    @Answer("13939")
    override fun partOne(input: String): Int {
        val (instructions, map) = parse(input)

        return countStepsToEnd(map, instructions, "AAA", end = { it == "ZZZ" })
    }

    @Answer("8906539031197")
    override fun partTwo(input: String): Long {
        val (instructions, map) = parse(input)

        val startingNodes = map.keys.filter { it.last() == 'A' }

        val steps = startingNodes.map { start ->
            countStepsToEnd(map, instructions, start, end = { it.last() == 'Z' }).toLong()
        }

        return lcm(steps)
    }

    private fun parse(input: String): Pair<Iterator<Char>, Map<String, Pair<String, String>>> {
        val lines = input.lines()
        val instructions = lines.first().asSequence().repeat().iterator()
        val map = lines.drop(2).associate { line ->
            val result = networkRegex.find(line)
            val (_, from, left, right) = result?.groupValues ?: error("Unable to match '$line'")
            from to (left to right)
        }

        return instructions to map
    }

    private fun countStepsToEnd(
        map: Map<String, Pair<String, String>>,
        instructions: Iterator<Char>,
        start: String,
        end: (String) -> Boolean
    ): Int {
        var steps = 0

        var currentNode = start
        while (!end.invoke(currentNode)) {
            val choices = map[currentNode] ?: error("'$currentNode' does not exist")
            currentNode = when (instructions.next()) {
                'L' -> choices.first
                'R' -> choices.second
                else -> error("Only 'L' and 'R' are valid instructions")
            }
            steps++
        }

        return steps
    }
}