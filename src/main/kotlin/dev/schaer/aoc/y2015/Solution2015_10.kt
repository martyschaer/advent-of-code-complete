package dev.schaer.aoc.y2015

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import dev.schaer.aoc.common.annotations.LongRunning

@LongRunning(5)
@Year(2015)
@Day(10)
class Solution2015_10 : Solution {

    @Answer("492982")
    override fun partOne(input: String): Int {
        var str = input
        for (i in 0 until 40) {
            str = elfLookElfSay(str)
        }
        return str.length
    }

    private fun elfLookElfSay(input: String): String {
        val sb = StringBuilder()
        val iterator = input.toCharArray().iterator()
        var lastChar = iterator.nextChar()
        var count = 1

        while (iterator.hasNext()) {
            val currentChar = iterator.nextChar()
            if (lastChar == currentChar) {
                count++
            } else {
                sb.append(count).append(lastChar)
                lastChar = currentChar
                count = 1
            }
        }
        sb.append(count).append(lastChar)

        return sb.toString()
    }

    @Answer("6989950")
    override fun partTwo(input: String): Int {
        var str = input
        for (i in 0 until 50) {
            str = elfLookElfSay(str)
        }
        return str.length
    }
}