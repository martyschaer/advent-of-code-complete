package dev.schaer.aoc.y2015

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import java.util.regex.Pattern

@Year(2015)
@Day(16)
class Solution2015_16 : Solution {

    private fun parse(input: String): List<Sue> {
        val regex = Pattern.compile("^Sue (\\d+): (.+)\$").toRegex()
        return input.split("\n").map { line ->
            val groups = regex.matchEntire(line)?.groupValues ?: throw Error("Unparsable: '$line'")
            return@map createSue(groups[1], groups[2])
        }
    }

    private fun createSue(number: String, properties: String): Sue {
        return Sue(number, properties.split(",").map { property ->
            val (key, value) = property.split(":").map { it.trim() }
            return@map key to value.toInt()
        }.toMap())
    }

    private fun constraints(): Map<String, Int> {
        val lines = """
           children: 3
            cats: 7
            samoyeds: 2
            pomeranians: 3
            akitas: 0
            vizslas: 0
            goldfish: 5
            trees: 3
            cars: 2
            perfumes: 1 
        """.trimIndent().split("\n")
        return lines.map { line ->
            val (name, value) = line.split(":").map { it.trim() }
            return@map name to value.toInt()
        }.toMap()
    }

    @Answer("103")
    override fun partOne(input: String): String {
        val sues = parse(input)
        val constraints = constraints()
        return sues.asSequence()
            .filter { sue -> constraints.all { filterByConstraint(sue, it.key, it.value) } }
            .first()
            .number
    }

    private fun filterByConstraint(sue: Sue, constraintName: String, constraintValue: Int): Boolean {
        return sue.properties[constraintName]?.equals(constraintValue) ?: true
    }

    @Answer("405")
    override fun partTwo(input: String): String {
        val sues = parse(input)
        val constraints = constraints()
        return sues.asSequence()
            .filter { sue -> constraints.all { filterByRangeConstraint(sue, it.key, it.value) } }
            .first()
            .number
    }

    private fun filterByRangeConstraint(sue: Sue, constraintName: String, constraintValue: Int): Boolean {
        if (constraintName == "cats" || constraintName == "trees") {
            return sue.properties[constraintName]?.let { it > constraintValue } ?: true
        } else if (constraintName == "pomeranians" || constraintName == "goldfish") {
            return sue.properties[constraintName]?.let { it < constraintValue } ?: true
        }
        return sue.properties[constraintName]?.equals(constraintValue) ?: true
    }

    private data class Sue(val number: String, val properties: Map<String, Int>)
}