package dev.schaer.aoc.y2015

import dev.schaer.aoc.algorithms.permutations
import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import java.util.regex.Pattern

@Year(2015)
@Day(13)
class Solution2015_13 : Solution {
    private val neighbours = HashMap<Pair<String, String>, Int>()
    private val people = HashSet<String>()

    private val parsingPattern =
        Pattern.compile("^(?<x>\\w+) would (?<op>gain|lose) (?<val>\\d+) happiness units by sitting next to (?<y>\\w+)\\.\$")
            .toRegex()

    override fun reset() {
        neighbours.clear()
        people.clear()
    }

    private fun parse(input: String) {
        input.split("\n").forEach { line ->
            val groups = parsingPattern.matchEntire(line)?.groupValues ?: throw Error("Unparsable '$line'")
            val x = groups[1]
            val operation = groups[2]
            val value = groups[3].toInt()
            val y = groups[4]
            neighbours[x to y] = if (operation == "gain") value else -value
            people.add(x)
        }
    }

    @Answer("664")
    override fun partOne(input: String): Int {
        parse(input)

        val seatingArrangements = permutations(people.toArray())

        return seatingArrangements.map {
            var deltaHappiness = neighbours[it[it.size - 1] to it[0]]!!
            deltaHappiness += neighbours[it[0] to it[it.size - 1]]!!
            for (i in 1 until it.size) {
                deltaHappiness += neighbours[it[i - 1] to it[i]]!!
                deltaHappiness += neighbours[it[i] to it[i - 1]]!!
            }
            return@map deltaHappiness
        }.maxOrNull() ?: error("No max happiness could be determined.")
    }

    @Answer("640")
    override fun partTwo(input: String): Int {
        parse(input)

        people.add("Me")

        val seatingArrangements = permutations(people.toArray())

        return seatingArrangements.map {
            var deltaHappiness = neighbours[it[it.size - 1] to it[0]] ?: 0
            deltaHappiness += neighbours[it[0] to it[it.size - 1]] ?: 0
            for (i in 1 until it.size) {
                deltaHappiness += neighbours[it[i - 1] to it[i]] ?: 0
                deltaHappiness += neighbours[it[i] to it[i - 1]] ?: 0
            }
            return@map deltaHappiness
        }.maxOrNull() ?: error("No max happiness could be determined.")

    }
}