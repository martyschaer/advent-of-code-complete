package dev.schaer.aoc.y2015

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import java.util.regex.Pattern

@Year(2015)
@Day(11)
class Solution2015_11 : Solution {
    private val pairPattern = Pattern.compile("^.*(\\w)\\1.*(\\w)\\2.*$").toRegex()

    @Answer("hxbxxyzz")
    override fun partOne(input: String): String {
        val password = input.toCharArray()
        while (!matchesRules(password)) {
            increment(password)
        }
        return String(password)
    }

    private fun matchesRules(password: CharArray): Boolean {
        var matched = false
        for (i in 2 until password.size) {
            matched = (password[i - 2] == password[i - 1] - 1)
                    && (password[i - 1] == password[i] - 1)
            if (matched) {
                break
            }
        }

        if (!matched) {
            return false
        }

        val passwordStr = String(password)

        return pairPattern.matches(passwordStr) && !('i' in password || 'o' in password || 'l' in password)
    }

    /**
     * Increments the given CharArray according to
     */
    fun increment(input: CharArray) {
        var ptr = input.size - 1
        while (ptr >= 0) {
            if (ptr == input.size - 1) {
                input[ptr]++
            } else {
                if (input[ptr + 1] == '{') {
                    input[ptr + 1] = 'a'
                    input[ptr]++
                }
            }
            ptr--
        }
    }

    @Answer("hxcaabcc")
    override fun partTwo(input: String): String {
        val password = partOne(input).toCharArray()
        increment(password)
        while (!matchesRules(password)) {
            increment(password)
        }
        return String(password)
    }
}