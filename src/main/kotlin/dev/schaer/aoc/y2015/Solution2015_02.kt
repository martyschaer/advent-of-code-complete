package dev.schaer.aoc.y2015

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer

@Year(2015)
@Day(2)
class Solution2015_02 : Solution {

    @Answer("1598415")
    override fun partOne(input: String): Int {
        return input.split("\n") //
            .map { it.split("x") }
            .map { Present(it[0].toInt(), it[1].toInt(), it[2].toInt()) }
            .map { it.wrappingPaperArea() + it.smallestSideArea() }
            .sum()
    }

    @Answer("3812909")
    override fun partTwo(input: String): Int {
        return input.split("\n") //
            .map { it.split("x") }
            .map { Present(it[0].toInt(), it[1].toInt(), it[2].toInt()) }
            .map { it.ribbonLength() }
            .sum()
    }

    data class Present(val l: Int, val w: Int, val h: Int) {
        fun wrappingPaperArea(): Int {
            return 2 * l * w + 2 * w * h + 2 * h * l
        }

        fun smallestSideArea(): Int {
            val smallEdges = listOf(l, w, h).sorted().take(2)
            return smallEdges[0] * smallEdges[1]
        }

        fun ribbonLength(): Int {
            val smallEdges = listOf(l, w, h).sorted().take(2)
            return (l * w * h) + 2 * smallEdges[0] + 2 * smallEdges[1]
        }
    }
}