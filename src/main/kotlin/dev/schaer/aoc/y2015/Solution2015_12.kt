package dev.schaer.aoc.y2015

import com.google.gson.JsonElement
import com.google.gson.JsonParser
import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import java.util.regex.Pattern

@Year(2015)
@Day(12)
class Solution2015_12 : Solution {
    private val numberPattern = Pattern.compile("(-?\\d+)").toRegex()

    @Answer("191164")
    override fun partOne(input: String): Int {
        return numberPattern.findAll(input)
            .map { it.value }
            .map { it.toInt() }
            .sum()
    }

    @Answer("87842")
    override fun partTwo(input: String): Int {
        val json = JsonParser.parseString(input)
        return sumUp(json)
    }

    private fun sumUp(element: JsonElement): Int {
        if (element.isJsonArray) {
            return element.asJsonArray.map { sumUp(it) }.sum()
        }
        if (element.isJsonObject) {
            val obj = element.asJsonObject
            if (obj.entrySet().none { e ->
                    e.value.isJsonPrimitive && e.value.asJsonPrimitive.isString && e.value.asJsonPrimitive.asString == "red"
                }) {
                return obj.keySet().map { sumUp(obj[it]) }.sum()
            }
        }
        if (element.isJsonPrimitive && element.asJsonPrimitive.isNumber) {
            return element.asJsonPrimitive.asInt
        }
        return 0
    }
}