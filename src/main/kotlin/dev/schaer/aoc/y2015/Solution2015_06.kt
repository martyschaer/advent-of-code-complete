package dev.schaer.aoc.y2015

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import dev.schaer.aoc.common.annotations.LongRunning
import java.util.regex.Pattern

@LongRunning(5)
@Year(2015)
@Day(6)
class Solution2015_06 : Solution {

    private val width = 1000
    private val height = 1000
    private val regex =
        Pattern.compile(".*(?<action>on|off|toggle)\\s(?<x1>\\d+),(?<y1>\\d+) through (?<x2>\\d+),(?<y2>\\d+).*")
            .toRegex()

    @Answer("569999")
    override fun partOne(input: String): Int {
        val lights = BooleanArray(width * height)
        for (instruction in input.split("\n")) {
            val groups = regex.matchEntire(instruction)!!.groups
            val action = groups["action"]!!.value
            val x1 = groups["x1"]!!.value.toInt()
            val y1 = groups["y1"]!!.value.toInt()
            val x2 = groups["x2"]!!.value.toInt()
            val y2 = groups["y2"]!!.value.toInt()
            for (y in y1..y2) {
                for (x in x1..x2) {
                    when (action) {
                        "on" -> lights[map(x, y)] = true
                        "off" -> lights[map(x, y)] = false
                        "toggle" -> lights[map(x, y)] = !lights[map(x, y)]
                    }
                }
            }
        }
        return lights.count { it }
    }

    @ExperimentalUnsignedTypes
    @Answer("17836115")
    override fun partTwo(input: String): UInt {
        val lights = UByteArray(width * height)
        for (instruction in input.split("\n")) {
            val groups = regex.matchEntire(instruction)!!.groups
            val action = groups["action"]!!.value
            val x1 = groups["x1"]!!.value.toInt()
            val y1 = groups["y1"]!!.value.toInt()
            val x2 = groups["x2"]!!.value.toInt()
            val y2 = groups["y2"]!!.value.toInt()
            for (y in y1..y2) {
                for (x in x1..x2) {
                    val idx = map(x, y)
                    when (action) {
                        "on" -> lights[idx]++
                        "off" -> {
                            if (lights[idx] >= 1u) {
                                lights[idx]--
                            }
                        }
                        "toggle" -> lights[idx] = (2u + lights[idx]).toUByte()
                    }
                }
            }
        }
        return lights.sum()
    }

    private fun map(x: Int, y: Int): Int {
        return x + y * width
    }
}