package dev.schaer.aoc.y2015

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import java.util.regex.Pattern

@Year(2015)
@Day(15)
class Solution2015_15 : Solution {

    private val parsingRegex =
        Pattern.compile("^(\\w+): capacity (-?\\d+), durability (-?\\d+), flavor (-?\\d+), texture (-?\\d+), calories (-?\\d+)\$")
            .toRegex()

    private fun parse(input: String): List<Ingredient> {
        return input.split("\n").map {
            val groups = parsingRegex.matchEntire(it)?.groupValues ?: throw Error("Unparsable: '$it'")
            return@map Ingredient(
                name = groups[1],
                capacity = groups[2].toInt(),
                durability = groups[3].toInt(),
                flavor = groups[4].toInt(),
                texture = groups[5].toInt(),
                calories = groups[6].toInt(),
            )
        }
    }

    @Answer("13882464")
    override fun partOne(input: String): Int {
        val ingredients = parse(input)
        var maxValue = Int.MIN_VALUE
        for (x in 0..100) {
            for (y in 0..100) {
                for (u in 0..100) {
                    for (v in 0..100) {
                        val score = evalPartOne(ingredients, x, y, u, v)
                        if (score > maxValue) {
                            maxValue = score
                        }
                    }
                }
            }
        }
        return maxValue
    }

    private fun evalPartOne(ingredients: List<Ingredient>, x: Int, y: Int, u: Int, v: Int): Int {
        if (x + y + u + v != 100) {
            return 0
        }

        val (sp, pb, fr, sg) = ingredients

        val capacity = sp.capacity * x + pb.capacity * y + fr.capacity * u + sg.capacity * v
        val durability = sp.durability * x + pb.durability * y + fr.durability * u + sg.durability * v
        val flavor = sp.flavor * x + pb.flavor * y + fr.flavor * u + sg.flavor * v
        val texture = sp.texture * x + pb.texture * y + fr.texture * u + sg.texture * v

        if (capacity < 0) return 0
        if (durability < 0) return 0
        if (flavor < 0) return 0
        if (texture < 0) return 0

        return capacity * durability * flavor * texture
    }

    @Answer("11171160")
    override fun partTwo(input: String): Int {
        val ingredients = parse(input)
        var maxValue = Int.MIN_VALUE
        for (x in 0..100) {
            for (y in 0..100) {
                for (u in 0..100) {
                    for (v in 0..100) {
                        val score = evalPartTwo(ingredients, x, y, u, v)
                        if (score > maxValue) {
                            maxValue = score
                        }
                    }
                }
            }
        }
        return maxValue
    }

    private fun evalPartTwo(ingredients: List<Ingredient>, x: Int, y: Int, u: Int, v: Int): Int {
        if (x + y + u + v != 100) {
            return 0
        }

        val (sp, pb, fr, sg) = ingredients

        val calories = sp.calories * x + pb.calories * y + fr.calories * u + sg.calories * v

        if (calories != 500) {
            return 0
        }

        val capacity = sp.capacity * x + pb.capacity * y + fr.capacity * u + sg.capacity * v
        val durability = sp.durability * x + pb.durability * y + fr.durability * u + sg.durability * v
        val flavor = sp.flavor * x + pb.flavor * y + fr.flavor * u + sg.flavor * v
        val texture = sp.texture * x + pb.texture * y + fr.texture * u + sg.texture * v

        if (capacity < 0) return 0
        if (durability < 0) return 0
        if (flavor < 0) return 0
        if (texture < 0) return 0

        return capacity * durability * flavor * texture
    }

    private class Ingredient(
        val name: String,
        val capacity: Int,
        val durability: Int,
        val flavor: Int,
        val texture: Int,
        val calories: Int
    ) {

    }
}