package dev.schaer.aoc.y2015

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import java.util.regex.Pattern

@Year(2015)
@Day(14)
class Solution2015_14 : Solution {

    private val parserRegex =
        Pattern.compile("^(\\w+) can fly (\\d+) km\\/s for (\\d+) seconds, but then must rest for (\\d+) seconds.\$")
            .toRegex()

    private fun parse(input: String): List<Reindeer> {
        return input.split("\n").map {
            val groups = parserRegex.matchEntire(it)?.groupValues ?: throw Error("Unparsable: $it")
            return@map Reindeer(
                speed = groups[2].toInt(),
                flyTime = groups[3].toInt(),
                restTime = groups[4].toInt()
            )
        }
    }

    @Answer("2640")
    override fun partOne(input: String): Int {
        val reindeer = parse(input)
        for (i in 0 until 2503) {
            reindeer.forEach { it.simulate() }
        }
        return reindeer
            .map { it.distanceTraveled }
            .maxOrNull() ?: error("Unable to find max")
    }

    @Answer("1102")
    override fun partTwo(input: String): Int {
        val reindeer = parse(input)
        for (i in 0 until 2503) {
            val maxDistance = reindeer
                .onEach { it.simulate() }
                .maxByOrNull { it.distanceTraveled }!!.distanceTraveled
            reindeer
                .sortedBy { it.distanceTraveled }
                .takeLastWhile { it.distanceTraveled == maxDistance }
                .forEach { it.points++ }
        }
        return reindeer
            .map { it.points }
            .maxOrNull() ?: error("Unable to find max")
    }

    private class Reindeer(
        private val speed: Int,
        private val flyTime: Int,
        private val restTime: Int
    ) {
        private var resting = false
        var distanceTraveled = 0
        private var timeToRest = restTime
        private var timeToTravel = flyTime
        var points = 0

        fun simulate() {
            if (resting) {
                timeToRest--
                if (timeToRest <= 0) {
                    resting = false
                    timeToRest = restTime
                }
            } else {
                distanceTraveled += speed
                timeToTravel--
                if (timeToTravel <= 0) {
                    resting = true
                    timeToTravel = flyTime
                }
            }
        }
    }
}