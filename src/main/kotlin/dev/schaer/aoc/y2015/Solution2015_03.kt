package dev.schaer.aoc.y2015

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer

@Year(2015)
@Day(3)
class Solution2015_03 : Solution {
    private val north = '^'
    private val east = '>'
    private val south = 'v'
    private val west = '<'

    @Answer("2081")
    override fun partOne(input: String): Int {
        val visited = hashSetOf<Pair<Int, Int>>()
        var x = 0
        var y = 0
        visited.add(0 to 0)
        for (instruction in input.toCharArray()) {
            when (instruction) {
                north -> y++
                east -> x++
                south -> y--
                west -> x--
            }
            visited.add(x to y)
        }
        return visited.size
    }

    @Answer("2341")
    override fun partTwo(input: String): Int {
        val visited = hashSetOf<Pair<Int, Int>>()
        var x = 0
        var y = 0
        var rx = 0
        var ry = 0
        visited.add(0 to 0)
        val elfInstructor = input.toCharArray().iterator()
        while (elfInstructor.hasNext()) {
            when (elfInstructor.nextChar()) {
                north -> y++
                east -> x++
                south -> y--
                west -> x--
            }
            visited.add(x to y)
            when (elfInstructor.nextChar()) {
                north -> ry++
                east -> rx++
                south -> ry--
                west -> rx--
            }
            visited.add(rx to ry)
        }
        return visited.size
    }
}