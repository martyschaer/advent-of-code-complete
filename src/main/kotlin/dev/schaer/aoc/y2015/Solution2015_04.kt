package dev.schaer.aoc.y2015

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import dev.schaer.aoc.common.annotations.LongRunning
import java.security.MessageDigest

fun main() {
    Solution2015_04().singleRun()
}

@Year(2015)
@Day(4)
@LongRunning
class Solution2015_04 : Solution {
    private val zero: Byte = 0

    @Answer("117946")
    override fun partOne(input: String): Int {
        val md = MessageDigest.getInstance("MD5")
        for (i in 0..Int.MAX_VALUE) {
            val md5 = md.digest("$input$i".toByteArray())
            if (hasFiveLeadingZeroes(md5)) {
                return i
            }
        }
        error("Could not find solution")
    }

    @Answer("3938038")
    override fun partTwo(input: String): Int {
        val md = MessageDigest.getInstance("MD5")!!
        for (i in 0..Int.MAX_VALUE) {
            val md5 = md.digest("$input$i".toByteArray())
            if (hasSixLeadingZeroes(md5)) {
                return i
            }
        }
        error("Could not find solution")
    }

    private fun hasFiveLeadingZeroes(bytes: ByteArray): Boolean {
        return bytes[0] == zero && bytes[1] == zero && (bytes[2].toInt() and 0xf0) == 0
    }

    private fun hasSixLeadingZeroes(bytes: ByteArray): Boolean {
        return bytes[0] == zero && bytes[1] == zero && bytes[2] == zero
    }
}

