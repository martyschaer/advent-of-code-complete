package dev.schaer.aoc.y2015

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import java.util.regex.Pattern

@ExperimentalStdlibApi
@ExperimentalUnsignedTypes
@Year(2015)
@Day(7)
class Solution2015_07 : Solution {
    private val components = HashMap<String, String>()
    private val cache = HashMap<String, UShort>()

    private val number = Pattern.compile("^\\d+$").toRegex()
    private val instructionPattern =
        Pattern.compile("^(((?<a>\\w+) )?(?<instruction>NOT|AND|OR|LSHIFT|RSHIFT) (?<b>\\w+))\$").toRegex()

    override fun reset() {
        components.clear()
        cache.clear()
    }

    @Answer("3176")
    override fun partOne(input: String): UShort {
        input.split("\n").forEach { line ->
            val (left, right) = line.split("->").map { it.trim() }
            components[right] = left
        }

        return eval("a")
    }

    private fun eval(wire: String): UShort {
        if (cache.containsKey(wire)) {
            return cache[wire]!!
        }
        if (wire.matches(number)) {
            return wire.toUShort()
        }
        val instruction = components[wire] ?: throw IllegalStateException("Unknown wire $wire")
        val match = instructionPattern.matchEntire(instruction)
            ?: return if (instruction.matches(number)) instruction.toUShort() else eval(instruction)
        val result = when (match.groups["instruction"]!!.value) {
            "NOT" -> eval(match.groups["b"]!!.value).inv()
            "AND" -> eval(match.groups["a"]!!.value).and(eval(match.groups["b"]!!.value))
            "OR" -> eval(match.groups["a"]!!.value).or(eval(match.groups["b"]!!.value))
            "LSHIFT" -> (eval(match.groups["a"]!!.value).toInt() shl match.groups["b"]!!.value.toInt()).toUShort()
            "RSHIFT" -> (eval(match.groups["a"]!!.value).toInt() shr match.groups["b"]!!.value.toInt()).toUShort()
            else -> throw IllegalArgumentException("Could not parse $wire -> $instruction")
        }
        cache[wire] = result
        return result
    }

    @Answer("14710")
    override fun partTwo(input: String): UShort {
        input.split("\n").forEach { line ->
            val (left, right) = line.split("->").map { it.trim() }
            components[right] = left
        }

        components["b"] = "3176"

        return eval("a")
    }
}

