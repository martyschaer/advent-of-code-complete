package dev.schaer.aoc.y2015

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import java.util.regex.Pattern

@Year(2015)
@Day(8)
class Solution2015_08 : Solution {

    private val escapeSequences = Pattern.compile("(\\\\x[a-f0-9]{2}|\\\\\\\\|\\\\\")").toRegex()
    private val quote = Pattern.compile("\\\"").toRegex()

    @Answer("1342")
    override fun partOne(input: String): Int {
        var memory = 0
        var stringLiterals = 0
        input.split("\n").forEach {
            memory += it.length
            stringLiterals += it.replace(escapeSequences, "_").replace(quote, "").length
        }
        return (memory - stringLiterals)
    }

    //TODO this regex is not working yet
    private val quotePattern = Pattern.compile("((?<!\\\\)\")").toRegex()

    override fun partTwo(input: String): Int {
        var memory = 0
        var doubleEncoded = 0
        input.split("\n").forEach {
            memory += it.length
            val str = it
                .replace("\\", "\\\\\\")
                .replace(quotePattern, "\"\\\"")

            doubleEncoded += str.length
        }
        return (doubleEncoded - memory)
    }
}