package dev.schaer.aoc.y2015

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import java.util.regex.Pattern

@Year(2015)
@Day(5)
class Solution2015_05 : Solution {

    @Answer("236")
    override fun partOne(input: String): Int {
        return input
            .split("\n")
            .count { partOneCriteriaMatches(it) }
    }

    private val forbiddenStrings = Pattern.compile(".*(ab|cd|pq|xy).*").toRegex()
    private val duplicateCharacter = Pattern.compile(".*(\\w)\\1.*").toRegex()
    private val vowels = listOf('a', 'e', 'i', 'o', 'u')

    private fun partOneCriteriaMatches(string: String): Boolean {
        if (string.matches(forbiddenStrings)) {
            return false
        }
        return string.matches(duplicateCharacter) && string.toCharArray() //
            .count { it in vowels } >= 3
    }

    private val twiceDuplicateCharacter = Pattern.compile(".*(\\w{2}).*\\1.*").toRegex()
    private val repeatsWithLetterInBetween = Pattern.compile(".*(\\w)\\w\\1.*").toRegex()

    @Answer("51")
    override fun partTwo(input: String): Int {
        return input
            .split("\n")
            .filter { it.matches(twiceDuplicateCharacter) }
            .filter { it.matches(repeatsWithLetterInBetween) }
            .count()
    }
}