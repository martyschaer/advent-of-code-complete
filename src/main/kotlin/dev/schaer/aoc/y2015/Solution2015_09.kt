package dev.schaer.aoc.y2015

import dev.schaer.aoc.algorithms.permutations
import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import dev.schaer.aoc.common.annotations.LongRunning
import java.util.regex.Pattern

@LongRunning
@Year(2015)
@Day(9)
class Solution2015_09 : Solution {

    private val parsePattern = Pattern.compile("^(?<a>\\w+) to (?<b>\\w+) = (?<d>\\d+)\$").toRegex()
    private val connections = HashMap<Pair<String, String>, Int>()

    override fun reset() {
        connections.clear()
    }

    @Answer("117")
    override fun partOne(input: String): Int {
        return parseInput(input)
            .map { calculateDistance(it) }
            .minOrNull()
            ?: error("No minimum found")
    }

    private fun parseInput(input: String): List<Array<String>> {
        val locations = HashSet<String>()
        input.split("\n").forEach {
            val groups = parsePattern.matchEntire(it)?.groups ?: throw Error("Didn't match '$it'")
            val nodeA = groups["a"]!!.value
            val nodeB = groups["b"]!!.value
            val distance = groups["d"]!!.value.toInt()
            connections[nodeA to nodeB] = distance
            connections[nodeB to nodeA] = distance
            locations.add(nodeA)
            locations.add(nodeB)
        }

        return permutations(locations.toTypedArray())
    }

    private fun calculateDistance(route: Array<String>): Int {
        var distance = 0
        for (i in 1 until route.size) {
            distance += connections[route[i - 1] to route[i]] ?: return Int.MAX_VALUE
        }
        return distance
    }

    @Answer("909")
    override fun partTwo(input: String): Int {
        return parseInput(input)
            .map { calculateDistance(it) }
            .maxOrNull()
            ?: error("No maximum found")
    }
}