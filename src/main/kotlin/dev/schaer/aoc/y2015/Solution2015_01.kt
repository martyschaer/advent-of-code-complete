package dev.schaer.aoc.y2015

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer

@Year(2015)
@Day(1)
class Solution2015_01 : Solution {

    private val up = '('
    private val down = ')'

    @Answer("74")
    override fun partOne(input: String): Int {
        val groups = input.toCharArray().groupBy { it }
        val up = groups[up]!!.size
        val down = groups[down]!!.size
        return (up - down)
    }

    @Answer("1795")
    override fun partTwo(input: String): Int {
        var floor = 0
        for (instruction in input.toCharArray().mapIndexed { idx, char -> idx to char }) {
            if (floor == -1) {
                return instruction.first
            }
            floor += if (instruction.second == up) 1 else -1
        }
        error("First negative floor not found")
    }
}