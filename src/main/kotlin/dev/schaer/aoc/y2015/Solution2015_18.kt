package dev.schaer.aoc.y2015

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer

@Year(2015)
@Day(18)
class Solution2015_18 : Solution {
    private val sideLength = 100

    private fun parse(input: String): IntArray {
        val chars = input.replace("\n", "")
        return IntArray(sideLength * sideLength) { if (chars[it] == '#') 1 else 0 }
    }

    operator fun IntArray.get(x: Int, y: Int): Int {
        val idx = y * sideLength + x
        if (x < 0 || y < 0 || x >= sideLength || y >= sideLength) {
            return 0
        }
        return get(idx)
    }

    operator fun IntArray.set(x: Int, y: Int, value: Int) {
        set(y * sideLength + x, value)
    }

    @Answer("1061")
    override fun partOne(input: String): Int {
        var cells = parse(input)

        for (step in 1..100) {
            cells = step1(cells)
        }

        return cells.count { it == 1 }
    }

    private fun step1(cells: IntArray): IntArray {
        val result = cells.copyOf()
        for (y in 0 until sideLength) {
            for (x in 0 until sideLength) {
                val neighbours = countNeighbours(x, y, cells)
                if (cells[x, y] == 1 && !(neighbours == 2 || neighbours == 3)) {
                    result[x, y] = 0
                } else if (cells[x, y] == 0 && neighbours == 3) {
                    result[x, y] = 1
                }
            }
        }
        return result
    }

    private fun countNeighbours(x: Int, y: Int, cells: IntArray): Int {
        return cells[x - 1, y - 1] +
                cells[x - 1, y] +
                cells[x - 1, y + 1] +
                cells[x, y - 1] +
                cells[x, y + 1] +
                cells[x + 1, y - 1] +
                cells[x + 1, y] +
                cells[x + 1, y + 1]
    }

    @Answer("1006")
    override fun partTwo(input: String): Int {
        var cells = parse(input)

        for (step in 1..100) {
            cells = step2(cells)
        }

        return cells.count { it == 1 }
    }

    private fun step2(cells: IntArray): IntArray {
        val result = cells.copyOf()
        for (y in 0 until sideLength) {
            for (x in 0 until sideLength) {
                val neighbours = countNeighbours(x, y, cells)
                if (cells[x, y] == 1 && !(neighbours == 2 || neighbours == 3)) {
                    result[x, y] = 0
                } else if (cells[x, y] == 0 && neighbours == 3) {
                    result[x, y] = 1
                }
            }
        }
        result[0, 0] = 1
        result[0, sideLength - 1] = 1
        result[sideLength - 1, 0] = 1
        result[sideLength - 1, sideLength - 1] = 1
        return result
    }
}