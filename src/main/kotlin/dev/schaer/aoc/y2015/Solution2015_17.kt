package dev.schaer.aoc.y2015

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer

@Year(2015)
@Day(17)
class Solution2015_17 : Solution {
    private var bitMasks: IntArray = IntArray(0)

    private fun parse(input: String): List<Int> {
        return input.split("\n")
            .map { it.toInt() }
            .sorted()
    }

    @Answer("1304")
    override fun partOne(input: String): Int {
        val containers = parse(input)
        bitMasks = containers.indices.map { 1 shl it }.toIntArray()

        var combinations = 0

        for (i in 1..(1 shl containers.size)) {// 2^20 - 1
            var sum = 0
            for (offset in 0 until 20) {
                if (i and (bitMasks[offset]) != 0) {
                    sum += containers[offset]
                }
            }
            if (sum == 150) {
                combinations++
            }
        }
        return combinations
    }

    @Answer("18")
    override fun partTwo(input: String): Int {
        val containers = parse(input)
        bitMasks = containers.indices.map { 1 shl it }.toIntArray()

        var combinations = HashMap<Int, Int>()
        var minContainerAmount = Int.MAX_VALUE

        for (i in 1..(1 shl containers.size)) {// 2^20 - 1
            var sum = 0
            var combination = 0
            var containerAmount = 0
            for (offset in 0 until 20) {
                if (i and (bitMasks[offset]) != 0) {
                    sum += containers[offset]
                    combination = combination or bitMasks[offset]
                    containerAmount++
                }
            }
            if (sum == 150) {
                combinations[combination] = containerAmount

                if (containerAmount < minContainerAmount) {
                    minContainerAmount = containerAmount
                }
            }
        }

        return combinations.filterValues { it == minContainerAmount }.count()
    }
}