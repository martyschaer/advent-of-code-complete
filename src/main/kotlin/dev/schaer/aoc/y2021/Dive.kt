package dev.schaer.aoc.y2021

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer

fun main() {
    Dive().singleRun()
}

@Year(2021)
@Day(2)
class Dive : Solution {

    @Answer("1636725")
    override fun partOne(input: String): Int {
        var horizontal  = 0
        var depth = 0
        for (line in input.lines()) {
            val (command, value) = line.split(" ")
            when (command) {
                "forward" -> horizontal += value.toInt()
                "down" -> depth += value.toInt()
                "up" -> depth -= value.toInt()
                else -> error("Unparsable command '$line'")
            }
        }
        return horizontal * depth
    }

    @Answer("1872757425")
    override fun partTwo(input: String): Int {
        var horizontal  = 0
        var depth = 0
        var aim = 0
        for (line in input.lines()) {
            val (command, arg) = line.split(" ")
            val value = arg.toInt()
            when (command) {
                "forward" -> {
                    horizontal += value
                    depth += (aim * value)
                }
                "down" -> aim += value
                "up" -> aim -= value
                else -> error("Unparsable command '$line'")
            }
        }
        return horizontal * depth
    }
}