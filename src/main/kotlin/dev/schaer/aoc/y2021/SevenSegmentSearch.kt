package dev.schaer.aoc.y2021

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import dev.schaer.aoc.datastructures.charSet

fun main() {
    SevenSegmentSearch().singleRun()
}

@Year(2021)
@Day(8)
class SevenSegmentSearch : Solution {

    @Answer("294")
    override fun partOne(input: String): Int {
        return input.lines()
            .map { it.split('|')[1] }
            .flatMap { line ->
                line.split(' ')
                    .map { it.trim() }
                    .map { it.length }
            }
            .count { it in (2..4) || it == 7 }
    }

    @Answer("973292")
    override fun partTwo(input: String): Int {
        return input.lines().sumOf { decodeLine(it) }
    }

    private fun decodeLine(input: String): Int {
        val parts = input.split('|')
        val unique = parts[0].trim()
            .split(' ')
            .map { it.charSet() }

        val one = unique.single { it.size == 2 }
        val four = unique.single { it.size == 4 }
        val seven = unique.single { it.size == 3 }
        val eight = unique.single { it.size == 7 }
        val lShape = four.subtract(one)

        val lenFive = unique.filter { it.size == 5 }
        val three = lenFive.single { it.intersect(one) == one }
        val five = lenFive.single { it.intersect(lShape) == lShape }
        val two = lenFive.single { it != three && it != five }

        val lenSix = unique.filter { it.size == 6 }
        val nine = lenSix.single { it.intersect(four) == four }
        val six = lenSix.single { it != nine && it.intersect(lShape) == lShape }
        val zero = lenSix.single { it != nine && it != six}

        val digits = parts[1].trim()
            .split(' ')
            .map { it.charSet() }

        return digits.joinToString(separator = "") { digit ->
                when (digit) {
                    zero -> "0"
                    one -> "1"
                    two -> "2"
                    three -> "3"
                    four -> "4"
                    five -> "5"
                    six -> "6"
                    seven -> "7"
                    eight -> "8"
                    nine -> "9"
                    else -> error("Couldn't match $digit to any number")
                }
            }
            .toInt()
    }
}