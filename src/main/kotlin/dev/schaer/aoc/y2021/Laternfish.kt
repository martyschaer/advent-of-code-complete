package dev.schaer.aoc.y2021

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer

fun main() {
    Lanternfish().singleRun()
}

@Year(2021)
@Day(6)
class Lanternfish : Solution {

    @Answer("386536")
    override fun partOne(input: String): Long {
        return solve(input, 80)
    }

    @Answer("1732821262171")
    override fun partTwo(input: String): Long {
        return solve(input, 256)
    }

    private fun solve(input: String, numberOfDays: Int): Long {
        val fishByAge = LongArray(9)

        for(fish in input.split(',').map { it.toInt() }) {
            fishByAge[fish]++
        }

        for(t in (0 until numberOfDays)) {
            val reproducing = fishByAge[0]
            for(i in 1 until fishByAge.size) {
                fishByAge[i - 1] = fishByAge[i]
            }
            fishByAge[8] = reproducing
            fishByAge[6] += reproducing
        }

        return fishByAge.sum()
    }
}