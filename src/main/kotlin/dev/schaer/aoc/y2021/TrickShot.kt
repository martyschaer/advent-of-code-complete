package dev.schaer.aoc.y2021

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import java.awt.Point
import kotlin.math.max
import kotlin.math.min

fun main() {
    TrickShot().singleRun()
}

@Year(2021)
@Day(17)
class TrickShot : Solution {

    @Answer("2628")
    override fun partOne(input: String): Int {
        val target = parseTargetArea(input)

        var maxY = 0
        for (x in (0..target.xRange.last)) {
            for (y in (target.yRange.first..-target.yRange.first)) {
                val localMaxY = simulate(x, y, target)
                if (localMaxY > maxY) {
                    maxY = localMaxY
                }
            }
        }
        return maxY
    }


    @Answer("1334")
    override fun partTwo(input: String): Int {
        return calculateValidLaunchVectors(input).size
    }

    fun calculateValidLaunchVectors(input: String): Set<Point> {
        val target = parseTargetArea(input)

        val validLaunchVelocities = mutableSetOf<Point>()

        for (x in (0..target.xRange.last)) {
            for (y in (target.yRange.first..target.yRange.first * -2)) {
                val reachesTarget = simulate(x, y, target) >= 0
                if (reachesTarget) {
                    validLaunchVelocities.add(Point(x, y))
                }
            }
        }

        return validLaunchVelocities
    }

    private fun simulate(initialX: Int, initialY: Int, target: Target): Int {
        var maxY = 0
        var posX = 0
        var posY = 0
        var velX = initialX
        var velY = initialY

        while(true) {
            if (target.isInside(posX, posY)) {
                return maxY
            } else if(!target.isReachable(posX, posY)) {
                return -1
            }

            posX += velX
            posY += velY

            if (posY > maxY) {
                maxY = posY
            }

            if(velX > 0) {
                velX--
            } else if(velX < 0) {
                velX++
            }

            velY--
        }
    }

    private fun parseTargetArea(input: String): Target {
        val (x, y) = input.split(',').map { parseAxis(it) }
        return Target(x, y)
    }

    private fun parseAxis(input: String): IntRange {
        val values = input.split('=')[1]
        val (a, b) = values.split("..").map { it.toInt() }
        return min(a, b)..max(a, b)
    }
}

private data class Target(val xRange: IntRange, val yRange: IntRange) {
    fun isInside(x: Int, y: Int): Boolean {
        return (x in xRange) && (y in yRange)
    }

    fun isReachable(x: Int, y: Int): Boolean {
        return x <=xRange.last && y >= yRange.first
    }
}