package dev.schaer.aoc.y2021

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import dev.schaer.aoc.datastructures.Array2D
import java.awt.Point
import java.util.*

fun main() {
    Chiton().singleRun()
}

@Year(2021)
@Day(15)
class Chiton : Solution {

    @Answer("562")
    override fun partOne(input: String): Int {
        val dimensions = input.lines().size
        val cave = input.replace("\n", "")
            .map { it - '0' }
            .toTypedArray()
        val graph = Array2D(cave, width = dimensions, height = dimensions, default = -1)
        val path = findShortestPath(graph, Point(0, 0), Point(dimensions - 1, dimensions - 1))
        return calculateCostOfShortestPath(graph, path)
    }


    @Answer("2874")
    override fun partTwo(input: String): Int {
        val dimensions = input.lines().size
        val cave = input.replace("\n", "")
            .map { it - '0' }
            .toTypedArray()
        val graph = buildRealMap(Array2D(cave, width = dimensions, height = dimensions, default = -1))
        val path = findShortestPath(graph, Point(0, 0), Point(graph.width - 1, graph.height - 1))
        return calculateCostOfShortestPath(graph, path)
    }

    fun buildRealMap(graph: Array2D<Int>): Array2D<Int> {
        val scale = 5
        val scaledWidth = graph.width * scale
        val scaledHeight = graph.height * scale
        val realMap = Array2D(Array(scaledWidth * scaledHeight) { 0 }, scaledWidth, scaledHeight, -1)

        for(y in 0 until graph.height) {
            for(x in 0 until graph.width) {
                for(tileY in 0 until scale) {
                    for(tileX in 0 until scale) {
                        var newValue = graph[x, y] + tileX + tileY
                        if(newValue > 9) {
                            newValue %= 9
                        }
                        realMap[x + tileX * graph.width, y + tileY * graph.height] = newValue
                    }
                }
            }
        }

        return realMap
    }

    private fun calculateCostOfShortestPath(graph: Array2D<Int>, path: Stack<Int>): Int {
        return path.dropLast(1).sumOf { graph[it] }
    }

    private fun findShortestPath(graph: Array2D<Int>, source: Point, target: Point): Stack<Int> {
        val (_, prev) = dijkstra(graph, source)
        val shortestPath = Stack<Int>()
        var u = graph.map(target)
        if (prev[u] != Int.MIN_VALUE || u == graph.map(source)) {
            while (u != Int.MIN_VALUE) {
                shortestPath.push(u)
                u = prev[u]
            }
        }

        return shortestPath
    }

    private fun dijkstra(graph: Array2D<Int>, source: Point): Pair<IntArray, IntArray> {
        val queue = PrioQueue<Int>()
        val dist = IntArray(graph.values.size) { Int.MAX_VALUE }
        dist[0] = graph.map(source)

        val prev = IntArray(graph.values.size) { Int.MIN_VALUE }

        for (v in graph.values.indices) {
            queue.addWithPriority(v, dist[v])
        }

        while (queue.isNotEmpty()) {
            val u = queue.getMinimum()

            for (v in graph.getOrthogonalNeighbours(u)) {
                val alt = dist[u] + graph[v]
                if (alt < dist[v]) {
                    dist[v] = alt
                    prev[v] = u
                    queue.decreasePriority(v, alt)
                }
            }
        }

        return dist to prev

    }
}

class PrioQueue<T> {
    private data class Node<T>(val value: T, val priority: Int, var removed: Boolean = false)

    private val index = HashMap<T, Node<T>>()
    private val queue = PriorityQueue<Node<T>>(Comparator.comparing { it.priority })

    fun addWithPriority(value: T, priority: Int) {
        val node = Node(value, priority)
        index[value] = Node(value, priority)
        queue.add(node)
    }

    fun isNotEmpty(): Boolean {
        return queue.isNotEmpty()
    }

    fun decreasePriority(value: T, newPriority: Int) {
        index[value]?.let { oldNode ->
            oldNode.removed = true
            val newNode = Node(value, newPriority)
            index[value] = newNode
            queue.add(newNode)
        }
    }

    fun getMinimum(): T {
        var min: Node<T>?
        do{
            min = queue.poll()
        }while (min?.removed == true)
        return min!!.value
    }
}
