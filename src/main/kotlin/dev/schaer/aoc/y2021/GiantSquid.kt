package dev.schaer.aoc.y2021

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import dev.schaer.aoc.datastructures.tail

fun main() {
    GiantSquid().singleRun()
}

@Year(2021)
@Day(4)
class GiantSquid : Solution {

    @Answer("2745")
    override fun partOne(input: String): Int {
        val tokens = input.split("\n\n")
        val numbers = tokens[0].split(",").map { it.toInt() }
        val cards = tokens.tail().map { parseCard(it) }
        for (number in numbers) {
            cards.forEach { it.playNumber(number) }
            val winner = cards.firstOrNull { it.hasWon() }
            if(winner != null) {
                return number * winner.calcUnplayedSum()
            }
        }
        error("No winner could be determined")
    }

    @Answer("6594")
    override fun partTwo(input: String): Int {
        val tokens = input.split("\n\n")
        val numbers = tokens[0].split(",").map { it.toInt() }
        val cards = tokens.tail().map { parseCard(it) }.toMutableList()
        for (number in numbers) {
            cards.forEach { it.playNumber(number) }

            if(cards.all { it.hasWon() }) {
                val loser = cards.singleOrNull() ?: error("Unexpectedly multiple losers")
                return number * loser.calcUnplayedSum()
            }

            cards.removeIf{it.hasWon()}
        }
        error("No winner could be determined")
    }

    private fun parseCard(token: String): BingoCard {
        return BingoCard(token.replace('\n', ' ')
            .split(" ")
            .map { it.trim() }
            .filter { it.isNotBlank() }
            .map { it.toInt() })
    }
}

class BingoCard(input: List<Int>) {
    val width = 5
    val height = 5
    private val idx = mutableMapOf<Int, Field>()
    private val numbers: Array<Field>

    init {
        numbers = Array(input.size) {
            val field = Field(input[it])
            idx[field.number] = field
            field
        }
    }

    fun calcUnplayedSum() : Int {
        return numbers.filter { !it.played }.sumOf { it.number }
    }

    fun playNumber(number: Int) {
        idx[number]?.played = true
    }

    fun hasWon(): Boolean {
        // check rows
        for (row in 0 until height) {
            val i = row * width
            if (
                numbers[i].played
                && numbers[i + 1].played
                && numbers[i + 2].played
                && numbers[i + 3].played
                && numbers[i + 4].played
            ) {
                return true
            }
        }

        // check columns
        for (col in 0 until width) {
            if (
                numbers[col].played
                && numbers[col + width * 1].played
                && numbers[col + width * 2].played
                && numbers[col + width * 3].played
                && numbers[col + width * 4].played
            ) {
                return true
            }
        }

        return false
    }

    private data class Field(val number: Int, var played: Boolean = false)
}