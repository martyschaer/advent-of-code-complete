package dev.schaer.aoc.y2021

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import dev.schaer.aoc.datastructures.Array2D

fun main() {
    DumboOctopus().singleRun()
}

private const val width = 10
private const val height = 10
private const val default = Int.MIN_VALUE

@Year(2021)
@Day(11)
class DumboOctopus : Solution {

    @Answer("1694")
    override fun partOne(input: String): Long {
        val octopi = Array2D(input.replace("\n", "").map { it - '0' }.toTypedArray(), width, height, default) {
            if(it in 10..99) "*" else if(it >= 1000) "!" else it.toString()
        }

        var flashes = 0L
        (1..100).forEach { _ ->
            flashes += step(octopi)
        }

        return flashes
    }

    @Answer("346")
    override fun partTwo(input: String): Int {
        val octopi = Array2D(input.replace("\n", "").map { it - '0' }.toTypedArray(), width, height, default) {
            if(it in 10..99) "*" else if(it >= 1000) "!" else it.toString()
        }

        (1..Int.MAX_VALUE).forEach { step ->
            if(step(octopi) == (width * height).toLong()) {
                return step
            }
        }

        error("No synchronous flash observed")
    }

    fun step(octopi: Array2D<Int>): Long {
        octopi.values.forEachIndexed { idx, it -> octopi.values[idx] = it + 1 }

        var anyFlashes: Boolean

        do {
            anyFlashes = false
            for (y in 0 until height) {
                for (x in 0 until width) {
                    if (octopi[x, y] in 10..999) {
                        anyFlashes = true
                        pump(octopi, x, y)
                    }
                }
            }
        } while (anyFlashes)

        var flashes = 0L

        octopi.values.forEachIndexed { idx, it ->
            if (it >= 1000) {
                octopi.values[idx] = 0
                flashes++
            }
        }
        return flashes
    }

    private fun pump(octopi: Array2D<Int>, x: Int, y: Int) {
        octopi[x, y] = 1000
        octopi[x - 1, y]++ // W
        octopi[x - 1, y - 1]++ // NW
        octopi[x, y - 1]++ // N
        octopi[x + 1, y - 1]++// NE
        octopi[x + 1, y]++ // E
        octopi[x + 1, y + 1]++// SE
        octopi[x, y + 1]++ // S
        octopi[x - 1, y + 1]++ // SW
    }
}