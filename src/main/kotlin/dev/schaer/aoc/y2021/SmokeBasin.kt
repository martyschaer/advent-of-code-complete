package dev.schaer.aoc.y2021

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import dev.schaer.aoc.datastructures.Array2D

fun main() {
    SmokeBasin().singleRun()
}

@Year(2021)
@Day(9)
class SmokeBasin : Solution {

    @Answer("522")
    override fun partOne(input: String): Int {
        val heightMap = parseInput(input)
        return findLowPoints(heightMap)
            .values
            .sumOf { it + 1 }
    }

    @Answer("916688")
    override fun partTwo(input: String): Int {
        val heightMap = parseInput(input)
        val lowPoints = findLowPoints(heightMap)
        return lowPoints.keys
            .map { floodFillBasin(heightMap, it) }
            .map { it.size }
            .sorted()
            .takeLast(3)
            .fold(1) {acc, i -> acc * i}
    }

    private fun floodFillBasin(heightMap: Array2D<Int>, lowPoint: Pair<Int, Int>): Set<Pair<Int, Int>> {
        val visited = mutableSetOf<Pair<Int, Int>>()
        fill(heightMap, lowPoint, visited)
        return visited
    }

    private fun fill(heightMap: Array2D<Int>, point: Pair<Int, Int>, visited: MutableSet<Pair<Int, Int>>) {
        val x = point.first
        val y = point.second
        if(visited.contains(point) || heightMap[x, y] >= 9) {
            return
        }
        visited.add(point)
        fill(heightMap, x - 1 to y, visited) // left
        fill(heightMap, x to y - 1, visited) // up
        fill(heightMap, x + 1 to y, visited) // right
        fill(heightMap, x to y + 1, visited) // down
    }

    private fun findLowPoints(heightMap: Array2D<Int>): Map<Pair<Int, Int>, Int> {
        val lowPoints = mutableMapOf<Pair<Int, Int>, Int>()

        for (y in 0 until heightMap.height) {
            for (x in 0 until heightMap.width) {
                val value = heightMap[x, y]
                if(value < heightMap[x-1, y]
                    && value < heightMap[x+1, y]
                    && value < heightMap[x, y-1]
                    && value < heightMap[x, y+1]) {
                    lowPoints[x to y] = value
                }
            }
        }
        return lowPoints
    }

    private fun parseInput(input: String): Array2D<Int> {
        val lines = input.lines()
        val height = lines.size
        val width = lines[0].length
        return Array2D(
            values = input.replace("\n", "")
                .map { it - '0' }
                .toTypedArray(),
            width = width,
            height = height,
            default = 10)
    }
}