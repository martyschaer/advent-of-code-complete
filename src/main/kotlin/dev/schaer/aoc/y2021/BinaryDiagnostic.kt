package dev.schaer.aoc.y2021

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer

fun main() {
    BinaryDiagnostic().singleRun()
}

@Year(2021)
@Day(3)
class BinaryDiagnostic : Solution {

    @Answer("4147524")
    override fun partOne(input: String): Int {
        val result = getMostAndLeastCommonBits(input)
        val gammaRate = result.first.toInt(2)
        val epsilonRate = result.second.toInt(2)

        return gammaRate * epsilonRate
    }

    @Answer("3570354")
    override fun partTwo(input: String): Int {
        val lines = input.lines()
        val o2 = findRating(lines, 1 to 0).toInt(2)
        val co2 = findRating(lines, 0 to 1).toInt(2)
        return o2 * co2
    }

    private fun findRating(lines: List<String>, bitPreference: Pair<Int, Int>, idx: Int = 0): String {
        if (lines.isEmpty()) {
            error("Could not find rating")
        }

        if (lines.size == 1) {
            return lines[0]
        }

        val bits = lines.map { it to it[idx] - '0' }
        val passBit = if (bits.sumOf { it.second } * 2 >= lines.size) bitPreference.first else bitPreference.second
        return findRating(bits.filter { it.second == passBit }.map { it.first }, bitPreference, idx + 1)
    }

    private fun getMostAndLeastCommonBits(input: String): Pair<String, String> {
        val lines = input.lines()
        val oneCountByColumn = mutableMapOf<Int, Int>()
        val columnWidth = lines[0].length
        lines.forEach { line ->
            line.map { it - '0' }.forEachIndexed { idx, bit ->
                oneCountByColumn[idx] = oneCountByColumn.computeIfAbsent(idx) { 0 } + bit
            }
        }

        var mostCommon = ""
        var leastCommon = ""

        for (i in 0 until columnWidth) {
            val oneCount = oneCountByColumn[i] ?: error("No column $i exists")
            if(oneCount * 2 >= lines.size) {
                mostCommon = "${mostCommon}1"
                leastCommon = "${leastCommon}0"
            } else {
                mostCommon = "${mostCommon}0"
                leastCommon = "${leastCommon}1"
            }
        }

        return mostCommon to leastCommon
    }
}