package dev.schaer.aoc.y2021

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import java.awt.Point

fun main() {
    TransparentOrigami().singleRun()
}

@Year(2021)
@Day(13)
class TransparentOrigami : Solution {

    @Answer("631")
    override fun partOne(input: String): Int {
        return runInstructions(input, instructionsToRun = 1).size
    }

    @Answer("""
████.████.█....████...██..██..███..████..
█....█....█....█.......█.█..█.█..█.█.....
███..███..█....███.....█.█....█..█.███...
█....█....█....█.......█.█.██.███..█.....
█....█....█....█....█..█.█..█.█.█..█.....
████.█....████.█.....██...███.█..█.█.....
.........................................
""")
    override fun partTwo(input: String): String {
        val (width, height) = determineOutputSize(input)
        val dots = runInstructions(input)

        val sb = StringBuilder()
        sb.append('\n')

        for(y in (0..height)) {
            for(x in (0..width)) {
                if(Point(x, y) in dots) {
                    sb.append('█')
                } else {
                    sb.append('.')
                }
            }
            sb.append('\n')
        }

        return sb.toString()
    }

    private fun determineOutputSize(input: String): Pair<Int, Int> {
        var x = 0
        var y = 0
        input.split("\n\n")[1].lines()
            .map { line ->
                val (a, b) = line.split('=')
                a.last() to b.toInt()
            }.forEach { (name, value) ->
                if(name == 'x') {
                    x = value
                } else if(name == 'y') {
                    y = value
                }
            }
        return x to y
    }

    private fun runInstructions(input: String, instructionsToRun: Int = Int.MAX_VALUE): Set<Point> {
        val (dotInput, instructionsInput) = input.split("\n\n")
        var dots = dotInput.split('\n')
            .map { line ->
                val tokens = line.split(',')
                Point(tokens[0].toInt(), tokens[1].toInt())
            }
            .toMutableSet()

        for(instruction in instructionsInput.lines().take(instructionsToRun)) {
            val (direction, fold) = instruction.split('=')
            val foldLine = fold.toInt()
            val f = foldLine * 2

            val dotCopy = mutableSetOf<Point>()

            if(direction.endsWith('y')) {
                for(dot in dots) {
                    if(dot.y > foldLine) {
                        dotCopy.add(Point(dot.x, f - dot.y))
                    } else {
                        dotCopy.add(dot)
                    }
                }
            } else if(direction.endsWith('x')) {
                for(dot in dots) {
                    if(dot.x > foldLine) {
                        dotCopy.add(Point(f - dot.x, dot.y))
                    } else {
                        dotCopy.add(dot)
                    }
                }
            } else {
                error("Unparsable: '$instruction'")
            }
            dots = dotCopy
        }

        return dots
    }
}