package dev.schaer.aoc.y2021

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import dev.schaer.aoc.datastructures.foldWindowTwo
import dev.schaer.aoc.datastructures.ints

fun main() {
    SonarSweep().singleRun()
}

@Year(2021)
@Day(1)
class SonarSweep : Solution {

    @Answer("1226")
    override fun partOne(input: String): Int {
        return input.ints().foldWindowTwo(0) { acc, prev, curr -> acc + if (prev < curr) 1 else 0 }
    }

    @Answer("1252")
    override fun partTwo(input: String): Int {
        return input.ints()
            .windowed(size = 3, step = 1, false) { it.sum() }
            .foldWindowTwo(0) {acc, prev, curr ->  acc + if (prev < curr) 1 else 0}
    }
}