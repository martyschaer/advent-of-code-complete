package dev.schaer.aoc.y2021

import dev.schaer.aoc.algorithms.toHumanReadableSIUnit
import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import java.text.CharacterIterator
import java.text.StringCharacterIterator

fun main() {
    ExtendedPolymerization().singleRun()
}

@Year(2021)
@Day(14)
class ExtendedPolymerization : Solution {

    @Answer("3213")
    override fun partOne(input: String): Long {
        return solve(input, 10)
    }

    @Answer("3711743744429")
    override fun partTwo(input: String): Long {
        return solve(input, 40)
    }

    private fun solve(input: String, steps: Int): Long {
        val (initial, rulesStr) = input.split("\n\n")
        val rules = parseRules(rulesStr)
        val initialPolymer = parseInitial(initial)
        val resultingPolymer = simulate(initialPolymer, rules, steps)
        val elementUsage = calculateElementUsage(resultingPolymer)

//        println("Polymer is roughly ${elementUsage.values.sum().toHumanReadableSIUnit("B")} in size.")

        val mostUsedElement = elementUsage.maxByOrNull { it.value }?.value ?: error("No single max in resulting polymer")
        val leastUsedElement = elementUsage.minByOrNull { it.value }?.value ?: error("No single min in resulting polymer")
        return mostUsedElement - leastUsedElement
    }


    private fun calculateElementUsage(polymer: Map<String, Long>): Map<Char, Long> {
        val elements = mutableMapOf<Char, Long>()

        polymer.forEach {(pair, count) ->
            val (a, b) = pair.toCharArray()
            elements[a] = (elements[a] ?: 0) + count
            elements[b] = (elements[b] ?: 0) + count
        }

        // correct for elements being double counted
        return elements.mapValues { (_, count) ->
            (if(count % 2 != 0L) count + 1 else count) / 2
        }
    }

    private fun simulate(polymer: Map<String, Long>, rules: Map<String, List<String>>, steps: Int): Map<String, Long> {
        var oldPolymer = polymer

        for(step in 1..steps) {
            val newPolymer = mutableMapOf<String, Long>()
            oldPolymer.forEach {(pair, count) ->
                rules[pair]?.let { (a, b) ->
                    newPolymer[a] = (newPolymer[a] ?: 0L) + count
                    newPolymer[b] = (newPolymer[b] ?: 0L) + count
                }
            }
            oldPolymer = newPolymer
        }
        return oldPolymer
    }

    private fun parseInitial(input: String): Map<String, Long> {
        val polymer = mutableMapOf<String, Long>()
        input.windowed(2)
            .forEach { key -> polymer[key] = polymer[key]?.plus(1) ?: 1 }

        return polymer
    }

    private fun parseRules(input: String): Map<String, List<String>> {
        return input.split('\n').flatMap { line ->
            val (src, dst) = line.split(" -> ")
            val (a, b) = src.toCharArray()
            setOf(src to "$a$dst", src to "$dst$b")
        }.groupBy({ it.first }, { it.second })
    }
}