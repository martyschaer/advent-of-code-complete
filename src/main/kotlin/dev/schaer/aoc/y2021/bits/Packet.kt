package dev.schaer.aoc.y2021.bits

interface Packet {
    val version: String
    val type: PacketType
}

data class PacketData(
    override val version: String,
    override val type: PacketType
): Packet

data class OperatorPacket(val meta: PacketData, val subpackets: List<Packet>): Packet by meta {
    constructor(version: String, type: PacketType, subpackets: List<Packet>) : this(PacketData(version, type), subpackets)
}
data class LiteralPacket(val meta: PacketData, val value: Long): Packet by meta {
    constructor(version: String, value: Long) : this(PacketData(version, PacketType.LITERAL), value)
}
