package dev.schaer.aoc.y2021.bits

enum class PacketType(val bits: String) {
    LITERAL("100"),
    SUM("000"),
    PRODUCT("001"),
    MIN("010"),
    MAX("011"),
    GREATER("101"),
    LESS("110"),
    EQUAL("111");

    companion object {
        fun of(type: String): PacketType {
            return when(type) {
                LITERAL.bits -> LITERAL
                SUM.bits -> SUM
                PRODUCT.bits -> PRODUCT
                MIN.bits -> MIN
                MAX.bits -> MAX
                GREATER.bits -> GREATER
                LESS.bits -> LESS
                EQUAL.bits -> EQUAL
                else -> error("Unknown type '$type'")
            }
        }
    }
}