package dev.schaer.aoc.y2021

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import java.util.*
import kotlin.math.floor

fun main() {
    SyntaxScoring().singleRun()
}

@Year(2021)
@Day(10)
class SyntaxScoring : Solution {
    private val matchingClosingChar = mapOf(
        '(' to ')',
        '[' to ']',
        '{' to '}',
        '<' to '>'
    )

    private val illegalClosingCharScore = mapOf(
        ')' to 3,
        ']' to 57,
        '}' to 1197,
        '>' to 25137
    )

    private val autoCompleteCharScore = mapOf(
        ')' to 1,
        ']' to 2,
        '}' to 3,
        '>' to 4
    )

    @Answer("341823")
    override fun partOne(input: String): Int {
        return input.lines()
            .mapNotNull { findFirstIllegalChar(it) }
            .sumOf { illegalClosingCharScore[it] ?: error("Unknown illegal char '$it'") }
    }

    private fun findFirstIllegalChar(line: String): Char? {
        val expectedClosing = Stack<Char>()

        for(char in line.toCharArray()) {
            val matching = matchingClosingChar[char]
            if(matching != null) { // char is opening
                expectedClosing.push(matching)
            } else if(expectedClosing.peek() == char) { //char is closing
                expectedClosing.pop()
            } else { // char is closing unexpectedly, syntax error
                return char
            }
        }

        return null
    }

    @Answer("2801302861")
    override fun partTwo(input: String): Long {
        val lineScores = input.lines()
            .mapNotNull { autoComplete(it) }
            .map { calcAutoCompleteScore(it) }
            .sorted()

        val midIndex = floor(lineScores.size / 2.0).toInt()
        return lineScores[midIndex]
    }

    fun calcAutoCompleteScore(chars: List<Char>): Long {
        return chars.reversed().map { autoCompleteCharScore[it]!! }
            .fold(0L) {acc, charScore -> acc * 5 + charScore}
    }

    private fun autoComplete(line: String): List<Char>? {
        val expectedClosing = Stack<Char>()

        for(char in line.toCharArray()) {
            val matching = matchingClosingChar[char]
            if(matching != null) { // char is opening
                expectedClosing.push(matching)
            } else if(expectedClosing.peek() == char) { //char is closing
                expectedClosing.pop()
            } else { // char is closing unexpectedly, syntax error
                return null
            }
        }

        return expectedClosing
    }
}