package dev.schaer.aoc.y2021

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import dev.schaer.aoc.datastructures.StringLengthProvider
import dev.schaer.aoc.datastructures.product
import dev.schaer.aoc.y2021.bits.LiteralPacket
import dev.schaer.aoc.y2021.bits.OperatorPacket
import dev.schaer.aoc.y2021.bits.Packet
import dev.schaer.aoc.y2021.bits.PacketType
import java.util.stream.Collectors

fun main() {
    PacketDecoder().singleRun()
}

@Year(2021)
@Day(16)
class PacketDecoder : Solution {

    @Answer("877")
    override fun partOne(input: String): Int {
        val packets = parse(StringLengthProvider(hex2bits(input)), 1)
        return sumVersions(packets)
    }

    private fun sumVersions(packets: List<Packet>): Int {
        return packets.sumOf { packet ->
            packet.version.toInt(2) + when (packet.type) {
                PacketType.LITERAL -> 0
                else -> sumVersions((packet as OperatorPacket).subpackets)
            }
        }
    }

    @Answer("194435634456")
    override fun partTwo(input: String): Long {
        val packets = parse(StringLengthProvider(hex2bits(input)), 1)
        return evaluate(packets[0])
    }

    private fun evaluate(packet: Packet): Long {
        if(packet.type == PacketType.LITERAL) {
            return (packet as LiteralPacket).value
        }
        val operator = packet as OperatorPacket
        val args = operator.subpackets.map { evaluate(it) }
        return when(operator.type) {
            PacketType.SUM -> args.sum()
            PacketType.PRODUCT -> args.product()
            PacketType.MIN -> args.minOrNull() ?: error("No minimum could be found for $args")
            PacketType.MAX -> args.maxOrNull() ?: error("No maximum could be found for $args")
            PacketType.GREATER -> if(args[0] > args[1]) 1 else 0
            PacketType.LESS -> if(args[0] < args[1]) 1 else 0
            PacketType.EQUAL -> if(args[0] == args[1]) 1 else 0
            else -> error("Should never happen")
        }
    }

    fun parse(input: StringLengthProvider, packetLimit: Int = Int.MAX_VALUE): List<Packet> {
        val packets = mutableListOf<Packet>()
        do {
            val version = input.next(3)
            val type = PacketType.of(input.next(3))
            if (type == PacketType.LITERAL) {
                packets.add(LiteralPacket(version, parseLiteralValue(input)))
            } else {
                if (input.next(1) == "0") {
                    val length = input.next(15).toInt(2)
                    val subPackets = parse(StringLengthProvider(input.next(length)))
                    packets.add(OperatorPacket(version, type, subPackets))
                } else {
                    val limit = input.next(11).toInt(2)
                    val subPackets = parse(input, limit)
                    packets.add(OperatorPacket(version, type, subPackets))
                }
            }
        } while (packets.size < packetLimit && input.hasNext())

        return packets
    }

    private fun parseLiteralValue(input: StringLengthProvider): Long {
        var bits = ""
        do {
            val next = input.next(5)
            bits += next.substring(1)
        } while (next[0] == '1')
        return bits.toLong(2)
    }

    fun hex2bits(input: String): String {
        return input.chars().map { c ->
            when (c.toChar()) {
                in '0'..'9' -> c - 0x30
                in 'A'..'F' -> c - 0x37
                else -> error("Unexpected token '$c' in '$input'")
            }
        }.mapToObj { it.toString(2).padStart(4, '0') }
            .collect(Collectors.joining())
    }
}
