package dev.schaer.aoc.y2021

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import dev.schaer.aoc.datastructures.Vector2
import java.util.function.Predicate
import kotlin.math.abs
import kotlin.math.max

fun main() {
    HydrothermalVenture().singleRun()
}

@Year(2021)
@Day(5)
class HydrothermalVenture : Solution {

    @Answer("6005")
    override fun partOne(input: String): Int {
        return solve(input, Line::isOrthogonal)
    }

    private fun parseLine(line: String): Line {
        val tokens = line.split(" -> ").map { parseVector(it) }
        return Line(tokens[0], tokens[1])
    }

    private fun parseVector(args: String): Vector2 {
        val tokens = args.split(',').map { it.trim().toInt() }
        return Vector2(tokens[0], tokens[1])
    }

    @Answer("23864")
    override fun partTwo(input: String): Int {
        return solve(input) { it.isOrthogonal() || it.isDiagonal() }
    }

    private fun solve(input: String, filter: Predicate<Line>): Int {
        return input.lines()
            .map { parseLine(it) }
            .filter { filter.test(it) }
            .flatMap { it.calcPoints() }
            .groupingBy { it }
            .eachCount()
            .values
            .count { it >= 2 }
    }
}

private data class Line(val p: Vector2, val q: Vector2) {
    val direction: Vector2
        get() = q.sub(p)

    fun calcPoints(): List<Vector2> {
        val length = max(abs(direction.x), abs(direction.y))
        val unit = Vector2(direction.x / length, direction.y / length)

        return (0..length)
            .map { unit.times(it) }
            .map { p.add(it) }
    }

    fun isOrthogonal(): Boolean {
        return p.x == q.x || p.y == q.y
    }

    fun isDiagonal(): Boolean {
        return abs(direction.x) == abs(direction.y)
    }
}