package dev.schaer.aoc.y2021

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import dev.schaer.aoc.datastructures.graph.Node

fun main() {
    PassagePathing().singleRun()
}

private const val root: Byte = 0
private const val goal: Byte = 100
private const val largeThreshold: Byte = 50

@Year(2021)
@Day(12)
class PassagePathing : Solution {

    private var nodeCounter: Byte = 0
    private val assignment = mutableMapOf(
        "start" to root,
        "end" to goal
    )
    private val graph = mutableMapOf<Byte, Node<Byte>>()

    override fun reset() {
        nodeCounter = 0
        graph.clear()
        assignment.clear()
        assignment["start"] = root
        assignment["end"] = goal
    }

    @Answer("5228")
    override fun partOne(input: String): Int {
        createGraph(input)
        val start = graph[root]!!
        val end = graph[goal]!!

        val initialPath = HashSet<Byte>()
        initialPath.add(root)

        return findAllPaths(start, end, initialPath)
    }

    private fun findAllPaths(curentNode: Node<Byte>, target: Node<Byte>, visited: HashSet<Byte>): Int {
        if (curentNode == target) {
            return 1
        }

        return curentNode.reachable
            .filter { next -> !visited.contains(next.data) }
            .sumOf { next ->
                var nextVisited = visited
                if (next.data < largeThreshold) {
                    nextVisited = visited.clone() as HashSet<Byte>
                    nextVisited.add(next.data)
                }
                findAllPaths(next, target, nextVisited)
            }
    }

    @Answer("131228")
    override fun partTwo(input: String): Int {
        createGraph(input)
        val start = graph[root]!!
        val end = graph[goal]!!

        val initialPath = HashSet<Byte>()
        initialPath.add(root)

        return findAllPathsAlternateRuleset(start, end, initialPath)
    }

    private fun findAllPathsAlternateRuleset(
        curentNode: Node<Byte>,
        target: Node<Byte>,
        visited: HashSet<Byte>,
        smallCaveAlreadyRevisited: Boolean = false
    ): Int {
        if (curentNode == target) {
            return 1
        }

        return curentNode.reachable
            .sumOf { next ->
                if (next.data != root && (!visited.contains(next.data) || !smallCaveAlreadyRevisited)) {
                    var nextVisited = visited
                    if(next.data < largeThreshold) {
                        nextVisited = visited.clone() as HashSet<Byte>
                        nextVisited.add(next.data)
                    }
                    findAllPathsAlternateRuleset(
                        next,
                        target,
                        nextVisited,
                        smallCaveAlreadyRevisited || visited.contains(next.data)
                    )
                } else {
                    0
                }
            }
    }

    private fun createGraph(input: String) {
        input.lines().map { line ->
            val (a, b) = line.split('-').map { nodeToByte(it) }
            graph.putIfAbsent(a, Node(a, mutableSetOf()))
            graph.putIfAbsent(b, Node(b, mutableSetOf()))
            a to b
        }.forEach { (a, b) ->
            graph[a]!!.reachable.add(graph[b]!!)
            graph[b]!!.reachable.add(graph[a]!!)
        }
    }

    private fun nodeToByte(name: String): Byte {
        return assignment.computeIfAbsent(name) {
            var result: Byte = nodeCounter++
            if (name.uppercase() == name) {
                result = (result + largeThreshold).toByte()
            }
            result
        }
    }

}