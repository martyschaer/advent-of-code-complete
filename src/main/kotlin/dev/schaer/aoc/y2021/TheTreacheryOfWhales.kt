package dev.schaer.aoc.y2021

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import kotlin.math.abs

fun main() {
    TheTreacheryOfWhales().singleRun()
}

@Year(2021)
@Day(7)
class TheTreacheryOfWhales : Solution {

    @Answer("323647")
    override fun partOne(input: String): Int {
        val positions = input.split(',').map { it.toInt() }
        val maxPosition = positions.maxOrNull() ?: error("Unable to find max position")
        return (0..maxPosition).map { calculateFuelCost(positions, it) }.minOrNull() ?: error("no min element")
    }

    private fun calculateFuelCost(positions: List<Int>, target: Int): Int {
        return positions.sumOf { abs(it - target) }
    }

    @Answer("87640209")
    override fun partTwo(input: String): Int {
        val positions = input.split(',').map { it.toInt() }
        val maxPosition = positions.maxOrNull() ?: error("Unable to find max position")
        return (0..maxPosition)
            .map { calculateCrabEngineeringFuelCost(positions, it) }
            .minOrNull() ?: error("no min element")
    }

    private fun calculateCrabEngineeringFuelCost(positions: List<Int>, target: Int): Int {
        return positions.sumOf { adjustFuelCost(abs(it - target)) }
    }

    fun adjustFuelCost(n: Int): Int {
        return n * (n + 1) / 2
    }
}