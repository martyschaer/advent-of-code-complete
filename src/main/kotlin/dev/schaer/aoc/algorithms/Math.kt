package dev.schaer.aoc.algorithms

import java.math.BigInteger
import java.text.CharacterIterator
import java.text.StringCharacterIterator
import kotlin.math.abs
import kotlin.math.pow
import kotlin.math.sqrt

fun pow2(n: Int): Double {
    return pow2(n.toDouble())
}

fun pow2(n: Double): Double {
    return n * n
}

fun factorial(n: Int): Int {
    return if (n == 1) 1 else n * factorial(n - 1)
}

fun factorial(n: Long): Long {
    return if (n == 1L) 1 else n * factorial(n - 1)
}

fun factorial(n: BigInteger): BigInteger {
    return if (n == BigInteger.ONE) BigInteger.ONE else n.times(factorial(n.minus(BigInteger.ONE)))
}

fun lcm(list: List<Long>): Long {
    return lcm(list.first(), list.subList(1, list.size))
}

private fun lcm(a: Long, rest: List<Long>): Long {
    if (rest.isEmpty()) {
        return a
    }
    return lcm(lcm(a, rest.first()), rest.subList(1, rest.size))
}

// adapted from https://stackoverflow.com/a/3758880/2941551
fun Number.toHumanReadableSIUnit(unit: String = ""): String {
    var bytes = this.toLong()
    if (-1000 < bytes && bytes < 1000) {
        return "$bytes B"
    }
    val ci: CharacterIterator = StringCharacterIterator("kMGTPE")
    while (bytes <= -999950 || bytes >= 999950) {
        bytes /= 1000
        ci.next()
    }
    return java.lang.String.format("%.2f %c$unit", bytes / 1000.0, ci.current())
}

fun Int.triangle(): Int {
    return this * (this + 1) / 2
}

fun Int.pow(n: Int): Int {
    return this.toFloat().pow(n).toInt()
}

fun Int.untriangle(): Int? {
    val D = 8 * this + 1
    val sqrtD = sqrt(D.toFloat())
    if (sqrtD.toInt().toFloat() != sqrtD) {
        return null
    }
    return ((sqrtD - 1) / 2).toInt()
}

fun IntRange.containsRange(other: IntRange): Boolean {
    return this.contains(other.first) and this.contains(other.last)
}

fun IntRange.overlaps(other: IntRange): Boolean {
    return this.contains(other.first) || this.contains(other.last)
            || other.contains(this.first) || other.contains(this.last)
}

/**
 * Calculates the least common multiple.
 * Assumes non-zero inputs.
 */
fun lcm(a: Long, b: Long): Long {
    return abs(a * b) / gcd(a, b)
}

fun gcd(list: List<Long>): Long {
    return gcd(list.first(), list.subList(1, list.size))
}

private fun gcd(a: Long, rest: List<Long>): Long {
    if (rest.isEmpty()) {
        return a
    }
    return gcd(gcd(a, rest.first()), rest.subList(1, rest.size))
}

fun gcd(aIn: Long, bIn: Long): Long {
    var a = aIn
    var b = bIn
    while (b != 0L) {
        val t = b
        b = a % b
        a = t
    }
    return a
}

fun inverse(a: Long, p: Long): Long {
    var t = 0L
    var newt = 1L
    var r = p
    var newr = a

    while (newr != 0L) {
        val quotient = r / newr
        val er = r
        r = newr
        newr = er - quotient * newr

        val et = t
        t = newt
        newt = et - quotient * newt
    }

    val res = 1.0 / r * t
    return res.toLong()
}

fun Long.digits(): Int {
    var result = 0
    var n = this
    while (n > 0) {
        n = n / 10
        result++
    }
    return result
}

fun Int.digits(): Int {
    var result = 0
    var n = this
    while (n > 0) {
        n = n / 10
        result++
    }
    return result
}

/**
 * Finds an x, where:
 * - `x mod p == a`
 * - `x mod q == b`
 *
 * https://en.wikipedia.org/wiki/Chinese_remainder_theorem
 */
fun chineseRemainderTheorem(a: Int, b: Int, p: Int, q: Int): Int {
    for (x in 1 until p * q) {
        if (x % p == a && x % q == b) {
            return x
        }
    }
    error("Unable to find a matching X")
}