package dev.schaer.aoc.algorithms

import dev.schaer.aoc.datastructures.Vector2
import dev.schaer.aoc.datastructures.foldWindowTwo

/**
 * Calculates the inner area of a polygon defined by the given
 * vertices using the shoelace formula and Pick's theorem
 *
 * Optionally specify the perimeter length, otherwise it is calculated
 * from the given vertices.
 *
 * - https://en.wikipedia.org/wiki/Shoelace_formula
 * - https://en.wikipedia.org/wiki/Pick%27s_theorem
 */
fun innerArea(vertices: List<Vector2>, perimeter: Long = perimeterLength(vertices)): Long {
    return shoelace(vertices) - (perimeter / 2) + 1
}

/**
 * Calculates the outer area of a polygon defined by the given
 * vertices using the shoelace formula and Pick's theorem
 *
 * Optionally specify the perimeter length, otherwise it is calculated
 * from the given vertices.
 *
 * - https://en.wikipedia.org/wiki/Shoelace_formula
 * - https://en.wikipedia.org/wiki/Pick%27s_theorem
 */
fun outerArea(vertices: List<Vector2>, perimeter: Long = perimeterLength(vertices)): Long {
    return shoelace(vertices) + (perimeter / 2) + 1
}

private fun perimeterLength(vertices: List<Vector2>): Long {
    return vertices.foldWindowTwo(0L) { acc, prev, curr ->
        acc + (prev - curr).manhattanLength()
    }
}

private fun shoelace(loop: List<Vector2>): Long {
    val area = (loop.indices).sumOf { i ->
        val va = loop[i]
        val vb = loop[(i + 1) % loop.size]

        val vay = va.y.toLong()
        val vby = vb.y.toLong()
        val vax = va.x.toLong()
        val vbx = vb.x.toLong()

        (vay + vby) * (vax - vbx)
    } / 2

    // area is negative when loop is not given in clockwise orientation
    if (area > 0) {
        return area
    }
    return -area
}
