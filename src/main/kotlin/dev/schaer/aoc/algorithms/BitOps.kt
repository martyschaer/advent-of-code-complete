package dev.schaer.aoc.algorithms

fun Long.setBit(bit: Int, value: Boolean): Long {
    if (value) {
        return this or (1L shl bit)
    }
    return this and ((1L shl bit) xor Long.MAX_VALUE)
}
