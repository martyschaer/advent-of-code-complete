package dev.schaer.aoc.algorithms

fun <T> permutations(values: Array<T>): List<Array<T>> {
    val permutations = mutableListOf<Array<T>>()
    val indexes = Array(values.size) { 0 }

    permutations.add(values.copyOf())

    var i = 0
    while (i < indexes.size) {
        if (indexes[i] < i) {
            swap(values, if (i % 2 == 0) 0 else indexes[i], i)
            permutations.add(values.copyOf())
            indexes[i]++
            i = 0
        } else {
            indexes[i] = 0
            i++
        }
    }

    return permutations
}

private fun <T> swap(arr: Array<T>, a: Int, b: Int) {
    val tmp = arr[a]
    arr[a] = arr[b]
    arr[b] = tmp
}