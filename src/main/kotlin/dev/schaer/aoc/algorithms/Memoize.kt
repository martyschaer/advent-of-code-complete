package dev.schaer.aoc.algorithms

fun <I, O> memoize(func: (I, (I) -> O) -> O): (I) -> O {
    val cache = hashMapOf<I, O>()
    lateinit var memoizedFunc: (I) -> O
    memoizedFunc = { input ->
        cache.getOrPut(input) { func(input, memoizedFunc) }
    }
    return memoizedFunc
}

// memoize the given non-recursive function
fun <I, O> cache(func: (I) -> O): (I) -> O {
    val cache = hashMapOf<I, O>()
    return { input ->
        cache.getOrPut(input) { func(input) }
    }
}

fun <I1, I2, O> memoize(func: (I1, I2, (I1, I2) -> O) -> O): (I1, I2) -> O {
    val cache = hashMapOf<Pair<I1, I2>, O>()
    lateinit var memoizedFunc: (I1, I2) -> O
    memoizedFunc = { input1, input2 ->
        cache.getOrPut(Pair(input1, input2)) { func(input1, input2, memoizedFunc) }
    }
    return memoizedFunc
}

fun <I1, I2, I3, O> memoize(func: (I1, I2, I3, (I1, I2, I3) -> O) -> O): (I1, I2, I3) -> O {
    val cache = hashMapOf<Triple<I1, I2, I3>, O>()
    lateinit var memoizedFunc: (I1, I2, I3) -> O
    memoizedFunc = { input1, input2, input3 ->
        cache.getOrPut(Triple(input1, input2, input3)) { func(input1, input2, input3, memoizedFunc) }
    }
    return memoizedFunc
}