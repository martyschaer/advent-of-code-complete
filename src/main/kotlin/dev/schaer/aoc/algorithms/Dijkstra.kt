package dev.schaer.aoc.algorithms

import java.util.*

interface State {
    val parent: State?
    fun transitions(): List<Transition>
}

data class Transition(val dst: State, val cost: Int)

fun dijkstra(startState: State, hasReachedGoal: (State) -> Boolean): List<State> {
    val distances = HashMap<State, Int>().withDefault { Int.MAX_VALUE }
    val priorityQueue = PriorityQueue<Pair<State, Int>>(compareBy { it.second })

    distances[startState] = 0
    priorityQueue.add(Pair(startState, 0))

    while (priorityQueue.isNotEmpty()) {
        val (currentState, _) = priorityQueue.poll()

        if (hasReachedGoal(currentState)) {
            return constructPath(currentState)
        }

        val transitions = currentState.transitions()

        for (transition in transitions) {
            val nextState = transition.dst
            val newDistance = distances.getValue(currentState) + transition.cost

            if (newDistance < distances.getValue(nextState)) {
                distances[nextState] = newDistance
                priorityQueue.add(Pair(nextState, newDistance))
            }
        }
    }

    return emptyList() // No path found
}

private fun constructPath(endState: State): List<State> {
    val path = LinkedList<State>()

    generateSequence(endState) { it.parent }.forEach { if (true) path.addFirst(it) }

    return path
}