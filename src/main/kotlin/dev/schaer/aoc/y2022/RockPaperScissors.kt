package dev.schaer.aoc.y2022

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import dev.schaer.aoc.y2022.RockPaperScissors.Outcome.*
import dev.schaer.aoc.y2022.RockPaperScissors.Symbol.*

fun main() {
    RockPaperScissors().singleRun()
}

@Year(2022)
@Day(2)
class RockPaperScissors : Solution {

    @Answer("15632")
    override fun partOne(input: String): Int {
        val parsed = parseInput(input, ::parseLinePart1)
        return parsed.sumOf { (symbol, outcome) -> symbol.score + outcome.score }
    }

    private fun parseLinePart1(line: String): Pair<Symbol, Symbol> {
        val (elfPlay, myPlay) = line.split(' ')
        return Symbol.of(elfPlay) to Symbol.of(myPlay)
    }

    @Answer("14416")
    override fun partTwo(input: String): Int {
        val parsed = parseInput(input, ::parseLinePart2)
        return parsed.sumOf { (symbol, outcome) -> symbol.score + outcome.score }
    }

    private fun parseLinePart2(line: String): Pair<Symbol, Symbol> {
        val (elfPlayStr, desiredOutcome) = line.split(' ')
        val elfPlay = Symbol.of(elfPlayStr)
        val myPlay = Outcome.of(desiredOutcome).calculateMyPlay(elfPlay)
        return elfPlay to myPlay
    }

    private fun parseInput(input: String, parseLine: (String) -> Pair<Symbol, Symbol>): List<Pair<Symbol, Outcome>> {
        return input.lines()
            .map { parseLine(it) }
            .map { (elf, me) -> me to me.playAgainst(elf) }
    }

    enum class Outcome (val score: Int) {
        LOST(0), DRAW(3), WON(6);

        companion object {
            fun of(input: String): Outcome {
                return when(input) {
                    "X" -> LOST
                    "Y" -> DRAW
                    "Z" -> WON
                    else -> error("'$input' is an unknown symbol")
                }
            }
        }

        fun calculateMyPlay(elfPlay : Symbol): Symbol {
            return when(this) {
                LOST -> when(elfPlay) {
                    ROCK -> SCISSORS
                    PAPER -> ROCK
                    SCISSORS -> PAPER
                }
                DRAW -> elfPlay
                WON -> when(elfPlay) {
                    ROCK -> PAPER
                    PAPER -> SCISSORS
                    SCISSORS -> ROCK
                }
            }
        }

    }

    enum class Symbol (val score: Int) {
        ROCK(1), PAPER(2), SCISSORS(3);

        companion object {
            fun of(input: String): Symbol {
                return when(input) {
                    "A" -> ROCK
                    "B" -> PAPER
                    "C" -> SCISSORS
                    "X" -> ROCK
                    "Y" -> PAPER
                    "Z" -> SCISSORS
                    else -> error("'$input' is an unknown symbol")
                }
            }
        }

        fun playAgainst(other: Symbol) : Outcome{
            return when(this) {
                ROCK -> when(other) {
                    ROCK -> DRAW
                    PAPER -> LOST
                    SCISSORS -> WON
                }
                PAPER -> when(other) {
                    ROCK -> WON
                    PAPER -> DRAW
                    SCISSORS -> LOST
                }
                SCISSORS -> when(other) {
                    ROCK -> LOST
                    PAPER -> WON
                    SCISSORS -> DRAW
                }
            }
        }
    }
}