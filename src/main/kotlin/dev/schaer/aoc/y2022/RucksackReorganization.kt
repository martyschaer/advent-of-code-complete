package dev.schaer.aoc.y2022

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import dev.schaer.aoc.datastructures.charSet
import dev.schaer.aoc.datastructures.split

fun main() {
    RucksackReorganization().singleRun()
}

@Year(2022)
@Day(3)
class RucksackReorganization : Solution {

    @Answer("8018")
    override fun partOne(input: String): Int {
        return input.lines()
            .map { line -> line.split(line.length / 2) }
            .map { it.first.charSet() to it.second.charSet() }
            .map { retainAll(it.first, it.second) }
            .map { calculatePriority(it.first()) }
            .sum()
    }

    @Answer("2518")
    override fun partTwo(input: String): Int {
        return input.lines()
            .map { it.charSet() }
            .chunked(3)
            .map { retainAll(it.first(), it.takeLast(2)) }
            .map { calculatePriority(it.first()) }
            .sum()
    }

    private fun retainAll(first: Set<Char>, more: Set<Char>): Set<Char> {
        return retainAll(first, listOf(more))
    }

    private fun retainAll(first: Set<Char>, more: List<Set<Char>>): Set<Char> {
        val result = first.toMutableSet()
        for (set in more) {
            result.retainAll(set)
        }
        return result
    }

    /**
     * Convert to alphabetical index (one-based),
     * with i(A) == i(a) + 26
     */
    private fun calculatePriority(item: Char): Int {
        if(item.isLowerCase()) {
            return item - '`'
        }
        return item - '@' + 26
    }
}