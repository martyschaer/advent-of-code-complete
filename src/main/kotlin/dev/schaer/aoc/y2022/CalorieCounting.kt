package dev.schaer.aoc.y2022

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer

fun main() {
    CalorieCounting().singleRun()
}

@Year(2022)
@Day(1)
class CalorieCounting : Solution {

    @Answer("69836")
    override fun partOne(input: String): Int {
        val packs = createSnackPacks(input.lines())
        return packs.maxOf { it.sum() }
    }

    @Answer("207968")
    override fun partTwo(input: String): Int {
        val packs = createSnackPacks(input.lines())
        return packs.map { it.sum() }
            .sorted()
            .takeLast(3)
            .sum()
    }

    private fun createSnackPacks(input: List<String>): List<List<Int>> {
        val iterator = input.iterator()
        val packets = mutableListOf<List<Int>>()
        while(iterator.hasNext()) {
            val packet = mutableListOf<Int>()
            var line = ""
            while(iterator.hasNext() && run {
                    line = iterator.next()
                    line.isNotEmpty()}) {
                packet.add(line.toInt())
            }
            packets.add(packet)
        }
        return packets
    }
}