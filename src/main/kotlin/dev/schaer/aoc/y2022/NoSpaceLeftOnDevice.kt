package dev.schaer.aoc.y2022

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import dev.schaer.aoc.y2022.NoSpaceLeftOnDevice.CommandType.CD
import dev.schaer.aoc.y2022.NoSpaceLeftOnDevice.CommandType.LS
import dev.schaer.aoc.y2022.NoSpaceLeftOnDevice.NodeType.DIR
import dev.schaer.aoc.y2022.NoSpaceLeftOnDevice.NodeType.FILE

fun main() {
    NoSpaceLeftOnDevice().singleRun()
}

private const val TOTAL_FS_SIZE     = 70000000
private const val FREE_SPACE_NEEDED = 30000000

@Year(2022)
@Day(7)
class NoSpaceLeftOnDevice : Solution {

    @Answer("1845346")
    override fun partOne(input: String): Int {
        return parseInput(input).values
            .filter { it < 100000 }
            .sum()
    }

    @Answer("3636703")
    override fun partTwo(input: String): Int {
        val dirSizes = parseInput(input)
        val used = dirSizes["/"] ?: error("No size for root")
        val free = TOTAL_FS_SIZE - used
        val spaceToFree = FREE_SPACE_NEEDED - free

        val directories = dirSizes.values.sortedBy { -it }.iterator()

        var prev = directories.next()
        var curr = directories.next()

        while (curr> spaceToFree && directories.hasNext()) {
            prev = curr
            curr = directories.next()
        }

        return prev
    }

    private fun parseInput(input: String): Map<String, Int> {
        val commands = input.split('$')
            .filter { it.isNotEmpty() }
            .map { Command.parse(it) }

        val root = parseCommands(commands)

        return calculateDirectorySizes(root)
    }

    private fun parseCommands(commands: List<Command>): Node {
        val root = Node(DIR, name = "")
        var pwd = root

        for (cmd in commands) {
            if(cmd.type == CD) {
                pwd = handleCd(cmd as CdCommand, pwd, root)
            } else {
                handleLs(cmd as LsCommand, pwd)
            }
        }

        return root
    }

    private fun calculateDirectorySizes(root: Node): Map<String, Int> {
        val res = mutableMapOf<String, Int>()
        calculateSize(root, res, "")
        return res
    }

    private fun calculateSize(node: Node, res: MutableMap<String, Int>, path: String): Int {
        if (node.type == FILE) {
            return node.size
        }

        val nextPath = "$path${node.name}/"

        val sumOfChildren = node.children.values.sumOf { calculateSize(it, res, nextPath) }

        res[nextPath] = sumOfChildren

        return sumOfChildren
    }

    private fun calculateSize(node: Node, limit: Int = Int.MAX_VALUE, sizeRelevantDirs: MutableMap<String, Int>, d: Int): Int {
        val ownSize = node.size
        val childSize = node.children.values.sumOf { child -> calculateSize(child, limit, sizeRelevantDirs, d + 1) }
        val size = childSize + ownSize

        if(size < limit) {
            sizeRelevantDirs.put(node.name, size)
        }

        return size
    }

    private fun handleCd(cmd: CdCommand, pwd: Node, root: Node): Node {
        return when (cmd.arg) {
            "/" -> root
            ".." -> pwd.parent ?: error("Cannot go back to unknown parent at $pwd")
            else -> {
                val target = pwd.children.computeIfAbsent(cmd.arg) { Node(DIR, it, parent = pwd) }
                target.parent = pwd
                target
            }
        }
    }

    private fun handleLs(cmd: LsCommand, pwd: Node) {
        for(result in cmd.result) {
            pwd.children.computeIfAbsent(result.name) { result }
        }
    }

    private interface Command {
        val type: CommandType

        companion object {
            fun parse(input: String): Command {
                val lines = input.split("\n").map { it.trim() }
                val firstLine = lines.first()
                val tokens = firstLine.split(" ")
                return when (tokens.first()) {
                    "cd" -> CdCommand(arg = tokens[1])
                    "ls" -> LsCommand(LsCommand.parseResult(lines.drop(1)))
                    else -> error("Unknown command type '${lines.first()}'")
                }
            }
        }
    }

    private data class LsCommand(val result: List<Node>, override val type: CommandType = LS) : Command {
        companion object {
            fun parseResult(input: List<String>): List<Node> {
                return input.map { it.trim() }
                    .filter { it.isNotEmpty() }
                    .map { Node.parse(it) }
            }
        }
    }

    private data class CdCommand(val arg: String, override val type: CommandType = CD) : Command {
    }

    private enum class CommandType {
        LS, CD
    }

    private data class Node(
        val type: NodeType,
        val name: String,
        val size: Int = 0,
        var parent: Node? = null,
        val children: MutableMap<String, Node> = mutableMapOf()
    ) {
        companion object {
            fun parse(input: String): Node {
                val (first, second) = input.split(" ")
                if (first == "dir") {
                    return Node(type = DIR, name = second)
                }
                return Node(type = FILE, name = second, size = first.toInt())
            }
        }

        override fun toString(): String {
            val sb = StringBuilder()
            toString(0, sb)
            return sb.toString()
        }

        private fun toString(depth: Int, result: StringBuilder) {
            if(type == DIR) {
                result.append(" ".repeat(depth))
                result.appendLine("$name (${children.size})")
                for(child in children.values) {
                    child.toString(depth + 1, result)
                }
            } else {
                result.append(" ".repeat(depth))
                result.append(size.toString().padStart(9, '.'))
                result.appendLine(" $name")
            }
        }
    }

    private enum class NodeType {
        DIR, FILE
    }
}