package dev.schaer.aoc.y2022

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import java.util.*

fun main() {
    SupplyStacks().singleRun()
}

@Year(2022)
@Day(5)
class SupplyStacks : Solution {

    @Answer("HBTMTBSDC")
    override fun partOne(input: String): String {
        val (stacks, instructions) = parseInput(input)
        instructions.forEach {it.applyToAsCrateMover9000(stacks)}
        return stacks.filter { it.isNotEmpty() }.joinToString("") { it.peek() }
    }

    @Answer("PQTJRSHWS")
    override fun partTwo(input: String): String {
        val (stacks, instructions) = parseInput(input)
        instructions.forEach {it.applyToAsCrateMover9001(stacks)}
        return stacks.filter { it.isNotEmpty() }.joinToString("") { it.peek() }
    }

    private fun parseInput(input: String): Pair<List<Stack<String>>, List<Instruction>> {
        val (initialStacks, instructionsStr) = input.split("\n\n")
        val stacks = parseStacks(initialStacks)
        val instructions = instructionsStr.lines().map { Instruction.parse(it) }

        return stacks to instructions
    }

    private fun parseStacks(initialStacks: String): List<Stack<String>> {
        val lines = initialStacks.lines().dropLast(1).reversed()
        val stacks = mutableListOf<Stack<String>>()

        val numberOfStacks = (lines.first().length + 1) / 4
        for (i in 0..numberOfStacks) {
            stacks.add(Stack())
        }

        lines.map { "$it " }
            .map { it.chunked(4) }
            .map { line -> line.map { it.replace("[", "").replace("]", "").trim() } }
            .forEach {line -> line.forEachIndexed { index, crate ->
                if(crate.isNotEmpty()) {
                    stacks[index + 1].push(crate)
                }
            }}


        return stacks
    }

    private data class Instruction(val repetitions: Int, val source: Int, val target: Int) {
        companion object {
            fun parse(instruction: String): Instruction {
                val (reps, source, target) = instruction.replace(Regex("[a-z]+ "), "").split(" ")
                return Instruction(reps.toInt(), source.toInt(), target.toInt())
            }
        }

        fun applyToAsCrateMover9000(stacks: List<Stack<String>>) {
            for(i in 0 until repetitions) {
                stacks[target].push(stacks[source].pop())
            }
        }

        fun applyToAsCrateMover9001(stacks: List<Stack<String>>) {
            val claw = Stack<String>()
            for(i in 0 until repetitions) {
                claw.push(stacks[source].pop())
            }
            for(i in 0 until repetitions) {
                stacks[target].push(claw.pop())
            }
        }
    }
}