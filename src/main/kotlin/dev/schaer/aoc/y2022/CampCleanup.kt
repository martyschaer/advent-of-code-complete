package dev.schaer.aoc.y2022

import dev.schaer.aoc.algorithms.containsRange
import dev.schaer.aoc.algorithms.overlaps
import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer

fun main() {
    CampCleanup().singleRun()
}

@Year(2022)
@Day(4)
class CampCleanup : Solution {

    @Answer("305")
    override fun partOne(input: String): Int {
        return input.lines()
            .map { parseLine(it) }
            .count { (first, second) -> first.containsRange(second) or second.containsRange(first) }
    }

    @Answer("811")
    override fun partTwo(input: String): Int {
        return input.lines()
            .map { parseLine(it) }
            .count { (first, second) -> first.overlaps(second) }
    }

    private fun parseLine(line: String): Pair<IntRange, IntRange> {
        val (assignment1, assignment2) = line.split(',')
        return parseAssignment(assignment1) to parseAssignment(assignment2)
    }

    private fun parseAssignment(assignment: String): IntRange {
        val (start, end) = assignment.split('-')
        return start.toInt()..end.toInt()
    }
}