package dev.schaer.aoc.model

import dev.schaer.aoc.datastructures.Vector2

interface Direction {
    val x: Int
    val y: Int
    val binaryOrdinal: Byte
    val opposite: Direction

    /**
     * The direction clockwise of this direction
     */
    val cw: Direction

    /**
     * The direction counterclockwise of this direction
     */
    val ccw: Direction
    val vec
        get() = Vector2(x, y)
    val horizontal: Boolean
        get() = y == 0
    val vertical: Boolean
        get() = x == 0
    val left: Direction
        get() = ccw
    val right: Direction
        get() = cw

    enum class Relative(override val x: Int, override val y: Int) : Direction {
        UP(0, -1), DOWN(0, 1), LEFT(-1, 0), RIGHT(1, 0);

        val symbol: String by lazy {
            when (this) {
                UP -> "^"
                DOWN -> "v"
                LEFT -> "<"
                RIGHT -> ">"
            }
        }

        override val binaryOrdinal: Byte by lazy { (1 shl ordinal).toByte() }

        override val opposite: Direction by lazy {
            when (this) {
                UP -> DOWN
                DOWN -> UP
                LEFT -> RIGHT
                RIGHT -> LEFT
            }
        }

        override val cw: Direction by lazy {
            when (this) {
                UP -> RIGHT
                DOWN -> LEFT
                LEFT -> UP
                RIGHT -> DOWN
            }
        }

        override val ccw: Direction by lazy {
            when (this) {
                UP -> LEFT
                DOWN -> RIGHT
                LEFT -> DOWN
                RIGHT -> UP
            }
        }
    }

    enum class Cardinal(override val x: Int, override val y: Int) : Direction {
        NORTH(0, -1), SOUTH(0, 1), EAST(1, 0), WEST(-1, 0);

        override val binaryOrdinal: Byte by lazy { (1 shl ordinal).toByte() }

        override val opposite: Direction by lazy {
            when (this) {
                NORTH -> SOUTH
                SOUTH -> NORTH
                EAST -> WEST
                WEST -> EAST
            }
        }

        override val cw: Direction by lazy {
            when (this) {
                NORTH -> EAST
                SOUTH -> WEST
                EAST -> SOUTH
                WEST -> NORTH
            }
        }

        override val ccw: Direction by lazy {
            when (this) {
                NORTH -> WEST
                SOUTH -> EAST
                EAST -> NORTH
                WEST -> SOUTH
            }
        }
    }

    enum class Intercardinal(override val x: Int, override val y: Int) : Direction {
        NORTHEAST(1, -1), SOUTHEAST(1, 1), SOUTHWEST(-1, 1), NORTHWEST(-1, -1);

        override val binaryOrdinal: Byte by lazy { (1 shl (ordinal + 4)).toByte() }

        override val opposite: Direction by lazy {
            when (this) {
                NORTHEAST -> SOUTHWEST
                SOUTHEAST -> NORTHWEST
                SOUTHWEST -> NORTHEAST
                NORTHWEST -> SOUTHEAST
            }
        }

        override val cw: Direction by lazy {
            when (this) {
                NORTHEAST -> SOUTHEAST
                NORTHWEST -> NORTHEAST
                SOUTHEAST -> SOUTHWEST
                SOUTHWEST -> NORTHWEST
            }
        }

        override val ccw: Direction by lazy {
            when (this) {
                NORTHEAST -> NORTHWEST
                NORTHWEST -> SOUTHWEST
                SOUTHEAST -> NORTHEAST
                SOUTHWEST -> SOUTHEAST
            }
        }
    }

    enum class Compass(override val x: Int, override val y: Int) : Direction {
        NORTH(0, -1), SOUTH(0, 1), EAST(1, 0), WEST(-1, 0), //
        NORTHEAST(1, -1), SOUTHEAST(1, 1), SOUTHWEST(-1, 1), NORTHWEST(-1, -1);

        override val binaryOrdinal: Byte by lazy { (1 shl ordinal).toByte() }

        override val opposite: Direction by lazy {
            when (this) {
                NORTH -> SOUTH
                SOUTH -> NORTH
                EAST -> WEST
                WEST -> EAST
                NORTHEAST -> SOUTHWEST
                SOUTHEAST -> NORTHWEST
                SOUTHWEST -> NORTHEAST
                NORTHWEST -> SOUTHEAST
            }
        }

        override val cw: Direction by lazy {
            when (this) {
                NORTH -> NORTHEAST
                SOUTH -> SOUTHWEST
                EAST -> SOUTHEAST
                WEST -> NORTHWEST
                NORTHEAST -> EAST
                SOUTHEAST -> SOUTH
                SOUTHWEST -> WEST
                NORTHWEST -> NORTH
            }
        }

        override val ccw: Direction by lazy {
            when (this) {
                NORTH -> NORTHWEST
                SOUTH -> SOUTHEAST
                EAST -> NORTHEAST
                WEST -> SOUTHWEST
                NORTHEAST -> NORTH
                SOUTHEAST -> EAST
                SOUTHWEST -> SOUTH
                NORTHWEST -> WEST
            }
        }
    }
}