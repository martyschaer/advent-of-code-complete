package dev.schaer.aoc.y2020

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer

fun main() {
    ComboBreaker().singleRun()
}

private const val SUBJECT_NUMBER = 7
private const val MODULUS = 20201227

@Year(2020)
@Day(25)
class ComboBreaker : Solution {

    @Answer("9420461")
    override fun partOne(input: String): Long {
        val (pubKeyCard, pubKeyDoor) = input.split("\n").map { it.toLong() }
        val exponentDoor = calculateExponent(pubKeyDoor)
        return calculateEncryptionKey(pubKeyCard, exponentDoor)
    }

    private fun calculateEncryptionKey(subjectNumber: Long, loopSize: Int): Long {
        var value = 1L
        repeat(loopSize) { value = (value * subjectNumber) % MODULUS }
        return value
    }

    fun calculateExponent(publicKey: Long): Int {
        var value = 1L
        for (exponent in 1..Int.MAX_VALUE) {
            value = (value * SUBJECT_NUMBER) % MODULUS
            if (value == publicKey) {
                return exponent
            }
        }
        error("No exponent found for publicKey $publicKey")
    }

    @Answer("0")
    override fun partTwo(input: String): Int {
        return 0
    }
}