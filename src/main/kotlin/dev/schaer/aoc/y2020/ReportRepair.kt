package dev.schaer.aoc.y2020

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer

fun main() {
    ReportRepair().singleRun()
}

@Year(2020)
@Day(1)
class ReportRepair : Solution {

    @Answer("970816")
    override fun partOne(input: String): Int {
        val expenses = input
            .split("\n")
            .map { it.toInt() }
            .sorted()

        var a = 0
        var b = expenses.size - 1
        val target = 2020
        while (a < b) {
            val sum = expenses[a] + expenses[b]
            when {
                sum == target -> return (expenses[a] * expenses[b])
                sum < target -> a++
                sum > target -> b--
            }
        }
        error("No answer found")
    }

    @Answer("96047280")
    override fun partTwo(input: String): Int {
        val expenses = input.split("\n").map { it.toInt() }
        for (a in (expenses.indices)) {
            for (b in (a until expenses.size)) {
                for (c in (b until expenses.size)) {
                    if (expenses[a] + expenses[b] + expenses[c] == 2020) {
                        return (expenses[a] * expenses[b] * expenses[c])
                    }
                }
            }
        }
        error("No answer found")
    }
}