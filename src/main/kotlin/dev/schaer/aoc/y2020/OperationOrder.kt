package dev.schaer.aoc.y2020

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import dev.schaer.aoc.datastructures.split
import dev.schaer.aoc.y2020.mathtree.LeafNode
import dev.schaer.aoc.y2020.mathtree.Node
import dev.schaer.aoc.y2020.mathtree.OpNode
import dev.schaer.aoc.y2020.mathtree.Operation

fun main() {
    OperationOrder().singleRun()
}

private const val ADDITION = " + "
private const val MULTIPLICATION = " * "

@Year(2020)
@Day(18)
class OperationOrder : Solution {

    @Answer("11076907812171")
    override fun partOne(input: String): Long {
        return input.split("\n")
            .map { evalExpression(it) }
            .sum()
    }

    private fun evalExpression(expression: String): Long {
        val expressionTree = parse(reverse(expression))
        return expressionTree.eval()
    }

    /**
     * Reverses a given expression in a way that keeps parentheses consistent
     */
    private fun reverse(expression: String): String {
        return expression
            .reversed()
            .replace('(', '_')
            .replace(')', '(')
            .replace('_', ')')
    }

    private val number = Regex("^\\d+$")

    private fun parse(expression: String): Node {
        if (number.matches(expression)) {
            return LeafNode(expression.reversed().toLong())
        }

        if (expression.startsWith("(")) {
            val endOfParentheses = calcEndOfOperand(expression)
            // if the entire expression is contained within parentheses
            if (endOfParentheses == expression.length) {
                // re-parse without the parentheses
                return parse(expression.substring(1, endOfParentheses - 1))
            }
        }

        val (operandA, operatorOperandRest) = expression.split(calcEndOfOperand(expression))
        val (rawOperator, operandRest) = operatorOperandRest.split(3)
        val operator = Operation.of(rawOperator.trim())
        return OpNode(operator, parse(operandA), parse(operandRest))
    }

    /**
     * Returns end-index of the operand to consume
     */
    private fun calcEndOfOperand(expression: String): Int {
        if (expression.startsWith("(")) {
            var counter = 0
            for (i in expression.indices) {
                val c = expression[i]
                counter += when (c) {
                    '(' -> 1
                    ')' -> -1
                    else -> 0
                }
                if (c == ')' && counter == 0) {
                    return i + 1
                }
            }
        }

        return expression.takeWhile { c -> c in '0'..'9' }.count()
    }

    @Answer("283729053022731")
    override fun partTwo(input: String): Long {
        return input.split("\n")
            .map { evalPartTwo(it) }
            .sum()
    }

    private val innermostParentheses = Regex("(\\([^()]+\\))")

    private fun evalPartTwo(expression: String): Long {
        var expr = expression
        do {
            val parens = innermostParentheses.findAll(expr).toList().asReversed()
            for (paren in parens) {
                expr = expr.replaceRange(paren.range, evalSimpleExpression(paren.value).toString())
            }
        } while (parens.count() > 0)
        return evalSimpleExpression(expr)
    }

    private val addition = Regex("(\\d+ \\+ \\d+)")
    private val multiplication = Regex("(\\d+ \\* \\d+)")

    /**
     * Evaluates an expression that does not contain any parentheses
     */
    private fun evalSimpleExpression(expression: String): Long {
        var expr = expression
        expr = evalOperations(expr, addition, ::evalAddition)
        expr = evalOperations(expr, multiplication, ::evalMultiplication)
        return expr.replace(Regex("[^\\d]"), "").toLong()
    }

    /**
     * Evaluates all operations matching the given regex.
     */
    private fun evalOperations(
        expression: String,
        regex: Regex,
        calculation: (expression: String) -> Long
    ): String {
        var expr = expression
        do {
            val occurrences = regex.findAll(expr).toList().asReversed()
            for (occurrence in occurrences) {
                expr = expr.replaceRange(occurrence.range, calculation(occurrence.value).toString())
            }
        } while (occurrences.count() > 0)
        return expr
    }

    private fun evalAddition(expression: String): Long {
        val tokens = expression.split(" + ")
        return tokens[0].toLong() + tokens[1].toLong()
    }

    private fun evalMultiplication(expression: String): Long {
        val tokens = expression.split(" * ")
        return tokens[0].toLong() * tokens[1].toLong()
    }
}
