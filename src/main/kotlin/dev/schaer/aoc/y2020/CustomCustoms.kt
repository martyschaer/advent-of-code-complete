package dev.schaer.aoc.y2020

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer

fun main() {
    CustomCustoms().singleRun()
}

@Year(2020)
@Day(6)
class CustomCustoms : Solution {

    @Answer("6443")
    override fun partOne(input: String): Int {
        return input.split("\n\n")
            .asSequence()
            .map { it.trim() }
            .map { group -> group.filter { it in 'a'..'z' } }
            .map { it.toSet().size }
            .sum()
    }

    @Answer("3232")
    override fun partTwo(input: String): Int {
        return input.split("\n\n")
            .map { it.split("\n") }
            .map { group -> group.filter { it.isNotBlank() } }
            .map { countAllAnsweredYes(it) }
            .sum()
    }

    private fun countAllAnsweredYes(answers: List<String>): Int {
        if (answers.size == 1) {
            return answers[0].length
        }
        var allAnsweredYes = answers[0].toCharArray().toSet()
        for (answer in answers.drop(1)) {
            allAnsweredYes = allAnsweredYes.intersect(answer.toCharArray().toSet())
        }
        return allAnsweredYes.size
    }
}