package dev.schaer.aoc.y2020

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer

fun main() {
    HandyHaversacks().singleRun()
}

@Year(2020)
@Day(7)
class HandyHaversacks : Solution {

    private val isContained = HashMap<String, MutableList<String>>()
    private val contains = HashMap<String, MutableList<String>>()
    private val containers = HashSet<String>()

    override fun reset() {
        isContained.clear()
        contains.clear()
        containers.clear()
    }

    // TODO extremely ugly solution, find a better one
    @Answer("115")
    override fun partOne(input: String): Int {
        // containee -> List(containers)
        input.split("\n")
            .map { rule -> rule.split(" bags contain ") }
            .forEach { rule ->
                val container = rule[0]
                if (!rule[1].startsWith("no other")) {
                    rule[1].split(", ")
                        .map { containee ->
                            val tokens = containee.split(" ")
                            return@map "${tokens[1]} ${tokens[2]}"
                        }
                        .onEach { contains.computeIfAbsent(container) { mutableListOf() }.add(it) }
                        .onEach { isContained.computeIfAbsent(it) { mutableListOf() }.add(container) }
                }
            }
        countPossibleContainers("shiny gold")
        return containers.size
    }

    private fun countPossibleContainers(containee: String) {
        for (container in isContained[containee] ?: emptyList()) {
            containers.add(container)
            countPossibleContainers(container)
        }
    }

    @Answer("1250")
    override fun partTwo(input: String): Int {
        val bags = HashMap<String, Bag>()
        for ((color, contains) in input.split("\n").map { it.split(" bags contain ") }) {
            val bag = bags.computeIfAbsent(color) { k -> Bag(k) }
            if (contains != "no other bags.") {
                for ((amount, color1, color2) in contains.split(",").map { it.trim().split(" ") }) {
                    val containedColor = "$color1 $color2"
                    val containedBag = bags.computeIfAbsent(containedColor) { k -> Bag(k) }
                    bag.contains[containedBag] = amount.toInt()
                }
            }
        }
        val myBag = bags["shiny gold"]!!
        return (countContainedBags(myBag) - 1)
    }

    private fun countContainedBags(bag: Bag): Int {
        return bag.contains.map {
            it.value * countContainedBags(it.key)
        }.sum() + 1
    }
}

private data class Bag(val color: String, val contains: MutableMap<Bag, Int> = HashMap())
