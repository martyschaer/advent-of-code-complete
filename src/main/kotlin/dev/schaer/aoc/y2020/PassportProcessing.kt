package dev.schaer.aoc.y2020

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer

fun main() {
    PassportProcessing().singleRun()
}

@Year(2020)
@Day(4)
class PassportProcessing : Solution {

    private val passportPattern = Regex("(.{3}:.+\\s)+", RegexOption.MULTILINE)
    private val hclPattern = Regex("^#[a-f0-9]{6}\$")
    private val pidPattern = Regex("^\\d{9}\$")

    private val mandatoryKeys = setOf("byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid")
    private val validEyeColors = setOf("amb", "blu", "brn", "gry", "grn", "hzl", "oth")

    private val validBirthYears = (1920..2002)
    private val validIssueYears = (2010..2020)
    private val validExpirationYears = (2020..2030)

    @Answer("200")
    override fun partOne(input: String): Int {
        return passportPattern.findAll(input)
            .map { it.groupValues[0] }
            .map { extractFields(it) }
            .count { containsMandatoryFields(it) }
    }


    private fun containsMandatoryFields(fields: Map<String, String>): Boolean {
        return fields.keys.containsAll(mandatoryKeys)
    }

    @Answer("116")
    override fun partTwo(input: String): Int {
        return passportPattern.findAll(input)
            .map { it.groupValues[0] }
            .map { extractFields(it) }
            .filter { containsMandatoryFields(it) }
            .count { fieldsHaveValidContent(it) }
    }

    private fun extractFields(rawPassport: String): Map<String, String> {
        return rawPassport
            .replace("\n", " ")
            .split(" ")
            .filter { it.isNotBlank() }
            .map { it.split(":") }
            .map { it[0] to it[1] }.toMap()
    }

    private fun fieldsHaveValidContent(fields: Map<String, String>): Boolean {
        var valid = true

        for ((key, value) in fields) {
            valid = valid and when (key) {
                "byr" -> value.toInt() in validBirthYears
                "iyr" -> value.toInt() in validIssueYears
                "eyr" -> value.toInt() in validExpirationYears
                "hgt" -> isHeightValid(value)
                "hcl" -> value.matches(hclPattern)
                "ecl" -> value in validEyeColors
                "pid" -> value.matches(pidPattern)
                "cid" -> true
                else -> error("Unexpected $key:$value field")
            }
        }

        return valid
    }

    private fun isHeightValid(raw: String): Boolean {
        if (raw.endsWith("cm") && raw.length == 5) {
            return raw.substring(0, 3).toInt() in (150..193)
        }
        if (raw.endsWith("in") && raw.length == 4) {
            return raw.substring(0, 2).toInt() in (59..76)
        }
        return false
    }
}