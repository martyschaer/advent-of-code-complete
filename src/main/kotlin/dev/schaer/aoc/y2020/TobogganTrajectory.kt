package dev.schaer.aoc.y2020

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer

fun main() {
    TobogganTrajectory().singleRun()
}

@Year(2020)
@Day(3)
class TobogganTrajectory : Solution {

    private val width = 31
    private val height = 323
    private val tree = '#'

    @Answer("209")
    override fun partOne(input: String): Int {
        val topo = input.replace("\n", "").toCharArray()
        return measureEncounteredTrees(topo, Slope(3, 1))
    }

    @Answer("1574890240")
    override fun partTwo(input: String): Int {
        val topo = input.replace("\n", "").toCharArray()
        var product = 1
        product *= measureEncounteredTrees(topo, Slope(1, 1))
        product *= measureEncounteredTrees(topo, Slope(3, 1))
        product *= measureEncounteredTrees(topo, Slope(5, 1))
        product *= measureEncounteredTrees(topo, Slope(7, 1))
        product *= measureEncounteredTrees(topo, Slope(1, 2))
        return product
    }

    private fun measureEncounteredTrees(topo: CharArray, slope: Slope): Int {
        var x = 0
        var y = 0
        var treesEncountered = 0
        while (y < height) {
            if (topo[transform(x, y)] == tree) {
                treesEncountered++
            }
            x += slope.x
            y += slope.y
        }
        return treesEncountered
    }

    private fun transform(x: Int, y: Int): Int {
        if (0 <= x && y in 0 until height) {
            return y * width + (x % width)
        }
        error("invalid coordinates x=$x, y=$y")
    }

}

private data class Slope(val x: Int, val y: Int)
