package dev.schaer.aoc.y2020

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer

fun main() {
    AllergenAssessment().singleRun()
}

@Year(2020)
@Day(21)
class AllergenAssessment : Solution {

    @Answer("2569")
    override fun partOne(input: String): Int {
        val allProducts = parseInput(input)

        val allIngredients = mutableMapOf<String, Int>()
        val possibleIngredients = mutableMapOf<String, MutableSet<String>>()

        for (product in allProducts) {
            product.ingredients.forEach {
                allIngredients.computeIfAbsent(it) { 0 }
                allIngredients[it] = allIngredients[it]!! + 1
            }
            for (allergen in product.allergens) {
                if (allergen in possibleIngredients) {
                    possibleIngredients[allergen]!!.retainAll(product.ingredients)
                } else {
                    possibleIngredients[allergen] = product.ingredients.toMutableSet()
                }
            }
        }

        val fixed = mutableMapOf<String, String>()
        while (possibleIngredients.isNotEmpty()) {
            val (allergen, ingredients) = possibleIngredients.minByOrNull { it.value.size }!!
            fixed[allergen] = ingredients.first()
            for (other in possibleIngredients.values) {
                other.remove(fixed[allergen])
            }
            possibleIngredients.remove(allergen)
        }
        return allIngredients.filterNot { fixed.values.contains(it.key) }.map { it.value }.sum()
    }

    private val productPattern = Regex("^(?<ingredients>(\\w+ )+)\\(contains (?<allergens>[^)]+)\\)\$")

    private fun parseInput(input: String): List<Product> {
        return input.split("\n")
            .map { line ->
                val groups = productPattern.matchEntire(line)?.groups!!
                val ingredients = groups["ingredients"]!!.value
                val allergens = groups["allergens"]!!.value
                Product(
                    ingredients = ingredients.split(" ").filter { it.isNotBlank() }.map { it.trim() },
                    allergens = allergens.split(",").map { it.trim() }
                )
            }
    }

    // TODO de-duplicate
    @Answer("vmhqr,qxfzc,khpdjv,gnrpml,xrmxxvn,rfmvh,rdfr,jxh")
    override fun partTwo(input: String): String {
        val allProducts = parseInput(input)

        val allIngredients = mutableMapOf<String, Int>()
        val possibleIngredients = mutableMapOf<String, MutableSet<String>>()

        for (product in allProducts) {
            product.ingredients.forEach {
                allIngredients.computeIfAbsent(it) { 0 }
                allIngredients[it] = allIngredients[it]!! + 1
            }
            for (allergen in product.allergens) {
                if (allergen in possibleIngredients) {
                    possibleIngredients[allergen]!!.retainAll(product.ingredients)
                } else {
                    possibleIngredients[allergen] = product.ingredients.toMutableSet()
                }
            }
        }

        val fixed = mutableMapOf<String, String>()
        while (possibleIngredients.isNotEmpty()) {
            val (allergen, ingredients) = possibleIngredients.minByOrNull { it.value.size }!!
            fixed[allergen] = ingredients.first()
            for (other in possibleIngredients.values) {
                other.remove(fixed[allergen])
            }
            possibleIngredients.remove(allergen)
        }
        return fixed.keys.sorted().map { fixed[it]!! }.joinToString(",")
    }
}

private data class Product(val ingredients: List<String>, val allergens: List<String>) {

}