package dev.schaer.aoc.y2020

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer

fun main() {
    RambunctiousRecitation().singleRun()
}

private const val LIMIT = 2020
private const val LIMIT_LARGE = 30000000

@Year(2020)
@Day(15)
class RambunctiousRecitation : Solution {

    @Answer("866")
    override fun partOne(input: String): Int {
        return solve(input, LIMIT)
    }

    @Answer("1437692")
    override fun partTwo(input: String): Int {
        return solve(input, LIMIT_LARGE)
    }

    private fun solve(input: String, limit: Int): Int {
        val startingNums = input.split(",").map { it.toInt() }
        val memory = IntArray(limit) { -1 }

        startingNums.dropLast(1).forEachIndexed { idx, it -> memory[it] = idx }

        var turn = startingNums.size - 1
        var lastNum = startingNums.last()
        var result = -2

        while (turn < limit) {
            result = lastNum
            val occurrence = memory[lastNum]
            memory[lastNum] = turn
            lastNum = if (occurrence == -1) 0 else turn - occurrence
            ++turn
        }

        return result
    }
}