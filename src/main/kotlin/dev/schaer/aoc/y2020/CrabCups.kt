package dev.schaer.aoc.y2020

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import dev.schaer.aoc.common.annotations.LongRunning
import dev.schaer.aoc.datastructures.circular.CircularLinkedList

fun main() {
    CrabCups().singleRun()
}

@Year(2020)
@Day(23)
@LongRunning(5)
class CrabCups : Solution {

    @Answer("47598263")
    override fun partOne(input: String): String {
        val cups = parseInput(input)
        val circle = play(cups, 100)
        return circle.iterateFromExclusive(1).map { it.toString() }.joinToString("")
    }

    @Answer("248009574232")
    override fun partTwo(input: String): Long {
        val cups = parseAndExtendInput(input)
        val circle = play(cups, 10_000_000)
        val cupA = circle.getAfter(1)
        val cupB = circle.getAfter(cupA).toLong()
        return cupA * cupB
    }

    private fun parseAndExtendInput(input: String): List<Int> {
        val list = parseInput(input).toMutableList()
        list.addAll((10..1_000_000))
        return list
    }

    private fun parseInput(input: String): List<Int> {
        // turn ascii string of [0-9] into list of ints 0-9
        return input.toCharArray().map { it.code - 0x30 }
    }

    private fun play(cups: List<Int>, moves: Int): CircularLinkedList<Int> {
        val max = cups.maxOrNull() ?: error("Could not determine max cup")
        val circle = CircularLinkedList(cups)
        move(circle.first(), circle, max, moves)
        return circle
    }

    private tailrec fun move(currentCup: Int, circle: CircularLinkedList<Int>, maxCup: Int, remainingMoves: Int) {
        if (remainingMoves == 0) {
            return
        }

        val a = circle.removeAfter(currentCup)
        val b = circle.removeAfter(currentCup)
        val c = circle.removeAfter(currentCup)

        val destinationCup = findDestinationCup(currentCup - 1, maxCup, a, b, c)

        circle.insertAfter(destinationCup, a, b, c)

        move(circle.getAfter(currentCup), circle, maxCup, remainingMoves - 1)
    }

    private tailrec fun findDestinationCup(destinationCup: Int, max: Int, a: Int, b: Int, c: Int): Int {
        if (destinationCup < 1) {
            // loop round and go again with the biggest cup
            return findDestinationCup(max, max, a, b, c)
        }
        if (destinationCup != a && destinationCup != b && destinationCup != c) {
            return destinationCup
        }
        return findDestinationCup(destinationCup - 1, max, a, b, c)
    }
}