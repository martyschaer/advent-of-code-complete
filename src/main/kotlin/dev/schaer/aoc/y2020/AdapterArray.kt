package dev.schaer.aoc.y2020

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer

fun main() {
    AdapterArray().singleRun()
}

@Year(2020)
@Day(10)
class AdapterArray : Solution {

    private val cache = HashMap<Int, Long>()

    override fun reset() {
        cache.clear()
    }

    @Answer("2368")
    override fun partOne(input: String): Any {
        val adapters = input.split("\n").map { it.toInt() }.sorted().toList()
        val joltDifferences = mutableMapOf(1 to 0, 2 to 0, 3 to 0)

        // count first difference from 0
        joltDifferences[adapters.first()] = 1

        // count final difference to device
        joltDifferences[3] = joltDifferences[3]!! + 1

        for (i in 1 until adapters.size) {
            val diff = adapters[i] - adapters[i - 1]
            joltDifferences[diff] = joltDifferences[diff]!! + 1
        }

        return joltDifferences[1]!! * joltDifferences[3]!!
    }

    @Answer("1727094849536")
    override fun partTwo(input: String): Long {
        val adapters = input.split("\n")
            .map { it.toInt() }
            .toMutableList()
        adapters.add(0, 0)

        val maxAdapter = adapters.maxOrNull() ?: error("Could not find max adapter")

        adapters.add(maxAdapter + 3)

        val adjacencyMatrix = buildAdjacencyMatrix(adapters) { a, b -> ((b - a) in 1..3) }

        return countPathsThroughGraph(adjacencyMatrix)
    }

    private fun buildAdjacencyMatrix(nodes: List<Int>, canConnect: (a: Int, b: Int) -> Boolean): Array<BooleanArray> {
        return Array(nodes.size) { a ->
            BooleanArray(nodes.size) { b -> canConnect(nodes[a], nodes[b]) }
        }
    }

    private fun countPathsThroughGraph(adjacencyMatrix: Array<BooleanArray>): Long {
        return countPathsThroughGraph(adjacencyMatrix, 0)
    }

    private fun countPathsThroughGraph(adjacencyMatrix: Array<BooleanArray>, node: Int): Long {
        // memoization
        if (cache.containsKey(node)) {
            return cache[node]!!
        }

        // base case
        if (node == adjacencyMatrix.size - 1) {
            return 1
        }

        // general case
        val edges = adjacencyMatrix[node]
        var sum = 0L
        for (nextNode in edges.indices) {
            if (edges[nextNode]) {
                sum += countPathsThroughGraph(adjacencyMatrix, nextNode)
            }
        }

        // memoization
        cache[node] = sum

        return sum
    }
}