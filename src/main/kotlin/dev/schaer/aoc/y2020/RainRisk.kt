package dev.schaer.aoc.y2020

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import dev.schaer.aoc.datastructures.Vector2

fun main() {
    RainRisk().singleRun()
}

private enum class Direction(val vec: Vector2) {
    N(Vector2(0, 1)),
    E(Vector2(1, 0)),
    S(Vector2(0, -1)),
    W(Vector2(-1, 0))
}

@Year(2020)
@Day(12)
class RainRisk : Solution {

    @Answer("441")
    override fun partOne(input: String): Int {
        var direction = Direction.E
        val shipPos = Vector2(0, 0)

        input.split("\n")
            .map { it[0].toString() to it.substring(1).toInt() }
            .forEach {
                val instruction = it.first
                val value = it.second
                when (instruction) {
                    "N", "E", "S", "W" -> {
                        val dir = Direction.valueOf(instruction)
                        shipPos.plusAssign(dir.vec.times(value))
                    }
                    "L" -> direction = calculateNewDirection(direction, -value)
                    "R" -> direction = calculateNewDirection(direction, value)
                    "F" -> shipPos.plusAssign(direction.vec.times(value))
                    else -> error("Unknown instruction $instruction($value)")
                }
            }
        return shipPos.manhattanLength()
    }

    private fun calculateNewDirection(currentDirection: Direction, value: Int): Direction {
        val increments = value / 90
        return Direction.values()[(currentDirection.ordinal + 8 + increments) % Direction.values().size]
    }

    @Answer("40014")
    override fun partTwo(input: String): Int {
        val wayPoint = Vector2(10, 1)
        val shipPos = Vector2(0, 0)

        input.split("\n")
            .map { it[0].toString() to it.substring(1).toInt() }
            .forEach {
                val instruction = it.first
                val value = it.second
                when (instruction) {
                    "N", "E", "S", "W" -> {
                        val dir = Direction.valueOf(instruction)
                        wayPoint.plusAssign(dir.vec.times(value))
                    }
                    "L" -> wayPoint.rotateMut(value)
                    "R" -> wayPoint.rotateMut(-value)
                    "F" -> shipPos.plusAssign(wayPoint.times(value))
                    else -> error("Unknown instruction $instruction($value)")
                }
            }
        return shipPos.manhattanLength()
    }
}