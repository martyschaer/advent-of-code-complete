package dev.schaer.aoc.y2020

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import dev.schaer.aoc.datastructures.head
import dev.schaer.aoc.datastructures.tail
import kotlin.math.ceil

fun main() {
    ShuttleSearch().singleRun()
}

@Year(2020)
@Day(13)
class ShuttleSearch : Solution {

    @Answer("102")
    override fun partOne(input: String): Long {
        val lines = input.split("\n")
        val earliestDepartureTime = lines[0].toInt()
        val busLines = parseInput(lines[1]).map { it.id }

        val result = busLines
            .map { calcEarliestPossibleBusDeparture(earliestDepartureTime, it) }
            .minByOrNull { it.first } ?: error("No solution found")
        val actualDepartureTime = result.first
        val busLine = result.second

        val waitTime = actualDepartureTime - earliestDepartureTime
        return waitTime * busLine
    }

    private fun calcEarliestPossibleBusDeparture(earliestDepartureTime: Int, busLine: Long): Pair<Long, Long> {
        return ceil(earliestDepartureTime / busLine.toFloat()).toInt() * busLine to busLine
    }

    @Answer("327300950120029")
    override fun partTwo(input: String): Long {
        return solvePart2(input.split("\n")[1])
    }

    fun solvePart2(input: String): Long {
        val busLines = parseInput(input).reversed()

        val firstBus = busLines.head()
        val max = busLines.map { it.id }.fold(1L) { acc, it -> acc * it }
        val timestamp = firstBus.id - firstBus.offset

        return solve(firstBus, busLines.tail(), stepSize = firstBus.id, timestamp, max) % max
    }

    private fun parseInput(input: String): List<BusLine> {
        return input.split(",")
            .mapIndexed { idx, it -> it to idx }
            .filter { it.first != "x" }
            .map { BusLine.of(it.first, it.second) }
    }

    private fun solve(current: BusLine, remaining: List<BusLine>, stepSize: Long, timestamp: Long, max: Long): Long {
        var ts = timestamp
        while (true) {
            if ((ts + current.offset) % current.id == 0L) {
                if (remaining.isEmpty()) {
                    return ts
                }
                return solve(remaining.head(), remaining.tail(), stepSize * current.id, ts, max)
            }
            ts += stepSize % max
        }
    }
}

private data class BusLine(val id: Long, val offset: Long) {
    companion object {
        fun of(id: String, offset: Int): BusLine {
            return BusLine(id.toLong(), offset.toLong())
        }
    }
}
