package dev.schaer.aoc.y2020

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import dev.schaer.aoc.datastructures.Rotation
import dev.schaer.aoc.datastructures.get
import dev.schaer.aoc.datastructures.product

fun main() {
    JurassicJigsaw().singleRun()
}

private const val MATCHING_BORDERS_CORNER = 192
private const val MATCHING_BORDERS_EDGE = 224
private const val MATCHING_BORDERS_CENTER = 256
private const val PATTERN_H = 3
private const val PATTERN_W = 3

private const val TILE_H = 10
private const val TILE_W = 10

@Year(2020)
@Day(20)
class JurassicJigsaw : Solution {

    @Answer("14129524957217")
    override fun partOne(input: String): Long {
        val fragments = parseInput(input)

        val corners = findCorners(fragments)

        return corners.map { it.id.toLong() }.product()
    }

    private fun findCorners(fragments: List<Fragment>): List<Fragment> {
        val occurrences = findEdgeOccurrences(fragments)

        return fragments
            .filter { fragment ->
                fragment.edges
                    .flatMap { it.value }
                    .map { occurrences[it]!! }
                    .sum() == 192
            }
    }

    private fun findEdgeOccurrences(fragments: List<Fragment>): Map<Int, Int> {
        return fragments
            .flatMap { it.edges.values }
            .flatten()
            .groupingBy { it }
            .eachCount()
    }

    private fun parseInput(input: String): List<Fragment> {
        return input.split("\n\n").map { entry ->
            val (rawTileId, content) = entry.split(":").map { it.replace("\n", "") }
            val tileId = rawTileId.substring(5).toInt()
            Fragment(tileId, content.toCharArray())
        }
    }

    override fun partTwo(input: String): Int {
        val fragments = parseInput(input)
        val occurrences = findEdgeOccurrences(fragments)
        val categorisedFragments = fragments
            .map { fragment ->
                fragment to fragment.edges
                    .flatMap { it.value }
                    .map { occurrences[it]!! }
                    .sum()
            }

        val corners = categorisedFragments.filter { it.second == MATCHING_BORDERS_CORNER }.map { it.first }
        val edges = categorisedFragments.filter { it.second == MATCHING_BORDERS_EDGE }.map { it.first }
        val centers = categorisedFragments.filter { it.second == MATCHING_BORDERS_CENTER }.map { it.first }

        val edgeEdges = edges.flatMap { edge -> edge.edges.flatMap { it.value } }
        val corner = corners[0]
        corner.edges.forEach { (transform, edges) ->
//            println("${transform.name} => ${edgeEdges.count { edges.contains(it) }}")
        }
        return 0
    }
}

enum class Transformation(val rotation: Rotation, val flipVertical: Boolean = false) {
    NONE(Rotation.ZERO),
    A(Rotation.NINETY),
    B(Rotation.TWOSEVENTY, flipVertical = true),
    C(Rotation.TWOSEVENTY),
    D(Rotation.NINETY, flipVertical = true),
    E(Rotation.ZERO, flipVertical = true),
    F(Rotation.ONEEIGHTY, flipVertical = true),
    G(Rotation.ONEEIGHTY)
}

enum class Position {
    TOP, RIGHT, BOTTOM, LEFT;
}

class Fragment(
    val id: Int,
    val content: CharArray,
    var transformation: Transformation = Transformation.NONE
) {
    val edges = Transformation.values()
        .map { it to calculateEdgesForRotation(content, it.rotation, it.flipVertical) }
        .toMap()

    fun getEdge(position: Position): Int {
        return getEdge(position, transformation)
    }

    fun getEdge(position: Position, transformation: Transformation): Int {
        return edges[transformation]!![position.ordinal]
    }

    private fun calculateEdgesForRotation(content: CharArray, rotation: Rotation, flip: Boolean): List<Int> {
        var top = 0
        var left = 0
        var right = 0
        var bottom = 0
        for (y in 0 until TILE_H) {
            for (x in 0 until TILE_W) {
                val v = if (content.get(x, y, TILE_H, TILE_W, flip, rotation) == '#') 1 else 0
                if (y == 0) {
                    top = top or v
                    top = top shl 1
                }
                if (x == 0) {
                    left = left or v
                    left = left shl 1
                }
                if (x == TILE_W - 1) {
                    right = right or v
                    right = right shl 1
                }
                if (y == TILE_H - 1) {
                    bottom = bottom or v
                    bottom = bottom shl 1
                }
            }
        }
        return listOf(top, right, bottom, left)
    }
}
