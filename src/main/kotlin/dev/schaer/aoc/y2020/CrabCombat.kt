package dev.schaer.aoc.y2020

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import java.util.*
import kotlin.math.max
import kotlin.math.min

fun main() {
    CrabCombat().singleRun()
}

@Year(2020)
@Day(22)
class CrabCombat : Solution {

    @Answer("31629")
    override fun partOne(input: String): Int {
        val (aDeck, bDeck) = parseInput(input)
        while (aDeck.isNotEmpty() && bDeck.isNotEmpty()) {
            val a = aDeck.poll()
            val b = bDeck.poll()
            if (a < b) {
                bDeck.offer(max(a, b))
                bDeck.offer(min(a, b))
            } else if (b < a) {
                aDeck.offer(max(a, b))
                aDeck.offer(min(a, b))
            }
        }
        return calculateWinnerScore(aDeck, bDeck)
    }

    private fun calculateWinnerScore(aDeck: Queue<Int>, bDeck: Queue<Int>): Int {
        val winner = if (aDeck.isNotEmpty()) aDeck else bDeck
        val cards = winner.size

        return winner.foldIndexed(0) { idx, acc, it -> acc + it * (cards - idx) }
    }

    private fun parseInput(input: String): List<Queue<Int>> {
        return input.split("\n\n")
            .map { player -> player.split("\n").drop(1).map { it.toInt() } }
            .map { LinkedList(it) }
    }

    @Answer("35196")
    override fun partTwo(input: String): Int {
        val (aDeck, bDeck) = parseInput(input)

        recursiveCombat(aDeck, bDeck)

        return calculateWinnerScore(aDeck, bDeck)
    }

    private fun recursiveCombat(aDeck: Queue<Int>, bDeck: Queue<Int>): Boolean {
        val previousRounds = hashSetOf<Int>()
        while (aDeck.isNotEmpty() && bDeck.isNotEmpty()) {
            val deckConfig = calculateDeckConfig(aDeck, bDeck)
            if (deckConfig in previousRounds) {
                return true
            } else {
                previousRounds.add(deckConfig)
            }

            val a = aDeck.poll()
            val b = bDeck.poll()

            if (shouldPlaySubgame(aDeck, a, bDeck, b)) {
                val playerAWins = recursiveCombat(copyDeck(aDeck, a), copyDeck(bDeck, b))
                if (playerAWins) {
                    aDeck.offer(a)
                    aDeck.offer(b)
                } else {
                    bDeck.offer(b)
                    bDeck.offer(a)
                }
            } else {
                if (a > b) {
                    aDeck.offer(max(a, b))
                    aDeck.offer(min(a, b))
                } else {
                    bDeck.offer(max(a, b))
                    bDeck.offer(min(a, b))
                }
            }

        }
        return aDeck.isNotEmpty()
    }

    private fun calculateDeckConfig(aDeck: Queue<Int>, bDeck: Queue<Int>): Int {
        return aDeck.hashCode() + bDeck.hashCode()
    }

    private fun shouldPlaySubgame(aDeck: Queue<Int>, aCard: Int, bDeck: Queue<Int>, bCard: Int): Boolean {
        return aDeck.size >= aCard && bDeck.size >= bCard
    }

    private fun copyDeck(deck: Queue<Int>, cardsDrawn: Int): Queue<Int> {
        return LinkedList(deck.take(cardsDrawn))
    }
}
