package dev.schaer.aoc.y2020.mathtree

enum class Operation {
    ADD, MUL;

    companion object {
        fun of(input: String): Operation {
            return when (input.trim()) {
                "+" -> ADD
                "*" -> MUL
                else -> error("Unknown operation '$input'")
            }
        }
    }
}