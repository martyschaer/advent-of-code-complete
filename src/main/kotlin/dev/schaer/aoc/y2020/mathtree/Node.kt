package dev.schaer.aoc.y2020.mathtree

interface Node {
    fun eval(): Long
}