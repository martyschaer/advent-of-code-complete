package dev.schaer.aoc.y2020.mathtree

class OpNode(private val op: Operation, private val left: Node, private val right: Node) : Node {
    override fun eval(): Long {
        val a = left.eval()
        val b = right.eval()
        return when (op) {
            Operation.ADD -> a + b
            Operation.MUL -> a * b
        }
    }

}