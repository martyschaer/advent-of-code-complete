package dev.schaer.aoc.y2020.mathtree

class LeafNode(private val value: Long) : Node {
    override fun eval(): Long {
        return value
    }
}