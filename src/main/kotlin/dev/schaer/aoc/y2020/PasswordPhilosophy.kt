package dev.schaer.aoc.y2020

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import java.util.regex.Pattern

fun main() {
    PasswordPhilosophy().singleRun()
}

@Year(2020)
@Day(2)
class PasswordPhilosophy : Solution {

    @Answer("467")
    override fun partOne(input: String): Int {
        return input.split("\n")
            .map { PasswordEntry.parse(it) }
            .count { validatePartOne(it) }
    }

    private fun validatePartOne(entry: PasswordEntry): Boolean {
        return entry.validate { from, to, char, pass -> pass.count { it == char } in from..to }
    }

    @Answer("441")
    override fun partTwo(input: String): Int {
        return input.split("\n")
            .map { PasswordEntry.parse(it) }
            .count { validatePartTwo(it) }
    }

    private fun validatePartTwo(entry: PasswordEntry): Boolean {
        return entry.validate { from, to, char, pass -> (pass[from - 1] == char) xor (pass[to - 1] == char) }
    }
}

private data class PasswordEntry(val occur_from: Int, val occur_to: Int, val validation: Char, val password: String) {
    fun validate(validator: (Int, Int, Char, String) -> Boolean): Boolean {
        return validator.invoke(occur_from, occur_to, validation, password)
    }

    companion object {
        val pattern = Pattern.compile("^(\\d+)-(\\d+) ([a-z]): ([a-z]+)\$").toRegex()

        fun parse(input: String): PasswordEntry {
            val tokens = pattern.matchEntire(input)?.groupValues ?: error("Unparsable input: '$input'")
            return PasswordEntry(
                occur_from = tokens[1].toInt(),
                occur_to = tokens[2].toInt(),
                validation = tokens[3].toCharArray()[0],
                password = tokens[4]
            )
        }
    }
}