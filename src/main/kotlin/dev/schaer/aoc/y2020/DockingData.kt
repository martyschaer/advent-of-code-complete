package dev.schaer.aoc.y2020

import dev.schaer.aoc.algorithms.setBit
import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import java.util.*
import kotlin.collections.HashMap

fun main() {
    DockingData().singleRun()
}

@Year(2020)
@Day(14)
class DockingData : Solution {

    @Answer("15172047086292")
    override fun partOne(input: String): Long {
        val mem: MutableMap<Long, Long> = HashMap()
        var mask = BitMask("X".repeat(MASK_LEN))

        input.split("\n")
            .map { line -> line.split("=").map { it.trim() } }
            .forEach { instruction ->
                if (instruction[0].startsWith("mask")) {
                    mask = BitMask(instruction[1].trim())
                } else {
                    val address = instruction[0].substring(4, instruction[0].length - 1).toLong()
                    val value = mask.apply(instruction[1].trim().toLong())
                    mem[address] = value
                }
            }

        return mem.values.sum()
    }

    @Answer("4197941339968")
    override fun partTwo(input: String): Long {
        val mem: MutableMap<Long, Long> = HashMap()
        var mask = BitMask("X".repeat(MASK_LEN))

        input.split("\n")
            .map { line -> line.split("=").map { it.trim() } }
            .forEach { instruction ->
                if (instruction[0].startsWith("mask")) {
                    mask = BitMask(instruction[1].trim())
                } else {
                    val addresses = mask.addressDecode(instruction[0].substring(4, instruction[0].length - 1).toLong())
                    val value = instruction[1].trim().toLong()
                    addresses.forEach { address -> mem[address] = value }
                }
            }

        return mem.values.sum()
    }
}

private const val MASK_LEN = 36
const val LENGTH_MASK = 0xFFFFFFFFF

class BitMask(rawInput: String) {
    private val ones: Long
    private val zeroes: Long
    private val floating = BitSet(MASK_LEN)

    init {
        var ones = 0L
        var zeroes = 0L
        rawInput.forEachIndexed { idx, char ->
            when (char) { // zero values don't need to be set
                'X' -> {
                    zeroes = zeroes or 0x1
                    floating.set(MASK_LEN - idx - 1)
                }
                '1' -> {
                    ones = ones or 0x1
                    zeroes = zeroes or 0x1
                }
            }
            ones = ones shl 1
            zeroes = zeroes shl 1
        }

        // the above loop shifts one too many times, shift back by one
        this.ones = ones shr 1
        this.zeroes = zeroes shr 1
    }

    fun apply(n: Long): Long {
        return ((n and LENGTH_MASK) or ones) and zeroes
    }

    fun addressDecode(n: Long): LongArray {
        val applied = (n and LENGTH_MASK) or ones
        val floatingBits = (0 until MASK_LEN).filter { floating.get(it) }
        val width = floatingBits.size
        val numAddress = 1 shl width // 2^width

        val addresses = LongArray(numAddress)

        for (i in 0 until numAddress) {
            addresses[i] = setFloatingBits(applied, floatingBits, i)
        }

        return addresses
    }

    /**
     * Sets the bits in the input to the bits of value at the positions indicated by indicesToSet
     */
    private fun setFloatingBits(input: Long, indicesToSet: List<Int>, value: Int): Long {
        var out = input
        for (offset in indicesToSet.indices) {
            val b = (value shr offset) and 1
            out = out.setBit(indicesToSet[offset], b == 1)
        }
        return out
    }
}