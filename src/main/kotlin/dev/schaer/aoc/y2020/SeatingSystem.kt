package dev.schaer.aoc.y2020

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer

fun main() {
    SeatingSystem().singleRun()
}

private const val FLOOR = '.'
private const val EMPTY = 'L'
private const val OCCUPIED = '#'

private const val W = 98
private const val H = 93

@Year(2020)
@Day(11)
class SeatingSystem : Solution {

    @Answer("2329")
    override fun partOne(input: String): Int {
        var seats = input.replace("\n", "").toCharArray()
        var newSeats = seats
        do {
            seats = newSeats
            newSeats = simulatePartOne(seats)
        } while (!newSeats.contentEquals(seats))

        return newSeats.count { it == OCCUPIED }
    }

    private fun simulatePartOne(seats: CharArray): CharArray {
        val newSeats = seats.clone() // TODO all this array copying is awfully slow
        for (y in 0 until H) {
            for (x in 0 until W) {
                val occupiedNeighbours = getOccupiedNeighbouringSeats(x, y, seats)
                val seat = getSeat(x, y, seats)
                if (seat == OCCUPIED && occupiedNeighbours >= 4) {
                    newSeats[calcIdx(x, y)] = EMPTY
                } else if (seat == EMPTY && occupiedNeighbours == 0) {
                    newSeats[calcIdx(x, y)] = OCCUPIED
                }
            }
        }
        return newSeats
    }

    private fun getOccupiedNeighbouringSeats(x: Int, y: Int, seats: CharArray): Int {
        return isOccupied(x - 1, y - 1, seats) + isOccupied(x, y - 1, seats) + isOccupied(x + 1, y - 1, seats) +
                isOccupied(x - 1, y, seats) + isOccupied(x + 1, y, seats) +
                isOccupied(x - 1, y + 1, seats) + isOccupied(x, y + 1, seats) + isOccupied(x + 1, y + 1, seats)
    }

    private fun isOccupied(x: Int, y: Int, seats: CharArray): Int {
        return if (getSeat(x, y, seats) == OCCUPIED) 1 else 0
    }

    private fun getSeat(x: Int, y: Int, seats: CharArray): Char {
        if (isValidSeat(x, y)) {
            return seats[calcIdx(x, y)]
        }
        return FLOOR
    }

    private fun isValidSeat(x: Int, y: Int): Boolean {
        return x in 0 until W && y in 0 until H
    }

    private fun calcIdx(x: Int, y: Int): Int {
        return y * W + x
    }

    @Answer("2138")
    override fun partTwo(input: String): Int {
        var seats = input.replace("\n", "").toCharArray()
        var newSeats = seats
        do {
            seats = newSeats
            newSeats = simulatePartTwo(seats)
        } while (!newSeats.contentEquals(seats))

        return newSeats.count { it == OCCUPIED }
    }

    private fun simulatePartTwo(seats: CharArray): CharArray {
        val newSeats = seats.clone() // TODO all this array copying is awfully slow
        for (y in 0 until H) {
            for (x in 0 until W) {
                val occupiedNeighbours = getOccupiedVisibleSeats(x, y, seats)
                val seat = getSeat(x, y, seats)
                if (seat == OCCUPIED && occupiedNeighbours >= 5) {
                    newSeats[calcIdx(x, y)] = EMPTY
                } else if (seat == EMPTY && occupiedNeighbours == 0) {
                    newSeats[calcIdx(x, y)] = OCCUPIED
                }
            }
        }
        return newSeats
    }

    private fun getOccupiedVisibleSeats(x: Int, y: Int, seats: CharArray): Int {
        return getVisibleSeat(x, y, seats, -1, -1) +
                getVisibleSeat(x, y, seats, 0, -1) +
                getVisibleSeat(x, y, seats, 1, -1) +
                getVisibleSeat(x, y, seats, -1, 0) +
                getVisibleSeat(x, y, seats, 1, 0) +
                getVisibleSeat(x, y, seats, -1, 1) +
                getVisibleSeat(x, y, seats, 0, 1) +
                getVisibleSeat(x, y, seats, 1, 1)
    }

    private fun getVisibleSeat(prevX: Int, prevY: Int, seats: CharArray, xTransform: Int, yTransform: Int): Int {
        val x = prevX + xTransform
        val y = prevY + yTransform
        if (!isValidSeat(x, y)) {
            return 0
        }
        return when (getSeat(x, y, seats)) {
            FLOOR -> getVisibleSeat(x, y, seats, xTransform, yTransform)
            EMPTY -> 0
            OCCUPIED -> 1
            else -> error("Unknown seat at ($x, $y) = ${getSeat(x, y, seats)}")
        }
    }
}