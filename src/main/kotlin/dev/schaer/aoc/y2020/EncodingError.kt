package dev.schaer.aoc.y2020

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer

fun main() {
    EncodingError().singleRun()
}

const val PREAMBLE_LENGTH = 25

@Year(2020)
@Day(9)
class EncodingError : Solution {

    @Answer("36845998")
    override fun partOne(input: String): Long {
        val stream = input.split("\n")
            .map { it.toLong() }
            .toList()

        return findInvalidNumber(stream)
    }

    private fun findInvalidNumber(stream: List<Long>): Long {
        for (i in PREAMBLE_LENGTH until stream.size) {
            if (!validate(stream, i)) {
                return stream[i]
            }
        }
        error("No result found")
    }

    private fun validate(stream: List<Long>, position: Int): Boolean {
        val summands = stream.subList(position - PREAMBLE_LENGTH, position)
        for (a in summands.indices) {
            for (b in (a + 1) until summands.size) {
                if (summands[a] + summands[b] == stream[position]) {
                    return true
                }
            }
        }
        return false
    }

    @Answer("4830226")
    override fun partTwo(input: String): Long {
        val stream = input.split("\n")
            .map { it.toLong() }
            .toList()

        val invalidNumber = findInvalidNumber(stream)
        val maxIndex = stream.indexOf(invalidNumber) - 1

        for (hi in maxIndex downTo 0) {
            for (lo in hi - 2 downTo hi - maxIndex) {
                val range = stream.subList(lo, hi)
                val sum = range.sum()
                if (sum > invalidNumber) {
                    break
                }
                if (sum == invalidNumber) {
                    return (range.minOrNull()!! + range.maxOrNull()!!)
                }
            }
        }
        error("No answer found")
    }
}