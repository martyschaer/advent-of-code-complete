package dev.schaer.aoc.y2020

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer

fun main() {
    MonsterMessages().singleRun()
}

private const val RULE_TO_MATCH = "0"

@Year(2020)
@Day(19)
class MonsterMessages : Solution {

    private val dynamicCache = mutableMapOf<String, String>()

    override fun reset() {
        dynamicCache.clear()
    }

    @Answer("118")
    override fun partOne(input: String): Int {
        val (rulesInput, messages) = input.split("\n\n").map { it.split("\n") }
        val rules = parseRules(rulesInput)
        val matcher = buildMatcherFromRules(rules, RULE_TO_MATCH)
        return messages.count { matcher.matches(it) }
    }

    // TODO this throws a 14k long Regex at the regex engine.
    // It works, but it ain't pretty.
    @Answer("246")
    override fun partTwo(input: String): Int {
        val (rulesInput, messages) = input.split("\n\n").map { it.split("\n") }
        val rules = fixRules(parseRules(rulesInput))
        val matcher = buildMatcherFromRules(rules, RULE_TO_MATCH)
        return messages.count { matcher.matches(it) }
    }

    private fun buildMatcherFromRules(rules: Map<String, String>, ruleToMatch: String): Regex {
        val ruleToRuleAllRules = evalRule(ruleToMatch, rules).replace(" ", "")
        val regex = "^$ruleToRuleAllRules$"
        return Regex(regex)
    }

    private val number = Regex("(\\d+)")

    private fun evalRule(ruleId: String, rules: Map<String, String>): String {
        if (ruleId in dynamicCache) {
            return dynamicCache[ruleId]!!
        }

        var rule = rules[ruleId]!!

        if (rule == "a" || rule == "b") {
            dynamicCache[ruleId] = rule
            return rule
        }
        val matches = number.findAll(rule).toList().asReversed()
        for (match in matches) {
            rule = rule.replaceRange(match.range, evalRule(match.value, rules))
        }

        val result = "($rule)"

        dynamicCache[ruleId] = result

        return result
    }

    private fun parseRules(input: List<String>): Map<String, String> {
        return input.map { it.split(":") }
            .map { it[0] to it[1].replace('"', ' ').trim() }
            .toMap()
    }

    private fun fixRules(input: Map<String, String>): Map<String, String> {
        val result = input.toMutableMap()
        result["8"] = "42 | (42 | 42+)+"
        result["11"] = "42 31 | ${buildNestedExpression(3, "42 (42 31 |", " 42 ", "31)")} 31"
        return result.toMap()
    }

    // this is not doable in a regular grammar.
    // some regex engines support balanced groups, Kotlin's does not
    private fun buildNestedExpression(depth: Int, head: String, body: String, tail: String): String {
        if (depth <= 0) {
            return body
        }
        return buildNestedExpression(depth - 1, head, "$head$body$tail", tail)
    }
}