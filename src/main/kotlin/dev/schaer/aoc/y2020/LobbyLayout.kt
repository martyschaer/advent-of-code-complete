package dev.schaer.aoc.y2020

import dev.schaer.aoc.cellular.Automata
import dev.schaer.aoc.cellular.Cell
import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import dev.schaer.aoc.datastructures.Vector2

fun main() {
    LobbyLayout().singleRun()
}

@Year(2020)
@Day(24)
class LobbyLayout : Solution {

    private val instructionPattern = Regex("(se|sw|nw|ne|e|w)")

    @Answer("228")
    override fun partOne(input: String): Int {
        val instructions = parseInput(input)
        return layTiles(instructions).map { it.value }.count { it % 2 != 0 }
    }

    @Answer("3672")
    override fun partTwo(input: String): Int {
        val instructions = parseInput(input)
        val floor = mutableMapOf<Vector2, Tile>()
        layTiles(instructions)
            .map { (pos, value) -> Tile(pos, floor, value % 2 != 0) }
            .forEach { floor[it.pos] = it }

        val automata = Automata(floor) { pos, cells -> Tile(pos, cells) }
        repeat(100) { automata.step() }
        return automata.count()
    }

    private fun layTiles(instructions: List<Sequence<Instruction>>): Map<Vector2, Int> {
        return instructions
            .map { perTile -> perTile.map { it.vec }.fold(Vector2.ZERO) { acc, it -> acc.add(it) } }
            .groupingBy { it }
            .eachCount()
    }

    private fun parseInput(input: String): List<Sequence<Instruction>> {
        return input.split("\n")
            .map { parseLine(it) }
    }

    private fun parseLine(line: String): Sequence<Instruction> {
        return instructionPattern.findAll(line)
            .map { it.value }
            .map { Instruction.of(it) }
    }
}

private enum class Instruction(val vec: Vector2) {
    E(Vector2(1, 0)),
    SE(Vector2(0, -1)),
    SW(Vector2(-1, -1)),
    W(Vector2(-1, 0)),
    NW(Vector2(0, 1)),
    NE(Vector2(1, 1));

    companion object {
        fun of(raw: String): Instruction {
            return valueOf(raw.uppercase())
        }
    }
}

private class Tile(pos: Vector2, floor: MutableMap<Vector2, Tile>, active: Boolean = false) :
    Cell<Vector2>(pos, floor, active) {
    override fun calculateNeighbours(): List<Vector2> {
        return Instruction.values().map { pos.add(it.vec) }
    }

    override fun nextActive(currentActive: Boolean, activeNeighbours: Int): Boolean {
        return if (currentActive && activeNeighbours == 0 || activeNeighbours > 2) {
            false
        } else if (!currentActive && activeNeighbours == 2) {
            true
        } else {
            currentActive
        }
    }
}
