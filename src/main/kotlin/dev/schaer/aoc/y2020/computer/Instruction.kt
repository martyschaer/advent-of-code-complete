package dev.schaer.aoc.y2020.computer

abstract class Instruction(val value: Int) {
    abstract fun run(acc: Int): Int
    abstract fun next(insPtr: Int): Int
}