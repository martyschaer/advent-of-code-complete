package dev.schaer.aoc.y2020.computer

class AccInstruction(value: Int) : Instruction(value) {
    override fun run(acc: Int): Int {
        return acc + value
    }

    override fun next(insPtr: Int): Int {
        return insPtr + 1
    }
}