package dev.schaer.aoc.y2020.computer

class JmpInstruction(value: Int) : Instruction(value) {
    override fun run(acc: Int): Int {
        return acc
    }

    override fun next(insPtr: Int): Int {
        return insPtr + value
    }
}