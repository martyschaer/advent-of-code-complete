package dev.schaer.aoc.y2020.computer

class InfiniteLoopError : IllegalStateException {
    constructor(msg: String) : super(msg)
    constructor(msg: Int) : super(msg.toString())
}
