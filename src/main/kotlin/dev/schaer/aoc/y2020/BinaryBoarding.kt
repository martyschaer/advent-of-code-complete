package dev.schaer.aoc.y2020

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer

fun main() {
    BinaryBoarding().singleRun()
}

@Year(2020)
@Day(5)
class BinaryBoarding : Solution {

    @Answer("911")
    override fun partOne(input: String): Int {
        return input.split("\n").maxOf { findSeatId(it) }
    }

    private fun findSeatId(code: String): Int {
        return code
            .replace('F', '0').replace('L', '0')
            .replace('B', '1').replace('R', '1')
            .toInt(2)
    }

    @Answer("629")
    override fun partTwo(input: String): Int {
        val seatIds = input.split("\n")
            .map { findSeatId(it) }
            .sorted()

        // find where the sequence breaks (an empty seat)
        val offSet = seatIds.first()
        return (seatIds
            .filterIndexed { idx, seatId -> idx + offSet != seatId }
            .first() - 1)
    }
}