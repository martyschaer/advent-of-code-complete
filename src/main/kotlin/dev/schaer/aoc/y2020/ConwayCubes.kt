package dev.schaer.aoc.y2020

import dev.schaer.aoc.cellular.Automata
import dev.schaer.aoc.cellular.Cell
import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import dev.schaer.aoc.datastructures.Vector
import dev.schaer.aoc.datastructures.Vector3
import dev.schaer.aoc.datastructures.Vector4

fun main() {
    ConwayCubes().singleRun()
}

@Year(2020)
@Day(17)
class ConwayCubes : Solution {

    @Answer("252")
    override fun partOne(input: String): Int {
        val cubes = parseInput(input) { x, y -> Vector3(x, y, 0) }
        return simulateBootSequence(cubes)
    }

    @Answer("2160")
    override fun partTwo(input: String): Int {
        val cubes = parseInput(input) { x, y -> Vector4(x, y, 0, 0) }
        return simulateBootSequence(cubes)
    }

    private fun <T : Vector<T>> simulateBootSequence(cubes: MutableMap<T, Cube<T>>): Int {
        val automata = Automata(cubes) { pos, cells -> Cube(pos, cells) }
        repeat(6) { automata.step() }
        return automata.count()
    }

    private fun <T : Vector<T>> parseInput(input: String, transform: (x: Int, y: Int) -> T): MutableMap<T, Cube<T>> {
        val cubes = mutableMapOf<T, Cube<T>>()
        input.split("\n").forEachIndexed { x, line ->
            line.mapIndexed { y, char -> y to char }
                .filter { it.second == '#' }
                .map { transform.invoke(x, it.first) }
                .map { pos -> Cube(pos, cubes, true) }
                .forEach { cubes[it.pos] = it }
        }
        return cubes
    }
}

private class Cube<T>(pos: T, cells: MutableMap<T, Cube<T>>, active: Boolean = false) :
    Cell<T>(pos, cells, active) {
    override fun calculateNeighbours(): List<T> {
        return (pos as Vector<T>).neighbours()
    }

    override fun nextActive(currentActive: Boolean, activeNeighbours: Int): Boolean {
        return if (active && activeNeighbours == 2 || activeNeighbours == 3) {
            true
        } else !active && activeNeighbours == 3
    }
}
