package dev.schaer.aoc.y2020

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import dev.schaer.aoc.y2020.computer.*

fun main() {
    HandheldHalting().singleRun()
}

@Year(2020)
@Day(8)
class HandheldHalting : Solution {

    @Answer("1553")
    override fun partOne(input: String): Int {
        val instructions = input.split("\n")
            .map { it.split(" ") }
            .map { tokens ->
                val value = tokens[1].toInt()
                when (tokens[0]) {
                    "nop" -> NopInstruction(value)
                    "jmp" -> JmpInstruction(value)
                    "acc" -> AccInstruction(value)
                    else -> error("Unknown instruction: $tokens")
                }
            }
        try {
            run(instructions).toString()
        } catch (e: InfiniteLoopError) {
            return e.message?.toInt() ?: error("No return value supplied")
        }
        error("No answer found")
    }

    @Answer("1877")
    override fun partTwo(input: String): Int {
        val instructions = input.split("\n")
            .map { it.split(" ") }
            .map { tokens ->
                val value = tokens[1].toInt()
                when (tokens[0]) {
                    "nop" -> NopInstruction(value)
                    "jmp" -> JmpInstruction(value)
                    "acc" -> AccInstruction(value)
                    else -> error("Unknown instruction: $tokens")
                }
            }
        val fixCandidates = generateFixCandidates(instructions)
        for (candidate in fixCandidates) {
            try {
                return run(candidate)
            } catch (e: InfiniteLoopError) {
                // NOP - try another one
            }
        }
        error("No answer found")
    }

    private fun generateFixCandidates(original: List<Instruction>): List<List<Instruction>> {
        val permutations = mutableListOf<List<Instruction>>()
        for (i in original.indices) {
            val copy = original.toMutableList()
            val toSwap = original[i]
            copy[i] = when (toSwap) {
                is JmpInstruction -> NopInstruction(toSwap.value)
                is NopInstruction -> JmpInstruction(toSwap.value)
                else -> toSwap
            }
            permutations.add(copy)
        }
        return permutations
    }

    private fun run(instructions: List<Instruction>): Int {
        val visited = HashSet<Int>()
        var insPtr = 0
        var acc = 0
        do {
            if (visited.contains(insPtr)) {
                throw InfiniteLoopError(acc)
            }
            visited.add(insPtr)
            val instruction = instructions[insPtr]
            acc = instruction.run(acc)
            insPtr = instruction.next(insPtr)
        } while (insPtr in instructions.indices)
        return acc
    }
}