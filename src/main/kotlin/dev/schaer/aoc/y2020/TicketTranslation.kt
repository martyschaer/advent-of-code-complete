package dev.schaer.aoc.y2020

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer

fun main() {
    TicketTranslation().singleRun()
}

@Year(2020)
@Day(16)
class TicketTranslation : Solution {

    private val rulePattern = Regex("^([\\w\\s]+): (\\d+)-(\\d+) or (\\d+)-(\\d+)\$")
    private val dummyRule = Rule("dummy", 0..0, 0..0)

    @Answer("28882")
    override fun partOne(input: String): Int {
        val chunks = input.split("\n\n")

        val rules = parseRules(chunks[0])
        val nearby = parseTicketValues(chunks[2].split("\n").drop(1))

        return nearby.filterNot { rules.any { rule -> rule.matches(it) } }.sum()
    }

    private fun parseTicketValues(input: List<String>): List<Int> {
        return input
            .flatMap { it.split(",") }
            .filter { it.isNotBlank() }
            .map { it.toInt() }
    }

    private fun parseRules(input: String): List<Rule> {
        return input.split("\n")
            .filter { it.isNotBlank() }
            .map { extractRule(it) }
    }

    private fun extractRule(input: String): Rule {
        val groups = rulePattern.matchEntire(input)?.groupValues ?: error("Couldn't match rule for '$input'")
        return Rule(
            name = groups[1],
            a = groups[2].toInt()..groups[3].toInt(),
            b = groups[4].toInt()..groups[5].toInt()
        )
    }

    @Answer("1429779530273")
    override fun partTwo(input: String): Long {
        val chunks = input.split("\n\n")

        val rules = parseRules(chunks[0])
        val ticket = parseTicketValues(chunks[1].split("\n").drop(1))
        val nearby = parseTickets(chunks[2].split("\n").drop(1), rules)
        val numFields = ticket.size

        val ruleCandidates = List<MutableList<Rule>>(numFields) { mutableListOf() }

        for (field in 0 until numFields) {
            val candidates = rules.filter { rule -> nearby.map { it[field] }.all { rule.matches(it) } }
            ruleCandidates[field].addAll(candidates)
        }

        val rulesInOrder = findRuleOrder(numFields, ruleCandidates)

        return calculateProductOfDepartureFields(rulesInOrder, ticket)
    }

    private fun calculateProductOfDepartureFields(rulesInOrder: List<Rule>, ticket: List<Int>): Long {
        return rulesInOrder.asSequence()
            .mapIndexed { idx, rule -> rule to idx }
            .filter { it.first.name.startsWith("departure") } // find all fields that start with "departure"
            .map { ticket[it.second].toLong() } // find the values on my ticket corresponding to those fields
            .fold(1L) { acc, it -> acc * it } // find the product of those values
    }

    private fun findRuleOrder(numFields: Int, ruleCandidates: List<MutableList<Rule>>): List<Rule> {
        val ruleOrder = MutableList(numFields) { dummyRule }

        // Find the positions where only 1 rule is applicable
        while (ruleCandidates.any { it.size == 1 }) {
            val (singleRuleList, position) = ruleCandidates
                .mapIndexed { idx, it -> it to idx }
                .first { it.first.size == 1 }
            val singleRule = singleRuleList.first()

            // set the rule for that position
            ruleOrder[position] = singleRule

            // remove the rule from the candidates (every rule only represents a single field)
            ruleCandidates.forEach { list -> list.remove(singleRule) }
        }

        return ruleOrder
    }

    private fun parseTickets(input: List<String>, rules: List<Rule>): List<List<Int>> {
        return input
            .filter { it.isNotBlank() }
            .map { line -> line.split(",").map { it.toInt() } }
            .filter { ticket -> ticket.all { rules.any { rule -> rule.matches(it) } } }
    }
}

private class Rule(val name: String, private val a: IntRange, private val b: IntRange) {
    fun matches(candidate: Int): Boolean {
        return candidate in a || candidate in b
    }
}