package dev.schaer.aoc.common

interface Solution {

    fun singleRun() {
        val yearAnnotation =
            this.javaClass.annotations.find { it is Year } as? Year ?: error("No @Year annotation given")
        val dayAnnotation =
            this.javaClass.annotations.find { it is Day } as? Day ?: error("No @Day annotation given")
        val year = yearAnnotation.year
        val day = dayAnnotation.day
        val input = InputProvider.instance().getInput(year, day)
        var start = System.nanoTime()
        println("Part One: ${partOne(input)} ~${(System.nanoTime() - start)/1000000}ms")
        reset()
        start = System.nanoTime()
        println("Part Two: ${partTwo(input)} ~${(System.nanoTime() - start)/1000000}ms")
    }

    fun reset() {
        // NOP by default
    }

    /**
     * Solves part one of the puzzle.
     * @return the solution to be pasted.
     */
    fun partOne(input: String): Any

    /**
     * Solves part two of the puzzle.
     * @return the solution to be pasted.
     */
    fun partTwo(input: String): Any
}

