package dev.schaer.aoc.common

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import okhttp3.OkHttpClient
import okhttp3.Request
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import kotlin.io.path.createDirectories
import kotlin.text.Charsets.UTF_8

private const val AOC_CACHE_DIR_ENV_VAR = "AOC_CACHE_DIR"
private const val COOKIE_ENV_VAR = "AOC_SESSION_COOKIE"
private const val COOKIE_HEADER = "Cookie"
private const val SESSION_KEY = "session"

class InputProvider private constructor() {
    companion object {
        @Volatile
        private var instance: InputProvider? = null

        fun instance() = instance ?: synchronized(this) {
            instance ?: InputProvider().also { instance = it }
        }
    }

    private val httpClient = OkHttpClient()
    private val sessionCookie = System.getenv(COOKIE_ENV_VAR) //
        ?: error("No AoC session cookie set ($COOKIE_ENV_VAR)")
    private val cacheDir = System.getenv(AOC_CACHE_DIR_ENV_VAR)?.let { Paths.get(it) } //
        ?: Paths.get(System.getProperty("user.home"), ".cache", "advent-of-code")

    init {
        println("InputProvider(cacheDir=${cacheDir.toAbsolutePath()})")
    }

    fun getInput(year: Int, day: Int): String {
        return readFile(year, day) //
            ?: downloadInput(year, day) //
                .also { content -> writeFile(year, day, content) }
    }

    private fun downloadInput(year: Int, day: Int): String {
        val url = "https://adventofcode.com/$year/day/$day/input"
        print("Downloading $url... ")
        val input = runBlocking { makeRequest(url) }
        println("Done")
        return input.trimEnd()
    }

    private suspend fun makeRequest(url: String): String {
        val request = Request.Builder() //
            .get() //
            .url(url) //
            .addHeader(COOKIE_HEADER, "$SESSION_KEY=$sessionCookie") //
            .build()

        return withContext(Dispatchers.IO) {
            httpClient.newCall(request).execute().use { response ->
                if (!response.isSuccessful) {
                    error("[${response.code}] Unable to download $url: ${response.message}")
                }
                response.body?.string() ?: error("Received successful but empty response for $url")
            }
        }
    }

    private fun writeFile(year: Int, day: Int, content: String) {
        ensureYearDir(year)

        val path = getPath(year, day)

        Files.writeString(path, content, UTF_8)
    }

    private fun readFile(year: Int, day: Int): String? {
        ensureYearDir(year)

        val path = getPath(year, day)

        if (Files.notExists(path)) {
            return null
        }

        val content = Files.readString(path, UTF_8) ?: return null

        if (content.isNotBlank()) {
            return content
        }

        return null
    }

    private fun getPath(year: Int, day: Int): Path {
        return cacheDir.resolve("$year").resolve("$day.txt")
    }

    private fun ensureYearDir(year: Int) {
        ensureCacheDir()

        val yearPath = cacheDir.resolve(year.toString())

        if (Files.notExists(yearPath)) {
            yearPath.createDirectories()
        }
    }

    private fun ensureCacheDir() {
        if (Files.notExists(cacheDir)) {
            cacheDir.createDirectories()
        }
    }
}