package dev.schaer.aoc.common.annotations

@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
annotation class LongRunning(val repetition: Int = 1) {
}
