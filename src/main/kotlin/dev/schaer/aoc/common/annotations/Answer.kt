package dev.schaer.aoc.common.annotations

@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
annotation class Answer(val solution: String = "")

