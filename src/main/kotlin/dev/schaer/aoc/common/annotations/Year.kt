package dev.schaer.aoc.common

@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
annotation class Year(val year: Int) {
}
