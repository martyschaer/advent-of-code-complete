package dev.schaer.aoc.cellular

class Automata<POS, T : Cell<POS>>(
    private val cells: MutableMap<POS, T>,
    private val init: (pos: POS, cells: MutableMap<POS, T>) -> T
) {
    fun step() {
        cells.values
            .flatMap { it.potentialNeighbours() }
            .filterNot { it in cells }
            .forEach { cells[it] = init.invoke(it, cells) }
        cells.values.forEach { it.prepare() }
        cells.values.forEach { it.evolve() }
    }

    fun count(): Int {
        return cells.count { (_, cell) -> cell.active }
    }

    fun count(predicate: (cell: Cell<POS>) -> Boolean): Int {
        return cells.count { (_, cell) -> predicate.invoke(cell) }
    }
}