package dev.schaer.aoc.cellular

abstract class Cell<POS>(
    val pos: POS,
    private val cells: MutableMap<POS, out Cell<POS>>,
    var active: Boolean = false
) {
    private val neighbours = calculateNeighbours()
    private var nextActive: Boolean = false

    abstract fun calculateNeighbours(): List<POS>
    abstract fun nextActive(currentActive: Boolean, activeNeighbours: Int): Boolean

    fun potentialNeighbours(): Set<POS> {
        return if (active) neighbours.toSet() else emptySet()
    }

    fun prepare() {
        val n = neighbours.map { cells[it] }
        val activeNeighbours = n.mapNotNull { it }.count { it.active }

        nextActive = nextActive(active, activeNeighbours)

    }

    fun evolve() {
        active = nextActive
    }
}
