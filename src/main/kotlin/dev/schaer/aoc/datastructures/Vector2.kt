package dev.schaer.aoc.datastructures

import kotlin.math.abs
import kotlin.math.cos
import kotlin.math.roundToInt
import kotlin.math.sin

data class Vector2(var x: Int, var y: Int) {
    operator fun plusAssign(other: Vector2) {
        x += other.x
        y += other.y
    }

    fun xComponent(): Vector2 {
        return Vector2(x, 0)
    }

    fun yComponent(): Vector2 {
        return Vector2(0, y)
    }

    operator fun plus(other: Vector2): Vector2 {
        return add(other)
    }

    fun add(other: Vector2): Vector2 {
        return Vector2(x + other.x, y + other.y)
    }

    operator fun minus(other: Vector2): Vector2 {
        return sub(other)
    }

    fun sub(other: Vector2): Vector2 {
        return Vector2(x - other.x, y - other.y)
    }

    fun manhattanLength(): Int {
        return abs(x) + abs(y)
    }

    operator fun times(n: Int): Vector2 {
        return Vector2(x * n, y * n)
    }

    fun rotateMut(deg: Int) {
        val rad = Math.toRadians(deg.toDouble())
        val sin = sin(rad).roundToInt()
        val cos = cos(rad).roundToInt()
        val oldX = x
        val oldY = y
        x = (oldX * cos - oldY * sin)
        y = (oldX * sin + oldY * cos)
    }

    fun copy(): Vector2 {
        return Vector2(x, y)
    }

    companion object {
        val ZERO = Vector2(0, 0)
    }
}