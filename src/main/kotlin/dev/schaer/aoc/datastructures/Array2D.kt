package dev.schaer.aoc.datastructures

import java.awt.Point


fun Array2D<Char>.getRowsAsString(): List<String> {
    return this.getRows("") { acc, value -> acc + value }
}

fun Array2D<Char>.getColsAsString(): List<String> {
    return this.getCols("") { acc, value -> acc + value }
}

fun parseIntoArray2dOfChars(input: String, default: Char): Array2D<Char> {
    val (width, height) = calculateDimensions(input)
    val values = input.replace("\n", "").toCharArray().toTypedArray()

    return Array2D(values, width, height, default)
}

fun parseIntoArray2dOfDigits(input: String, default: Int = 0): Array2D<Int> {
    val (width, height) = calculateDimensions(input)
    val values = input.replace("\n", "").map { it.digitToInt() }.toTypedArray()

    return Array2D(values, width, height, default)
}

/**
 * @return Pair(width, height)
 */
private fun calculateDimensions(input: String): Pair<Int, Int> {
    val lines = input.lines()
    return calculateWidth(lines) to calculateHeight(lines)
}

private fun calculateWidth(lines: List<String>): Int {
    return lines.first().length
}

private fun calculateHeight(lines: List<String>): Int {
    return lines.size
}

class Array2D<T>(
    val values: Array<T>,
    val width: Int,
    val height: Int,
    private val default: T,
    private val toStr: (v: T) -> String = { it.toString() }
) {
    val size: Int = width * height

    fun clone(): Array2D<T> {
        return Array2D(values.clone(), width, height, default, toStr)
    }

    operator fun get(i: Int): T {
        return values[i]
    }

    operator fun get(v: Vector2): T {
        return get(v.x, v.y)
    }

    fun pos(value: T): Vector2? {
        val index = values.indexOf(value)
        if (index == -1) return null
        return unmapToVec(index)
    }

    operator fun get(x: Int, y: Int): T {
        if (isOutOfBounds(x, y)) {
            return default
        }
        return values[x + y * width]
    }

    operator fun set(i: Int, value: T) {
        values[i] = value
    }

    operator fun set(v: Vector2, value: T) {
        set(v.x, v.y, value)
    }

    operator fun set(x: Int, y: Int, value: T) {
        if (isInBounds(x, y)) {
            values[x + y * width] = value
        }
    }

    fun map(input: Point): Int {
        return map(input.x, input.y)
    }

    fun map(input: Vector2): Int {
        return map(input.x, input.y)
    }

    fun map(x: Int, y: Int): Int {
        return x + y * width
    }

    fun unmapToVec(input: Int): Vector2 {
        val p = unmap(input)
        return Vector2(p.x, p.y)
    }

    fun unmap(input: Int): Point {
        return Point(input % width, input / width)
    }

    /**
     * Returns all neighbours (orthogonal and diagonal)
     * from left to right, top to bottom around the given
     * [x, y] coordinates.
     * Does not return the value at [x, y].
     * Does not return anything for values that would be out of bounds
     * e.g. [0, 0] only has 3 neighbours.
     */
    fun getNeighbouringValues(x: Int, y: Int): List<T> {
        val result = mutableListOf<T>()
        for (yOff in -1..1) {
            for (xOff in -1..1) {
                if (yOff == 0 && xOff == 0) {
                    continue
                }
                val nx = x + xOff
                val ny = y + yOff
                if (isInBounds(nx, ny)) {
                    result.add(get(nx, ny))
                }
            }
        }
        return result
    }

    fun getOrthogonalNeighbours(input: Int): Set<Int> {
        return getOrthogonalNeighbours(unmap(input))
    }

    fun getOrthogonalNeighbours(input: Point): Set<Int> {
        val result = mutableSetOf<Int>()

        if (isInBounds(input.x - 1, input.y)) {
            result.add(map(input.x - 1, input.y))
        }

        if (isInBounds(input.x + 1, input.y)) {
            result.add(map(input.x + 1, input.y))
        }

        if (isInBounds(input.x, input.y - 1)) {
            result.add(map(input.x, input.y - 1))
        }

        if (isInBounds(input.x, input.y + 1)) {
            result.add(map(input.x, input.y + 1))
        }
        return result
    }

    fun getRow(y: Int): List<T> {
        return getRow(y, mutableListOf()) { acc, value -> acc += value; acc }
    }

    fun <R> getRows(inital: R, transform: (acc: R, value: T) -> R): List<R> {
        return (0 until height).map { y -> getRow(y, inital, transform) }
    }

    fun <R> getRow(y: Int, initial: R, transform: (acc: R, value: T) -> R): R {
        if (isOutOfBounds(0, y)) {
            error("Row $y is not valid")
        }

        var result = initial

        for (x in 0 until width) {
            result = transform.invoke(result, this[x, y])
        }

        return result
    }

    fun getCol(x: Int): List<T> {
        return getCol(x, mutableListOf()) { acc, value -> acc += value; acc }
    }

    fun <R> getCols(inital: R, transform: (acc: R, value: T) -> R): List<R> {
        return (0 until width).map { x -> getCol(x, inital, transform) }
    }

    fun <R> getCol(x: Int, initial: R, transform: (acc: R, value: T) -> R): R {
        if (isOutOfBounds(x, 0)) {
            error("Column $x is not valid")
        }

        var result = initial

        for (y in 0 until height) {
            result = transform.invoke(result, this[x, y])
        }

        return result
    }

    fun isInBounds(v: Vector2): Boolean {
        return isInBounds(v.x, v.y)
    }

    fun isInBounds(x: Int, y: Int): Boolean {
        return !isOutOfBounds(x, y)
    }

    fun isOutOfBounds(v: Vector2): Boolean {
        return isOutOfBounds(v.x, v.y)
    }

    fun isOutOfBounds(x: Int, y: Int): Boolean {
        return x < 0 || y < 0 || x >= width || y >= height
    }

    override fun toString(): String {
        return toString(" ")
    }

    fun toString(separator: String = " "): String {
        val sb = StringBuilder(width * height * 2)

        for (y in 0 until height) {
            for (x in 0 until width) {
                sb.append(toStr(get(x, y)))
                sb.append(separator)
            }
            sb.append('\n')
        }

        return sb.toString()
    }

    fun hash(): Int {
        return values.contentHashCode()
    }
}