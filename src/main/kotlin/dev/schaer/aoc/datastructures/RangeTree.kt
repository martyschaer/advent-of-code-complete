package dev.schaer.aoc.datastructures

class RangeTree<T>(val ranges: List<Pair<LongRange, T>>) {

    companion object {
        fun of(ranges: List<LongRange>): RangeTree<Unit> {
            return RangeTree(ranges.map { it to Unit }.sortedBy { it.first.first })
        }
    }

    // sorted by the range start
    private val root: Node<T>? = buildTree(ranges.sortedBy { it.first.first })

    private fun buildTree(ranges: List<Pair<LongRange, T>>): Node<T>? {
        if (ranges.isEmpty()) return null

        val midIndex = ranges.size / 2
        val midRange = ranges[midIndex]

        return Node(
            range = midRange.first,
            left = buildTree(ranges.subList(0, midIndex)),
            right = buildTree(ranges.subList(midIndex + 1, ranges.size)),
            value = midRange.second
        )
    }

    fun queryRange(number: Long): LongRange? {
        return query(number)?.range
    }

    fun queryValue(number: Long): T? {
        return query(number)?.value
    }

    /**
     * Returns true if the given number is within a range of the tree,
     * false if it's outside all ranges.
     */
    fun contains(number: Long): Boolean {
        return query(number) != null
    }

    private fun query(number: Long): Node<T>? {
        var current = root
        while (current != null) {
            if (number in current.range) {
                return current
            }

            current = if (number < current.range.first) current.left else current.right
        }
        return null
    }

    private data class Node<T>(val range: LongRange, val left: Node<T>?, val right: Node<T>?, val value: T)
}