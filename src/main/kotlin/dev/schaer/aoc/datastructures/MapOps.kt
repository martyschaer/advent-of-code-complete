package dev.schaer.aoc.datastructures

fun <K, V> Map<K, V>.invert(): Map<V, K> {
    return this.entries.associate { it.value to it.key }
}

fun <K, V, U, R> Map<K, V>.combineWith(other: Map<K, U>, combinator: (a: V, b: U) -> R): Map<K, R> {
    if (this.keys != other.keys) {
        error("Can't combine maps that don't share the same keys")
    }
    return keys.associateWith { key -> combinator(this[key]!!, other[key]!!) }
}