package dev.schaer.aoc.datastructures

/**
 * Returns the first entry of this list
 */
fun <T> List<T>.head(): T {
    return this.first()
}

/**
 * Returns a new list containing all entries
 * except the first
 */
fun <T> List<T>.tail(): List<T> {
    return this.subList(1, this.size)
}

fun <T> List<T>.repeat(n: Int): List<T> {
    val result = mutableListOf<T>()
    for (i in 0 until n) {
        result.addAll(this)
    }
    return result
}

fun <T> List<T>.without(vararg idxs: Int): List<T> {
    val mut = toMutableList()
    for (idx in idxs.sorted().asReversed()) {
        mut.removeAt(idx)
    }
    return mut.toList()
}

fun List<Long>.product(): Long {
    return fold(1L) { acc, it -> acc * it }
}

fun <T, R> List<T>.foldWindowTwo(initial: R, f: (acc: R, prev: T, curr: T) -> R): R {
    return this.fold(this.first() to initial) {(prev, acc), curr -> curr to f(acc, prev, curr)}.second
}

/**
 * Applies the given transform to all pairs of values in this list.
 *
 * Each pair is only transformed once, _not_ in "both" directions
 *
 * e.g.
 * ```
 * list = [a, b, c]
 * `result = [ transform(a, b), transform(a, c), transform(b, c) ]`
 * ```
 */
fun <T, R> List<T>.transformPairwise(transform: (T, T) -> R): List<R> {
    val result = mutableListOf<R>()

    for (i in indices)  {
        for (j in i until size) {
            val a = get(i)
            val b = get(j)
            result.add(transform.invoke(a, b))
        }
    }

    return result
}
