package dev.schaer.aoc.datastructures.graph

sealed class AbstractNode<T> {
    abstract val data: T
    abstract val reachable: MutableSet<out AbstractNode<T>>

    override fun toString(): String {
        return "$data -> ${reachable.map { it.data }.toList()}"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as AbstractNode<*>

        if (data != other.data) return false
        if (reachable.map { it.data } != other.reachable.map { it.data }) return false

        return true
    }

    override fun hashCode(): Int {
        var result = data?.hashCode() ?: 0
        result = 31 * result + reachable.map{it.data}.hashCode()
        return result
    }
}

data class Node<T>(override val data: T, override val reachable: MutableSet<Node<T>>): AbstractNode<T>() {
    override fun toString(): String {
        return "$data -> ${reachable.map { it.data }.toList()}"
    }

    override fun equals(other: Any?): Boolean {
        return super.equals(other)
    }

    override fun hashCode(): Int {
        return super.hashCode()
    }
}
