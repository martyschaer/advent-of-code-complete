package dev.schaer.aoc.datastructures

fun <T> Sequence<T>.repeat() = sequence { while (true) yieldAll(this@repeat) }