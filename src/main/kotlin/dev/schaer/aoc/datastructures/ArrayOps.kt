package dev.schaer.aoc.datastructures

enum class Rotation(val clockwiseRotations: Int) {
    ZERO(0),
    NINETY(1),
    ONEEIGHTY(2),
    TWOSEVENTY(3)
}

fun CharArray.get(
    x: Int,
    yIn: Int,
    height: Int,
    width: Int,
    flipVertical: Boolean = false,
    rotation: Rotation = Rotation.ZERO
): Char {
    val y = if (flipVertical) (height - 1) - yIn else yIn
    val xTransform = when (rotation) {
        Rotation.ZERO -> x
        Rotation.NINETY -> y
        Rotation.ONEEIGHTY -> (width - 1) - x
        Rotation.TWOSEVENTY -> (height - 1) - y
    }

    val yTransform = when (rotation) {
        Rotation.ZERO -> y
        Rotation.NINETY -> (width - 1) - x
        Rotation.ONEEIGHTY -> (height - 1) - y
        Rotation.TWOSEVENTY -> x
    }

    return this[xTransform + yTransform * width]
}