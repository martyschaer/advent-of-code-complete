package dev.schaer.aoc.datastructures.circular

class CircularLinkedList<T> {
    private var tail: Node<T>? = null
    private val nodes = hashMapOf<T, Node<T>>()

    constructor()

    constructor(elements: List<T>) {
        addAll(elements)
    }

    fun first(): T {
        return tail?.next?.data ?: throw NoSuchElementException("Cannot get first element of empty list")
    }

    fun size(): Int {
        return nodes.size
    }

    fun addLast(element: T) {
        val node = createNode(element)
        node.next = tail?.next ?: node
        tail?.let { it.next = node }
        tail = node
    }

    fun insertAfter(insertAfter: T, a: T, b: T, c: T) {
        val prevNode = getNode(insertAfter)

        val cNode = createNode(c, prevNode.next!!)
        val bNode = createNode(b, cNode)
        val aNode = createNode(a, bNode)
        prevNode.next = aNode

        if (prevNode === tail) {
            tail = cNode
        }
    }

    fun insertAfter(toAdd: T, insertAfter: T) {
        val prevNode = getNode(insertAfter)
        val node = createNode(toAdd)
        node.next = prevNode.next
        prevNode.next = node
        if (prevNode === tail) {
            tail = node
        }
    }

    private fun createNode(data: T, next: Node<T>? = null): Node<T> {
        val node = Node(data, next)
        nodes[data] = node
        return node
    }

    private fun getNode(data: T): Node<T> {
        return nodes[data] ?: throw NoSuchElementException("Cannot find element $data in list")
    }

    fun removeAfter(removeAfter: T): T {
        val prevNode = getNode(removeAfter)
        val toRemove = prevNode.next!!
        if (toRemove === tail) {
            tail = prevNode
        }
        prevNode.next = toRemove.next
        return nodes.remove(toRemove.data)!!.data
    }

    fun addAll(elements: List<T>) {
        elements.forEach { addLast(it) }
    }

    fun getAfter(element: T): T {
        return nodes[element]?.next?.data ?: throw NoSuchElementException("$element is not in list")
    }

    /**
     * Sequence through the circle, excluding the starting value
     */
    fun iterateFromExclusive(element: T): Sequence<T> {
        val startingNode = getNode(element)
        return generateSequence(startingNode.next!!.data) { prev ->
            val prevNode = getNode(prev)
            val next = prevNode.next!!
            if (next === startingNode) null else next.data
        }
    }

    override fun toString(): String {
        if (tail == null) {
            return "[]"
        }
        return toString("[${tail!!.next!!.data}", tail!!.next!!.next!!)
    }

    private tailrec fun toString(string: String, next: Node<T>): String {
        if (next === tail) {
            return "$string,${next.data}]"
        }
        return toString("$string,${next.data}", next.next!!)
    }
}