package dev.schaer.aoc.datastructures.circular

data class Node<T>(val data: T, var next: Node<T>? = null) {
    override fun toString(): String {
        if (next == null) {
            return "Node($data) => null"
        }
        return "Node($data) => ..."
    }
}
