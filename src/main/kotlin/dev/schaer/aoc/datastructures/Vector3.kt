package dev.schaer.aoc.datastructures

import dev.schaer.aoc.algorithms.pow2
import kotlin.math.sqrt

data class Vector3(val x: Int, val y: Int, val z: Int) : Vector<Vector3> {
    override fun distance(other: Vector3): Double {
        return sqrt(
            pow2(x - other.x.toDouble()) + pow2(y - other.y.toDouble()) + pow2(z - other.z.toDouble())
        )
    }

    override fun neighbours(): List<Vector3> {
        return (-1..1).flatMap { xOff ->
            (-1..1).flatMap { yOff ->
                (-1..1).map { zOff ->
                    Vector3(x + xOff, y + yOff, z + zOff)
                }
            }
        }.filter { distance(it) > 0.0 }
    }
}
