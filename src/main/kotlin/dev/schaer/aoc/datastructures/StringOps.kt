package dev.schaer.aoc.datastructures

fun String.split(index: Int): Pair<String, String> {
    if (index in indices) {
        return substring(0, index) to substring(index, length)
    }
    error("Given index=$index must be within 0 until $length")
}

fun String.ints(): List<Int> {
    return this.lines().map { it.toInt() }
}

fun String.charSet(): Set<Char> {
    return this.toCharArray().toSet()
}

fun String.head(): Char {
    return this.first()
}

fun String.tail(): String {
    return this.drop(1)
}

class StringLengthProvider(private var value: String) {
    fun hasNext() : Boolean {
        return value.isNotBlank()
    }

    fun next(size: Int): String {
        val result = value.substring(0, size)
        value = value.substring(size)
        return result
    }
}
