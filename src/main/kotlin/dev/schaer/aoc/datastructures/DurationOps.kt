package dev.schaer.aoc.datastructures

import java.time.Duration

fun Duration.toMicros(): Long {
    return this.toNanos() / 1000
}