package dev.schaer.aoc.datastructures

/**
 * Applies the given transform to both properties of the pair.
 */
fun <T, R> Pair<T, T>.map(transform: (T) -> R): Pair<R, R> {
    return transform(first) to transform(second)
}

fun <T> Pair<T, T>.any(predicate: (T) -> Boolean): Boolean {
    return predicate.invoke(first) || predicate.invoke(second)
}

fun <T> Pair<T, T>.all(predicate: (T) -> Boolean): Boolean {
    return predicate.invoke(first) && predicate.invoke(second)
}
