package dev.schaer.aoc.datastructures

interface Vector<T> {
    fun neighbours(): List<T>
    fun distance(other: T): Double
}