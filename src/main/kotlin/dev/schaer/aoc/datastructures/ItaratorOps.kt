package dev.schaer.aoc.datastructures

/**
 * Skips the next value and returns the next-next value instead.
 */
fun <T> Iterator<T>.nextNext(): T {
    next()
    return next()
}