package dev.schaer.aoc.datastructures

import dev.schaer.aoc.algorithms.pow2
import kotlin.math.sqrt

data class Vector4(val x: Int, val y: Int, val z: Int, val w: Int) : Vector<Vector4> {
    override fun distance(other: Vector4): Double {
        return sqrt(
            pow2(x - other.x.toDouble())
                    + pow2(y - other.y.toDouble())
                    + pow2(z - other.z.toDouble())
                    + pow2(w - other.w.toDouble())
        )
    }

    override fun neighbours(): List<Vector4> {
        return (-1..1).flatMap { xOff ->
            (-1..1).flatMap { yOff ->
                (-1..1).flatMap { zOff ->
                    (-1..1).map { wOff ->
                        Vector4(x + xOff, y + yOff, z + zOff, w + wOff)
                    }
                }
            }
        }.filter { distance(it) > 0.0 }
    }
}
