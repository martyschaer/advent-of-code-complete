package dev.schaer.aoc.y2016

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer
import dev.schaer.aoc.model.Direction
import dev.schaer.aoc.model.Direction.Cardinal.NORTH
import java.awt.Point
import kotlin.math.abs

fun main() {
    Day01_Taxicab().singleRun()
}

@Year(2016)
@Day(1)
class Day01_Taxicab : Solution {
    @Answer("230")
    override fun partOne(input: String): Any {
        val instructions = parseInstructions(input)
        var direction: Direction = NORTH
        var position = Point(0, 0)
        for ((relative, distance) in instructions) {
            direction = when (relative) {
                "L" -> direction.left
                "R" -> direction.right
                else -> error("Malformed relative direction '$relative'")
            }
            position = Point(position.x + direction.x * distance, position.y + direction.y * distance)
        }
        return abs(position.x) + abs(position.y)
    }

    override fun partTwo(input: String): Any {
        val instructions = parseInstructions(input)
        var direction: Direction = NORTH
        var position = Point(0, 0)
        val visited = mutableSetOf(position)
        for ((relative, distance) in instructions) {
            direction = when (relative) {
                "L" -> direction.left
                "R" -> direction.right
                else -> error("Malformed relative direction '$relative'")
            }

            for (step in 0 until distance) {
                position = Point(position.x + direction.x, position.y + direction.y)
                if(!visited.add(position)) {
                    return abs(position.x) + abs(position.y)
                }
            }

        }
        error("Unable to determine first twice-visited position")
    }

    private fun parseInstructions(input: String): Sequence<Pair<String, Int>> {
        return input.split(",").asSequence()
            .map { it.trim() }
            .map { it.take(1) to it.drop(1).toInt() }
    }
}