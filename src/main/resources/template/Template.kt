package dev.schaer.metaprogramming.aoc

import dev.schaer.aoc.common.Day
import dev.schaer.aoc.common.Solution
import dev.schaer.aoc.common.Year
import dev.schaer.aoc.common.annotations.Answer

fun main() {
    Template().singleRun()
}

@Year(0)
@Day(0)
class Template : Solution {

    @Answer("")
    override fun partOne(input: String): Int {
        TODO("Implement part one")
    }

    @Answer("")
    override fun partTwo(input: String): Int {
        TODO("Implement part two")
    }
}