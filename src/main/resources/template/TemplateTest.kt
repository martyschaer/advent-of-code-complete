package dev.schaer.metaprogramming.aoc

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class TemplateTest {
    @Test
    fun examplePartOne() {
        val input = """
        """.trimIndent()

        val solution = Template().partOne(input)

        assertEquals(TODO(), solution)
    }

    @Test
    fun examplePartTwo() {
        val input = """
        """.trimIndent()

        val solution = Template().partTwo(input)

        assertEquals(TODO(), solution)
    }
}